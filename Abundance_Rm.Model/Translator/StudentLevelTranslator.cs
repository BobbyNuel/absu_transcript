﻿using System;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;


namespace Abundance_Rm.Model.Translator
{
    public class StudentLevelTranslator : BaseTranslator<StudentLevel, STUDENT_LEVEL>
    {
        private readonly DepartmentTranslator departmentTranslator;
        private readonly LevelTranslator levelTranslator;
        private readonly ProgrammeTypeTranslator programmeTypeTranslator;
        private readonly SessionTranslator sessionTranslator;
        private readonly StudentTranslator studentTranslator;
        //private SessionSemesterTranslator sessionSemesterTranslator;

        public StudentLevelTranslator()
        {
            levelTranslator = new LevelTranslator();
            studentTranslator = new StudentTranslator();
            sessionTranslator = new SessionTranslator();
            programmeTypeTranslator = new ProgrammeTypeTranslator();
            departmentTranslator = new DepartmentTranslator();
        }

        public override StudentLevel TranslateToModel(STUDENT_LEVEL entity)
        {
            try
            {
                StudentLevel model = null;
                if (entity != null)
                {
                    model = new StudentLevel();
                    model.Id = entity.Student_Level_Id;
                    model.Student = studentTranslator.Translate(entity.STUDENT);
                    model.Level = levelTranslator.Translate(entity.LEVEL);
                    model.Session = sessionTranslator.Translate(entity.SESSION);
                    model.ProgrammeType = programmeTypeTranslator.Translate(entity.PROGRAMME_TYPE);
                    model.Department = departmentTranslator.Translate(entity.DEPARTMENT);
                    if (model.Session == null)
                    {
                        model.Session = new Session() {Id = entity.Session_Id};
                    }
                }

                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override STUDENT_LEVEL TranslateToEntity(StudentLevel model)
        {
            try
            {
                STUDENT_LEVEL entity = null;
                if (model != null)
                {
                    entity = new STUDENT_LEVEL();
                    entity.Student_Level_Id = model.Id;
                    entity.Person_Id = model.Student.Id;
                    entity.Level_Id = model.Level.Id;
                    entity.Session_Id = model.Session.Id;
                    entity.Programme_Type_Id = model.ProgrammeType.Id;
                    entity.Department_Id = model.Department.Id;

                }

                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}