﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;

namespace Abundance_Rm.Model.Translator
{
    public class ReligionTranslator:BaseTranslator<Religion,RELIGION>
    {
        public override Religion TranslateToModel(RELIGION entity)
        {
            try
            {
                Religion religion = null;
                if (entity != null)
                {
                    religion = new Religion();
                    religion.Id = entity.ReligionId;
                    religion.Name = entity.Name;
                    religion.Activated = entity.Activated;
                }
                return religion;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override RELIGION TranslateToEntity(Religion model)
        {
            try
            {
                RELIGION religion = null;
                if (model != null)
                {
                    religion = new RELIGION();
                    religion.ReligionId = model.Id;
                    religion.Name = model.Name;
                    religion.Activated = model.Activated;

                }
                return religion;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
