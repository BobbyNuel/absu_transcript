﻿using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Translator
{
    public class CountryTranslator : BaseTranslator<Country, COUNTRY>
    {
        public override Country TranslateToModel(COUNTRY entity)
        {
            try
            {
                Country country = null;
                if (entity != null)
                {
                    country = new Country();
                    country.Id = entity.CountryId;
                    country.Name = entity.Name;
                }
                return country;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override COUNTRY TranslateToEntity(Country model)
        {
            try
            {
                COUNTRY country = null;
                if (model != null)
                {
                    country = new COUNTRY();
                    country.CountryId = model.Id;
                    country.Name = model.Name;

                }
                return country;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
