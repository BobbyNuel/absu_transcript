﻿using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Translator
{
    public class ProgrammeTypeTranslator : BaseTranslator<ProgrammeType, PROGRAMME_TYPE>
    {
        public override ProgrammeType TranslateToModel(PROGRAMME_TYPE entity)
        {
            try
            {
                ProgrammeType programmeType = null;
                if (entity != null)
                {
                    programmeType = new ProgrammeType();
                    programmeType.Id = entity.ProgrammeTypeId;
                    programmeType.Name = entity.ProgrammeType;
                    programmeType.Activated = entity.Activated;
                }
                return programmeType;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override PROGRAMME_TYPE TranslateToEntity(ProgrammeType model)
        {
            try
            {
                PROGRAMME_TYPE programmeType = null;
                if (model != null)
                {
                    programmeType = new PROGRAMME_TYPE();
                    programmeType.ProgrammeTypeId = model.Id;
                    programmeType.ProgrammeType = model.Name;
                    programmeType.Activated = model.Activated;
                }
                return programmeType;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
