﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Entity;

namespace Abundance_Rm.Model.Translator
{
    public class SemesterTranslator:BaseTranslator<Semester,SEMESTER>
    {
        public override Semester TranslateToModel(SEMESTER entity)
        {
            try
            {
                Semester semester = null;
                if (entity != null)
                {
                    semester = new Semester();
                    semester.Id = entity.SemesterId;
                    semester.Name = entity.SemesterName;
                    semester.SemesterCode = entity.SemesterCode;
                    semester.IsCurrent = entity.IsCurrent;
                    semester.Activated = entity.Activated;

                }
                return semester;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override SEMESTER TranslateToEntity(Semester model)
        {
            try
            {
               SEMESTER semester = null;
                if (model != null)
                {
                    semester = new SEMESTER();
                    semester.SemesterId = model.Id;
                    semester.SemesterName = model.Name;
                    semester.SemesterCode = model.SemesterCode;
                    semester.IsCurrent = model.IsCurrent;
                    semester.Activated = model.Activated;
                }
                return semester;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
