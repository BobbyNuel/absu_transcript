﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;

namespace Abundance_Rm.Model.Translator
{
    public class SessionSemesterTranslator : BaseTranslator<SessionSemester, SESSION_SEMESTER>
    {
        private SessionTranslator sessionTranslator;
        private SemesterTranslator semesterTranslator;

        public SessionSemesterTranslator()
        {
            sessionTranslator = new SessionTranslator();
            semesterTranslator = new SemesterTranslator();
        }

        public override SessionSemester TranslateToModel(SESSION_SEMESTER entity)
        {
            try
            {
                SessionSemester model = null;
                if (entity != null)
                {
                    model = new SessionSemester();
                   model.Id = entity.Session_Semester_Id;
                    model.Semester = semesterTranslator.Translate(entity.SEMESTER);
                    model.Session = sessionTranslator.Translate(entity.SESSION);
                    model.SequenceNumber = entity.Sequence_Number;
                    model.StartDate = entity.Start_Date;
                    model.EndDate = entity.End_Date;
                }

                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SESSION_SEMESTER TranslateToEntity(SessionSemester model)
        {
            try
            {
                SESSION_SEMESTER entity = null;
                if (model != null)
                {
                    entity = new SESSION_SEMESTER();
                    
                    entity.Session_Id = model.Session.Id;
                    entity.Semester_Id = model.Semester.Id;
                    entity.Sequence_Number = model.SequenceNumber;
                    entity.Start_Date = model.StartDate;
                    entity.End_Date = model.EndDate;
                }

                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
