﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;

namespace Abundance_Rm.Model.Translator
{
    public class StudentCourseMarkTranslator:BaseTranslator<StudentCourseMark,STUDENT_COURSE_MARK>
    {
        private ProgrammeCourseRegistrationTranslator programmeCourseRegistrationTranslator;

        public StudentCourseMarkTranslator()
        {
            programmeCourseRegistrationTranslator = new ProgrammeCourseRegistrationTranslator();
        }
        public override StudentCourseMark TranslateToModel(STUDENT_COURSE_MARK entity)
        {
            try
            {
                StudentCourseMark studentCourseMark = null;
                if (entity != null)
                {
                    studentCourseMark = new StudentCourseMark();
                    studentCourseMark.Id = entity.CourseRegId;
                    studentCourseMark.DateRecorded = entity.DateRecorded;
                    studentCourseMark.ExamScore = entity.ExamScore;
                    studentCourseMark.Grade = entity.Grade;
                    studentCourseMark.GradePoint = entity.GradePoint;
                    studentCourseMark.CourseUnit = entity.CourseUnit;
                    studentCourseMark.IsMarkOmitted = entity.IsMarkOmitted;
                    studentCourseMark.TestScore = entity.TestScore;
                    studentCourseMark.Total = entity.Total;
                    studentCourseMark.ProgrammeCourseRegistration = programmeCourseRegistrationTranslator.Translate(entity.PROGRAMME_COURSE_REGISTRATION);
                }
                return studentCourseMark;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override STUDENT_COURSE_MARK TranslateToEntity(StudentCourseMark model)
        {
            try
            {
                STUDENT_COURSE_MARK studentCourseMark = null;
                if (model != null)
                {
                    studentCourseMark = new STUDENT_COURSE_MARK();
                    studentCourseMark.CourseRegId = model.Id;
                    studentCourseMark.DateRecorded = model.DateRecorded;
                    studentCourseMark.ExamScore = model.ExamScore;
                    if (model.Grade.Length > 3)
                    {
                        studentCourseMark.Grade = "";
                    }
                    else
                    {
                        studentCourseMark.Grade = model.Grade;
                    }
                    
                    studentCourseMark.CourseUnit = model.CourseUnit;
                    studentCourseMark.GradePoint = model.GradePoint;
                    studentCourseMark.IsMarkOmitted = model.IsMarkOmitted;
                    studentCourseMark.TestScore = model.TestScore;
                    studentCourseMark.Total = model.Total;
                    studentCourseMark.CourseRegId = model.ProgrammeCourseRegistration.Id;
                }
                return studentCourseMark;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
