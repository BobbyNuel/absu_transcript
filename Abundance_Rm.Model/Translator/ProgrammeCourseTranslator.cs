﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;

namespace Abundance_Rm.Model.Translator
{
    public class ProgrammeCourseTranslator:BaseTranslator<ProgrammeCourse,PROGRAMME_COURSES>
    {
         private CourseTranslator courseTranslator;
        private ProgrammeTranslator programmeTranslator;
        private SemesterTranslator semesterTranslator;
        private CourseTypeTranslator courseTypeTranslator;
        private LevelTranslator levelTranslator;
        private SessionTranslator sessionTranslator;

        public ProgrammeCourseTranslator()
        {
            courseTranslator = new CourseTranslator();
            semesterTranslator = new SemesterTranslator();
            programmeTranslator = new ProgrammeTranslator();
            courseTypeTranslator = new CourseTypeTranslator();
            levelTranslator = new LevelTranslator();
            sessionTranslator = new SessionTranslator();
        }
        public override ProgrammeCourse TranslateToModel(PROGRAMME_COURSES entity)
        {
            try
            {
                ProgrammeCourse programmeCourse = null;
                if (entity != null)
                {
                    programmeCourse = new ProgrammeCourse();
                    programmeCourse.Id = entity.ProgrammeCourseId;
                    programmeCourse.Programme = programmeTranslator.Translate(entity.PROGRAMME);
                    programmeCourse.Activated = entity.Activated;
                    programmeCourse.Pass_mark = entity.pass_mark;
                    programmeCourse.IsRemoveRequested = entity.IsRemoveRequested;
                    programmeCourse.CourseUnit = entity.CourseUnit;
                    programmeCourse.Level = levelTranslator.Translate(entity.LEVEL);
                    programmeCourse.Semester = semesterTranslator.Translate(entity.SEMESTER);
                    programmeCourse.Course = courseTranslator.Translate(entity.COURSE);
                    programmeCourse.CourseType = courseTypeTranslator.Translate(entity.COURSE_TYPE);
                    programmeCourse.Session = sessionTranslator.Translate(entity.SESSION);


                }
                return programmeCourse;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override PROGRAMME_COURSES TranslateToEntity(ProgrammeCourse model)
        {
            try
            {
                PROGRAMME_COURSES programmeCourse = null;
                if (model != null)
                {
                    programmeCourse = new PROGRAMME_COURSES();
                    programmeCourse.ProgrammeCourseId = model.Id;
                    programmeCourse.ProgrammeId = model.Programme.Id;
                    programmeCourse.Activated = model.Activated;
                    programmeCourse.pass_mark = model.Pass_mark;
                    programmeCourse.IsRemoveRequested = model.IsRemoveRequested;
                    programmeCourse.CourseUnit = model.CourseUnit;
                    programmeCourse.LevelId = model.Level.Id;
                    programmeCourse.SemesterId = model.Semester.Id;
                    programmeCourse.CourseId = model.Course.Id;
                    programmeCourse.CourseTypeId = model.CourseType.Id;
                    programmeCourse.Session_Id = model.Session.Id;
                }
                return programmeCourse;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
