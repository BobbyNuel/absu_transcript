﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Entity;

namespace Abundance_Rm.Model.Translator
{
    public class LevelTranslator:BaseTranslator<Level,LEVEL>
    {
        public override Level TranslateToModel(LEVEL entity)
        {
            try
            {
                Level level = null;
                if (entity != null)
                {
                    level = new Level();
                    level.Id = entity.LevelId;
                    level.Name = entity.Name;

                }
                return level;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override LEVEL TranslateToEntity(Level model)
        {
            try
            {
                LEVEL level = null;
                if (model != null)
                {
                    level = new LEVEL();
                    level.LevelId = model.Id;
                    level.Name = model.Name;

                }
                return level;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
