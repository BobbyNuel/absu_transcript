﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;

namespace Abundance_Rm.Model.Translator
{
    public class ProgrammeCourseRegistrationTranslator:BaseTranslator<ProgrammeCourseRegistration,PROGRAMME_COURSE_REGISTRATION>
    {
        private SemesterTranslator semesterTranslator;
        private LevelTranslator levelTranslator;
        private ProgrammeCourseTranslator programmeCourseTranslator;
        private SessionTranslator sessionTranslator;
        private StudentTranslator studentTranslator;
        private SessionSemesterTranslator sessionSemesterTranslator;
        public ProgrammeCourseRegistrationTranslator()
        {
            semesterTranslator = new SemesterTranslator();
            levelTranslator = new LevelTranslator();
            programmeCourseTranslator = new ProgrammeCourseTranslator();
            sessionTranslator = new SessionTranslator();
            studentTranslator = new StudentTranslator();
            sessionSemesterTranslator = new SessionSemesterTranslator();
        }

        public override ProgrammeCourseRegistration TranslateToModel(PROGRAMME_COURSE_REGISTRATION entity)
        {
            
            try
            {
                ProgrammeCourseRegistration programmeCourseRegistration = null;
                if (entity != null)
                {
                    programmeCourseRegistration = new ProgrammeCourseRegistration();
                    programmeCourseRegistration.Id = entity.CourseRegId;
                    programmeCourseRegistration.Approved = entity.Approved;
                    programmeCourseRegistration.CourseUnit = entity.CourseUnit;
                    programmeCourseRegistration.DateApproved = entity.DateApproved;
                    programmeCourseRegistration.DateReg = entity.DateReg;
                    programmeCourseRegistration.IsCarriedOverCourses = entity.IsCarriedOverCourses;
                    programmeCourseRegistration.Level = levelTranslator.TranslateToModel(entity.LEVEL);
                    programmeCourseRegistration.ProgrammeCourse = programmeCourseTranslator.TranslateToModel(entity.PROGRAMME_COURSES);
                    programmeCourseRegistration.SessionSemester = sessionSemesterTranslator.TranslateToModel(entity.SESSION_SEMESTER);
                    programmeCourseRegistration.TotalMark = entity.TotalMark;
                    programmeCourseRegistration.Student = studentTranslator.TranslateToModel(entity.STUDENT);
                    programmeCourseRegistration.DateReg = entity.DateReg;

                }
                return programmeCourseRegistration;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override PROGRAMME_COURSE_REGISTRATION TranslateToEntity(ProgrammeCourseRegistration model)
        {
            try
            {
                PROGRAMME_COURSE_REGISTRATION programmeCourseRegistration = null;
                if (model != null)
                {
                    programmeCourseRegistration = new PROGRAMME_COURSE_REGISTRATION();
                    programmeCourseRegistration.CourseRegId = model.Id;
                    programmeCourseRegistration.Approved = model.Approved;
                    programmeCourseRegistration.CourseUnit = model.CourseUnit;
                    programmeCourseRegistration.DateApproved = model.DateApproved;
                    programmeCourseRegistration.DateReg = model.DateReg;
                    programmeCourseRegistration.IsCarriedOverCourses = model.IsCarriedOverCourses;
                    programmeCourseRegistration.LevelId = model.Level.Id;
                    programmeCourseRegistration.ProgrammeCourseId = model.ProgrammeCourse.Id;
                    programmeCourseRegistration.SessionSemesterId = model.SessionSemester.Id;
                    programmeCourseRegistration.TotalMark = model.TotalMark;
                    programmeCourseRegistration.StudentId = model.Student.Id;
                    programmeCourseRegistration.DateReg = DateTime.Now;

                }
                return programmeCourseRegistration;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
