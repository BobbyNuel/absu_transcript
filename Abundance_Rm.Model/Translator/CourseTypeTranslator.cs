﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Entity;

namespace Abundance_Rm.Model.Translator
{
    public class CourseTypeTranslator:BaseTranslator<CourseType,COURSE_TYPE>
    {
        public override CourseType TranslateToModel(COURSE_TYPE entity)
        {
            try
            {
                CourseType courseType = null;
                if (entity != null)
                {
                    courseType = new CourseType();
                    courseType.Id = entity.CourseTypeId;
                    courseType.Name = entity.CourseTypeName;
                    courseType.CourseTypeCode = entity.CourseTypeCode;
                    
                }
                return courseType;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override COURSE_TYPE TranslateToEntity(CourseType model)
        {
            try
            {
                COURSE_TYPE courseType = null;
                if (model != null)
                {
                    courseType = new COURSE_TYPE();
                    courseType.CourseTypeId = model.Id;
                    courseType.CourseTypeName = model.Name;
                    courseType.CourseTypeCode = model.CourseTypeCode;
                }
                return courseType;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
