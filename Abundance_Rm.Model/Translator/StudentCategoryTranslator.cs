﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;

namespace Abundance_Rm.Model.Translator
{
    public class StudentCategoryTranslator:BaseTranslator<StudentCategory,STUDENT_CATEGORY>
    {
        public override StudentCategory TranslateToModel(STUDENT_CATEGORY entity)
        {
            try
            {
                StudentCategory studentCategory = null;
                if (entity != null)
                {
                    studentCategory = new StudentCategory();
                    studentCategory.Id = entity.CategoryId;
                    studentCategory.Name = entity.Name;
                    studentCategory.active = entity.active;
                }
                return studentCategory;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override STUDENT_CATEGORY TranslateToEntity(StudentCategory model)
        {
            try
            {
                STUDENT_CATEGORY studentCategory = null;
                if (model != null)
                {
                    studentCategory = new STUDENT_CATEGORY();
                    studentCategory.CategoryId = model.Id;
                    studentCategory.Name = model.Name;
                    studentCategory.active = model.active;

                }
                return studentCategory;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
