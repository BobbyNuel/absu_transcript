﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;

namespace Abundance_Rm.Model.Translator
{
    public class ProgrammeTranslator:BaseTranslator<Programme,PROGRAMME>
    {
        private CertificateTranslator certificateTranslator;
        private ProgrammeTypeTranslator programmeTypeTranslator;
        private DepartmentTranslator departmentTranslator;
        public ProgrammeTranslator()
        {
            certificateTranslator = new CertificateTranslator();
            departmentTranslator = new DepartmentTranslator();
            programmeTypeTranslator = new ProgrammeTypeTranslator();
        }
        public override Programme TranslateToModel(PROGRAMME entity)
        {
            try
            {
                Programme programme = null;
                if (entity != null)
                {
                    programme = new Programme();
                    programme.CategoryId = entity.CategoryId;
                    programme.Duration = entity.Duration;
                    programme.Activated = entity.Activated;
                    programme.EndLevel = entity.EndLevel;
                    programme.IsRemoveRequested = entity.IsRemoveRequested;
                    programme.Id = entity.ProgrammeId;
                    programme.ProgrammeName = entity.ProgrammeName;
                    programme.StartLevel = entity.StartLevel;
                    programme.UnitsRequired = entity.UnitsRequired;
                    programme.department = departmentTranslator.Translate(entity.DEPARTMENT);
                    programme.ProgrammeType = programmeTypeTranslator.Translate(entity.PROGRAMME_TYPE);
                    programme.certificate = certificateTranslator.Translate(entity.CERTIFICATE);
                }
                return programme;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override PROGRAMME TranslateToEntity(Programme model)
        {
            try
            {
                PROGRAMME programme = null;
                if (model != null)
                {
                    programme = new PROGRAMME();
                    programme.CategoryId = model.CategoryId;
                    programme.Duration = model.Duration;
                    programme.Activated = model.Activated;
                    programme.EndLevel = model.EndLevel;
                    programme.IsRemoveRequested = model.IsRemoveRequested;
                    programme.ProgrammeId = model.Id;
                    programme.ProgrammeName = model.ProgrammeName;
                    programme.StartLevel = model.StartLevel;
                    programme.UnitsRequired = model.UnitsRequired;
                    programme.DepartmentId = model.department.Id;
                    programme.ProgrammeTypeId = model.ProgrammeType.Id;
                    if (model.certificate != null)
                    {
                        programme.CertificateId = model.certificate.Id;
                    }

                   
                }
                return programme;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
