﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;

namespace Abundance_Rm.Model.Translator
{
    public class MaritalStatusTranslator:BaseTranslator<MaritalStatus,MARITAL_STATUS>
    {
        public override MaritalStatus TranslateToModel(MARITAL_STATUS entity)
        {
            try
            {
                MaritalStatus maritalStatus = null;
                if (entity != null)
                {
                    maritalStatus = new MaritalStatus();
                    maritalStatus.Id = entity.Marital_StatusId;
                    maritalStatus.Name = entity.Name;
                }
                return maritalStatus;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override MARITAL_STATUS TranslateToEntity(MaritalStatus model)
        {
            try
            {
                MARITAL_STATUS maritalStatus = null;
                if (model != null)
                {
                    maritalStatus = new MARITAL_STATUS();
                    maritalStatus.Marital_StatusId = model.Id;
                    maritalStatus.Name = model.Name;

                }
                return maritalStatus;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
