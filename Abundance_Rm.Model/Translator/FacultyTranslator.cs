﻿using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Translator
{
    public class FacultyTranslator:BaseTranslator<Faculty,FACULTY>
    {
        public override Faculty TranslateToModel(FACULTY entity)
        {
            try
            {
                Faculty faculty = null;
                if(entity!=null)
                {
                    faculty = new Faculty();
                    faculty.Id = entity.FacultyId;
                    faculty.FacultyCode = entity.FacultyCode;
                    faculty.Description = entity.Description;
                    faculty.IsRemoveRequested = entity.IsRemoveRequested;
                    faculty.Name = entity.Name;
                }
                return faculty;

            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public override FACULTY TranslateToEntity(Faculty model)
        {
            try
            {
                FACULTY faculty = null;
                if (model != null)
                {
                    faculty = new FACULTY();
                    faculty.FacultyId = model.Id;
                    faculty.FacultyCode = model.FacultyCode;
                    faculty.Description = model.Description;
                    faculty.IsRemoveRequested = model.IsRemoveRequested;
                    faculty.Name = model.Name;
                }
                return faculty;

            }
            catch (Exception)
            {

                throw;
            }
        }
        
    }
}
