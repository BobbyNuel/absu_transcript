﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;

namespace Abundance_Rm.Model.Translator
{
    public class ScoreGradeTranslator : BaseTranslator<ScoreGrade,SCORE_GRADE>
    {
        public override ScoreGrade TranslateToModel(SCORE_GRADE entity)
        {
            try
            {
                ScoreGrade scoreGrade = null;
                if (entity != null)
                {
                    scoreGrade = new ScoreGrade();
                    scoreGrade.Id = entity.Grade_Id;
                    scoreGrade.Description = entity.Grade_Description;
                    scoreGrade.Grade = entity.Grade;
                    scoreGrade.GradePoint = entity.Grade_Point;
                    scoreGrade.Performance = entity.Performance;
                    scoreGrade.ScoreFrom = entity.Score_From;
                    scoreGrade.ScoreTo = entity.Score_To;
                    scoreGrade.SessionStart = entity.Session_Start;
                    scoreGrade.SessionEnd = entity.Session_End;
                }
                return scoreGrade;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public override SCORE_GRADE TranslateToEntity(ScoreGrade model)
        {
            try
            {
                SCORE_GRADE entity = null;
                if (model != null)
                {
                    entity = new SCORE_GRADE();
                    entity.Grade_Id = model.Id;
                    entity.Grade_Description = model.Description;
                    entity.Grade = model.Grade;
                    entity.Grade_Point = model.GradePoint;
                    entity.Performance = model.Performance;
                    entity.Score_From = model.ScoreFrom;
                    entity.Score_To = model.ScoreTo;
                    entity.Session_Start = model.SessionStart;
                    entity.Session_End = model.SessionEnd;
                }
                return entity;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
    }
}
