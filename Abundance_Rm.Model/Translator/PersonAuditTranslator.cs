﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;

namespace Abundance_Rm.Model.Translator
{
    public class PersonAuditTranslator : BaseTranslator<PersonAudit,PERSON_AUDIT>
    {
        private UserTranslator userTranslator;
        private PersonTranslator personTranslator;

        public PersonAuditTranslator()
        {
            userTranslator = new UserTranslator();
            personTranslator = new PersonTranslator();
        }
        public override PersonAudit TranslateToModel(PERSON_AUDIT entity)
        {
            try
            {
                PersonAudit personAudit = null;
                if (entity != null)
                {
                    personAudit = new PersonAudit();
                    personAudit.Person = personTranslator.Translate(entity.PERSON);
                    personAudit.NewFirstName = entity.New_First_Name;
                    personAudit.NewLastName = entity.New_Last_Name;
                    personAudit.NewMiddleName = entity.New_Middle_Name;
                    personAudit.NewCountryId = entity.New_Country_Id;
                    personAudit.NewSex = entity.New_Sex;
                    personAudit.NewProgrammeId = entity.New_Programme_Id;
                    personAudit.NewDepartmentId = entity.New_Department_Id;
                    personAudit.NewCGPA = entity.New_CGPA;
                    personAudit.OldFirstName = entity.Old_First_Name;
                    personAudit.OldLastName = entity.Old_Last_Name;
                    personAudit.OldMiddleName = entity.Old_Middle_Name;
                    personAudit.OldSex = entity.Old_Sex;
                    personAudit.OldCountryId = entity.Old_Country_Id;
                    personAudit.OldProgrammeId = entity.Old_Programme_Id;
                    personAudit.OldDepartmentId = entity.Old_Department_Id;
                    personAudit.OldCGPA = entity.Old_CGPA;
                    personAudit.Action = entity.Action;
                    personAudit.Client = entity.Client;
                    personAudit.Time = entity.Time;
                    personAudit.Operation = entity.Operation;
                    personAudit.User = userTranslator.Translate(entity.USER);

                }
                return personAudit;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override PERSON_AUDIT TranslateToEntity(PersonAudit model)
        {
            try
            {
                PERSON_AUDIT entity = null;
                if (model != null)
                {
                    entity = new PERSON_AUDIT();
                    entity.Person_Id = model.Person.Id;
                    entity.New_First_Name = model.NewFirstName;
                    entity.New_Last_Name = model.NewLastName;
                    entity.New_Middle_Name = model.NewMiddleName;
                    entity.New_Country_Id = model.NewCountryId;
                    entity.New_Sex = model.NewSex;
                    entity.New_Year_Of_Entry = model.NewYearOfEntry;
                    entity.New_Year_Of_Graduation = model.NewYearOfGraduation;
                    entity.New_Programme_Id = model.NewProgrammeId;
                    entity.New_Department_Id = model.NewDepartmentId;
                    entity.New_CGPA = model.NewCGPA;
                    entity.Old_First_Name = model.OldFirstName;
                    entity.Old_Last_Name = model.OldLastName;
                    entity.Old_Middle_Name = model.OldMiddleName;
                    entity.Old_Sex = model.OldSex;
                    entity.Old_Country_Id = model.OldCountryId;
                    entity.Old_Year_Of_Entry = model.OldYearOfEntry;
                    entity.Old_Year_Of_Graduation = model.OldYearOfGraduation;
                    entity.Old_Programme_Id = model.OldDepartmentId;
                    entity.Old_Department_Id = model.OldDepartmentId;
                    entity.Old_CGPA = model.OldCGPA;
                    entity.Action = model.Action;
                    entity.Client = model.Client;
                    entity.Time = model.Time;
                    entity.Operation = model.Operation;
                    entity.User_Id = model.User.Id;
                }
                return entity;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
