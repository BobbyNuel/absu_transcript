﻿using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Translator
{
    public class LgaTranslator : BaseTranslator<Lga,LGA>
    {
        StateTranslator stateTranslator;
        public LgaTranslator()
        {
            stateTranslator = new StateTranslator();
        }
        public override Lga TranslateToModel(LGA entity)
        {
            try
            {
                Lga lga = null;
                if (entity != null)
                {
                    lga = new Lga();
                    lga.Id = entity.LGAId;
                    lga.Name = entity.Name;
                    lga.State = stateTranslator.Translate(entity.STATE);
                }
                return lga;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override LGA TranslateToEntity(Lga model)
        {
            try
            {
                LGA lga = null;
                if (model != null)
                {
                    lga = new LGA();
                    lga.LGAId = model.Id;
                    lga.Name = model.Name;
                    lga.StateId = model.State.Id;

                }
                return lga;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
