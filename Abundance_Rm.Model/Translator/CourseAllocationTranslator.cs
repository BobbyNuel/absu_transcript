﻿using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Translator
{
    public class CourseAllocationTranslator : BaseTranslator<CourseAllocation, COURSE_ALLOCATION>
    {
        private CourseTranslator courseTranslator;
        private UserTranslator userTranslator;
        private SessionTranslator sessionTranslator;
        private SemesterTranslator semesterTranslator;
        private LevelTranslator levelTranslator;
        private DepartmentTranslator departmentTranslator;
        private ProgrammeTypeTranslator programmeTypeTranslator;
        public CourseAllocationTranslator()
        {
            courseTranslator = new CourseTranslator();
            userTranslator = new UserTranslator();
            sessionTranslator = new SessionTranslator();
            levelTranslator = new LevelTranslator();
            departmentTranslator = new DepartmentTranslator();
            programmeTypeTranslator = new ProgrammeTypeTranslator();
            semesterTranslator = new SemesterTranslator();
        }
        public override CourseAllocation TranslateToModel(COURSE_ALLOCATION entity)
        {
            try
            {
                CourseAllocation model = null;
                if (entity != null)
                {
                    model = new CourseAllocation();
                    model.Id = entity.Course_Allocation_Id;
                    model.Course = courseTranslator.TranslateToModel(entity.COURSE);
                    model.Department = departmentTranslator.TranslateToModel(entity.DEPARTMENT);
                    model.Level = levelTranslator.TranslateToModel(entity.LEVEL);
                    model.ProgrammeType = programmeTypeTranslator.TranslateToModel(entity.PROGRAMME_TYPE);
                    model.Semester = semesterTranslator.TranslateToModel(entity.SEMESTER);
                    model.Session = sessionTranslator.TranslateToModel(entity.SESSION);
                    model.User = userTranslator.TranslateToModel(entity.USER);
                }

                return model;
            }
            catch (Exception)
            {                
                throw;
            }
        }
        public override COURSE_ALLOCATION TranslateToEntity(CourseAllocation model)
        {
            try
            {
                COURSE_ALLOCATION entity = null;
                if (model != null)
                {
                    entity = new COURSE_ALLOCATION();
                    entity.Course_Allocation_Id = model.Id;
                    entity.Course_Id = model.Course.Id;
                    entity.Department_Id = model.Department.Id;
                    entity.Level_Id = model.Level.Id;
                    entity.Programme_Type_Id = model.ProgrammeType.Id;
                    entity.Semester_Id = model.Semester.Id;
                    entity.Session_Id = model.Session.Id;
                    entity.User_Id = model.User.Id;
                }

                return entity;
            }
            catch (Exception)
            {                
                throw;
            }
        }
    }
}
