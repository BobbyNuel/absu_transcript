﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;

namespace Abundance_Rm.Model.Translator
{
    public class SponsorTranslator:BaseTranslator<Sponsor,SPONSOR>
    {
        public override Sponsor TranslateToModel(SPONSOR entity)
        {
            try
            {
                Sponsor sponsor = null;
                if (entity != null)
                {
                    sponsor = new Sponsor();
                    sponsor.Id = entity.SponsorId;
                    sponsor.Name = entity.Name;
                    sponsor.Address = entity.Address;
                    sponsor.Occupation = entity.Occupation;
                    sponsor.PhoneNumber = entity.PhoneNumber;
                }
                return sponsor;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override SPONSOR TranslateToEntity(Sponsor model)
        {
            try
            {
                SPONSOR sponsor = null;
                if (model != null)
                {
                    sponsor = new SPONSOR();
                    sponsor.SponsorId = model.Id;
                    sponsor.Name = model.Name;
                    sponsor.Address = model.Address;
                    sponsor.Occupation = model.Occupation;
                    sponsor.PhoneNumber = model.PhoneNumber;

                }
                return sponsor;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
