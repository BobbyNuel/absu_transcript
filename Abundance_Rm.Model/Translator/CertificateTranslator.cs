﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Entity;

namespace Abundance_Rm.Model.Translator
{
    public class CertificateTranslator:BaseTranslator<Certificate,CERTIFICATE>
    {
        public override Certificate TranslateToModel(CERTIFICATE entity)
        {
            try
            {
                Certificate certificate = null;
                if (entity != null)
                {
                    certificate = new Certificate();
                    certificate.Activated = entity.Activated;
                    certificate.CertificateCode = entity.CertificateCode;
                    certificate.Id = entity.CertificateId;
                    certificate.IsRemoveRequested = entity.IsRemoveRequested;
                    certificate.Name = entity.CertificateName;
                }
                return certificate;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override CERTIFICATE TranslateToEntity(Certificate model)
        {
            try
            {
                CERTIFICATE certificate = null;
                if (model != null)
                {
                    certificate = new CERTIFICATE();
                    certificate.Activated = model.Activated;
                    certificate.CertificateCode = model.CertificateCode;
                    certificate.CertificateId = model.Id;
                    certificate.IsRemoveRequested = model.IsRemoveRequested;
                    certificate.CertificateName = model.Name;

                }
                return certificate;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
