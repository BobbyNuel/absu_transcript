﻿using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Translator
{
    public class PersonTypeTranslator : BaseTranslator<PersonType,PERSON_TYPE>
    {
        public override PersonType TranslateToModel(PERSON_TYPE entity)
        {
            try
            {
                PersonType personType = null;
                if (entity != null)
                {
                    personType = new PersonType();
                    personType.Id = entity.PersonTypeId;
                    personType.Name = entity.Name;
                }
                return personType;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override PERSON_TYPE TranslateToEntity(PersonType model)
        {
            try
            {
                PERSON_TYPE personType = null;
                if (model != null)
                {
                    personType = new PERSON_TYPE();
                    personType.PersonTypeId = model.Id;
                    personType.Name = model.Name;

                }
                return personType;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
