﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;

namespace Abundance_Rm.Model.Translator
{
    public class UploadedResultTranslator:BaseTranslator<UploadedResult,UPLOADED_RESULT>
    {
        private ProgrammeTranslator programmeTranslator;
        private StudentTranslator studentTranslator;
      
        public UploadedResultTranslator()
        {
            programmeTranslator = new ProgrammeTranslator();
            studentTranslator = new StudentTranslator();
          
          
        }
        public override UploadedResult TranslateToModel(UPLOADED_RESULT entity)
        {
            try
            {
                UploadedResult uploadedResult = null;
                if (entity != null)
                {
                    uploadedResult = new UploadedResult();
                    uploadedResult.Id = entity.ResultId;
                    uploadedResult.Programme = new Programme()
                    {
                        Id = entity.ProgrammeId
                    };
                    uploadedResult.DateUploaded = entity.Date_Uploaded;
                    uploadedResult.FileUrl = entity.FileUrl;
                    uploadedResult.UploadedBy = entity.UploadedBy;
                    uploadedResult.WorkSheetIndex = entity.WorkSheetIndex;
                    uploadedResult.Student = studentTranslator.Translate(entity.STUDENT);
                    uploadedResult.Client = entity.Client;
                }
                return uploadedResult;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override UPLOADED_RESULT TranslateToEntity(UploadedResult model)
        {
            try
            {
                UPLOADED_RESULT uploadedResult = null;
                if (model != null)
                {
                    uploadedResult = new UPLOADED_RESULT();
                    uploadedResult.ResultId = model.Id;
                    uploadedResult.ProgrammeId = model.Programme.Id;
                    uploadedResult.Date_Uploaded = model.DateUploaded;
                    uploadedResult.FileUrl = model.FileUrl;
                    uploadedResult.UploadedBy = model.UploadedBy;
                    uploadedResult.WorkSheetIndex = model.WorkSheetIndex;
                    uploadedResult.StudentId = model.Student.Id;
                    uploadedResult.Client = model.Client;

                }
                return uploadedResult;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
