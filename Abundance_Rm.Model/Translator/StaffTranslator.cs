﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;

namespace Abundance_Rm.Model.Translator
{
    public class StaffTranslator : BaseTranslator<Staff,STAFF>
    {
        private UserTranslator UserTranslator;
        private DepartmentTranslator departmentTranslator;

        public StaffTranslator()
        {
            UserTranslator = new UserTranslator();
            departmentTranslator = new DepartmentTranslator();
        }
        public override Staff TranslateToModel(STAFF entity)
        {
            try
            {
                Staff model = null;
                if (entity != null)
                {
                    model = new Staff();
                    model.Id = entity.Staff_Id;
                    model.Department = departmentTranslator.Translate(entity.DEPARTMENT);
                    model.User = UserTranslator.Translate(entity.USER);
                    model.Activated = entity.Activated;
                }
                return model;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public override STAFF TranslateToEntity(Staff model)
        {
            try
            {
                STAFF entity = null;
                if (model != null)
                {
                    entity = new STAFF();
                    entity.Staff_Id = model.Id;
                    if (model.Department != null)
                    {
                        entity.Department_Id = model.Department.Id;
                    }
                    entity.User_Id = model.User.Id;
                    entity.Activated = model.Activated;
                }
                return entity;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
    }
}
