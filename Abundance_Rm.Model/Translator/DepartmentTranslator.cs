﻿using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Translator
{
    public class DepartmentTranslator:BaseTranslator<Department,DEPARTMENT>
    {
        private FacultyTranslator facultyTranslator;
        public DepartmentTranslator()
        {
            facultyTranslator = new FacultyTranslator();
        }
        public override Department TranslateToModel(DEPARTMENT entity)
        {
            try
            {
                Department department = null;
                if (entity != null)
                {
                    department = new Department();
                    department.Activated = entity.Activated;
                    department.Department_Code = entity.Department_Code;
                    department.Description = entity.Description;
                    department.IsRemoveRequested = entity.IsRemoveRequested;
                    department.Name = entity.Name;
                    department.Id = entity.DepartmentId;
                    department.faculty = facultyTranslator.Translate(entity.FACULTY);
                }
                return department;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override DEPARTMENT TranslateToEntity(Department model)
        {
            try
            {
                DEPARTMENT department = null;
                if (model != null)
                {
                    department = new DEPARTMENT();
                    department.FacultyId = model.faculty.Id;
                    department.Department_Code = model.Department_Code;
                    department.Description = model.Description;
                    department.IsRemoveRequested = model.IsRemoveRequested;
                    department.Name = model.Name;
                    department.DepartmentId = model.Id;
                    department.Activated = model.Activated;
                    
                }
                return department;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
