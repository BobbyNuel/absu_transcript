﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;

namespace Abundance_Rm.Model.Translator
{
    public class RoleTranslator:BaseTranslator<Role,ROLE>
    {
        public override Role TranslateToModel(ROLE entity)
        {
            try
            {
                Role role = null;
                if (entity != null)
                {
                    role = new Role();
                    role.Id = entity.Role_Id;
                    role.Name = entity.Role_Name;
                    role.Role_Description = entity.Role_Description;
                }
                return role;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override ROLE TranslateToEntity(Role model)
        {
            try
            {
                ROLE role = null;
                if (model != null)
                {
                    role = new ROLE();
                    role.Role_Id = model.Id;
                    role.Role_Name = model.Name;
                    role.Role_Description = model.Role_Description;
                }
                return role;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
