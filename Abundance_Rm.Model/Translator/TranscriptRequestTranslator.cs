﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;

namespace Abundance_Rm.Model.Translator
{
    public class TranscriptRequestTranslator : BaseTranslator<TranscriptRequest,TRANSCRIPT_REQUEST>
    {
        private StateTranslator stateTranslator;
        private CountryTranslator countryTranslator;
        private PersonTranslator personTranslator;
        private ProgrammeTranslator programmeTranslator;
        public LevelTranslator LevelTranslator;

        public TranscriptRequestTranslator()
        {
            stateTranslator = new StateTranslator();
            countryTranslator = new CountryTranslator();
            personTranslator = new PersonTranslator();
            programmeTranslator = new ProgrammeTranslator();
            LevelTranslator = new LevelTranslator();
        }
        public override TranscriptRequest TranslateToModel(TRANSCRIPT_REQUEST entity)
        {
            try
            {
                TranscriptRequest transcriptRequest = null;
                if (entity != null)
                {
                    transcriptRequest = new TranscriptRequest();
                    transcriptRequest.Id = entity.Transcript_Request_Id;
                    transcriptRequest.StudentId = entity.Student_id;
                    transcriptRequest.PaymentId = entity.Payment_Id;
                    transcriptRequest.DateRequested = entity.Date_Requested;
                    transcriptRequest.DestinationAddress = entity.Destination_Address;
                    transcriptRequest.TranscriptStatusId = entity.Transcript_Status_Id;
                    transcriptRequest.TranscriptclearanceStatusId = entity.Transcript_clearance_Status_Id;
                    transcriptRequest.Country = countryTranslator.Translate(entity.COUNTRY);
                    transcriptRequest.State = stateTranslator.Translate(entity.STATE);
                    transcriptRequest.Processed = entity.Processed;
                    transcriptRequest.ProcessedDate = entity.Processed_Date;
                    if (entity.PERSON != null)
                    {
                        transcriptRequest.Person = personTranslator.Translate(entity.PERSON);
                    }
                    transcriptRequest.MatricNumber = entity.Matric_Number;
                    transcriptRequest.StudentName = entity.Student_Name;
                    transcriptRequest.Status = entity.Status;
                    transcriptRequest.Programme = programmeTranslator.Translate(entity.PROGRAMME);
                    transcriptRequest.Level = LevelTranslator.Translate(entity.LEVEL);
                    transcriptRequest.EmailAddress = entity.Email_Address;
                    transcriptRequest.DeliveryService = entity.Delivery_Service;
                    transcriptRequest.DeliveryZone = entity.Delivery_Zone;

                }
                return transcriptRequest;

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public override TRANSCRIPT_REQUEST TranslateToEntity(TranscriptRequest model)
        {
            try
            {
                TRANSCRIPT_REQUEST transcriptRequest = null;
                if (model != null)
                {
                    transcriptRequest = new TRANSCRIPT_REQUEST();
                    transcriptRequest.Transcript_Request_Id  = model.Id;
                    transcriptRequest.Student_id = model.StudentId;
                    transcriptRequest.Payment_Id = model.PaymentId;
                    transcriptRequest.Date_Requested = model.DateRequested;
                    transcriptRequest.Destination_Address = model.DestinationAddress;
                    transcriptRequest.Transcript_Status_Id = model.TranscriptStatusId;
                    transcriptRequest.Transcript_clearance_Status_Id = model.TranscriptclearanceStatusId;
                    transcriptRequest.Destination_Country_Id = model.Country.Id;
                    transcriptRequest.Destination_State_Id = model.State.Id;
                    transcriptRequest.Processed = model.Processed;
                    transcriptRequest.Processed_Date = model.ProcessedDate;
                    if (model.Person != null)
                    {
                        transcriptRequest.Person_Id = model.Person.Id;
                    }
                    transcriptRequest.Matric_Number = model.MatricNumber;
                    transcriptRequest.Student_Name = model.StudentName;
                    transcriptRequest.Status = model.Status;
                    transcriptRequest.ProgrammeId = model.Programme.Id;
                    transcriptRequest.Level_Id = model.Level.Id;
                    transcriptRequest.Email_Address = model.EmailAddress;
                    transcriptRequest.Delivery_Zone = model.DeliveryZone;
                    transcriptRequest.Delivery_Service = model.DeliveryService;

                }
                return transcriptRequest;

            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
