﻿using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Translator
{
    public class SessionTranslator : BaseTranslator<Session, SESSION>
    {
        public override Session TranslateToModel(SESSION entity)
        {
            try
            {
                Session session = null;
                if (entity != null)
                {
                    session = new Session();
                    session.Id = entity.SessionId;
                    session.Name = entity.Name;
                    session.Description = entity.Description;
                    session.IsRemoveRequested = entity.IsRemoveRequested;
                    session.IsCurrenSession = entity.IsCurrenSession;
                }
                return session;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override SESSION TranslateToEntity(Session model)
        {
            try
            {
                SESSION session = null;
                if (model != null)
                {
                    session = new SESSION();
                    session.SessionId = model.Id;
                    session.Name = model.Name;
                    session.Description = model.Description;
                    session.IsRemoveRequested = model.IsRemoveRequested;
                    session.IsCurrenSession = model.IsCurrenSession;

                }
                return session;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
