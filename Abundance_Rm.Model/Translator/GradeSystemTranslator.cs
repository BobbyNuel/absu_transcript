﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Entity;

namespace Abundance_Rm.Model.Translator
{
    public class GradeSystemTranslator:BaseTranslator<GradeSystem,GRADE_STYSTEM>
    {
        private LevelTranslator levelTransaltor;
        public GradeSystemTranslator()
        {
            levelTransaltor = new LevelTranslator();
        }
        public override GradeSystem TranslateToModel(GRADE_STYSTEM entity)
        {
            try
            {
                GradeSystem gradeSystem = null;
                if (entity != null)
                {
                    gradeSystem = new GradeSystem();
                    gradeSystem.Grade = entity.Grade;
                    gradeSystem.Grade_Range = entity.Grade_Range;
                    gradeSystem.Id = entity.Grade_SystemId;
                    gradeSystem.GradePoint = entity.GradePoint;
                    gradeSystem.RemoveRequested = entity.RemoveRequested;
                    gradeSystem.Level = levelTransaltor.Translate(entity.LEVEL);
                }
                return gradeSystem;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override GRADE_STYSTEM TranslateToEntity(GradeSystem model)
        {
            try
            {
                GRADE_STYSTEM gradeSystem = null;
                if (model != null)
                {
                    gradeSystem = new GRADE_STYSTEM();
                    gradeSystem.Grade = model.Grade;
                    gradeSystem.Grade_Range = model.Grade_Range;
                    gradeSystem.Grade_SystemId = model.Id;
                    gradeSystem.GradePoint = model.GradePoint;
                    gradeSystem.RemoveRequested = model.RemoveRequested;
                    gradeSystem.LevelId = model.Level.Id;
                }
                return gradeSystem;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
