﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;

namespace Abundance_Rm.Model.Translator
{
    public class StudentCourseMarkAuditTranslator : BaseTranslator<StudentCourseMarkAudit,STUDENT_COURSE_MARK_AUDIT>
    {
        private UserTranslator userTranslator;
        private ProgrammeCourseTranslator programmeCourseTranslator;

        public StudentCourseMarkAuditTranslator()
        {
            userTranslator = new UserTranslator();
            programmeCourseTranslator = new ProgrammeCourseTranslator();
        }
        public override STUDENT_COURSE_MARK_AUDIT TranslateToEntity(StudentCourseMarkAudit model)
        {
            try
            {
                STUDENT_COURSE_MARK_AUDIT entity = null;

                if (model != null)
                {
                    entity = new STUDENT_COURSE_MARK_AUDIT();
                    entity.Programme_Course_Id = model.ProgrammeCourse.Id;
                    entity.Time = model.Time;
                    entity.Client = model.Client;
                    entity.Operation = model.Operation;
                    entity.User_Id = model.User.Id;
                    entity.Action = model.Action;
                }
                return entity;
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }

        public override StudentCourseMarkAudit TranslateToModel(STUDENT_COURSE_MARK_AUDIT entity)
        {
            try
            {
                StudentCourseMarkAudit model = null;
                if (entity != null)
                {
                   model = new StudentCourseMarkAudit();
                    model.Id = entity.Student_Course_Mark_Audit_Id;
                    model.User = userTranslator.Translate(entity.USER);
                    model.ProgrammeCourse = programmeCourseTranslator.Translate(entity.PROGRAMME_COURSES);
                    model.Time = entity.Time;
                    model.Client = entity.Client;
                    model.Operation = entity.Operation;
                    model.Action = entity.Action;

                }
                return model;
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }
    }
}
