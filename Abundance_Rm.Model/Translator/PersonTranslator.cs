﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;


namespace Abundance_Rm.Model.Translator
{
    public class PersonTranslator:BaseTranslator<Person,PERSON>
    {
        private RoleTranslator roleTranslator;
        private CountryTranslator countryTranslator;
        private StateTranslator stateTranslator;
        private MaritalStatusTranslator maritalStatusTranslator;
        private LgaTranslator lgaTranslator;
        private ReligionTranslator religionTranslator;
        private PersonTypeTranslator personTypeTranslator;
        public PersonTranslator()
        {
            roleTranslator = new RoleTranslator();
            countryTranslator = new CountryTranslator();
            stateTranslator = new StateTranslator();
            maritalStatusTranslator = new MaritalStatusTranslator();
            lgaTranslator = new LgaTranslator();
            religionTranslator = new ReligionTranslator();
            personTypeTranslator = new PersonTypeTranslator();
        }
        public override Person TranslateToModel(PERSON entity)
        {
            try
            {
                Person person = null;
                if (entity != null)
                {
                    person = new Person();
                    person.contact_address = entity.contact_address;
                    person.Country = countryTranslator.Translate(entity.COUNTRY);
                    person.DateFilled = entity.DateFilled;
                    person.DateofBirth = entity.DateofBirth;
                    person.EmailAddress = entity.EmailAddress;
                    person.FirstName = entity.FirstName;
                    person.HomeTown = entity.HomeTown;
                    person.Lga = lgaTranslator.Translate(entity.LGA);
                    person.MaritalStatus = maritalStatusTranslator.Translate(entity.MARITAL_STATUS);
                    person.MiddleName = entity.MiddleName;
                    person.ModeOfEntry = entity.ModeOfEntry;
                    person.Id = entity.PersonId;
                    person.PersonType = personTypeTranslator.Translate(entity.PERSON_TYPE);
                    person.PhoneNumber = entity.PhoneNumber;
                    person.PictureURL = entity.PictureURL;
                    person.PlaceofBirth = entity.PlaceofBirth;
                    person.Religion = religionTranslator.Translate(entity.RELIGION);
                    person.RemoveRequested = entity.RemoveRequested;
                    person.Role = roleTranslator.Translate(entity.ROLE);
                    person.Sex = entity.Sex;
                    person.State = stateTranslator.Translate(entity.STATE);
                    person.Surname = entity.Surname;
                    person.Session = entity.SessionId;
                }
                return person;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override PERSON TranslateToEntity(Person model)
        {
            try
            {
                PERSON person = null;
                if (model != null)
                {
                    person = new PERSON();
                    
                    person.contact_address = model.contact_address;
                    if (model.Country != null)
                    {
                        person.CountryId = model.Country.Id;  
                    }
                    else
                    {
                        person.CountryId = 130;
                    }
                    person.DateFilled = model.DateFilled;
                    person.DateofBirth = model.DateofBirth;
                    person.EmailAddress = model.EmailAddress;
                    person.FirstName = model.FirstName;
                    person.HomeTown = model.HomeTown;
                    if (model.Lga != null)
                    {
                        person.LGAId = model.Lga.Id;
                    }
                    if (model.MaritalStatus != null)
                    {
                        person.Marital_StatusId = model.MaritalStatus.Id;
                    }
                    
                    person.MiddleName = model.MiddleName;
                    person.ModeOfEntry = model.ModeOfEntry;
                    person.PersonId = model.Id;
                    person.PersonTypeId = model.PersonType.Id;
                    person.PhoneNumber = model.PhoneNumber;
                    person.PictureURL = model.PictureURL;
                    person.PlaceofBirth = model.PlaceofBirth;
                    if (model.Religion != null)
                    {
                        person.ReligionId = model.Religion.Id;
                    }
                   
                    person.RemoveRequested = model.RemoveRequested;
                    if (model.Role != null)
                    {
                        person.Role_Id = model.Role.Id; 
                    }
                    
                    person.Sex = model.Sex;
                    if (model.State != null)
                    {
                        person.StateId = model.State.Id;
                    }
                   
                    person.Surname = model.Surname;
                }
                return person;

            }
            catch (Exception)
            {
                throw; 
            }
        }
    }
}
