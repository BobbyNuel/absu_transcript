﻿using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Translator
{
    public class CourseTranslator : BaseTranslator<Course, COURSE>
    {
        public override Course TranslateToModel(COURSE entity)
        {
            try
            {
                Course course = null;
                if (entity != null)
                {
                    course = new Course();
                    course.Id = entity.CourseId;
                    course.Name = entity.Title;
                    course.Course_Code = entity.Course_Code;
                    course.IsRemoveRequested = entity.IsRemoveRequested;
                }
                return course;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override COURSE TranslateToEntity(Course model)
        {
            try
            {
                COURSE course = null;
                if (model != null)
                {
                    course = new COURSE();
                    course.CourseId = model.Id;
                    course.Title = model.Name;
                    course.Course_Code = model.Course_Code;
                    course.IsRemoveRequested = model.IsRemoveRequested;
                }
                return course;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
