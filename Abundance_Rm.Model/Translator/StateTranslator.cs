﻿using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Translator
{
    public class StateTranslator : BaseTranslator<State,STATE>
    {
        CountryTranslator countryTranslator;
        public StateTranslator()
        {
            countryTranslator = new CountryTranslator();
        }
        public override State TranslateToModel(STATE entity)
        {
            try
            {
                State state = null;
                if (entity != null)
                {
                    state = new State();
                    state.Id = entity.StateId;
                    state.Name = entity.Name;
                    state.Country = countryTranslator.Translate(entity.COUNTRY);
                    state.Activated = entity.Activated;
                }
                return state;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override STATE TranslateToEntity(State model)
        {
            try
            {
                STATE state = null;
                if (model != null)
                {
                    state = new STATE();
                    state.StateId = model.Id;
                    state.Name = model.Name;
                    state.CountryId = model.Country.Id;
                    state.Activated = model.Activated;

                }
                return state;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
