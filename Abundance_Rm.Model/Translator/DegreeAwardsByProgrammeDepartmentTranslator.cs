﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;

namespace Abundance_Rm.Model.Translator
{
    public  class DegreeAwardsByProgrammeDepartmentTranslator : BaseTranslator<DegreeAwardsByProgrammeDepartment,DEGREE_AWARDS_BY_PROGRAMME_DEPARTMENT>
    {
        private ProgrammeTranslator programmeTranslator;
        private DepartmentTranslator departmentTranslator;

        public DegreeAwardsByProgrammeDepartmentTranslator()
        {
           programmeTranslator = new ProgrammeTranslator();
            departmentTranslator = new DepartmentTranslator();
            

        }
       
        public override DegreeAwardsByProgrammeDepartment TranslateToModel(DEGREE_AWARDS_BY_PROGRAMME_DEPARTMENT entity)
        {
            try
            {
                DegreeAwardsByProgrammeDepartment model = null;
                if (entity != null)
                {
                    model = new DegreeAwardsByProgrammeDepartment();
                    model.Id = entity.Id;
                    model.Department = departmentTranslator.Translate(entity.DEPARTMENT);
                    model.Programme = programmeTranslator.Translate(entity.PROGRAMME);
                    model.DegreeName = entity.Degree_Name;
                    model.Duration = entity.Duration;

                }
                return model;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override DEGREE_AWARDS_BY_PROGRAMME_DEPARTMENT TranslateToEntity(DegreeAwardsByProgrammeDepartment model)
        {
            try
            {
                DEGREE_AWARDS_BY_PROGRAMME_DEPARTMENT entity = null;
                if (model != null)
                {
                    entity = new DEGREE_AWARDS_BY_PROGRAMME_DEPARTMENT();
                    entity.Degree_Name = model.DegreeName;
                    entity.Department_Id = model.Department.Id;
                    entity.Programme_Id = model.Programme.Id;
                    entity.Duration = model.Duration;


                }
                return entity;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
    
}
