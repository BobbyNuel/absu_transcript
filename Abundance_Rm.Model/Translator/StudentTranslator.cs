﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;

namespace Abundance_Rm.Model.Translator
{
    public class StudentTranslator:BaseTranslator<Student,STUDENT>
    {
        private SponsorTranslator sponsorTranslator;
        private LevelTranslator levelTranslator;
        private ProgrammeTranslator programmeTranslator;
        private SessionTranslator sessionTranslator;
        private StudentCategoryTranslator studentCategoryTranslator;
        private DepartmentTranslator departmentTranslator;
        private PersonTranslator personTranslator;
        public StudentTranslator()
        {
            sponsorTranslator = new SponsorTranslator();
            levelTranslator = new LevelTranslator();
            programmeTranslator = new ProgrammeTranslator();
            sessionTranslator = new SessionTranslator();
            studentCategoryTranslator = new StudentCategoryTranslator();
            departmentTranslator = new DepartmentTranslator();
            personTranslator = new PersonTranslator();
        }
        public override Student TranslateToModel(STUDENT entity)
        {
            try
            {
                Student student = null;
                if (entity != null)
                {
                    student = new Student();
                    student.Id = entity.StudentId;
                    student.AwardAndDate = entity.AwardAndDate;
                    student.BasisOfAdmission = entity.BasisOfAdmission;
                    student.CurrentLevel = levelTranslator.Translate(entity.LEVEL);
                    student.CurrentSession = sessionTranslator.Translate(entity.SESSION);
                    student.Department = departmentTranslator.Translate(entity.DEPARTMENT);
                    student.EntryRegNo = entity.EntryRegNo;
                    student.CGPA = entity.CGPA;
                    student.LastSchoolAttended = entity.LastSchoolAttended;
                    student.MatricNo = entity.MatricNo;
                    student.NDGPA = entity.NDGPA;
                    student.Programme = programmeTranslator.Translate(entity.PROGRAMME);
                    student.Sponsor = sponsorTranslator.Translate(entity.SPONSOR);
                    student.StudentCategory = studentCategoryTranslator.Translate(entity.STUDENT_CATEGORY);
                    student.StudentParentId = entity.StudentParentId;
                    student.YearOfEntry = entity.YearOfEntry;
                    student.GraduationDate = entity.YearOfGraduation;
                    student.WesVerificationNumber = entity.WesVerificationNumber;
                    student.Person = personTranslator.Translate(entity.PERSON);
                }
                return student;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public override STUDENT TranslateToEntity(Student model)
        {
            try
            {
                STUDENT student = null;
                if (model != null)
                {
                    student = new STUDENT();
                    student.StudentId = model.Id;
                    student.AwardAndDate = model.AwardAndDate;
                    student.BasisOfAdmission = model.BasisOfAdmission;
                    student.CurrentLevelId = model.CurrentLevel.Id;
                    student.CurrentSessionId = model.CurrentSession.Id;
                    student.DepartmentId = model.Department.Id;
                    student.EntryRegNo = model.EntryRegNo;
                    student.CGPA = model.CGPA;
                    student.LastSchoolAttended = model.LastSchoolAttended;
                    student.MatricNo = model.MatricNo;
                    student.NDGPA = model.NDGPA;
                    student.ProgrammeId = model.Programme.Id;
                    student.CategoryId = model.StudentCategory.Id;
                    if (model.Sponsor != null)
                    {
                        student.StudentParentId = model.Sponsor.Id;
                        student.StudentSponsorId = model.Sponsor.Id;
                    }
                    student.YearOfEntry = model.YearOfEntry;
                    student.YearOfGraduation = model.GraduationDate;
                    student.WesVerificationNumber = model.WesVerificationNumber;

                }
                return student;

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
