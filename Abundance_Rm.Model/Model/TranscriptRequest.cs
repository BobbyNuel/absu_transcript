﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class TranscriptRequest
    {
        public long Id { get; set; }
        public long? StudentId { get; set; }
        public long? PaymentId { get; set; }
        public DateTime DateRequested { get; set; }
        public string DestinationAddress { get; set; }
        public int? TranscriptclearanceStatusId { get; set; }
        public int TranscriptStatusId { get; set; }
        public bool? Processed { get; set; }
        public DateTime? ProcessedDate { get; set; }
        public string StudentName { get; set; }
        public string MatricNumber { get; set; }
        public Programme Programme { get; set; }
        public Level Level { get; set; }
        public  Country Country { get; set; }
        public  Person Person { get; set; }
        public  State State { get; set; }
        public string Status { get; set; }

        public string EmailAddress { get; set; }
        public string DeliveryZone { get; set; }
        public string DeliveryService { get; set; }

    }
}
