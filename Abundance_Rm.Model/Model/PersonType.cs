﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class PersonType
    {
        public short Id { get; set; }
        public string Name { get; set; }
    }
}
