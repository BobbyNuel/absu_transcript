﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    
    public enum Roles
    {
        Guest = 1,
        Admin = 2,
        ApplicationManager = 3,
        MarksEntry = 4,
        ResultPrinting = 5,
        Supervisor = 6,
        Records = 7,
        Staff = 8

    }
}
