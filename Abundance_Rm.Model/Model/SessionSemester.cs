﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class SessionSemester
    {
        //private string name;

        public int Id { get; set; }
        public Session Session { get; set; }
        public Semester Semester { get; set; }
        public String Name { get; set; }
        public int SequenceNumber { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
