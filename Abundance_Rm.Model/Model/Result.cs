﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class Result
    {
        public long StudentId { get; set; }
        public string Sex { get; set; }
        public string Name { get; set; }
        public string MatricNumber { get; set; }
        public string LevelName { get; set; }
        public string SessionName { get; set; }
        public string SemesterName { get; set; }

        public string PassportUrl { get; set; }
        public string ProgrammeTypeName { get; set; }
        public string DepartmentName { get; set; }
        public long CourseId { get; set; }
        public string CourseCode { get; set; }
        public string CourseName { get; set; }
        public int CourseUnit { get; set; }
        public decimal? TotalScore { get; set; }
        public decimal? Score { get; set; }
        public string Grade { get; set; }              
        public string FacultyName { get; set; }
        public string FacultyCode { get; set; }
        public decimal? TestScore { get; set; }
        public decimal? ExamScore { get; set; }
        public decimal? GradePoint { get; set; }
        public decimal? GPCU { get; set; }
        public decimal? GPA { get; set; }
        public decimal? CGPA { get; set; }
        public decimal? WGP { get; set; }

        public int? UnitPassed { get; set; }
        public int? UnitOutstanding { get; set; }

        public int? TotalSemesterCourseUnit { get; set; }
        public decimal? TotalGradePoint { get; set; }

        public int SessionId { get; set; }
        public int SemesterId { get; set; }
        public int ProgrammeId { get; set; }
        public int DepartmentId { get; set; }
        public int LevelId { get; set; }

        public string Address { get; set; }
        public string MobilePhone { get; set; }
        public string Email { get; set; }
        public string Remark { get; set; }
        public int? PreviousSemesterTotalCourseUnit { get; set; }
        public decimal? PreviousSemesterTotalGPCU { get; set; }
        public int FirstYearReferences { get; set; }
        public decimal? FirstYearCGPA { get; set; }
        public string DegreeClassification { get; set; }
        public string GraduationDate { get; set; }
        public string AdmissionDate { get; set; }
        public int SessionSemesterId { get; set; }
        public int SequenceNumber { get; set; }
        public string Date { get; set; }
        public string GradePoints { get; set; }
        public string DateOfBirth { get; set; }
        public string cGPA { get; set; }
        public string AddressHeader { get; set; }
        public string AddressFooter { get; set; }
        public string QrCode { get; set; }
        public string WaterMark { get; set; }
        public string Country { get; set; }
        public string WesVerificationNumber { get; set; }
        public int NumberOfNonCompletedSession { get; set; }
    }
}
