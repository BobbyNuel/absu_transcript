﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class StudentCourseMark
    {
        public long Id { get; set; }
        public Nullable<decimal> TestScore { get; set; }
        public Nullable<decimal> ExamScore { get; set; }
        public Nullable<decimal> Total { get; set; }
        public Nullable<System.DateTime> DateRecorded { get; set; }
        public string Grade { get; set; }
        public double GradePoint { get; set; }
        public int CourseUnit { get; set; }
        public bool IsMarkOmitted { get; set; }
        public ProgrammeCourseRegistration ProgrammeCourseRegistration { get; set; }
    }
}
