﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class PersonAudit
    {
        public long Id { get; set; }
        public string OldFirstName { get; set; }
        public string OldLastName { get; set; }
        public string OldMiddleName { get; set; }
        public int OldCountryId { get; set; }
        public string OldSex { get; set; }
        public string NewFirstName { get; set; }
        public string NewLastName { get; set; }
        public string NewMiddleName { get; set; }
        public int NewCountryId { get; set; }
        public string NewSex { get; set; }
        public string Operation { get; set; }
        public string Action { get; set; }
        public System.DateTime Time { get; set; }
        public string Client { get; set; }
        public string OldYearOfEntry { get; set; }
        public string OldYearOfGraduation { get; set; }
        public string NewYearOfEntry { get; set; }
        public string NewYearOfGraduation { get; set; }
        public int OldDepartmentId { get; set; }
        public int OldProgrammeId { get; set; }
        public int NewDepartmentId { get; set; }
        public int NewProgrammeId { get; set; }

        public string OldCGPA { get; set; }
        public string NewCGPA { get; set; }
        public Person Person { get; set; }
        public User User { get; set; }
    }
}
