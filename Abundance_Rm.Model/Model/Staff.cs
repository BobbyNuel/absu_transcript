﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class Staff
    {
        public int  Id { get; set; }
        public bool? Activated { get; set; }

        public Department  Department { get; set; }
        public User User { get; set; }
    }
}
