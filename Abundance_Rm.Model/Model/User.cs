﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Abundance_Rm.Model.Model;

namespace Abundance_Rm.Model.Model
{
    public class User 
    {
        public long Id { get; set; }
        [Required]
        public string Username { get; set; }
        [DataType(DataType.Password)]
        [Required]      
        public string Password { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        public SecurityQuestion SecurityQuestion { get; set; }
        [Required]
        [Display (Name="Security Answer")]
        public string SecurityAnswer { get; set; }
        [Required]
        public Role Role { get; set; }
        public DateTime LastLoginDate { get; set; }
        [Required]
        public Person person { get; set; }
        [Required]
        public bool Activated { get; set; }
    }


}
