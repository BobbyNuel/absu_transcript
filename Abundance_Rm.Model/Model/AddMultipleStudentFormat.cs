﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class AddMultipleStudentFormat
    {
        public int SN { get; set; }
        public string RegNo { get; set; }
        public string Surname { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Sex { get; set; }
        public string DateOfBirth { get; set; }
        public string Nationality { get; set; }
        public string YearOfEntry { get; set; }
        public string YearOfGraduation { get; set; }
        //public string Department { get; set; }
        public string Level { get; set; }
        public string Session { get; set; }
        public int Semester { get; set; }
        public string CourseCode { get; set; }
        public string CourseTitle { get; set; }
        public int CourseUnit { get; set; }
        public string GradeScore { get; set; }

        public string Address { get; set; }
    }
}
