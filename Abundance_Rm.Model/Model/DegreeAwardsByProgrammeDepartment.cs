﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class DegreeAwardsByProgrammeDepartment
    {
        public long Id { get; set; }
        public Department Department { get; set; }
        public Programme Programme { get; set; }
        public string DegreeName { get; set; }
        public int? Duration { get; set; }
    }
}
