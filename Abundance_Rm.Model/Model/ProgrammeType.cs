﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class ProgrammeType
    {
        [Required]
        [Display(Name = "Programme")]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Programme")]
        public string Name { get; set; }
        public bool Activated { get; set; }
    }
}
