﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class UploadedResult
    {
        public int Id { get; set; }
        public long UploadedBy { get; set; }
        public Programme Programme { get; set; }
        public string FileUrl { get; set; }
        public System.DateTime DateUploaded { get; set; }
        public int WorkSheetIndex { get; set; }
        public Student Student { get; set; }
        public string Client { get; set; }
    }
}
