﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class CourseData
    {
        public string Level { get; set; }
        public string Session { get; set; }
        public string Semester { get; set; }
        public string CourseCode { get; set; }
        public string CourseTitle { get; set; }
        public string CourseUnit { get; set; }
        public string Programme { get; set; }
    }
}
