﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class Department
    {
        [Required]
        [Display(Name="Department")]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Department")]
        public string Name { get; set; }
        public Faculty faculty { get; set; }
        public bool Activated { get; set; }
        public string Description { get; set; }
        public bool IsRemoveRequested { get; set; }
        [Required]
        public string Department_Code { get; set; }
        public List<Department> DepartmentList { get; set; }
    }
}
