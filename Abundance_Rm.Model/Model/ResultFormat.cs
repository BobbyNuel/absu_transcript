﻿namespace Abundance_Rm.Model.Model
{
    public class ResultFormat
    {
        public int SN { get; set; }
        public string Name { get; set; }
        public string Matric_NO { get; set; }
        public string Test_Score { get; set; }
        public string Exam_Score { get; set; }
        public string Programme { get; set; }
        public string Department { get; set; }
        public string Level { get; set; }
        public string Session { get; set; }
        public string Semester { get; set; }

        public string CourseCode { get; set; }
    }
}
