﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class TranscriptRequestFormat
    {
        public string Id { get; set; }
        public string StudentId { get; set; }
        public string PaymentId { get; set; }
        public string DateRequested { get; set; }
        public string DestinationAddress { get; set; }
        public string TranscriptclearanceStatusId { get; set; }
        public string TranscriptStatusId { get; set; }
        public string Processed { get; set; }
        public string ProcessedDate { get; set; }
        public string StudentName { get; set; }
        public string MatricNumber { get; set; }
        public string Country { get; set; }
        public string Person { get; set; }
        public string State { get; set; }
        public string Status { get; set; }
        public string DepartmentId { get; set; }
        public string ProgrammeId { get; set; }
        public string LevelId { get; set; }
        public string FacultyId { get; set; }
        public string DepartmentName { get; set; }
        public string EmailAddress { get; set; }
        public string DeliveryZone { get; set; }
        public string DeliveryService { get; set; }

    }
}
