﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class GradeSystem
    {
        public int Id { get; set; }
        public string Grade_Range { get; set; }
        public string Grade { get; set; }
        public decimal GradePoint { get; set; }
        public Level Level { get; set; }
        public bool RemoveRequested { get; set; }
    }
}
