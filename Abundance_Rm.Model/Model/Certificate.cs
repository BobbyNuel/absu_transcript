﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class Certificate
    {
        public int Id { get; set; }
        public string CertificateCode { get; set; }
        public string Name { get; set; }
        public Nullable<bool> Activated { get; set; }
        public bool IsRemoveRequested { get; set; }
        public List<Certificate> CertificateList { get; set; }
    }
}
