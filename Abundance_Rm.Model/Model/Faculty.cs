﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class Faculty
    {
         [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string FacultyCode { get; set; }
        public string Description { get; set; }
        public bool IsRemoveRequested { get; set; }
        public List<Faculty> FacultyList { get; set; }
    }
}
