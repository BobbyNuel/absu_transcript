﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class StudentCourseMarkAudit
    {
        public long Id { get; set; }


        public string Operation { get; set; }

        public string Action { get; set; }

        public System.DateTime Time { get; set; }

        public string Client { get; set; }
        public User User { get; set; }
        public ProgrammeCourse ProgrammeCourse { get; set; }

    }
}
