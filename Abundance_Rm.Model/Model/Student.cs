﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class Student
    {
        public long Id { get; set; }
        public string MatricNo { get; set; }
        public Programme Programme { get; set; }
        public Level CurrentLevel { get; set; }
        public Session CurrentSession { get; set; }
        public string EntryRegNo { get; set; }
        public StudentCategory StudentCategory { get; set; }
        public string NDGPA { get; set; }
        public string CGPA { get; set; }
        public Department Department { get; set; }
        public Sponsor Sponsor { get; set; }
        public Nullable<int> StudentParentId { get; set; }
        public string YearOfEntry { get; set; }
        public string GraduationDate { get; set; }
        public string BasisOfAdmission { get; set; }
        public string LastSchoolAttended { get; set; }
        public string AwardAndDate { get; set; }
        public Person Person { get; set; }
        public string WesVerificationNumber { get; set; }

    }
}
