﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class Course
    {
        private string codeName;
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Course_Code { get; set; }
        public string CodeName
        {
            get { return Course_Code + " - " + Name; }
            set { codeName = value; }
        }
        public bool IsRemoveRequested { get; set; }
        public List<Course> CourseList {get; set;}
        public string Grade { get; set; }
    }
}
