﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class ProgrammeCourse
    {
        public long Id { get; set; }
        public Programme Programme { get; set; }
        public Course Course { get; set; }
        public CourseType CourseType { get; set; }
        public int CourseUnit { get; set; }
        public Semester Semester { get; set; }
        public Level Level { get; set; }
        public bool Activated { get; set; }
        public Nullable<decimal> Pass_mark { get; set; }
        public bool IsRemoveRequested { get; set; }
        public string Grade { get; set; }

        public bool IsRegistered { get; set; }

        public bool IsCarryOverCourse { get; set; }
        public bool IsApproved { get; set; }

        public int Unit { get; set; }
        public Session Session { get; set; }
        public decimal ExamScore { get; set; }
    }
}
