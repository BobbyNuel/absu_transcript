﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class ScoreGrade
    {
        public int Id { get; set; }
        public int ScoreFrom { get; set; }
        public int ScoreTo { get; set; }
        public string Grade { get; set; }
        public decimal GradePoint { get; set; }
        public string Performance { get; set; }
        public string Description { get; set; }
        public int? SessionStart { get; set; }
        public int? SessionEnd { get; set; }
    }
}
