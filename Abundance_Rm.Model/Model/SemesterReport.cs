﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class SemesterReport
    {
        public string CourseCode { get; set; }
        public string CourseTitle { get; set; }
        public int CourseUnit { get; set; }
        public string GradeScore { get; set; }
        public double GradePoint { get; set; }
        public string Grade { get; set; }
        public double GPA { get; set; }
        public double CGPA { get; set; }
        public string Session { get; set; }
        public string Semester { get; set; }
        public string Level { get; set; }
        public int SessionSemesterId { get; set; }
        public int Total { get; set; }
        public double GradePoint_CourseUnit { get; set; }
        public string DeptCourse { get; set; }
    }
}
