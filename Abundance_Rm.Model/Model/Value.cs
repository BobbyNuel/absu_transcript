﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Abundance_Rm.Model.Model
{
    public class Value
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
