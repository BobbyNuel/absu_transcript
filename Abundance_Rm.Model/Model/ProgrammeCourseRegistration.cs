﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class ProgrammeCourseRegistration
    {
        public long Id { get; set; }
        public Student Student { get; set; }
        public ProgrammeCourse ProgrammeCourse { get; set; }
        public SessionSemester SessionSemester { get; set; }
       
        public Level Level { get; set; }
        public Nullable<System.DateTime> DateReg { get; set; }
        public Nullable<bool> Approved { get; set; }
        public Nullable<System.DateTime> DateApproved { get; set; }
        public Nullable<decimal> TotalMark { get; set; }
        public bool IsCarriedOverCourses { get; set; }
        public int CourseUnit { get; set; }
        public string DeptCourse { get; set; }
        public string Grade { get; set; }
        public double GradePoint { get; set; }


        public StudentCourseMark Detail { get; set; }
    }
}
