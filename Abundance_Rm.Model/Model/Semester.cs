﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class Semester
    {
        public int Id { get; set; }
        public string SemesterCode { get; set; }
        public string Name { get; set; }
        public Nullable<bool> Activated { get; set; }
        public bool IsCurrent { get; set; }
    }
}
