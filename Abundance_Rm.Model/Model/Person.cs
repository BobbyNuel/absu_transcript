﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class Person
    {
        private string fullName;
        public long Id { get; set; }
        public Nullable<int> Session { get; set; }
        public string Surname { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string DateofBirth { get; set; }
        [Required]
        public string PlaceofBirth { get; set; }
        public string Sex { get; set; }
        public Role Role { get; set; }
        public Religion Religion { get; set; }
        public MaritalStatus MaritalStatus { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public Country Country { get; set; }
        public State State { get; set; }
        public Lga Lga { get; set; }
        public string HomeTown { get; set; }
        public Nullable<System.DateTime> DateFilled { get; set; }
        public string PictureURL { get; set; }
        public string contact_address { get; set; }
        public string ModeOfEntry { get; set; }
        public PersonType PersonType { get; set; }
        public Nullable<bool> RemoveRequested { get; set; }
        public Value DayOfBirth { get; set; }
        public Value MonthOfBirth { get; set; }
        public Value YearOfBirth { get; set; }
        [Display(Name = "Name")]
        public string FullName
        {
            get { return Surname + " " + FirstName + " " + MiddleName; }
            set { fullName = value; }
        }
    }
}
