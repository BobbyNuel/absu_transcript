﻿using Abundance_Rm.Model.Model;

namespace Abundance_Rm.Model.Model
{
    public class StudentLevel
    {
        public long Id { get; set; }
        public Student Student { get; set; }
        public Level Level { get; set; }
        public ProgrammeType ProgrammeType { get; set; }
        public Department Department { get; set; }
        public Session Session { get; set; }

      
    }
}