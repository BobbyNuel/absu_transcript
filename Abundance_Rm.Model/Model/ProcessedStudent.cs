﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class ProcessedStudent
    {
        public string Student { get; set; }
        public string RegNo { get; set; }
        public string ProgrammeName { get; set; }
        public string DepartmentName { get; set; }
        public string DateUplaoded { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
       
    }
}
