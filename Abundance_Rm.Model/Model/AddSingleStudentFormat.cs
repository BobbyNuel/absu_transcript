﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class AddSingleStudentFormat
    {
        public int SN { get; set; }
        public string Level { get; set; }
        public string Session { get; set; }
        public int Semester { get; set; }
        public string CourseCode { get; set; }
        public string CourseTitle { get; set; }
        public int CourseUnit { get; set; }
        public string GradeScore { get; set; }
    }

    public class MedicalStudentFormat
    {
        public int SN { get; set; }
        public int MBBS { get; set; }
        public string CourseTitle { get; set; }
        public int Grade { get; set; }  
    }
}
