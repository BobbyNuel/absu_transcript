﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class State
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public Country Country { get; set; }
        public Nullable<bool> Activated { get; set; }
    }
}
