﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Abundance_Rm.Model.Model
{
    public class Programme
    {
        public int Id { get; set; }
        public ProgrammeType ProgrammeType { get; set; }
        public Department department { get; set; }
        public Certificate certificate { get; set; }
        public string ProgrammeName { get; set; }
        public string StartLevel { get; set; }
        public string EndLevel { get; set; }
        public Nullable<int> Duration { get; set; }
        public Nullable<int> UnitsRequired { get; set; }
        public Nullable<bool> Activated { get; set; }
        public Nullable<short> CategoryId { get; set; }
        public bool? IsRemoveRequested { get; set; }
    }
}
