﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class StudentClassListReport
    {
        public string Name { get; set; }
        public string RegistrationNumber { get; set; }
        public decimal? CGPA { get; set; }
        public double GradePoint_CourseUnit { get; set; }
        public decimal TotalUnitLoad { get; set; }
        public decimal TotalGradePoint { get; set; }
        public string Remark { get; set; }
        public string GraduationYear { get; set; }
        public string DepartmentName { get; set; }

        public string ProgrammeName { get; set; }
    }
}
