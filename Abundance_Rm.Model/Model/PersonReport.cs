﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Model.Model
{
    public class PersonReport
    {
        public string RegNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Sex { get; set; }
        public string SponsorName { get; set; }
        public string SponsorAddress { get; set; }
        public string SponsorOccupation { get; set; }
        public string DateOfBirth { get; set; }
        public string Nationality { get; set; }
        public string State { get; set; }
        public string LocalGovernmentArea { get; set; }
        public string Address { get; set; }
        public string PlaceOfBirth { get; set; }
        public string Religion { get; set; }
        public string MaritalStatus { get; set; }
        public string BasisOfAdmission { get; set; }
        public string YearOfEntry { get; set; }
        public string LastSchoolAttended { get; set; }
        public string Level { get; set; }
        public string Session { get; set; }
        public string Semester { get; set; }
        public string CourseCode { get; set; }
        public string CourseTitle { get; set; }
        public string CreditUnit { get; set; }
        public string Grade { get; set; }
        public string Department { get; set; }
        public string Faculty { get; set; }
        public string GraduatingTitle { get; set; }
        public string ProgrammeCode { get; set; }
    }
}
