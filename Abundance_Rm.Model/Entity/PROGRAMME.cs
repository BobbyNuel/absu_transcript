
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace Abundance_Rm.Model.Entity
{

using System;
    using System.Collections.Generic;
    
public partial class PROGRAMME
{

    public PROGRAMME()
    {

        this.PROGRAMME_COURSES = new HashSet<PROGRAMME_COURSES>();

        this.PROGRAMME_DEPARTMENT = new HashSet<PROGRAMME_DEPARTMENT>();

        this.STUDENT = new HashSet<STUDENT>();

        this.DEGREE_AWARDS_BY_PROGRAMME_DEPARTMENT = new HashSet<DEGREE_AWARDS_BY_PROGRAMME_DEPARTMENT>();

        this.TRANSCRIPT_REQUEST = new HashSet<TRANSCRIPT_REQUEST>();

        this.UPLOADED_RESULT = new HashSet<UPLOADED_RESULT>();

    }


    public int ProgrammeId { get; set; }

    public int ProgrammeTypeId { get; set; }

    public int DepartmentId { get; set; }

    public Nullable<int> CertificateId { get; set; }

    public string ProgrammeName { get; set; }

    public string StartLevel { get; set; }

    public string EndLevel { get; set; }

    public Nullable<int> Duration { get; set; }

    public Nullable<int> UnitsRequired { get; set; }

    public Nullable<bool> Activated { get; set; }

    public Nullable<short> CategoryId { get; set; }

    public Nullable<bool> IsRemoveRequested { get; set; }



    public virtual CERTIFICATE CERTIFICATE { get; set; }

    public virtual DEPARTMENT DEPARTMENT { get; set; }

    public virtual ICollection<PROGRAMME_COURSES> PROGRAMME_COURSES { get; set; }

    public virtual ICollection<PROGRAMME_DEPARTMENT> PROGRAMME_DEPARTMENT { get; set; }

    public virtual PROGRAMME_TYPE PROGRAMME_TYPE { get; set; }

    public virtual STUDENT_CATEGORY STUDENT_CATEGORY { get; set; }

    public virtual ICollection<STUDENT> STUDENT { get; set; }

    public virtual ICollection<DEGREE_AWARDS_BY_PROGRAMME_DEPARTMENT> DEGREE_AWARDS_BY_PROGRAMME_DEPARTMENT { get; set; }

    public virtual ICollection<TRANSCRIPT_REQUEST> TRANSCRIPT_REQUEST { get; set; }

    public virtual ICollection<UPLOADED_RESULT> UPLOADED_RESULT { get; set; }

}

}
