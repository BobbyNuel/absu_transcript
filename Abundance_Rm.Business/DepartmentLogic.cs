﻿using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Business
{
    public class DepartmentLogic:BaseBusinessLogic<Department,DEPARTMENT>
    {
        public DepartmentLogic()
        {
            translator = new DepartmentTranslator();
        }

        public List<DEPARTMENT> GetBy(PROGRAMME_TYPE programmeType)
        {
            ProgrammeLogic programmeLogic = new ProgrammeLogic();
            List<PROGRAMME> programmeList = programmeLogic.GetEntitiesBy(p => p.ProgrammeTypeId == programmeType.ProgrammeTypeId);
            List<DEPARTMENT> departmentList = new List<DEPARTMENT>();
            foreach (PROGRAMME item in programmeList)
            {
                departmentList.Add(item.DEPARTMENT);
            }
            return departmentList.OrderBy(x => x.Name).ToList();
        }
        public List<Department> GetBy(ProgrammeType programmeType)
        {
            ProgrammeLogic programmeLogic = new ProgrammeLogic();
            List<Programme> programmeList = programmeLogic.GetModelsBy(p => p.ProgrammeTypeId == programmeType.Id);
            List<Department> departmentList = new List<Department>();
            foreach (Programme item in programmeList)
            {
                departmentList.Add(item.department);
            }
            return departmentList.OrderBy(x => x.Name).ToList();
        }

        public bool Modify(Department department)
        {

            Expression<Func<DEPARTMENT, bool>> selector = a => a.DepartmentId == department.Id;
            DEPARTMENT entity = GetEntityBy(selector);

            entity.DepartmentId = department.Id;
            entity.Name = department.Name;
            entity.Department_Code = department.Department_Code;
            entity.FacultyId = department.faculty.Id;
            entity.Description = "NaN";
            entity.Activated = true;

            int modifiedRecordCount = Save();
            if (modifiedRecordCount <= 0)
            {
                return false;
            }

            return true;
        }
        
    }
}
