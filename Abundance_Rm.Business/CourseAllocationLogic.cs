﻿using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Business
{
    public class CourseAllocationLogic : BaseBusinessLogic<CourseAllocation, COURSE_ALLOCATION>
    {
        public CourseAllocationLogic()
        {
            translator = new CourseAllocationTranslator();
        }
    }
}
