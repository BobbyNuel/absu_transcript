﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Translator;

namespace Abundance_Rm.Business
{
    public class StudentCourseMarkLogic : BaseBusinessLogic<StudentCourseMark, STUDENT_COURSE_MARK>
    {
        public StudentCourseMarkLogic()
        {
            translator = new StudentCourseMarkTranslator();
        }
        public List<SemesterReport> GetSemesterCourseMarkInformation(List<PROGRAMME_COURSE_REGISTRATION> courseRegistrationList)
        {
            StudentCourseMarkLogic studentCourseMarkLogic = new StudentCourseMarkLogic();
            STUDENT_COURSE_MARK studentCourseMark = new STUDENT_COURSE_MARK();
            List<SemesterReport> firstSemesterReportList = new List<SemesterReport>();
            foreach (PROGRAMME_COURSE_REGISTRATION courseRegItem in courseRegistrationList)
            {
                studentCourseMark = studentCourseMarkLogic.GetEntityBy(p => p.CourseRegId == courseRegItem.CourseRegId);
                SemesterReport firstSemesterReport = new SemesterReport();
                firstSemesterReport.CourseCode = courseRegItem.PROGRAMME_COURSES.COURSE.Course_Code;
                firstSemesterReport.CourseTitle = courseRegItem.PROGRAMME_COURSES.COURSE.Title;
                firstSemesterReport.CourseUnit = courseRegItem.PROGRAMME_COURSES.CourseUnit;
                firstSemesterReport.Grade = studentCourseMark.Grade;
                firstSemesterReport.Level = courseRegItem.LEVEL.Name;
                // firstSemesterReport.Semester = courseRegItem.se.SemesterName;
                firstSemesterReport.SessionSemesterId = courseRegItem.SessionSemesterId;
                //firstSemesterReport.Session = courseRegItem.SESSION.Name;
                firstSemesterReport.GradePoint = studentCourseMark.GradePoint;
                firstSemesterReport.DeptCourse = courseRegItem.DeptCourse;
                firstSemesterReport.GradePoint_CourseUnit = (firstSemesterReport.CourseUnit * firstSemesterReport.GradePoint);
                firstSemesterReportList.Add(firstSemesterReport);
            }
            double GPA = 0.0;
            double total_GradePoint_CourseUnit = 0.0;
            double total_CourseUnit = 0.0;
            total_GradePoint_CourseUnit = firstSemesterReportList.Sum(p => p.GradePoint_CourseUnit);
            total_CourseUnit = firstSemesterReportList.Sum(p => p.CourseUnit);
            GPA = (total_GradePoint_CourseUnit / total_CourseUnit);
            return firstSemesterReportList;
        }

        public List<StudentClassListReport> GetStudentClassListWithCGPA(List<STUDENT> studentList)
        {
            try
            {
                List<StudentClassListReport> studentClassListReportList = new List<StudentClassListReport>();
                ProgrammeCourseRegistrationLogic programmeCourseRegistrationLogic = new ProgrammeCourseRegistrationLogic();
                List<PROGRAMME_COURSE_REGISTRATION> programmeCourseRegistrationList = new List<PROGRAMME_COURSE_REGISTRATION>();
                StudentCourseMarkLogic studentCourseMarkLogic = new StudentCourseMarkLogic();


                foreach (STUDENT studentItem in studentList)
                {
                    StudentClassListReport studentClassListReport = new StudentClassListReport();
                    programmeCourseRegistrationList = programmeCourseRegistrationLogic.GetEntitiesBy(p => p.StudentId == studentItem.StudentId);


                    studentClassListReport.Name = (studentItem.PERSON.Surname + " " + studentItem.PERSON.FirstName + " " + studentItem.PERSON.MiddleName).ToUpper();
                    studentClassListReport.RegistrationNumber = studentItem.EntryRegNo;
                    List<int> totalUnits = new List<int>();
                    List<decimal> totalGradePoint = new List<decimal>();

                    foreach (PROGRAMME_COURSE_REGISTRATION ProgrammeCourseRegistrationItem in programmeCourseRegistrationList)
                    {
                        STUDENT_COURSE_MARK studentCourseMark = new STUDENT_COURSE_MARK();
                        studentCourseMark = studentCourseMarkLogic.GetEntityBy(p => p.CourseRegId == ProgrammeCourseRegistrationItem.CourseRegId);

                        totalUnits.Add(studentCourseMark.CourseUnit);
                        totalGradePoint.Add((decimal)(studentCourseMark.GradePoint * studentCourseMark.CourseUnit));

                    }
                    studentClassListReport.TotalGradePoint = totalGradePoint.Sum();
                    studentClassListReport.TotalUnitLoad = totalUnits.Sum();
                    studentClassListReport.CGPA = Math.Round((studentClassListReport.TotalGradePoint / studentClassListReport.TotalUnitLoad), 2);
                    string graduationYear = studentItem.YearOfGraduation.Split('/').LastOrDefault();
                    studentClassListReport.Remark = GetGraduationStatus(studentClassListReport.CGPA, graduationYear);
                    studentClassListReport.DepartmentName = studentItem.DEPARTMENT.Name;
                    studentClassListReport.ProgrammeName = studentItem.PROGRAMME.PROGRAMME_TYPE.ProgrammeType;
                    studentClassListReport.GraduationYear = studentItem.YearOfGraduation;
                    studentClassListReportList.Add(studentClassListReport);
                }





                return studentClassListReportList;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public StudentClassListReport GetStudentWithCGPA(STUDENT studentItem)
        {
            try
            {

                ProgrammeCourseRegistrationLogic programmeCourseRegistrationLogic = new ProgrammeCourseRegistrationLogic();
                List<PROGRAMME_COURSE_REGISTRATION> programmeCourseRegistrationList = new List<PROGRAMME_COURSE_REGISTRATION>();
                StudentCourseMarkLogic studentCourseMarkLogic = new StudentCourseMarkLogic();




                programmeCourseRegistrationList = programmeCourseRegistrationLogic.GetEntitiesBy(p => p.StudentId == studentItem.StudentId);
                List<STUDENT_COURSE_MARK> studentCourseMarkList = new List<STUDENT_COURSE_MARK>();
                StudentClassListReport studentClassListReport = new StudentClassListReport();
                studentClassListReport.Name = studentItem.PERSON.Surname + " " + studentItem.PERSON.FirstName + " " + studentItem.PERSON.MiddleName;
                studentClassListReport.RegistrationNumber = studentItem.EntryRegNo;

                foreach (PROGRAMME_COURSE_REGISTRATION ProgrammeCourseRegistrationItem in programmeCourseRegistrationList)
                {
                    STUDENT_COURSE_MARK studentCourseMark = new STUDENT_COURSE_MARK();
                    studentCourseMark = studentCourseMarkLogic.GetEntityBy(p => p.CourseRegId == ProgrammeCourseRegistrationItem.CourseRegId);
                    studentCourseMarkList.Add(studentCourseMark);
                }
                double totalUnitLoad = studentCourseMarkList.Sum(p => p.CourseUnit);
                double totalGradePointCourseUnit = studentCourseMarkList.Sum(p => (p.CourseUnit * p.GradePoint));
                decimal CGPA = Math.Round((decimal)(totalGradePointCourseUnit / totalUnitLoad), 2);
                studentClassListReport.CGPA = CGPA;


                return studentClassListReport;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public List<Result> GetResultSheet(Department department, ProgrammeType programmeType, Level level, Session session, Semester semester, Course course)
        {
            try
            {
                List<Result> results = (from a in repository.GetBy<VW_STUDENT_RESULT>(a => a.SessionId == session.Id && a.ProgrammeTypeId == programmeType.Id && a.DepartmentId == department.Id && a.CourseId == course.Id && a.LevelId == level.Id && a.SemesterId == semester.Id).OrderBy(p => p.EntryRegNo)
                                        select new Result
                                        {
                                            Name = a.FullName.ToUpper(),
                                            SessionName = a.SessionName,
                                            SemesterName = a.SemesterName,
                                            DepartmentName = a.DepartmentName,
                                            ProgrammeTypeName = a.ProgrammeType,
                                            MatricNumber = a.EntryRegNo,
                                            Grade = a.Grade,
                                            LevelName = a.LevelName,
                                            CourseCode = a.Course_Code,
                                            CourseName = a.CourseName,
                                            CourseUnit = a.CourseUnit,
                                            TestScore = a.TestScore,
                                            ExamScore = a.ExamScore,
                                            TotalScore = a.Total,
                                        }).ToList();

                List<Result> masterList = new List<Result>();
                List<string> distinctMatricNumbers = results.Select(r => r.MatricNumber).AsParallel().Distinct().ToList();
                foreach (string matricNumber in distinctMatricNumbers)
                {
                    Result thisResult = results.Where(p => p.MatricNumber == matricNumber).LastOrDefault();
                    masterList.Add(thisResult);
                }
                return masterList.OrderBy(p => p.MatricNumber).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Result> GetSingleStudentResult(Student student, Session session, Semester semester, Level level, ProgrammeType programme, Department department)
        {
            try
            {

                SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                SessionSemester sessionSemester = sessionSemesterLogic.GetModelBy(ss => ss.Semester_Id == semester.Id && ss.Session_Id == session.Id);

                List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT>(x => x.StudentId == student.Id && x.SessionId == session.Id && x.SemesterId == semester.Id && x.LevelId == level.Id && x.ProgrammeTypeId == programme.Id && x.DepartmentId == department.Id)
                                        select new Result
                                        {
                                            StudentId = sr.StudentId,
                                            Sex = sr.Sex,
                                            Name = sr.FullName,
                                            MatricNumber = sr.EntryRegNo,
                                            SessionName = sr.SessionName,
                                            FacultyName = sr.FacultyName,
                                            ProgrammeTypeName = sr.ProgrammeType,
                                            DepartmentName = sr.DepartmentName,
                                            LevelName = sr.LevelName,
                                            CourseId = sr.CourseId,
                                            CourseCode = sr.Course_Code,
                                            CourseName = sr.CourseName,
                                            CourseUnit = sr.CourseUnit,
                                            PassportUrl = sr.PictureURL,
                                            MobilePhone = sr.PhoneNumber,
                                            TestScore = sr.TestScore,
                                            ExamScore = sr.ExamScore,
                                            TotalScore = sr.Total,
                                            Grade = sr.Grade,
                                            GradePoint = Convert.ToDecimal(sr.GradePoint),

                                            GPCU = Convert.ToDecimal(sr.WGP),
                                            TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                        }).OrderBy(p => p.CourseCode).ToList();

                if (results.Count == 0)
                {
                    return new List<Result>();
                }
                List<string> carryOverCourses = new List<string>();
                List<Result> firstYeaResult = new List<Result>();
                if (level.Id == 2 || level.Id == 4)
                {
                    firstYeaResult = GetFirstYearResult(student, sessionSemester, level, programme, department, carryOverCourses);
                }
                List<Result> previousSemesteResult = GetPreviousSemesterResult(student, sessionSemester, level, programme, department);
                foreach (Result result in results)
                {
                    result.GPA = results.Sum(p => p.GPCU) / result.TotalSemesterCourseUnit;

                    if ((level.Id == 1 || level.Id == 3) && semester.Id == 2)
                    {
                        result.PreviousSemesterTotalGPCU = previousSemesteResult.Sum(psr => psr.GPCU);
                        result.PreviousSemesterTotalCourseUnit = previousSemesteResult.LastOrDefault().TotalSemesterCourseUnit;

                        decimal? thisSemesterCGPUSum = results.Sum(r => r.GPCU);
                        result.CGPA = (thisSemesterCGPUSum + result.PreviousSemesterTotalGPCU) / (result.TotalSemesterCourseUnit + result.PreviousSemesterTotalCourseUnit);

                        carryOverCourses = GetFirstYearCarryOverCourses(sessionSemester, level, programme, department, student);
                    }
                    if ((level.Id == 1 || level.Id == 3) && semester.Id == 1)
                    {
                        carryOverCourses = GetFirstYearCarryOverCourses(sessionSemester, level, programme, department, student);
                    }
                    if ((level.Id == 2 || level.Id == 4) && semester.Id == 2)
                    {
                        result.PreviousSemesterTotalGPCU = previousSemesteResult.Sum(psr => psr.GPCU);
                        result.PreviousSemesterTotalCourseUnit = previousSemesteResult.LastOrDefault().TotalSemesterCourseUnit;
                        decimal? thisSemesterCGPUSum = results.Sum(r => r.GPCU);

                        decimal? secondYearCGPA = (thisSemesterCGPUSum + result.PreviousSemesterTotalGPCU) / (result.TotalSemesterCourseUnit + result.PreviousSemesterTotalCourseUnit);
                        decimal? firstYearFirstSemesterGPCUSum = firstYeaResult.Sum(r => r.GPCU);
                        decimal? firstYearCGPA = (firstYearFirstSemesterGPCUSum + firstYeaResult.LastOrDefault().PreviousSemesterTotalGPCU) / (firstYeaResult.LastOrDefault().TotalSemesterCourseUnit + firstYeaResult.LastOrDefault().PreviousSemesterTotalCourseUnit);

                        result.CGPA = Convert.ToDecimal((40M / 100M) * firstYearCGPA) + Convert.ToDecimal((60M / 100M) * secondYearCGPA);
                        carryOverCourses = GetSecondYearCarryOverCourses(sessionSemester, level, programme, department, student);
                    }
                    if ((level.Id == 2 || level.Id == 4) && semester.Id == 1)
                    {
                        carryOverCourses = GetSecondYearFirstSemesterCarryOverCourses(sessionSemester, level, programme, department, student);
                    }

                    if (carryOverCourses.Count != 0)
                    {
                        result.Remark = "References";
                        foreach (string courseCode in carryOverCourses)
                        {
                            result.Remark += ("|" + courseCode);
                        }
                    }
                }

                return results.OrderBy(p => p.CourseCode).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private List<string> GetSecondYearFirstSemesterCarryOverCourses(SessionSemester sessionSemester, Level level, ProgrammeType programme, Department department, Student student)
        {
            throw new NotImplementedException();
        }

        private List<string> GetSecondYearCarryOverCourses(SessionSemester sessionSemester, Level level, ProgrammeType programme, Department department, Student student)
        {
            throw new NotImplementedException();
        }

        private List<string> GetFirstYearCarryOverCourses(SessionSemester sessionSemester, Level level, ProgrammeType programme, Department department, Student student)
        {
            throw new NotImplementedException();
        }
        public List<Result> GetExamPassList(SessionSemester sessionSemester, Level level, ProgrammeType programme, Department department)
        {
            try
            {
                if (sessionSemester == null || sessionSemester.Id <= 0 || level == null || level.Id <= 0 || programme == null || programme.Id <= 0 || department == null || department.Id <= 0)
                {
                    throw new Exception("One or more criteria to get Exam Pass List not set! Please check your input criteria selection and try again.");
                }

                StudentLogic studentLogic = new StudentLogic();
                SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                SessionSemester ss = sessionSemesterLogic.GetBy(sessionSemester.Id);

                List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT>(x => x.SessionId == ss.Session.Id && x.SemesterId == ss.Semester.Id && x.LevelId == level.Id && x.ProgrammeTypeId == programme.Id && x.DepartmentId == department.Id)
                                        select new Result
                                        {
                                            StudentId = sr.StudentId,
                                            Sex = sr.Sex,
                                            Name = sr.FullName,
                                            MatricNumber = sr.EntryRegNo,
                                            SessionName = sr.SessionName,
                                            FacultyName = sr.FacultyName,
                                            ProgrammeTypeName = sr.ProgrammeType,
                                            LevelName = sr.LevelName,
                                            CourseId = sr.CourseId,
                                            CourseCode = sr.Course_Code,
                                            CourseName = sr.CourseName,
                                            CourseUnit = sr.CourseUnit,

                                            TestScore = sr.TestScore,
                                            ExamScore = sr.ExamScore,
                                            TotalScore = sr.Total,
                                            Grade = sr.Grade,
                                            GradePoint = Convert.ToDecimal(sr.GradePoint),

                                            GPCU = Convert.ToDecimal(sr.WGP),
                                            TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                        }).ToList();

                List<Result> masterList = new List<Result>();
                List<string> carryOverCourses = new List<string>();

                if (ss.Semester.Id == 2)
                {
                    if (level.Id == 1 || level.Id == 3)
                    {
                        List<string> distinctMatricNumbers = results.Select(r => r.MatricNumber).AsParallel().Distinct().ToList();
                        foreach (string matricNumber in distinctMatricNumbers)
                        {
                            Student student = studentLogic.GetModelBy(s => s.EntryRegNo == matricNumber);
                            carryOverCourses = GetFirstYearCarryOverCourses(ss, level, programme, department, student);
                            if (carryOverCourses.Count == 0)
                            {
                                List<Result> previousSemesteResult = GetPreviousSemesterResult(student, ss, level, programme, department);

                                foreach (Result result in results.Where(r => r.MatricNumber == matricNumber))
                                {
                                    result.PreviousSemesterTotalGPCU = previousSemesteResult.Sum(psr => psr.GPCU);
                                    result.PreviousSemesterTotalCourseUnit = previousSemesteResult.LastOrDefault().TotalSemesterCourseUnit;

                                    decimal? thisSemesterCGPUSum = results.Where(r => r.MatricNumber == matricNumber).Sum(r => r.GPCU);
                                    result.GPA = thisSemesterCGPUSum / result.TotalSemesterCourseUnit;
                                    result.CGPA = (thisSemesterCGPUSum + result.PreviousSemesterTotalGPCU) / (result.TotalSemesterCourseUnit + result.PreviousSemesterTotalCourseUnit);
                                    result.Remark = GetResultRemark(result.CGPA);

                                    masterList.Add(result);
                                }
                            }
                        }
                    }
                    if (level.Id == 2 || level.Id == 4)
                    {
                        List<string> distinctMatricNumbers = results.Select(r => r.MatricNumber).AsParallel().Distinct().ToList();
                        foreach (string matricNumber in distinctMatricNumbers)
                        {
                            Student student = studentLogic.GetModelBy(s => s.EntryRegNo == matricNumber);
                            carryOverCourses = GetSecondYearCarryOverCourses(ss, level, programme, department, student);
                            if (carryOverCourses.Count == 0)
                            {
                                List<Result> firstYeaResult = GetFirstYearResult(student, ss, level, programme, department, carryOverCourses);
                                List<Result> previousSemesteResult = GetPreviousSemesterResult(student, ss, level, programme, department);
                                foreach (Result result in results.Where(r => r.MatricNumber == matricNumber))
                                {
                                    result.PreviousSemesterTotalGPCU = previousSemesteResult.Sum(psr => psr.GPCU);
                                    result.PreviousSemesterTotalCourseUnit = previousSemesteResult.LastOrDefault().TotalSemesterCourseUnit;
                                    decimal? thisSemesterCGPUSum = results.Where(r => r.MatricNumber == matricNumber).Sum(r => r.GPCU);
                                    result.GPA = thisSemesterCGPUSum / result.TotalSemesterCourseUnit;

                                    decimal? secondYearCGPA = (thisSemesterCGPUSum + result.PreviousSemesterTotalGPCU) / (result.TotalSemesterCourseUnit + result.PreviousSemesterTotalCourseUnit);
                                    decimal? firstYearFirstSemesterGPCUSum = firstYeaResult.Sum(r => r.GPCU);
                                    decimal? firstYearCGPA = (firstYearFirstSemesterGPCUSum + firstYeaResult.LastOrDefault().PreviousSemesterTotalGPCU) / (firstYeaResult.LastOrDefault().TotalSemesterCourseUnit + firstYeaResult.LastOrDefault().PreviousSemesterTotalCourseUnit);

                                    result.CGPA = Convert.ToDecimal((40M / 100M) * firstYearCGPA) + Convert.ToDecimal((60M / 100M) * secondYearCGPA);
                                    result.Remark = GetResultRemark(result.CGPA);

                                    masterList.Add(result);
                                }

                            }
                        }
                    }
                }
                else if (ss.Semester.Id == 1)
                {
                    if (level.Id == 1 || level.Id == 3)
                    {
                        List<string> distinctMatricNumbers = results.Select(r => r.MatricNumber).AsParallel().Distinct().ToList();
                        foreach (string matricNumber in distinctMatricNumbers)
                        {
                            Student student = studentLogic.GetModelBy(s => s.EntryRegNo == matricNumber);
                            carryOverCourses = GetFirstYearCarryOverCourses(ss, level, programme, department, student);
                            if (carryOverCourses.Count == 0)
                            {
                                foreach (Result result in results.Where(r => r.MatricNumber == matricNumber))
                                {
                                    decimal? CGPUSum = results.Where(r => r.MatricNumber == matricNumber).Sum(r => r.GPCU);
                                    result.GPA = CGPUSum / result.TotalSemesterCourseUnit;
                                    masterList.Add(result);
                                }
                            }
                        }

                    }

                    if (level.Id == 2 || level.Id == 4)
                    {
                        List<string> distinctMatricNumbers = results.Select(r => r.MatricNumber).AsParallel().Distinct().ToList();
                        foreach (string matricNumber in distinctMatricNumbers)
                        {
                            Student student = studentLogic.GetModelBy(s => s.EntryRegNo == matricNumber);
                            carryOverCourses = GetSecondYearFirstSemesterCarryOverCourses(ss, level, programme, department, student);
                            if (carryOverCourses.Count == 0)
                            {
                                foreach (Result result in results.Where(r => r.MatricNumber == matricNumber))
                                {
                                    decimal? CGPUSum = results.Where(r => r.MatricNumber == matricNumber).Sum(r => r.GPCU);
                                    result.GPA = CGPUSum / result.TotalSemesterCourseUnit;
                                    masterList.Add(result);
                                }
                            }
                        }
                    }
                }

                return masterList.OrderBy(p => p.GPA).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private string GetResultRemark(decimal? CGPA)
        {
            try
            {
                string remark = "";
                if (CGPA >= 3.5M && CGPA <= 4.0M)
                {
                    remark = "DISTINCTION";
                }
                else if (CGPA >= 3.0M && CGPA <= 3.49M)
                {
                    remark = "UPPER CREDIT";
                }
                else if (CGPA >= 2.50M && CGPA <= 2.99M)
                {
                    remark = "LOWER CREDIT";
                }
                else if (CGPA >= 2.0M && CGPA <= 2.49M)
                {
                    remark = "PASS";
                }
                else if (CGPA <= 2.00M)
                {
                    remark = "FAIL";
                }

                return remark;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Result> GetExamReferenceList(SessionSemester sessionSemester, Level level, ProgrammeType programme, Department department)
        {
            try
            {
                if (sessionSemester == null || sessionSemester.Id <= 0 || level == null || level.Id <= 0 || programme == null || programme.Id <= 0 || department == null || department.Id <= 0)
                {
                    throw new Exception("One or more criteria to get Exam Pass List not set! Please check your input criteria selection and try again.");
                }
                StudentLogic studentLogic = new StudentLogic();
                SessionSemesterLogic sessionSemesterLogic = new Business.SessionSemesterLogic();
                SessionSemester ss = sessionSemesterLogic.GetBy(sessionSemester.Id);

                List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT>(x => x.SessionId == ss.Session.Id && x.SemesterId == ss.Semester.Id && x.LevelId == level.Id && x.ProgrammeTypeId == programme.Id && x.DepartmentId == department.Id)
                                        select new Result
                                        {
                                            StudentId = sr.StudentId,
                                            Sex = sr.Sex,
                                            Name = sr.FullName,
                                            MatricNumber = sr.EntryRegNo,
                                            SessionName = sr.SessionName,
                                            FacultyName = sr.FacultyName,
                                            ProgrammeTypeName = sr.ProgrammeType,
                                            LevelName = sr.LevelName,
                                            CourseId = sr.CourseId,
                                            CourseCode = sr.Course_Code,
                                            CourseName = sr.CourseName,
                                            CourseUnit = sr.CourseUnit,

                                            TestScore = sr.TestScore,
                                            ExamScore = sr.ExamScore,
                                            TotalScore = sr.Total,
                                            Grade = sr.Grade,
                                            GradePoint = Convert.ToDecimal(sr.GradePoint),

                                            GPCU = Convert.ToDecimal(sr.WGP),
                                            TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                        }).ToList();

                List<Result> masterList = new List<Result>();
                List<string> carryOverCourses = new List<string>();
                if (ss.Semester.Id == 2)
                {
                    if (level.Id == 1 || level.Id == 3)
                    {
                        List<string> distinctMatricNumbers = results.Select(r => r.MatricNumber).AsParallel().Distinct().ToList();
                        foreach (string matricNumber in distinctMatricNumbers)
                        {
                            Student student = studentLogic.GetModelBy(s => s.EntryRegNo == matricNumber);
                            carryOverCourses = GetFirstYearCarryOverCourses(ss, level, programme, department, student);
                            if (carryOverCourses.Count != 0)
                            {
                                foreach (Result result in results.Where(r => r.MatricNumber == matricNumber))
                                {
                                    string remark = "";
                                    foreach (string courseCode in carryOverCourses)
                                    {
                                        remark += ("|" + courseCode);
                                    }
                                    result.Remark = remark;

                                    masterList.Add(result);
                                }
                            }
                        }
                    }
                    if (level.Id == 2 || level.Id == 4)
                    {
                        List<string> distinctMatricNumbers = results.Select(r => r.MatricNumber).AsParallel().Distinct().ToList();
                        foreach (string matricNumber in distinctMatricNumbers)
                        {
                            Student student = studentLogic.GetModelBy(s => s.EntryRegNo == matricNumber);
                            carryOverCourses = GetSecondYearCarryOverCourses(ss, level, programme, department, student);
                            if (carryOverCourses.Count != 0)
                            {
                                foreach (Result result in results.Where(r => r.MatricNumber == matricNumber))
                                {
                                    string remark = "";
                                    foreach (string courseCode in carryOverCourses)
                                    {
                                        remark += ("|" + courseCode);
                                    }
                                    result.Remark = remark;

                                    masterList.Add(result);
                                }
                            }
                        }
                    }
                }
                if (ss.Semester.Id == 1)
                {
                    if (level.Id == 1 || level.Id == 3)
                    {
                        List<string> distinctMatricNumbers = results.Select(r => r.MatricNumber).AsParallel().Distinct().ToList();
                        foreach (string matricNumber in distinctMatricNumbers)
                        {
                            Student student = studentLogic.GetModelBy(s => s.EntryRegNo == matricNumber);
                            carryOverCourses = GetFirstYearCarryOverCourses(ss, level, programme, department, student);
                            if (carryOverCourses.Count != 0)
                            {
                                foreach (Result result in results.Where(r => r.MatricNumber == matricNumber))
                                {
                                    string remark = "";
                                    foreach (string courseCode in carryOverCourses)
                                    {
                                        remark += ("|" + courseCode);
                                    }
                                    result.Remark = remark;

                                    masterList.Add(result);
                                }
                            }
                        }
                    }
                    if (level.Id == 2 || level.Id == 4)
                    {
                        List<string> distinctMatricNumbers = results.Select(r => r.MatricNumber).AsParallel().Distinct().ToList();
                        foreach (string matricNumber in distinctMatricNumbers)
                        {
                            Student student = studentLogic.GetModelBy(s => s.EntryRegNo == matricNumber);
                            carryOverCourses = GetSecondYearFirstSemesterCarryOverCourses(ss, level, programme, department, student);
                            if (carryOverCourses.Count != 0)
                            {
                                foreach (Result result in results.Where(r => r.MatricNumber == matricNumber))
                                {
                                    string remark = "";
                                    foreach (string courseCode in carryOverCourses)
                                    {
                                        remark += ("|" + courseCode);
                                    }
                                    result.Remark = remark;

                                    masterList.Add(result);
                                }
                            }
                        }
                    }
                }

                return masterList.OrderBy(p => p.MatricNumber).ToList();

            }
            catch (Exception)
            {
                throw;
            }
        }
        //public List<Result> GetPreviousSemesterPassList(Student student, SessionSemester sessionSemester, Level level, Programme programme, Department department)
        //{
        //    try
        //    {
        //        SessionSemesterLogic sessionSemesterLogic = new Business.SessionSemesterLogic();
        //        SessionSemester ss = sessionSemesterLogic.GetBy(sessionSemester.Id);
        //        Semester semester = new Semester() { Id = 1 };

        //        List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_3>(x => x.Session_Id == ss.Session.Id && x.Semester_Id == semester.Id && x.Person_Id == student.Id && x.Level_Id == level.Id && x.Programme_Id == programme.Id && x.Department_Id == department.Id)
        //                                select new Result
        //                                {
        //                                    StudentId = sr.Person_Id,
        //                                    Sex = sr.Sex_Name,
        //                                    Name = sr.Name,
        //                                    MatricNumber = sr.Matric_Number,
        //                                    CourseId = sr.Course_Id,
        //                                    CourseCode = sr.Course_Code,
        //                                    CourseName = sr.Course_Name,
        //                                    CourseUnit = sr.Course_Unit,

        //                                    TestScore = sr.Test_Score,
        //                                    ExamScore = sr.Exam_Score,
        //                                    Score = sr.Total_Score,
        //                                    Grade = sr.GradeUploaded,
        //                                    GradePoint = sr.Grade_Point,

        //                                    GPCU = sr.WGP,
        //                                    TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
        //                                }).ToList();


        //        foreach (Result result in results)
        //        {
        //            if (result.Grade == "FX")
        //            {
        //                return new List<Result>();
        //            }
        //        }
        //        return results;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
        public List<Result> GetMaterSheetBy(SessionSemester sessionSemester, Level level, ProgrammeType programme, Department department)
        {
            try
            {
                if (sessionSemester == null || sessionSemester.Id <= 0 || level == null || level.Id <= 0 || programme == null || programme.Id <= 0 || department == null || department.Id <= 0)
                {
                    throw new Exception("One or more criteria to get Mater Result Sheet not set! Please check your input criteria selection and try again.");
                }

                SessionSemesterLogic sessionSemesterLogic = new Business.SessionSemesterLogic();
                SessionSemester ss = sessionSemesterLogic.GetBy(sessionSemester.Id);
                List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT>(x => x.SessionId == ss.Session.Id && x.SemesterId == ss.Semester.Id && x.LevelId == level.Id && x.ProgrammeTypeId == programme.Id && x.DepartmentId == department.Id)
                                        select new Result
                                        {
                                            StudentId = sr.StudentId,
                                            Sex = sr.Sex,
                                            Name = sr.FullName,
                                            MatricNumber = sr.EntryRegNo,
                                            SessionName = sr.SessionName,
                                            FacultyName = sr.FacultyName,
                                            ProgrammeTypeName = sr.ProgrammeType,
                                            LevelName = sr.LevelName,
                                            CourseId = sr.CourseId,
                                            CourseCode = sr.Course_Code,
                                            CourseName = sr.CourseName,
                                            CourseUnit = sr.CourseUnit,
                                            Score = sr.Total,
                                            TestScore = sr.TestScore,
                                            ExamScore = sr.ExamScore,
                                            TotalScore = sr.Total,
                                            Grade = sr.Grade,
                                            GradePoint = Convert.ToDecimal(sr.GradePoint),

                                            GPCU = Convert.ToDecimal(sr.WGP),
                                            TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                        }).OrderBy(p => p.MatricNumber).ToList();


                if (level.Id == 1 || level.Id == 3)
                {
                    if (ss.Semester.Id == 2)
                    {
                        List<string> distinctMatricNumbers = results.Select(r => r.MatricNumber).AsParallel().Distinct().ToList();
                        foreach (string matricNumber in distinctMatricNumbers)
                        {
                            StudentLogic studentLogic = new StudentLogic();
                            Student student = studentLogic.GetModelBy(s => s.EntryRegNo == matricNumber);
                            List<Result> previousSemesterResult = GetPreviousSemesterResult(student, sessionSemester, level, programme, department);

                            int referenceCount = 0;
                            string reference = null;
                            List<Result> resultList = results.Where(r => r.MatricNumber == matricNumber).ToList();
                            foreach (Result result in resultList)
                            {
                                result.PreviousSemesterTotalCourseUnit = previousSemesterResult.FirstOrDefault().TotalSemesterCourseUnit;
                                result.PreviousSemesterTotalGPCU = previousSemesterResult.Sum(p => p.GPCU);
                                if (result.Grade == "F" || result.Grade == "CD" || result.Grade == "D" || result.Grade == "E")
                                {
                                    referenceCount += 1;
                                    reference = referenceCount.ToString() + "R";
                                }
                                if (referenceCount != 0 && reference != null && previousSemesterResult.LastOrDefault().Remark != "PASS")
                                {
                                    result.Remark = reference + "+" + previousSemesterResult.LastOrDefault().Remark;
                                }
                                else if (referenceCount != 0 && reference != null && previousSemesterResult.LastOrDefault().Remark == "PASS")
                                {
                                    result.Remark = reference;
                                }
                                else if (referenceCount == 0 && reference == null && previousSemesterResult.LastOrDefault().Remark != "PASS")
                                {
                                    result.Remark = "+" + previousSemesterResult.LastOrDefault().Remark;
                                }
                                else if (referenceCount == 0 && reference == null && previousSemesterResult.LastOrDefault().Remark == "PASS")
                                {
                                    result.Remark = "PASS";
                                }

                                result.PreviousSemesterTotalGPCU = previousSemesterResult.Sum(psr => psr.GPCU);
                                result.PreviousSemesterTotalCourseUnit = previousSemesterResult.LastOrDefault().TotalSemesterCourseUnit;

                                decimal? thisSemesterCGPUSum = results.Where(r => r.MatricNumber == matricNumber).Sum(r => r.GPCU);
                                result.GPA = thisSemesterCGPUSum / result.TotalSemesterCourseUnit;
                                result.CGPA = (thisSemesterCGPUSum + result.PreviousSemesterTotalGPCU) / (result.TotalSemesterCourseUnit + result.PreviousSemesterTotalCourseUnit);

                            }
                        }
                    }
                    else
                    {
                        List<string> distinctMatricNumbers = results.Select(r => r.MatricNumber).AsParallel().Distinct().ToList();
                        foreach (string matricNumber in distinctMatricNumbers)
                        {
                            int referenceCount = 0;
                            string reference = null;
                            foreach (Result result in results.Where(r => r.MatricNumber == matricNumber).ToList())
                            {
                                if (result.Grade == "F" || result.Grade == "CD" || result.Grade == "D" || result.Grade == "E")
                                {
                                    referenceCount += 1;
                                    reference = referenceCount.ToString() + "R";
                                }
                                if (referenceCount != 0 && reference != null)
                                {
                                    result.Remark = reference;
                                }
                                decimal? CGPUSum = results.Where(r => r.MatricNumber == matricNumber).Sum(r => r.GPCU);
                                result.GPA = CGPUSum / result.TotalSemesterCourseUnit;
                            }
                            if (results.Where(r => r.MatricNumber == matricNumber).LastOrDefault().Remark == null)
                            {
                                results.Where(r => r.MatricNumber == matricNumber).LastOrDefault().Remark = "PASS";
                            }


                        }
                    }
                }
                else if (level.Id == 2 || level.Id == 4)
                {
                    if (ss.Semester.Id == 2)
                    {
                        List<string> distinctMatricNumbers = results.Select(r => r.MatricNumber).AsParallel().Distinct().ToList();
                        foreach (string matricNumber in distinctMatricNumbers)
                        {
                            StudentLogic studentLogic = new StudentLogic();
                            Student student = studentLogic.GetModelBy(s => s.EntryRegNo == matricNumber);

                            List<string> carryOverCourseCodes = GetSecondYearCarryOverCourses(ss, level, programme, department, student);

                            List<Result> firseYearResult = GetFirstYearResult(student, ss, level, programme, department, carryOverCourseCodes);
                            List<Result> previousSemesterResult = GetPreviousSemesterResult(student, sessionSemester, level, programme, department);

                            int referenceCount = 0;
                            string reference = null;
                            List<Result> resultList = results.Where(r => r.MatricNumber == matricNumber).ToList();
                            foreach (Result result in resultList)
                            {
                                result.PreviousSemesterTotalCourseUnit = previousSemesterResult.FirstOrDefault().TotalSemesterCourseUnit;
                                result.PreviousSemesterTotalGPCU = previousSemesterResult.Sum(p => p.GPCU);
                                if (result.Grade == "F" || result.Grade == "CD" || result.Grade == "D" || result.Grade == "E")
                                {
                                    referenceCount += 1;
                                    reference = referenceCount.ToString();
                                }
                                //CourseLogic courseLogic = new CourseLogic();
                                //int levelId = courseLogic.GetModelBy(c => c.CourseId == result.CourseId).Level.Id;
                                //if (levelId == 1 || levelId == 3)
                                //{
                                //    result.GPCU = 0;
                                //    results.LastOrDefault().TotalSemesterCourseUnit = result.TotalSemesterCourseUnit - result.CourseUnit;
                                //    results.Remove(result);
                                //}

                                GetRemark(firseYearResult, previousSemesterResult, reference, result);

                                result.FirstYearCGPA = (firseYearResult.Sum(r => r.GPCU) + firseYearResult.LastOrDefault().PreviousSemesterTotalGPCU) / (firseYearResult.LastOrDefault().TotalSemesterCourseUnit + firseYearResult.LastOrDefault().PreviousSemesterTotalCourseUnit);
                                results.LastOrDefault().TotalSemesterCourseUnit = results.Min(p => p.TotalSemesterCourseUnit);

                                result.PreviousSemesterTotalGPCU = previousSemesterResult.Sum(psr => psr.GPCU);
                                result.PreviousSemesterTotalCourseUnit = previousSemesterResult.LastOrDefault().TotalSemesterCourseUnit;
                                decimal? thisSemesterCGPUSum = results.Where(r => r.MatricNumber == matricNumber).Sum(r => r.GPCU);
                                result.GPA = thisSemesterCGPUSum / result.TotalSemesterCourseUnit;

                                decimal? secondYearCGPA = (thisSemesterCGPUSum + result.PreviousSemesterTotalGPCU) / (results.Min(p => p.TotalSemesterCourseUnit) + result.PreviousSemesterTotalCourseUnit);
                                decimal? firstYearFirstSemesterGPCUSum = firseYearResult.Sum(r => r.GPCU);
                                decimal? firstYearCGPA = (firstYearFirstSemesterGPCUSum + firseYearResult.LastOrDefault().PreviousSemesterTotalGPCU) / (firseYearResult.LastOrDefault().TotalSemesterCourseUnit + firseYearResult.LastOrDefault().PreviousSemesterTotalCourseUnit);

                                //result.CGPA = Convert.ToDecimal((40M / 100M) * firstYearCGPA) + Convert.ToDecimal((60M / 100M) * secondYearCGPA);
                                decimal? thisSemesterMinTotalCourseUnit = results.Where(r => r.MatricNumber == matricNumber).LastOrDefault().TotalSemesterCourseUnit;
                                result.CGPA = Convert.ToDecimal(Math.Round(((60M / 100M) * (((decimal)thisSemesterCGPUSum + (decimal)result.PreviousSemesterTotalGPCU) / (((decimal)thisSemesterMinTotalCourseUnit) + (decimal)result.PreviousSemesterTotalCourseUnit))), 2) + Math.Round(((40M / 100M) * (decimal)result.FirstYearCGPA), 2));
                            }
                        }
                    }
                    else
                    {
                        List<string> distinctMatricNumbers = results.Select(r => r.MatricNumber).AsParallel().Distinct().ToList();
                        foreach (string matricNumber in distinctMatricNumbers)
                        {
                            int referenceCount = 0;
                            string reference = null;
                            foreach (Result result in results.Where(r => r.MatricNumber == matricNumber).ToList())
                            {
                                if (result.Grade == "F" || result.Grade == "CD" || result.Grade == "D" || result.Grade == "E")
                                {
                                    referenceCount += 1;
                                    reference = referenceCount.ToString() + "R";
                                }
                                if (referenceCount != 0 && reference != null)
                                {
                                    result.Remark = reference;
                                }
                                //CourseLogic courseLogic = new CourseLogic();
                                //int levelId = courseLogic.GetModelBy(c => c.Course_Id == result.CourseId).Level.Id;
                                //if (levelId == 1 || levelId == 3)
                                //{
                                //    result.GPCU = 0;
                                //    results.LastOrDefault().TotalSemesterCourseUnit = result.TotalSemesterCourseUnit - result.CourseUnit;
                                //    results.Remove(result);
                                //}
                                decimal? CGPUSum = results.Where(r => r.MatricNumber == matricNumber).Sum(r => r.GPCU);
                                result.GPA = CGPUSum / result.TotalSemesterCourseUnit;
                            }
                            if (results.Where(r => r.MatricNumber == matricNumber).LastOrDefault().Remark == null)
                            {
                                results.Where(r => r.MatricNumber == matricNumber).LastOrDefault().Remark = "PASS";
                            }

                        }
                    }
                }




                //List<Result> resultList = results.OrderBy(p => p.MatricNumber).ToList();
                return results.OrderBy(p => p.MatricNumber).ToList();
                //return results;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static void GetRemark(List<Result> firseYearResult, List<Result> previousSemesterResult, string reference, Result result)
        {
            if (reference == null && previousSemesterResult.LastOrDefault().Remark == "PASS" && firseYearResult.LastOrDefault().Remark == "PASS")
            {
                result.Remark = "PASS";
            }
            else if (reference == null && previousSemesterResult.LastOrDefault().Remark == "PASS" && firseYearResult.LastOrDefault().Remark != "PASS")
            {
                result.Remark = "+" + firseYearResult.LastOrDefault().Remark + "R";
            }
            else if (reference == null && previousSemesterResult.LastOrDefault().Remark != "PASS" && firseYearResult.LastOrDefault().Remark == "PASS")
            {
                result.Remark = "+" + previousSemesterResult.LastOrDefault().Remark.Substring(0, 1) + "R";
            }
            else if (reference != null && previousSemesterResult.LastOrDefault().Remark == "PASS" && firseYearResult.LastOrDefault().Remark == "PASS")
            {
                result.Remark = reference + "R";
            }
            else if (reference == null && previousSemesterResult.LastOrDefault().Remark != "PASS" && firseYearResult.LastOrDefault().Remark != "PASS")
            {
                result.Remark = "+" + (Convert.ToInt32(previousSemesterResult.LastOrDefault().Remark.Substring(0, 1)) + Convert.ToInt32(firseYearResult.LastOrDefault().Remark)).ToString() + "R";
            }
            else if (reference != null && previousSemesterResult.LastOrDefault().Remark != "PASS" && firseYearResult.LastOrDefault().Remark == "PASS")
            {
                result.Remark = reference + "R" + "+" + previousSemesterResult.LastOrDefault().Remark.Substring(0, 1) + "R";
            }
            else if (reference != null && previousSemesterResult.LastOrDefault().Remark == "PASS" && firseYearResult.LastOrDefault().Remark != "PASS")
            {
                result.Remark = reference + "R" + "+" + firseYearResult.LastOrDefault().Remark + "R";
            }
            else if (reference != null && previousSemesterResult.LastOrDefault().Remark != "PASS" && firseYearResult.LastOrDefault().Remark != "PASS")
            {
                result.Remark = reference + "R" + "+" + (Convert.ToInt32(previousSemesterResult.LastOrDefault().Remark.Substring(0, 1)) + Convert.ToInt32(firseYearResult.LastOrDefault().Remark)).ToString() + "R";
            }
        }
        public List<Result> GetPreviousSemesterResult(Student student, SessionSemester sessionSemester, Level level, ProgrammeType programme, Department department)
        {
            try
            {
                SessionSemesterLogic sessionSemesterLogic = new Business.SessionSemesterLogic();
                SessionSemester ss = sessionSemesterLogic.GetBy(sessionSemester.Id);
                Semester semester = new Semester() { Id = 1 };

                List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT>(x => x.SessionId == ss.Session.Id && x.SemesterId == semester.Id && x.StudentId == student.Id && x.LevelId == level.Id && x.ProgrammeTypeId == programme.Id && x.DepartmentId == department.Id)
                                        select new Result
                                        {
                                            StudentId = sr.StudentId,
                                            Sex = sr.Sex,
                                            Name = sr.FullName,
                                            MatricNumber = sr.EntryRegNo,
                                            CourseId = sr.CourseId,
                                            CourseCode = sr.Course_Code,
                                            CourseName = sr.CourseName,
                                            CourseUnit = sr.CourseUnit,

                                            TestScore = sr.TestScore,
                                            ExamScore = sr.ExamScore,
                                            TotalScore = sr.Total,
                                            Grade = sr.Grade,
                                            GradePoint = Convert.ToDecimal(sr.GradePoint),

                                            GPCU = Convert.ToDecimal(sr.WGP),
                                            TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                        }).ToList();

                if (results.Count != 0)
                {
                    int referenceCount = 0;
                    string reference = null;
                    foreach (Result result in results)
                    {
                        if (result.Grade == "F" || result.Grade == "CD" || result.Grade == "D" || result.Grade == "E")
                        {
                            referenceCount += 1;
                            reference = referenceCount.ToString() + "R";
                        }
                        if (referenceCount != 0 && reference != null)
                        {
                            result.Remark = reference;
                        }
                    }
                    if (results.LastOrDefault().Remark == null)
                    {
                        results.LastOrDefault().Remark = "PASS";
                    }
                }


                return results;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Result> GetFirstYearResult(Student student, SessionSemester sessionSemester, Level level, ProgrammeType programme, Department department, List<string> carryOverCourses)
        {
            try
            {
                if (sessionSemester == null || sessionSemester.Id <= 0 || level == null || level.Id <= 0 || programme == null || programme.Id <= 0 || department == null || department.Id <= 0)
                {
                    throw new Exception("One or more criteria to get Mater Result Sheet not set! Please check your input criteria selection and try again.");
                }

                string[] sessionArray = sessionSemester.Session.Name.Split('/');
                int firstIndex = Convert.ToInt32(sessionArray[0]) - 1;
                string previousSession = firstIndex + "/" + sessionArray[0];

                SessionLogic sessionLogic = new SessionLogic();
                Session session = sessionLogic.GetModelBy(s => s.Name == previousSession);
                Semester firstSemester = new Semester() { Id = 1 };
                Semester secondSemester = new Semester() { Id = 2 };
                Level previousLevel = new Level();

                if (level.Id == 4)
                {
                    previousLevel = new Level() { Id = 3 };
                }
                else if (level.Id == 2)
                {
                    previousLevel = new Level() { Id = 1 };
                }

                SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();

                List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT>(x => x.StudentId == student.Id && x.SessionId == session.Id && x.SemesterId == secondSemester.Id && x.LevelId == previousLevel.Id && x.ProgrammeId == programme.Id && x.DepartmentId == department.Id)
                                        select new Result
                                        {
                                            StudentId = sr.StudentId,
                                            Sex = sr.Sex,
                                            Name = sr.FullName,
                                            MatricNumber = sr.EntryRegNo,
                                            SessionName = sr.SessionName,
                                            FacultyName = sr.FacultyName,
                                            ProgrammeTypeName = sr.ProgrammeType,
                                            LevelName = sr.LevelName,
                                            CourseId = sr.CourseId,
                                            CourseCode = sr.Course_Code,
                                            CourseName = sr.CourseName,
                                            CourseUnit = sr.CourseUnit,

                                            TestScore = sr.TestScore,
                                            ExamScore = sr.ExamScore,
                                            TotalScore = sr.Total,
                                            Grade = sr.Grade,
                                            GradePoint = Convert.ToDecimal(sr.GradePoint),

                                            GPCU = Convert.ToDecimal(sr.WGP),
                                            TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                        }).OrderBy(p => p.MatricNumber).ToList();

                SessionSemester prevSessionSemester = sessionSemesterLogic.GetModelBy(ssm => ssm.Session_Id == session.Id && ssm.Semester_Id == firstSemester.Id);
                List<Result> previousSemesterResult = GetFirstYearFirstSemesterResult(carryOverCourses, student, prevSessionSemester, previousLevel, programme, department);

                int referenceCount = 0;
                int reference = 0;
                foreach (Result result in results)
                {
                    result.PreviousSemesterTotalCourseUnit = previousSemesterResult.FirstOrDefault().TotalSemesterCourseUnit;
                    result.PreviousSemesterTotalGPCU = previousSemesterResult.Sum(p => p.GPCU);
                    if (result.Grade == "F" || result.Grade == "CD" || result.Grade == "D" || result.Grade == "E")
                    {
                        if (carryOverCourses.Contains(result.CourseCode))
                        {
                            referenceCount += 1;
                            reference = referenceCount;
                        }
                        else
                        {
                            ProgrammeCourseRegistrationLogic programmeCourseRegistrationLogic = new ProgrammeCourseRegistrationLogic();
                            ProgrammeCourseRegistration programmeCourseRegistration = programmeCourseRegistrationLogic.GetModelBy(crd => crd.PROGRAMME_COURSES.CourseId == result.CourseId && crd.SessionSemesterId == sessionSemester.Session.Id && crd.StudentId == student.Id);
                            StudentCourseMark studentCourseMark = base.GetModelBy(scm => scm.CourseRegId == programmeCourseRegistration.Id);
                            result.Grade = studentCourseMark.Grade;
                            result.GPCU = Convert.ToDecimal(studentCourseMark.GradePoint * result.CourseUnit);
                        }
                    }
                    if (referenceCount != 0 && reference != 0 && previousSemesterResult.LastOrDefault().Remark != "PASS")
                    {
                        result.Remark = (reference + Convert.ToInt32(previousSemesterResult.LastOrDefault().Remark.Substring(0, 1))).ToString();
                    }
                    else if (referenceCount != 0 && reference != 0 && previousSemesterResult.LastOrDefault().Remark == "PASS")
                    {
                        result.Remark = reference.ToString();
                    }
                    else if (referenceCount == 0 && reference == 0 && previousSemesterResult.LastOrDefault().Remark != "PASS")
                    {
                        result.Remark = previousSemesterResult.LastOrDefault().Remark.Substring(0, 1).ToString();
                    }
                    else if (referenceCount == 0 && reference == 0 && previousSemesterResult.LastOrDefault().Remark == "PASS")
                    {
                        result.Remark = "PASS";
                    }
                }

                return results;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private List<Result> GetFirstYearFirstSemesterResult(List<string> carryOverCourses, Student student, SessionSemester prevSessionSemester, Level previousLevel, ProgrammeType programme, Department department)
        {
            throw new NotImplementedException();
        }
        //public List<Result> GetFirstYearFirstSemesterResult(List<string> carryOverCourses, Student student, SessionSemester sessionSemester, Level level, ProgrammeType programme, Department department)
        //{
        //    try
        //    {
        //        SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
        //        SessionSemester ss = sessionSemesterLogic.GetBy(sessionSemester.Id);
        //        Semester semester = new Semester() { Id = 1 };

        //        List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT>(x => x.SessionId == ss.Session.Id && x.SemesterId == semester.Id && x.StudentId == student.Id && x.LevelId == level.Id && x.ProgrammeTypeId == programme.Id && x.DepartmentId == department.Id)
        //                                select new Result
        //                                {
        //                                    StudentId = sr.StudentId,
        //                                    Sex = sr.Sex,
        //                                    Name = sr.FullName,
        //                                    MatricNumber = sr.EntryRegNo,
        //                                    CourseId = sr.CourseId,
        //                                    CourseCode = sr.Course_Code,
        //                                    CourseName = sr.CourseName,
        //                                    CourseUnit = sr.CourseUnit,

        //                                    TestScore = sr.TestScore,
        //                                    ExamScore = sr.ExamScore,
        //                                    TotalScore = sr.Total,
        //                                    Grade = sr.Grade,
        //                                    GradePoint = Convert.ToDecimal(sr.GradePoint),

        //                                    GPCU = Convert.ToDecimal(sr.WGP),
        //                                    TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
        //                                }).ToList();


        //        string[] sessionArray = sessionSemester.Session.Name.Split('/');
        //        int secondIndex = Convert.ToInt32(sessionArray[1]) + 1;
        //        string currentSession = sessionArray[1] + "/" + secondIndex;

        //        int referenceCount = 0;
        //        int reference = 0;
        //        foreach (Result result in results)
        //        {
        //            if (result.Grade == "F" || result.Grade == "CD" || result.Grade == "D" || result.Grade == "E")
        //            {
        //                if (carryOverCourses.Contains(result.CourseCode))
        //                {
        //                    referenceCount += 1;
        //                    reference = referenceCount;
        //                }
        //                else
        //                {
        //                    ProgrammeCourseRegistrationLogic programmeCourseRegistrationLogic = new ProgrammeCourseRegistrationLogic();
        //                    ProgrammeCourseRegistration programmeCourseRegistration = programmeCourseRegistrationLogic.GetModelBy(crd => crd.PROGRAMME_COURSES.CourseId == result.CourseId && crd.SemesterId == 1 && crd.SESSION.Name == currentSession && crd.StudentId == student.Id);
        //                    StudentCourseMark studentCourseMark = base.GetModelBy(scm => scm.CourseRegId == programmeCourseRegistration.Id);
        //                    result.Grade = studentCourseMark.Grade;
        //                    result.GPCU = Convert.ToDecimal(studentCourseMark.GradePoint * result.CourseUnit);
        //                }
        //            }
        //            if (referenceCount != 0 && reference != null)
        //            {
        //                result.Remark = reference.ToString();
        //            }
        //        }
        //        if (results.LastOrDefault().Remark == null)
        //        {
        //            results.LastOrDefault().Remark = "PASS";
        //        }

        //        return results;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        //private List<string> GetFirstYearCarryOverCourses(SessionSemester sessionSemester, Level lvl, ProgrammeType programme, Department department, Student student)
        //{
        //    try
        //    {
        //        List<StudentCourseMark> studentCourseMarks = new List<StudentCourseMark>();
        //        ProgrammeCourseRegistrationLogic courseRegistrationLogic = new ProgrammeCourseRegistrationLogic();
        //        List<string> courseCodes = new List<string>();
        //        if (lvl.Id == 1 || lvl.Id == 3)
        //        {
        //            studentCourseMarks = base.GetModelsBy(scm => scm.PROGRAMME_COURSE_REGISTRATION.SessionId == sessionSemester.Session.Id && scm.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.DepartmentId == department.Id && scm.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.ProgrammeTypeId == programme.Id && scm.PROGRAMME_COURSE_REGISTRATION.StudentId == student.Id && (scm.Grade == "CD" || scm.Grade == "D" || scm.Grade == "E" || scm.Grade == "F"));
        //            if (sessionSemester.Semester.Id == 1)
        //            {
        //                studentCourseMarks = base.GetModelsBy(scm => scm.PROGRAMME_COURSE_REGISTRATION.SessionId == sessionSemester.Session.Id && scm.PROGRAMME_COURSE_REGISTRATION.SemesterId == 1 && scm.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.DepartmentId == department.Id && scm.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.ProgrammeTypeId == programme.Id && scm.PROGRAMME_COURSE_REGISTRATION.StudentId == student.Id && (scm.Grade == "CD" || scm.Grade == "D" || scm.Grade == "E" || scm.Grade == "F"));
        //                if (studentCourseMarks.Count > 0)
        //                {
        //                    foreach (StudentCourseMark studentCourseMark in studentCourseMarks)
        //                    {
        //                        string courseCode = courseRegistrationLogic.GetModelBy(cr => cr.CourseRegId == studentCourseMark.Id).ProgrammeCourse.course.Course_Code;
        //                        courseCodes.Add(courseCode);
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                if (studentCourseMarks.Count > 0)
        //                {
        //                    foreach (StudentCourseMark studentCourseMark in studentCourseMarks)
        //                    {
        //                        string courseCode = courseRegistrationLogic.GetModelBy(cr => cr.CourseRegId == studentCourseMark.Id).ProgrammeCourse.course.Course_Code;
        //                        courseCodes.Add(courseCode);
        //                    }
        //                }
        //            }
        //        }

        //        return courseCodes;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
        //private List<string> GetSecondYearCarryOverCourses(SessionSemester sessionSemester, Level lvl, ProgrammeType programme, Department department, Student student)
        //{
        //    try
        //    {
        //        List<StudentCourseMark> studentCourseMarks = new List<StudentCourseMark>();
        //        ProgrammeCourseRegistrationLogic courseRegistrationLogic = new ProgrammeCourseRegistrationLogic();
        //        StudentLogic studentLevelLogic = new StudentLogic();
        //        List<string> courseCodes = new List<string>();
        //        List<string> firstYearCarryOverCourseCodes = null;

        //        if (lvl.Id == 2)
        //        {
        //            string[] sessionArray = student.CurrentSession.Name.Split('/');
        //            int index1 = Convert.ToInt32(sessionArray[0]) - 1;
        //            string prevSession = index1 + "/" + sessionArray[0];
        //            SessionLogic sessionLogic = new SessionLogic();
        //            Session session = sessionLogic.GetModelBy(s => s.Name == prevSession.Trim());
        //            SessionSemester ss = new SessionSemester();
        //            ss.Session = session;
        //            ss.Semester = new Semester() { Id = 2 };// Second semester to get all carry over for first year
        //            Level levelNew = new Level(){Id = 1};

        //            firstYearCarryOverCourseCodes = GetFirstYearCarryOverCourses(ss, levelNew, programme, department, student);
        //        }
        //        else if (lvl.Id == 4)
        //        {
        //            string[] sessionArray = student.CurrentSession.Name.Split('/');
        //            int index1 = Convert.ToInt32(sessionArray[0]) - 1;
        //            string prevSession = index1 + "/" + sessionArray[0];
        //            SessionLogic sessionLogic = new SessionLogic();
        //            Session session = sessionLogic.GetModelBy(s => s.Name == prevSession.Trim());
        //            SessionSemester ss = new SessionSemester();
        //            ss.Session = session;
        //            ss.Semester = new Semester() { Id = 2 };// Second semester to get all carry over for first year
        //            Level levelNew = new Level() { Id = 3 };

        //            firstYearCarryOverCourseCodes = GetFirstYearCarryOverCourses(ss, levelNew, programme, department, student);
        //        }

        //        if (lvl.Id == 2 || lvl.Id == 4)
        //        {
        //            studentCourseMarks = base.GetModelsBy(scm => scm.PROGRAMME_COURSE_REGISTRATION.SessionId == sessionSemester.Session.Id && scm.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.DepartmentId == department.Id && scm.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.ProgrammeTypeId == programme.Id && scm.PROGRAMME_COURSE_REGISTRATION.StudentId == student.Id && (scm.Grade == "CD" || scm.Grade == "D" || scm.Grade == "E" || scm.Grade == "F"));
        //            if (sessionSemester.Semester.Id == 1)
        //            {
        //                studentCourseMarks = base.GetModelsBy(scm => scm.PROGRAMME_COURSE_REGISTRATION.SessionId == sessionSemester.Session.Id && scm.PROGRAMME_COURSE_REGISTRATION.SemesterId == 1 && scm.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.DepartmentId == department.Id && scm.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.ProgrammeTypeId == programme.Id && scm.PROGRAMME_COURSE_REGISTRATION.StudentId == student.Id && (scm.Grade == "CD" || scm.Grade == "D" || scm.Grade == "E" || scm.Grade == "F"));
        //                if (studentCourseMarks.Count > 0)
        //                {
        //                    foreach (StudentCourseMark studentCourseMark in studentCourseMarks)
        //                    {
        //                        string courseCode = courseRegistrationLogic.GetModelBy(cr => cr.CourseRegId == studentCourseMark.Id).ProgrammeCourse.course.Course_Code;
        //                        courseCodes.Add(courseCode);
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                if (studentCourseMarks.Count > 0)
        //                {
        //                    foreach (StudentCourseMark studentCourseMark in studentCourseMarks)
        //                    {
        //                        string courseCode = courseRegistrationLogic.GetModelBy(cr => cr.CourseRegId == studentCourseMark.Id).ProgrammeCourse.course.Course_Code;
        //                        courseCodes.Add(courseCode);
        //                    }
        //                }
        //            }

        //        }
        //        //compare courses
        //        courseCodes = CompareCourses(courseCodes, firstYearCarryOverCourseCodes, sessionSemester, lvl, programme, department, student);
        //        return courseCodes;
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}
        //private List<string> GetSecondYearFirstSemesterCarryOverCourses(SessionSemester sessionSemester, Level lvl, ProgrammeType programme, Department department, Student student)
        //{
        //    try
        //    {
        //        List<StudentCourseMark> studentCourseMarks = new List<StudentCourseMark>();
        //        ProgrammeCourseRegistrationLogic courseRegistrationLogic = new ProgrammeCourseRegistrationLogic();
        //        List<string> courseCodes = new List<string>();
        //        if (lvl.Id == 2 || lvl.Id == 4)
        //        {
        //            studentCourseMarks = base.GetModelsBy(scm => scm.PROGRAMME_COURSE_REGISTRATION.SessionId == sessionSemester.Session.Id && scm.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.DepartmentId == department.Id && scm.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.ProgrammeTypeId == programme.Id && scm.PROGRAMME_COURSE_REGISTRATION.StudentId == student.Id && (scm.Grade == "CD" || scm.Grade == "D" || scm.Grade == "E" || scm.Grade == "F"));
        //            if (sessionSemester.Semester.Id == 1)
        //            {
        //                studentCourseMarks = base.GetModelsBy(scm => scm.PROGRAMME_COURSE_REGISTRATION.SessionId == sessionSemester.Session.Id && scm.PROGRAMME_COURSE_REGISTRATION.SemesterId == 1 && scm.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.DepartmentId == department.Id && scm.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.ProgrammeTypeId == programme.Id && scm.PROGRAMME_COURSE_REGISTRATION.StudentId == student.Id && (scm.Grade == "CD" || scm.Grade == "D" || scm.Grade == "E" || scm.Grade == "F"));
        //                if (studentCourseMarks.Count > 0)
        //                {
        //                    foreach (StudentCourseMark studentCourseMark in studentCourseMarks)
        //                    {
        //                        string courseCode = courseRegistrationLogic.GetModelBy(cr => cr.CourseRegId == studentCourseMark.Id).ProgrammeCourse.course.Course_Code;
        //                        courseCodes.Add(courseCode);
        //                    }
        //                }
        //            }
        //        }

        //        return courseCodes;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
        //private List<string> CompareCourses(List<string> courseCodes, List<string> firstYearCarryOverCourseCodes, SessionSemester sessionSemester, Level lvl, ProgrammeType programme, Department department, Student student)
        //{
        //    try
        //    {
        //        ProgrammeCourseRegistrationLogic courseRegistrationLogic = new ProgrammeCourseRegistrationLogic();
        //        for (int i = 0; i < firstYearCarryOverCourseCodes.Count(); i++)
        //        {
        //            if (courseCodes != null && firstYearCarryOverCourseCodes != null)
        //            {
        //                if (courseCodes.Contains(firstYearCarryOverCourseCodes[i]))
        //                {
        //                    courseCodes.Add(firstYearCarryOverCourseCodes[i]);
        //                    firstYearCarryOverCourseCodes.RemoveAt(i);
        //                }
        //                else
        //                {
        //                    string Coursecode = firstYearCarryOverCourseCodes[i];
        //                    ProgrammeCourseRegistration course = courseRegistrationLogic.GetModelBy(p => p.PROGRAMME_COURSES.COURSE.Course_Code == Coursecode && p.StudentId == student.Id && p.SessionId == sessionSemester.Session.Id);
        //                    if (course != null)
        //                    {
        //                        firstYearCarryOverCourseCodes.RemoveAt(i);
        //                    }
        //                    else
        //                    {
        //                        courseCodes.Add(firstYearCarryOverCourseCodes[i]);
        //                    }
        //                }
        //            }

        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    return courseCodes.Distinct().ToList();
        //}

        public string GetGraduationStatus(decimal? CGPA, string graduationDate)
        {
            string remark = null;
            int graduationId = Convert.ToInt32(graduationDate);
            try
            {
                if (CGPA != null && graduationId < 2015)
                {

                    if (CGPA >= (decimal)3.50 && CGPA <= (decimal)4.00)
                    {
                        remark = "FIRST CLASS";
                    }
                    else if (CGPA >= (decimal)3.00 && CGPA < (decimal)3.49)
                    {
                        remark = "SECOND CLASS (UPPER DIVISION)";
                    }
                    else if (CGPA >= (decimal)2.50 && CGPA <= (decimal)2.99)
                    {
                        remark = "SECOND CLASS (LOWER DIVISION)";
                    }
                    else if (CGPA >= (decimal)2.20 && CGPA <= (decimal)2.49)
                    {
                        remark = "THIRD CLASS";
                    }
                    else if (CGPA >= (decimal)2.00 && CGPA <= (decimal)2.19)
                    {
                        remark = "PASS";
                    }
                    else if (CGPA < (decimal)2.0)
                    {
                        remark = "FAIL";
                    }
                }
                else
                {
                    if (CGPA >= (decimal)4.5 && CGPA <= (decimal)5.0)
                    {
                        remark = "FIRST CLASS";
                    }
                    else if (CGPA >= (decimal)3.50 && CGPA <= (decimal)4.49)
                    {
                        remark = "SECOND CLASS (UPPER DIVISION)";
                    }
                    else if (CGPA >= (decimal)2.40 && CGPA < (decimal)3.49)
                    {
                        remark = "SECOND CLASS (LOWER DIVISION)";
                    }
                    else if (CGPA >= (decimal)1.5 && CGPA <= (decimal)2.39)
                    {
                        remark = "THIRD CLASS";
                    }
                    else if (CGPA >= (decimal)1.00 && CGPA <= (decimal)1.49)
                    {
                        remark = "PASS";
                    }
                    else if (CGPA < (decimal)1.0)
                    {
                        remark = "FAIL";
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return remark;
        }

        public bool Modify(StudentCourseMark studentCourseMark)
        {
           
            try
            {
                Expression<Func<STUDENT_COURSE_MARK, bool>> selector = a => a.CourseRegId == studentCourseMark.Id;
                STUDENT_COURSE_MARK entity = GetEntityBy(selector);

               // if (entity != null && !entity.Grade.Contains(studentCourseMark.Grade) && (int)entity.GradePoint == gradePoint)
                if (entity != null )
                {
                    if (!string.IsNullOrEmpty(studentCourseMark.Grade))
                    {
                        entity.Grade = studentCourseMark.Grade;
                    }
                    if (studentCourseMark.GradePoint > 0)
                    {
                        entity.GradePoint = studentCourseMark.GradePoint;

                    }
                    if (studentCourseMark.CourseUnit > 0)
                    {
                        entity.CourseUnit = studentCourseMark.CourseUnit;
                    }
                    int modifiedCount = Save();

                    if (modifiedCount > 0)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return false;
        }

        public StudentCourseMark Create(StudentCourseMark studentCourseMark, StudentCourseMarkAudit courseRegistrationDetailAudit)
        {
            StudentCourseMark detail = new StudentCourseMark();
            try
            {

                StudentCourseMarkAuditLogic courseRegistrationDetailAuditLogic = new StudentCourseMarkAuditLogic();
                detail = base.Create(studentCourseMark);
                if (courseRegistrationDetailAudit.Client != null && courseRegistrationDetailAudit.Action != null)
                {
                    courseRegistrationDetailAudit.ProgrammeCourse = studentCourseMark.ProgrammeCourseRegistration.ProgrammeCourse;
                    courseRegistrationDetailAudit.Time = DateTime.Now;
                    courseRegistrationDetailAuditLogic.Create(courseRegistrationDetailAudit);
                }
                return detail;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
