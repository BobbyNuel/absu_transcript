﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Translator;

namespace Abundance_Rm.Business
{
    public class UploadedResultLogic:BaseBusinessLogic<UploadedResult,UPLOADED_RESULT>
    {
        public UploadedResultLogic()
        {
            translator = new UploadedResultTranslator();
        }

        public List<ProcessedStudent> GetProcessedStudentBy(Department department,ProgrammeType programmeType, string dateTo, string dateFrom)
        {
            try
            {
                DateTime processedDateFrom = ConvertToDate(dateFrom);
                DateTime processedDateTo = ConvertToDate(dateTo);

                TimeSpan ts = new TimeSpan(00,00, 0);
                processedDateFrom = processedDateFrom.Date + ts;

                ts = new TimeSpan(23,59, 0);
                processedDateTo = processedDateTo.Date + ts;


                UploadedResultLogic uploadedResultLogic = new UploadedResultLogic();
                StudentLogic studentLogic = new StudentLogic();
                ProgrammeLogic programmeLogic = new ProgrammeLogic();
                Programme programme = programmeLogic.GetModelBy(p => p.DepartmentId == department.Id && p.ProgrammeTypeId == programmeType.Id);
                List<ProcessedStudent> processedStudents = new List<ProcessedStudent>();
                {
                    List<UploadedResult> processedResults = uploadedResultLogic.GetModelsBy(s => (s.Date_Uploaded >= processedDateFrom && s.Date_Uploaded <= processedDateTo) && s.ProgrammeId == programme.Id);

                    if (processedResults != null && processedResults.Count > 0)
                    {
                        
                        for (int i = 0; i < processedResults.Count; i++)
                        {
                            ProcessedStudent uploadedResult = new ProcessedStudent();
                            if (processedResults[i].Student != null)
                            {
                                uploadedResult.Student = (processedResults[i].Student.Person.Surname + " " + processedResults[i].Student.Person.MiddleName + " " + processedResults[i].Student.Person.FirstName).ToUpper();
                                uploadedResult.ProgrammeName = processedResults[i].Student.Programme.ProgrammeType.Name;
                                uploadedResult.DepartmentName = processedResults[i].Student.Programme.department.Name;
                                uploadedResult.DateUplaoded = processedResults[i].DateUploaded.ToString(CultureInfo.InvariantCulture);
                                var studentId = processedResults[i].Student.Id;
                                uploadedResult.RegNo = studentLogic.GetModelBy(s => s.StudentId == studentId).EntryRegNo;

                                processedStudents.Add(uploadedResult);
                            }
                           
                        }
                    }
                }
                return processedStudents;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public List<ProcessedStudent> GetProcessedStudentBy(string dateTo, string dateFrom)
        {
            try
            {
                DateTime processedDateFrom = ConvertToDate(dateFrom);
                DateTime processedDateTo = ConvertToDate(dateTo);

                TimeSpan ts = new TimeSpan(00, 00, 0);
                processedDateFrom = processedDateFrom.Date + ts;

                ts = new TimeSpan(23, 59, 0);
                processedDateTo = processedDateTo.Date + ts;


                UploadedResultLogic uploadedResultLogic = new UploadedResultLogic();
                StudentLogic studentLogic = new StudentLogic();

                List<ProcessedStudent> processedStudents = new List<ProcessedStudent>();
                {
                    List<UploadedResult> processedResults = uploadedResultLogic.GetModelsBy(s => (s.Date_Uploaded >= processedDateFrom && s.Date_Uploaded <= processedDateTo));

                    if (processedResults != null && processedResults.Count > 0)
                    {

                        for (int i = 0; i < processedResults.Count; i++)
                        {
                            ProcessedStudent uploadedResult = new ProcessedStudent();
                            if (processedResults[i].Student != null)
                            {
                                uploadedResult.Student = (processedResults[i].Student.Person.Surname + " " + processedResults[i].Student.Person.MiddleName + " " + processedResults[i].Student.Person.FirstName).ToUpper();
                                uploadedResult.ProgrammeName = processedResults[i].Student.Programme.ProgrammeType.Name;
                                uploadedResult.DepartmentName = processedResults[i].Student.Programme.department.Name;
                                uploadedResult.DateUplaoded = processedResults[i].DateUploaded.ToString(CultureInfo.InvariantCulture);
                                var studentId = processedResults[i].Student.Id;
                                uploadedResult.RegNo = studentLogic.GetModelBy(s => s.StudentId == studentId).EntryRegNo;

                                processedStudents.Add(uploadedResult);
                            }

                        }
                    }
                }
                return processedStudents;
            }
            catch (Exception)
            {

                throw;
            }
        }
        private DateTime ConvertToDate(string date)
        {
            DateTime newDate = new DateTime();
            try
            {
                //newDate = DateTime.Parse(date);
                string[] dateSplit = date.Split('/');
                newDate = new DateTime(Convert.ToInt32(dateSplit[2]), Convert.ToInt32(dateSplit[1]), Convert.ToInt32(dateSplit[0]));
            }
            catch (Exception)
            {
                throw;
            }

            return newDate;
        }
    }
}
