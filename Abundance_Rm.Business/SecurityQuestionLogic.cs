﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Abundance_Rm.Model.Entity;
using Abundance_Rm.Business;
using Abundance_Rm.Model.Translator;
using Abundance_Rm.Model.Model;

namespace Abundance_Rm.Business
{
    public class SecurityQuestionLogic : BaseBusinessLogic<SecurityQuestion, SECURITY_QUESTION>
    {
        public SecurityQuestionLogic()
        {
            translator = new SecurityQuestionTranslator();
        }
        
    }
}
