﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Translator;
namespace Abundance_Rm.Business
{
    public class ProgrammeCourseLogic:BaseBusinessLogic<ProgrammeCourse,PROGRAMME_COURSES>
    {
        public ProgrammeCourseLogic()
        {
            translator = new ProgrammeCourseTranslator();
        }

        public bool Modify(Student model)
        {
            try
            {
                ProgrammeCourseRegistrationLogic programmeCourseRegistrationLogic = new ProgrammeCourseRegistrationLogic();
                ProgrammeCourseLogic programmeCourseLogic = new ProgrammeCourseLogic();
                List<PROGRAMME_COURSE_REGISTRATION> programmeCourseRegistration = programmeCourseRegistrationLogic.GetEntitiesBy(pc => pc.StudentId == model.Id);
                int modifiedCount = 0;
                if (programmeCourseRegistration != null && programmeCourseRegistration.Count > 0)
                {
                    for (int i = 0; i < programmeCourseRegistration.Count; i++)
                    {
                        var id = programmeCourseRegistration[i].ProgrammeCourseId;

                        Expression<Func<PROGRAMME_COURSES, bool>> selector = a => a.ProgrammeCourseId == id;
                        PROGRAMME_COURSES programmeCoursesEntity = GetEntityBy(selector);


                        programmeCoursesEntity.ProgrammeId = model.Programme.Id;
                        modifiedCount = Save();
                    }
                }
               
                if (modifiedCount <= 0)
                {
                    return false;
                }
                return true;
            }
            catch (Exception)
            {
                
                throw;
            }
          
        }

        public int Modify(List<ProgrammeCourseRegistration> programmeCourseRegistrations)
        {
            int rowsAffected = 0;
            try
            {
                for (int i = 0; i < programmeCourseRegistrations.Count; i++)
                {
                    long id = programmeCourseRegistrations[i].ProgrammeCourse.Id;
                    Expression<Func<PROGRAMME_COURSES, bool>> predicate = m => m.ProgrammeCourseId == id;
                    PROGRAMME_COURSES entity = GetEntityBy(predicate);
                    entity.SemesterId = programmeCourseRegistrations[i].ProgrammeCourse.Semester.Id;
                    rowsAffected += Save();
                }
              
            }
            catch (Exception ex)
            {

                throw;
            }
            return rowsAffected;
        }

        public List<ProgrammeCourse> GetBy(Department department, Level level, Semester semester,Session session)
        {
            try
            {
                Expression<Func<PROGRAMME_COURSES, bool>> selector = c => c.PROGRAMME.DepartmentId == department.Id && c.LevelId == level.Id && c.SemesterId == semester.Id && c.Session_Id == session.Id;
                return base.GetModelsBy(selector);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public List<ProgrammeCourse> GetBy(Department department, Semester semester,Session session)
        {
            try
            {
                Expression<Func<PROGRAMME_COURSES, bool>> selector = c => c.PROGRAMME.DepartmentId == department.Id && c.SemesterId == semester.Id && c.Session_Id == session.Id ;
                return base.GetModelsBy(selector);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public bool Modify(List<ProgrammeCourse> programmeCourse)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (ProgrammeCourse programCourse in programmeCourse)
                    {
                        int modified = 0;
                        var currentProgrammeCourse = programCourse;

                        Expression<Func<PROGRAMME_COURSES, bool>> selector = c => c.ProgrammeCourseId == currentProgrammeCourse.Id;
                        PROGRAMME_COURSES courseEntity = GetEntityBy(selector);

                        CourseLogic courseLogic = new CourseLogic();
                        var course = courseLogic.AddCourse(programCourse.Course);
                      
                          if (courseEntity == null && currentProgrammeCourse.Course.Course_Code != null && currentProgrammeCourse.Course.Name != null)
                        {
                            ProgrammeCourse newCourse = new ProgrammeCourse();
                            newCourse.Course = course;
                            var programme = programmeCourse.FirstOrDefault();
                            if (programme != null) newCourse.Programme = programme.Programme;
                            newCourse.IsRegistered = false;
                            newCourse.Level = currentProgrammeCourse.Level;
                            newCourse.Semester = currentProgrammeCourse.Semester;
                            newCourse.Session = currentProgrammeCourse.Session;
                            newCourse.CourseType = new CourseType() { Id = 1 };
                            newCourse.CourseUnit = currentProgrammeCourse.CourseUnit;
                            newCourse.Activated = true;
                            Create(newCourse);
                        }
                          else
                          {
                              // This condition is to allow medical course to delete because i have a default course code Med 101 for each course
                              if (currentProgrammeCourse.Course.Course_Code != null && currentProgrammeCourse.Course.Course_Code.Contains("Med") && currentProgrammeCourse.Course.Name == null && currentProgrammeCourse.Id > 0 && currentProgrammeCourse.CourseUnit <= 0)
                              {
                                  currentProgrammeCourse.Course.Course_Code = null;
                              }
                              if (currentProgrammeCourse.Course.Course_Code == null && currentProgrammeCourse.Course.Name == null && currentProgrammeCourse.Id > 0 && currentProgrammeCourse.CourseUnit <= 0)
                              {
                                  Delete(selector);
                              }
                              else
                              {
                                  if (courseEntity != null && course != null)
                                  {
                                      courseEntity.COURSE.Title = course.Name;
                                      courseEntity.CourseUnit = currentProgrammeCourse.CourseUnit;
                                      courseEntity.COURSE.Course_Code = course.Course_Code;
                                      modified = Save();
                                  }

                              }

                          }
                    }
                    scope.Complete();
                }
               
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            
        }
    }
}
