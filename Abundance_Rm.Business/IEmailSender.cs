﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Model;

namespace Abundance_Rm.Business
{
    public interface IEmailSender
    {
        string SendEmail(List<TranscriptRequest> transcriptRequests);
    }
}
