﻿using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Business
{
    public class PersonTypeLogic : BaseBusinessLogic<PersonType,PERSON_TYPE>
    {
        public PersonTypeLogic()
        {
            translator = new PersonTypeTranslator();
        }
    }
}
