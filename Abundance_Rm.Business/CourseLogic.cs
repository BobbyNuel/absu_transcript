﻿using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Business
{
    public class CourseLogic : BaseBusinessLogic<Course, COURSE>
    {
        public CourseLogic()
        {
            translator = new CourseTranslator();
        }

        public bool Modify(Course course)
        {

            Expression<Func<COURSE, bool>> selector = a => a.CourseId == course.Id;
            COURSE entity = GetEntityBy(selector);

            entity.CourseId = course.Id;
            entity.Course_Code = course.Course_Code;
            entity.IsRemoveRequested = false;

            int modifiedRecordCount = Save();
            if (modifiedRecordCount <= 0)
            {
                return false;
            }

            return true;
        }



        public Course AddCourse(Course course)
        {
            Course modifiedCourse = new Course();
            try
            {
                Expression<Func<COURSE, bool>> selector = a => a.CourseId == course.Id;
                COURSE entity = GetEntityBy(selector);

                if (course.Id > 0)
                {
                    if (entity == null && course.Course_Code != null && course.Name != null)
                    {
                        Course newCourse = new Course();
                        newCourse.Name = course.Name;
                        newCourse.Course_Code = course.Course_Code;
                        newCourse.IsRemoveRequested = false;
                        modifiedCourse = Create(newCourse);
                    }
                    else if (course.Course_Code != null && course.Name != null)
                    {
                        Modify(course);
                        modifiedCourse = course;
                    }
                }
                else
                {
                    if (entity == null && course.Course_Code != null && course.Name != null)
                    {
                        CourseLogic courseLogic = new CourseLogic();

                        var existingCourseCode =
                            courseLogic.GetModelsBy(
                                s => s.Course_Code.Contains(course.Course_Code) && s.Title.Contains(course.Name)).LastOrDefault();

                        if (existingCourseCode == null)
                        {
                            Course newCourse = new Course();
                            newCourse.Name = course.Name;
                            newCourse.Course_Code = course.Course_Code;
                            newCourse.IsRemoveRequested = false;
                            modifiedCourse = Create(newCourse);
                        }
                        else
                        {
                            modifiedCourse = existingCourseCode;
                        }

                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return modifiedCourse;
        }



    }
}
