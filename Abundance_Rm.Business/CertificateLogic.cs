﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Translator;

namespace Abundance_Rm.Business
{
    public class CertificateLogic:BaseBusinessLogic<Certificate,CERTIFICATE>
    {
        public CertificateLogic()
        {
            translator = new CertificateTranslator();
        }
    }
}
