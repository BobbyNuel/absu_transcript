﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Translator;

namespace Abundance_Rm.Business
{
    public class DegreeAwardsByProgrammeDepartmentLogic : BaseBusinessLogic<DegreeAwardsByProgrammeDepartment,DEGREE_AWARDS_BY_PROGRAMME_DEPARTMENT>
    {
        public DegreeAwardsByProgrammeDepartmentLogic()
        {
            translator = new DegreeAwardsByProgrammeDepartmentTranslator();
        }

        public bool Modify(DegreeAwardsByProgrammeDepartment model)
        {
            Expression<Func<DEGREE_AWARDS_BY_PROGRAMME_DEPARTMENT, bool>> selector = a => a.Id == model.Id;
            DEGREE_AWARDS_BY_PROGRAMME_DEPARTMENT entity = GetEntityBy(selector);

            entity.Department_Id = model.Programme.department.Id;
            entity.Programme_Id = model.Programme.Id;
            entity.Degree_Name = model.DegreeName;
            entity.Duration = model.Duration;

            int modifiedRecordCount = Save();
            if (modifiedRecordCount <= 0)
            {
                return false;
            }

            return true;
        }
    }
}
