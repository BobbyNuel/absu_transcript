﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Translator;

namespace Abundance_Rm.Business
{
    public class StudentLogic:BaseBusinessLogic<Student,STUDENT>
    {
        public StudentLogic()
        {
            translator = new StudentTranslator();
        }

        public List<STUDENT> GetBy(string yearOfGraduation, DEPARTMENT department, PROGRAMME_TYPE programmeType)
        {
            try
            {
                List<STUDENT> studentList = new List<STUDENT>();
                StudentLogic studenLogic = new StudentLogic();
                ProgrammeLogic programmeLogic = new ProgrammeLogic();
                PROGRAMME programme = new PROGRAMME();
                programme = programmeLogic.GetEntityBy(p => p.DepartmentId == department.DepartmentId && p.ProgrammeTypeId == programmeType.ProgrammeTypeId);
                if (programme != null)
                {
                    studentList = studenLogic.GetEntitiesBy(p => p.YearOfGraduation == yearOfGraduation && p.ProgrammeId == programme.ProgrammeId && p.DepartmentId == programme.DepartmentId);
                    return studentList;
                }
                return new List<STUDENT>();
               
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public Student GetBy(string matricNumber)
        {
            try
            {
                Expression<Func<STUDENT, bool>> selector = s => s.EntryRegNo == matricNumber;
                return base.GetModelBy(selector);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Result> GetTranscriptBy(Student student)
        {
            try
            {
                List<Result> results  = new List<Result>();
                if (student == null || student.Id <= 0)
                {
                    throw new Exception("Student not set! Please select student and try again.");
                }
                else if (student.Department.Id == 50)
                {
                    results = (from sr in repository.GetBy<VW_STUDENT_RESULT>(x => x.StudentId == student.Id)
                               select new Result
                               {
                                   StudentId = sr.StudentId,
                                   Sex = sr.Sex,
                                   Name = sr.FullName,
                                   MatricNumber = sr.EntryRegNo,
                                   CourseId = sr.CourseId,
                                   CourseName = sr.CourseName,
                                   CourseUnit = sr.CourseUnit,
                                   FacultyName = sr.FacultyName,
                                   TestScore = sr.TestScore,
                                   ExamScore = sr.ExamScore,
                                   Score = sr.Total,
                                   Grade = sr.Grade,
                                   GradePoint = sr.ExamScore,
                                   Email = sr.EmailAddress,
                                   Address = sr.Adsress,
                                   MobilePhone = sr.PhoneNumber,
                                   PassportUrl = sr.PictureURL,
                                   GPCU = (decimal?)(sr.GradePoint * sr.CourseUnit),
                                   SessionName = sr.SessionName,
                                   SemesterName = sr.SemesterName,
                                   LevelName = sr.LevelName,
                                   ProgrammeTypeName = sr.ProgrammeName,
                                   DepartmentName = sr.DepartmentName,
                                   SessionSemesterId = sr.Session_Semester_Id,
                                   SequenceNumber = sr.Sequence_Number,
                                   SessionId = sr.SessionId,
                                   SemesterId = sr.SemesterId,
                                   LevelId = sr.LevelId,
                                   ProgrammeId = sr.ProgrammeId,
                                   DepartmentId = sr.DepartmentId,
                                   DateOfBirth = sr.Date_Of_Birth,
                                   Country =  sr.Country
                                   
                               }).ToList();
                }
                else
                {
                    results = (from sr in repository.GetBy<VW_STUDENT_RESULT>(x => x.StudentId == student.Id)
                                            select new Result
                                            {
                                                StudentId = sr.StudentId,
                                                Sex = sr.Sex,
                                                Name = sr.FullName,
                                                MatricNumber = sr.EntryRegNo,
                                                CourseId = sr.CourseId,
                                                CourseCode = sr.Course_Code,
                                                CourseName = sr.CourseName,
                                                CourseUnit = sr.CourseUnit,
                                                FacultyName = sr.FacultyName,
                                                TestScore = sr.TestScore,
                                                ExamScore = sr.ExamScore,
                                                Score = sr.Total,
                                                Grade = sr.Grade,
                                                GradePoint = Convert.ToDecimal(sr.GradePoint),
                                                Email = sr.EmailAddress,
                                                Address = sr.Adsress,
                                                MobilePhone = sr.PhoneNumber,
                                                PassportUrl = sr.PictureURL,
                                                GPCU = (decimal?)(sr.GradePoint * sr.CourseUnit),
                                                SessionName = sr.SessionName,
                                                SemesterName = sr.SemesterName,
                                                LevelName = sr.LevelName,
                                                ProgrammeTypeName = sr.ProgrammeName,
                                                DepartmentName = sr.DepartmentName,
                                                SessionSemesterId = sr.Session_Semester_Id,
                                                SequenceNumber = sr.Sequence_Number,
                                                SessionId = sr.SessionId,
                                                SemesterId = sr.SemesterId,
                                                LevelId = sr.LevelId,
                                                ProgrammeId = sr.ProgrammeId,
                                                DepartmentId = sr.DepartmentId,
                                                DateOfBirth = sr.Date_Of_Birth,
                                                cGPA = sr.CGPA
                                            }).ToList();
                }

                

                return results;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string GetGraduationStatus(decimal? CGPA,string graduationDate)
        {
            string remark = null;
            int graduationId = Convert.ToInt32(graduationDate);
            try
            {
                if (CGPA != null && graduationId < 1989)
                {
                    
                    if (CGPA >= (decimal)3.50 && CGPA <= (decimal)4.00)
                    {
                        remark = "FIRST CLASS";
                    }
                    else if (CGPA >= (decimal)3.00 && CGPA <= (decimal)3.49)
                    {
                        remark = "SECOND CLASS (UPPER DIVISION)";
                    }
                    else if (CGPA >= (decimal)2.50 && CGPA <= (decimal)2.99)
                    {
                        remark = "SECOND CLASS (LOWER DIVISION)";
                    }
                    else if (CGPA >= (decimal)2.20 && CGPA <= (decimal)2.49)
                    {
                        remark = "THIRD CLASS";
                    }
                    else if (CGPA >= (decimal)2.00 && CGPA <= (decimal)2.19)
                    {
                        remark = "PASS";
                    }
                    else if (CGPA < (decimal)2.0)
                    {
                        remark = "FAIL";
                    }   
                }
                else
                {
                    if (CGPA >= (decimal)4.5 && CGPA <= (decimal)5.0)
                    {
                        remark = "FIRST CLASS";
                    }
                    else if (CGPA >= (decimal)3.50 && CGPA <= (decimal)4.49)
                    {
                        remark = "SECOND CLASS (UPPER DIVISION)";
                    }
                    else if (CGPA >= (decimal)2.40 && CGPA <= (decimal)3.49)
                    {
                        remark = "SECOND CLASS (LOWER DIVISION)";
                    }
                    else if (CGPA >= (decimal)1.5 && CGPA <= (decimal)2.39)
                    {
                        remark = "THIRD CLASS";
                    }
                    else if (CGPA >= (decimal)1.00 && CGPA <= (decimal)1.49)
                    {
                        remark = "PASS";
                    }
                    else if (CGPA < (decimal)1.0)
                    {
                        remark = "FAIL";
                    }   
                }
          
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return remark;
        }

        public string GetTodaysDateFormat()
        {
            string dateFormat = "";
            try
            {
                DateTime date = DateTime.Today;
                int monthId = date.Month;
                string monthName = DateTimeFormatInfo.CurrentInfo.GetMonthName(monthId);

                //var s = (date.Day%10 == 1 && date.Day != 11)
                //    ? "st"
                //    : (date.Day%10 == 2 && date.Day != 12)
                //        ? "nd"
                //        : (date.Day % 10 == 3 && date.Day != 13)
                //            ? "rd"
                //            : "th";

                dateFormat = monthName + " " + date.Day + "," + " " + date.Year;
            }
            catch (Exception)
            {
                
                throw;
            }
            return dateFormat;
        }


        public bool Modify (Student model)
        {
            try
            {
                Expression<Func<STUDENT, bool>> selector = a => a.StudentId == model.Id;
                STUDENT entity = GetEntityBy(selector);

                entity.YearOfEntry = model.YearOfEntry;
                entity.YearOfGraduation = model.GraduationDate;
                entity.ProgrammeId = model.Programme.Id;
                entity.DepartmentId = model.Department.Id;
                entity.PERSON.FirstName = model.Person.FirstName;
                entity.PERSON.Surname = model.Person.Surname;
                entity.PERSON.Sex = model.Person.Sex;
                entity.PERSON.contact_address = model.Person.contact_address;
                entity.EntryRegNo = !string.IsNullOrEmpty(model.EntryRegNo) ? model.EntryRegNo : entity.EntryRegNo;
                entity.CGPA = !string.IsNullOrEmpty(model.CGPA) ? model.CGPA : entity.CGPA;
                entity.WesVerificationNumber = !string.IsNullOrEmpty(model.WesVerificationNumber) ? model.WesVerificationNumber : null;
                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
