﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Translator;

namespace Abundance_Rm.Business
{
    public class StudentLevelLogic : BaseBusinessLogic<StudentLevel, STUDENT_LEVEL>
    {
        public StudentLevelLogic()
        {
            translator = new StudentLevelTranslator();
        }
        


        public StudentLevel GetBy(Student student, Session session)
        {
            try
            {
                Expression<Func<STUDENT_LEVEL, bool>> selector =
                    sl => sl.Person_Id == student.Id && sl.Session_Id == session.Id;
                return base.GetModelsBy(selector).FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<StudentLevel> GetBy(Level level, Session session)
        {
            try
            {
                Expression<Func<STUDENT_LEVEL, bool>> selector =
                    sl => sl.Level_Id == level.Id && sl.Session_Id == session.Id;
                return base.GetModelsBy(selector);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<StudentLevel> GetBy(Level level, Programme programme, Department department, Session session)
        {
            try
            {
                Expression<Func<STUDENT_LEVEL, bool>> selector =
                    sl =>
                        sl.Level_Id == level.Id && sl.Programme_Type_Id == programme.Id && sl.Department_Id == department.Id &&
                        sl.Session_Id == session.Id;
                return base.GetModelsBy(selector);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Modify(StudentLevel student)
        {
            try
            {
                Expression<Func<STUDENT_LEVEL, bool>> selector = sl => sl.Student_Level_Id == student.Id;
                STUDENT_LEVEL entity = GetEntityBy(selector);

               
                if (student.Level != null && student.Level.Id > 0)
                {
                    entity.Level_Id = student.Level.Id;
                }
                if (student.Session != null && student.Session.Id > 0)
                {
                    entity.Session_Id = student.Session.Id;
                }

                int modifiedRecordCount = Save();

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public StudentLevel GetBy(long studentId,long sessionId)
        {
            try
            {
                var sessionLogic = new SessionLogic();
                Expression<Func<STUDENT_LEVEL, bool>> selector = sl => sl.Person_Id == studentId;
                List<StudentLevel> studentLevels = base.GetModelsBy(selector);
                Session session = sessionLogic.GetModelBy(p => p.SessionId == sessionId);
                if (studentLevels != null && studentLevels.Count > 0)
                {
                    int maxLevel = studentLevels.Max(p => p.Level.Id);
                    Expression<Func<STUDENT_LEVEL, bool>> selector2 =
                        sl => sl.Person_Id == studentId && sl.Level_Id == maxLevel && sl.Session_Id == session.Id;
                    StudentLevel CurrentLevel = base.GetModelBy(selector2);
                    if (CurrentLevel == null)
                    {
                        int minLevel = studentLevels.Min(p => p.Level.Id);
                        Expression<Func<STUDENT_LEVEL, bool>> selector3 =
                            sl => sl.Person_Id == studentId && sl.Level_Id == minLevel;
                        StudentLevel CurrentLevelAlt = base.GetModelBy(selector3);
                        CurrentLevel = CurrentLevelAlt;
                    }
                    return CurrentLevel;
                }

                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}