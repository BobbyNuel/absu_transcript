﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Translator;

namespace Abundance_Rm.Business
{
    public class SessionSemesterLogic : BaseBusinessLogic<SessionSemester, SESSION_SEMESTER>
    {
        public SessionSemesterLogic()
        {
            translator = new SessionSemesterTranslator();
        }
        public SessionSemester GetBy(int id)
        {
            try
            {
                Expression<Func<SESSION_SEMESTER, bool>> selector = s => s.Session_Semester_Id == id;
                return base.GetModelBy(selector);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

}
