﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Translator;

namespace Abundance_Rm.Business
{
    public class TranscriptRequestLogic : BaseBusinessLogic<TranscriptRequest, TRANSCRIPT_REQUEST>
    {
        public TranscriptRequestLogic()
        {
            translator = new TranscriptRequestTranslator();
        }

        public bool Modify(TranscriptRequest model)
        {
            try
            {
                Expression<Func<TRANSCRIPT_REQUEST, bool>> selector = a => a.Transcript_Request_Id == model.Id;
                TRANSCRIPT_REQUEST entity = GetEntityBy(selector);

                entity.Processed_Date = model.ProcessedDate;
                entity.Processed = true;
                entity.Status = "Request Processed";
                entity.Person_Id = model.Person.Id;

                int modifedCount = Save();
                if (modifedCount > 0)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
