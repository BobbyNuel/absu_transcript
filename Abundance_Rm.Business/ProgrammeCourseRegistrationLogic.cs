﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Translator;

namespace Abundance_Rm.Business
{
    public class ProgrammeCourseRegistrationLogic:BaseBusinessLogic<ProgrammeCourseRegistration,PROGRAMME_COURSE_REGISTRATION>
    {
        public ProgrammeCourseRegistrationLogic()
        {
            translator = new ProgrammeCourseRegistrationTranslator();
        }

        public int Modify(List<ProgrammeCourseRegistration> programmeCourseRegistrations)
        {
            int rowsAffected = 0;
            try
            {
                for (int i = 0; i < programmeCourseRegistrations.Count; i++)
                {
                    long id = programmeCourseRegistrations[i].Id;
                    Expression<Func<PROGRAMME_COURSE_REGISTRATION, bool>> predicate = m => m.CourseRegId == id;
                    PROGRAMME_COURSE_REGISTRATION entity = GetEntityBy(predicate);

                    entity.LevelId = programmeCourseRegistrations[i].Level.Id;
                    rowsAffected = Save();
                }
               
            }
            catch (Exception ex)
            {

                throw;
            }
            return rowsAffected;
        }
        public bool Modify(ProgrammeCourseRegistration programmeCourseRegistration)
        {

            try
            {
                Expression<Func<PROGRAMME_COURSE_REGISTRATION, bool>> selector = m => m.CourseRegId == programmeCourseRegistration.Id;
                PROGRAMME_COURSE_REGISTRATION entity = GetEntityBy(selector);

                entity.Approved = programmeCourseRegistration.Approved;
                entity.IsCarriedOverCourses = programmeCourseRegistration.IsCarriedOverCourses;
                int modifiedCount = Save();
                if (modifiedCount == 1)
                {
                    return true;
                }


            }
            catch (Exception ex)
            {

                throw;
            }
            return false;
        }
    }
}
