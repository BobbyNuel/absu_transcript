﻿using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Business
{
    public class SessionLogic : BaseBusinessLogic<Session,SESSION>
    {
        public SessionLogic()
        {
            translator = new SessionTranslator();
        }

        public bool Modify(Session session)
        {

            Expression<Func<SESSION, bool>> selector = a => a.SessionId == session.Id;
            SESSION entity = GetEntityBy(selector);

            entity.SessionId = session.Id;
            entity.Name = session.Name;
            entity.IsRemoveRequested = false;
            entity.IsCurrenSession = true;

            int modifiedRecordCount = Save();
            if (modifiedRecordCount <= 0)
            {
                return false;
            }

            return true;
        }
    }
}
