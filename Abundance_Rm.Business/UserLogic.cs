﻿using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Business
{
    public class UserLogic : BaseBusinessLogic<User, USER>
    {
        public UserLogic()
        {
            translator = new UserTranslator();
        }
        public bool ValidateUser(string Username, string Password)
        {
            try
            {
                Expression<Func<USER, bool>> selector = p => p.User_Name == Username && p.Password == Password;
                User UserDetails = GetModelBy(selector);
                if (UserDetails != null && UserDetails.Password != null)
                {
                    UpdateLastLogin(UserDetails);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool UpdateLastLogin(User user)
        {
            try
            {
                Expression<Func<USER, bool>> selector = p => p.User_Name == user.Username && p.Password == user.Password;
                USER userEntity = GetEntityBy(selector);
                if (userEntity == null || userEntity.User_Id <= 0)
                {
                    throw new Exception(NoItemFound);
                }

                userEntity.User_Name = user.Username;
                userEntity.Password = user.Password;
                userEntity.Email = user.Email;
                userEntity.Role_Id = user.Role.Id;
                userEntity.Person_Id = user.person.Id;
                userEntity.LastLoginDate = DateTime.Now;
                userEntity.activated = user.Activated;
                userEntity.Security_Question_Id = user.SecurityQuestion.Id;
                userEntity.Security_Answer = user.SecurityAnswer;

                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    throw new Exception(NoItemModified);
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool ChangeUserPassword(User user)
        {
            try
            {
                Expression<Func<USER, bool>> selector = p => p.User_Name == user.Username;
                USER userEntity = GetEntityBy(selector);
                if (userEntity == null || userEntity.User_Id <= 0)
                {
                    throw new Exception(NoItemFound);
                }

                userEntity.User_Name = user.Username;
                userEntity.Password = user.Password;
                userEntity.Email = user.Email;
                userEntity.Person_Id = user.person.Id;
                userEntity.Role_Id = user.Role.Id;

                userEntity.LastLoginDate = user.LastLoginDate;

                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    throw new Exception(NoItemModified);
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
        //public override User Create(User user)
        //{
        //    using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
        //    {

        //        User newUser = new User();
        //        PersonType personType = new PersonType() { Id = 1 };
        //        Nationality nationality = new Nationality() { Id = 1 };
        //        State state = new State() { Id = "AB" };
        //        user.person.PersonType = personType;
        //        user.person.Nationality = nationality;
        //        user.person.State = state;
        //        user.person.DateEntered = DateTime.Now;
        //        Person newPerson = new Person();
        //        PersonLogic personLogic = new PersonLogic();
        //        newPerson = personLogic.Create(user.person);
        //        user.person = newPerson;
        //        newUser = base.Create(user);
        //        transaction.Complete();
        //        return newUser;
        //    }
        //}

        public User Update(User model)
        {
            try
            {
                Expression<Func<USER, bool>> selector = a => a.User_Id == model.Id;
                USER entity = GetEntityBy(selector);
                Expression<Func<PERSON, bool>> personSelector = a => a.PersonId == entity.Person_Id;
                PersonLogic personLogic = new PersonLogic();
                PERSON personEntity = personLogic.GetEntityBy(personSelector);

                entity.User_Name = model.Username;
                entity.Password = model.Password;
                entity.Email = model.person.EmailAddress;
                entity.Role_Id = model.Role.Id;
                entity.activated = model.Activated;
                entity.Security_Answer = model.SecurityAnswer;
                entity.Security_Question_Id = model.SecurityQuestion.Id;

                int modifiedRecordCount = Save();
                //if (modifiedRecordCount <= 0)
                //{
                //    throw new Exception(NoItemModified);
                //}

                return model;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
