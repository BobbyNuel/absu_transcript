﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Translator;

namespace Abundance_Rm.Business
{
    public  class StaffLogic : BaseBusinessLogic<Staff,STAFF>
    {
        public StaffLogic()
        {
            translator = new StaffTranslator();
        }

        public bool Modify(Staff staff)
        {
            try
            {
                Expression<Func<STAFF, bool>> selector = a => a.Staff_Id == staff.Id;
                STAFF entity = GetEntityBy(selector);

                entity.Department_Id = staff.Department.Id;
                entity.Activated = staff.Activated;

                int modifiedCount = Save();
                if (modifiedCount > 0)
                {
                    return true;
                }
            }
            catch (Exception)
            {
                
                throw;
            }
            return false;
        }
    }
}
