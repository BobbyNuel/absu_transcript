﻿using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Business
{
    public class FacultyLogic:BaseBusinessLogic<Faculty,FACULTY>
    {
        public FacultyLogic()
        {
            translator = new FacultyTranslator();
        }

        public bool Modify(Faculty faculty)
        {
            Expression<Func<FACULTY, bool>> selector = a => a.FacultyId == faculty.Id;
            FACULTY entity = GetEntityBy(selector);

            entity.FacultyId = faculty.Id;
            entity.Name = faculty.Name;
            entity.FacultyCode = faculty.FacultyCode;
            entity.Description = "NaN";
            entity.IsRemoveRequested = false;

            int modifiedRecordCount = Save();
            if (modifiedRecordCount <= 0)
            {
                return false;
            }

            return true;
        }
    }
}
