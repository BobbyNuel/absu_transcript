﻿using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Rm.Business
{
	public class PersonLogic:BaseBusinessLogic<Person,PERSON>
	{
		public PersonLogic()
		{
			translator = new PersonTranslator();
		}
		public Person Update(User model)
		{
			try
			{
				Expression<Func<USER, bool>> selector = a => a.User_Id == model.Id;
				UserLogic userLogic = new UserLogic();
				USER entity = userLogic.GetEntityBy(selector);
				Expression<Func<PERSON, bool>> personSelector = a => a.PersonId == entity.Person_Id;
				Person person = new Person();
				PERSON personEntity = GetEntityBy(personSelector);

				personEntity.FirstName = model.person.FirstName;
				personEntity.Surname = model.person.Surname;
				personEntity.MiddleName = model.person.MiddleName;
				personEntity.PlaceofBirth = model.person.PlaceofBirth;
				if (model.person.Sex != null) { personEntity.Sex = model.person.Sex; }
				if (model.person.EmailAddress != null) { personEntity.EmailAddress = model.person.EmailAddress; }
				if (model.person.PhoneNumber != null) { personEntity.PhoneNumber = model.person.PhoneNumber; }
				personEntity.Role_Id = model.Role.Id;
				int modifiedRecordCount = Save();
				//if (modifiedRecordCount <= 0)
				//{
				//    throw new Exception(NoItemModified);
				//}
				person = translator.TranslateToModel(personEntity);
				return person;
			}
			catch (Exception)
			{

				throw;
			}

		}
		public List<PersonReport> GetPersonInfromation(STUDENT student, StudentClassListReport StudentCGPA)
		{
			List<PersonReport> personReportList = new List<PersonReport>();
			PersonReport personInformation = new PersonReport();
			PersonLogic personLogic = new PersonLogic();
			PERSON person = new PERSON();
			SponsorLogic sponsorLogic = new SponsorLogic();
			SPONSOR sponsor = new SPONSOR();
			sponsor = sponsorLogic.GetEntityBy(p => p.SponsorId == student.SPONSOR.SponsorId);
			DEPARTMENT department = student.DEPARTMENT;
			person = GetEntityBy(p => p.PersonId == student.StudentId);
			personInformation.Address = person.contact_address;
			personInformation.LastName = person.Surname;
			personInformation.FirstName = person.FirstName;
			personInformation.MiddleName = person.MiddleName;
			personInformation.PlaceOfBirth = person.PlaceofBirth;
			personInformation.RegNo = student.EntryRegNo;
			personInformation.GraduatingTitle = GetGraduatingTitle(StudentCGPA);
			if (student.PROGRAMME.ProgrammeTypeId == 1 || student.PROGRAMME.ProgrammeTypeId == 2 || student.PROGRAMME.ProgrammeTypeId == 3)
			{
				personInformation.ProgrammeCode = "ND";
			}
			else
			{
				personInformation.ProgrammeCode = "HND";
			}
			if (person.COUNTRY != null)
			{
				personInformation.Nationality = person.COUNTRY.Name;
			}
			if (student.DEPARTMENT != null)
			{
				personInformation.Department = student.DEPARTMENT.Name;
				personInformation.Faculty = student.DEPARTMENT.FACULTY.Name;
			}
		   
			personInformation.BasisOfAdmission = student.BasisOfAdmission;
			personInformation.DateOfBirth = person.DateofBirth;
			personInformation.Department = department.Name;
			if (person.MARITAL_STATUS != null)
			{
				personInformation.MaritalStatus = person.MARITAL_STATUS.Name; ;
			}
			if (person.RELIGION != null)
			{
				personInformation.Religion = person.RELIGION.Name;
			}
			
			personInformation.SponsorAddress = sponsor.Address;
			personInformation.SponsorName = sponsor.Name;
			personInformation.SponsorOccupation = sponsor.Occupation;
			if (person.STATE != null)
			{
				personInformation.State = person.STATE.Name;
			}
			
			if (person.LGA != null)
			{
				personInformation.LocalGovernmentArea = person.LGA.Name;
			}
		   
			personInformation.YearOfEntry = student.YearOfEntry;
			personInformation.LastSchoolAttended = student.LastSchoolAttended;
			personInformation.Sex = person.Sex;
			personReportList.Add(personInformation);
			return personReportList;
		}

		private string GetGraduatingTitle(StudentClassListReport StudentCGPA)
		{
			string title = null;
			try
			{
				if (StudentCGPA.CGPA >= (decimal)3.50 && StudentCGPA.CGPA <= (decimal)4.00)
				{
					title = "DISTICTION";
				}
				else if (StudentCGPA.CGPA >= (decimal)3.0 && StudentCGPA.CGPA <= (decimal)3.49)
				{
					title = "UPPER CREDIT";
				}
				else if (StudentCGPA.CGPA >= (decimal)2.5 && StudentCGPA.CGPA <= (decimal)2.99)
				{
					 title = "LOWER CREDIT";
				}
				else if (StudentCGPA.CGPA >= (decimal)2.0 && StudentCGPA.CGPA <= (decimal)2.49)
				{
					title = "PASS";
				}
				else if (StudentCGPA.CGPA < (decimal)2.0)
				{
					title = "POOR";
				}
			}
			catch (Exception)
			{
				
				throw;
			}
			return title;
		}


		public bool Modify(Student model)
		{
			Expression<Func<PERSON, bool>> selector = a => a.PersonId == model.Id;
			PERSON personEntity = GetEntityBy(selector);


			personEntity.FirstName = model.Person.FirstName;
			personEntity.MiddleName = model.Person.MiddleName;
			personEntity.Surname = model.Person.Surname;
			personEntity.CountryId = model.Person.Country.Id;
			personEntity.Sex = model.Person.Sex;
		    personEntity.contact_address = model.Person.contact_address;

			int modified = Save();
			if (modified <= 0)
			{
				return false;
			}
			return true;
		}
        public bool Modify(Person model)
        {
            Expression<Func<PERSON, bool>> selector = a => a.PersonId == model.Id;
            PERSON personEntity = GetEntityBy(selector);


            personEntity.FirstName = model.FirstName;
            personEntity.MiddleName = model.MiddleName;
            personEntity.Surname = model.Surname;
            personEntity.CountryId = model.Country.Id;
            personEntity.Sex = model.Sex;
            personEntity.contact_address = model.contact_address;
            personEntity.ModeOfEntry = model.ModeOfEntry;

            int modified = Save();
            if (modified <= 0)
            {
                return false;
            }
            return true;
        }
    }
}
