﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Abundance_Rm.Startup))]
namespace Abundance_Rm
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
