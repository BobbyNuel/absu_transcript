﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Models;

namespace Abundance_Rm.Areas.Student.ViewModels
{
    public class ResultViewModel
    {
        public ResultViewModel()
        {
            SessionSelectList = Utility.PopulateSessionListItem();
            SemesterSelectList = Utility.PopulateSemesterListItem();
        }
        public string MatricNumber { get; set; }
        public Semester Semester { get; set; }
        public Session Session { get; set; }
        public List<Result> Results { get; set; }
        public List<SelectListItem> SessionSelectList { get; set; }
        public List<SelectListItem> SemesterSelectList { get; set; }  

    }

    



}