﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

using Abundance_Rm.Areas.Student.ViewModels;
using Abundance_Rm.Business;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Controllers;
using System.Threading;

namespace Abundance_Rm.Areas.Student.Controllers
{
    [AllowAnonymous]
    public class ResultController : BaseController
    {
        private ResultViewModel viewModel;
        private StudentLogic studentLogic;

        public ResultController()
        {
            viewModel = new ResultViewModel();
            studentLogic = new StudentLogic();
        }

        //public ActionResult Check()
        //{
        //    viewModel = new ResultViewModel();
        //    ViewBag.Session = viewModel.SessionSelectList;
        //    ViewBag.Semester = viewModel.SemesterSelectList;
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult Check(ResultViewModel vModel)
        //{
        //    try
        //    {
        //        if (vModel.Semester.Id > 0 && vModel.Session.Id > 0 && vModel.MatricNumber != null)
        //        {
        //            Model.Model.Student student = studentLogic.GetBy(vModel.MatricNumber);
        //            if (student != null && student.Id > 0)
        //            {
        //                StudentResultLogic studentResultLogic = new StudentResultLogic();
        //                StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
        //                Semester semester = new Semester(){Id = vModel.Semester.Id};
        //                StudentLevel studentLevel = studentLevelLogic.GetModelBy(sl => sl.Person_Id == student.Id && sl.Session_Id == vModel.Session.Id);

        //                if (studentLevel != null)
        //                {
        //                    vModel.Results = studentResultLogic.GetSingleStudentResult(student, studentLevel.Session, semester, studentLevel.Level, studentLevel.Programme, studentLevel.Department);
        //                    vModel.StudentLevel = studentLevel;
        //                }
        //                else
        //                {
        //                    SetMessage("No Student Level Record!", Message.Category.Error);
        //                    ViewBag.Session = viewModel.SessionSelectList;
        //                    ViewBag.Semester = viewModel.SemesterSelectList;
        //                    return View("Check");
        //                }
                        
        //            }
        //            else
        //            {
        //                SetMessage("Invalid Matric Number!", Message.Category.Error);
        //                return RedirectToAction("Check");
        //            }
                    
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
        //    }

        //    ViewBag.Session = viewModel.SessionSelectList;
        //    ViewBag.Semester = viewModel.SemesterSelectList;
        //    return View(vModel);
        //}
        public ActionResult Check()
        {
            try
            {
                viewModel = new ResultViewModel();
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = viewModel.SemesterSelectList;
            }
            catch (Exception ex)
            {
               SetMessage("Error! " + ex.Message, Message.Category.Error); 
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult Check(ResultViewModel vModel)
        {
            try
            {
                if (vModel.Semester.Id > 0 && vModel.Session.Id > 0 && vModel.MatricNumber != null )
                {
                    Model.Model.Student student = studentLogic.GetBy(vModel.MatricNumber);
                    if (student != null && student.Id > 0)
                    {
                        Semester semester = new Semester() { Id = vModel.Semester.Id };
                        ;
                            return RedirectToAction("ViewResult",
                                new
                                {
                                    studentId = student.Id,
                                    sessionId = vModel.Session.Id,
                                    semesterId = semester.Id,
                                    levelId = student.CurrentLevel.Id,
                                    programmeId = student.Programme.ProgrammeType.Id,
                                    departmentId = student.Department.Id,
                                });
                    }
                    else
                    {
                        SetMessage("No Student found for this Matric Number!", Message.Category.Error);
                        ViewBag.Session = viewModel.SessionSelectList;
                        ViewBag.Semester = viewModel.SemesterSelectList;
                        return View("Check", viewModel);
                    }

                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            ViewBag.Session = viewModel.SessionSelectList;
            ViewBag.Semester = viewModel.SemesterSelectList;
            return View(vModel);
        }

        public ActionResult ViewResult(long studentId, int sessionId, int semesterId, int levelId, int programmeId, int departmentId)
        {
            try
            {
                viewModel = new ResultViewModel();
                StudentCourseMarkLogic studentCourseMarkLogic = new StudentCourseMarkLogic();

                Model.Model.Session session = new Session() { Id = sessionId };
                Semester semester = new Semester() { Id = semesterId };
                Level level = new Level() { Id = levelId };
                ProgrammeType programme = new ProgrammeType() { Id = programmeId };
                Department department = new Department() { Id = departmentId };
                Model.Model.Student student = new Model.Model.Student() { Id = studentId };
                viewModel.Results = studentCourseMarkLogic.GetSingleStudentResult(student, session, semester, level, programme, department);
                
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }

     
        [AllowAnonymous]
        public ActionResult VerifyTranscript(string id)
        {
            if (!id.Contains('_'))
            {
                throw new ArgumentNullException("No Result Found Please Try Again");
            }
            ViewBag.StudentId = id;
            return View();
        }
    }
}