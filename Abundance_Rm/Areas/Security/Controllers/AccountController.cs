﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Web.Security;
using Abundance_Rm.Models;
using Abundance_Rm.Controllers;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Business;

namespace Abundance_Rm.Areas.Security.Controllers
{

    public class AccountController : BaseController
    {
        public ActionResult Home()
        {
            return View();
        }

        public ActionResult ChangePassword()
        {
            ManageUserViewModel manageUserviewModel = new ManageUserViewModel();

            try
            {
                ViewBag.UserId = User.Identity.Name;
                manageUserviewModel.Username = User.Identity.Name;
            }
            catch (Exception)
            {
                throw;
            }
            return View(manageUserviewModel);
        }
        [HttpPost]
        public ActionResult ChangePassword(ManageUserViewModel manageUserviewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    UserLogic userLogic = new UserLogic();
                    User LoggedInUser = new User();
                    LoggedInUser = userLogic.GetModelBy(u => u.User_Name == manageUserviewModel.Username && u.Password == manageUserviewModel.OldPassword);
                    if (LoggedInUser != null)
                    {
                        LoggedInUser.Password = manageUserviewModel.NewPassword;
                        userLogic.ChangeUserPassword(LoggedInUser);
                        TempData["Message"] = "Password Changed successfully! Please keep password in a safe place";
                        return RedirectToAction("Home", "Account", new { Area = "Security" });
                    }
                    else
                    {
                        SetMessage("Please log off and log in then try again.", Message.Category.Error);
                    }

                    return View(manageUserviewModel);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured! " + ex.Message, Message.Category.Error);
            }

            return View();
        }

        [AllowAnonymous]
        public ActionResult Login(string ReturnUrl)
        {
            ViewBag.ReturnUrl = ReturnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginViewModel viewModel, string returnUrl)
        {
            try
            {
                UserLogic userLogic = new UserLogic();
                if (userLogic.ValidateUser(viewModel.UserName, viewModel.Password))
                {
                    FormsAuthentication.SetAuthCookie(viewModel.UserName, false);
                    string name = User.Identity.Name;
                    if (string.IsNullOrEmpty(returnUrl))
                    {
                        return RedirectToAction("Home", "Account", new { Area = "Security" });
                    }
                    else
                    {
                        return RedirectToLocal(returnUrl);
                    }
                }
                else
                {
                    SetMessage("Invalid Username or Password!", Message.Category.Error);
                    return View();
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
            return View();

        }

        [HttpPost]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account", new { Area = "Security" });
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

    }
}