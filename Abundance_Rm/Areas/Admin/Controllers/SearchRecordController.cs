﻿using Abundance_Rm.Areas.Admin.ViewModel;
using Abundance_Rm.Business;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Abundance_Rm.Areas.Admin.Controllers
{
    public class SearchRecordController : Controller
    {
        TranscriptViewModel viewModel;
        // GET: Admin/SearchRecord
        public ActionResult StudentDisplay()
        {  
          return View();
        }

        [HttpPost]
        public ActionResult StudentDisplay(TranscriptViewModel model)
        {
            
            try
            {
                viewModel = new TranscriptViewModel();
                StudentLogic studentLogic = new StudentLogic();
                List<STUDENT> studentList = studentLogic.GetEntitiesBy(p => p.EntryRegNo == model.MatricNumber);
                viewModel.StudentList = studentList;
                return View(viewModel);
            }
            catch (Exception)
            {
                
                throw;
            }
        }
    }
}