﻿using System.Web.UI.WebControls;
using Abundance_Nk.Business;
using Abundance_Rm.Areas.Admin.ViewModel;
using Abundance_Rm.Business;
using Abundance_Rm.Controllers;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Abundance_Rm.Models;


namespace Abundance_Rm.Areas.Admin.Controllers
{
    //[Authorize(Roles = "Admin,Records")]
    [Authorize]
    public class UploadResultController : BaseController
    {
        public const string ID = "Id";
        public const string NAME = "Name";
        NEKEDE_RESULT_MANAGEREntities db = new NEKEDE_RESULT_MANAGEREntities();

        public ActionResult DownloadResultSheet()
        {
            try
            {
                GridView gv = new GridView();
                DataTable ds = new DataTable();
                List<ResultFormat> resultFormatList = new List<ResultFormat>();

                int count = 1;
                for (int i = 0; i < 150; i++)
                {
                    ResultFormat resultFormat = new ResultFormat();
                    resultFormat.SN = count;
                    if (i == 0)
                    {
                        resultFormat.CourseCode = "GNS101";
                        resultFormat.Department = "Accountancy";
                        resultFormat.Exam_Score = "45";
                        resultFormat.Name = "Benard Benbela";
                        resultFormat.Level = "YR1";
                        resultFormat.Matric_NO = "15/12345/REGULAR";
                        resultFormat.Programme = "REG";
                        resultFormat.Semester = "First Semester";
                        resultFormat.Session = "2015/2016";
                        resultFormat.Test_Score = "30";

                    }
                    resultFormatList.Add(resultFormat);
                    count++;
                }

                gv.AutoGenerateColumns = true;
                gv.DataSource = resultFormatList;
                gv.DataBind();

                return new DownloadFileActionResult(gv, "ResultSheet.xls");
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Error! " + ex.Message;
            }

            return View();
        }

        public ActionResult DownloadSingleStudentFormat()
        {
            try
            {
                List<AddSingleStudentFormat> resultFormatList = new List<AddSingleStudentFormat>();

                AddSingleStudentFormat studentFormat = new AddSingleStudentFormat
                {
                    SN = 1,
                    Level = "YR1",
                    Session = "2015/2016",
                    Semester = 1,
                    CourseCode = "GNS 111 ",
                    CourseTitle = "CITIZENSHIP EDUCATION",
                    CourseUnit = 2,
                    GradeScore = "A"
                };

                resultFormatList.Add(studentFormat);

                IExcelManager excelServiceManager = new ExcelManager();
                MemoryStream ms = excelServiceManager.DownloadExcel(resultFormatList);
                ms.WriteTo(System.Web.HttpContext.Current.Response.OutputStream);
                System.Web.HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename = SingleStudentSheet.xlsx");
                System.Web.HttpContext.Current.Response.StatusCode = 200;
                System.Web.HttpContext.Current.Response.End();

            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return View();
        }

        public ActionResult DownloadMultipleStudentFormat()
        {
            try
            {
                List<AddMultipleStudentFormat> resultFormatList = new List<AddMultipleStudentFormat>();
                AddMultipleStudentFormat studentFormat = new AddMultipleStudentFormat
                {
                    SN = 1,
                    Sex = "M",
                    DateOfBirth = "19/10/1991",
                    Nationality = "NIGERIAN",
                    YearOfEntry = "2010/2011",
                    YearOfGraduation = "2015/2016",
                    Address = "The Secretary, School of Postgraduate Studies,Abia State University,Uturu.",
                    Session = "2015/2016",
                    Level = "YR1",
                    Semester = 1,
                    CourseCode = "GNS 111 ",
                    CourseTitle = "CITIZENSHIP EDUCATION",
                    CourseUnit = 2,
                    GradeScore = "A"
                };

                resultFormatList.Add(studentFormat);

                IExcelManager excelServiceManager = new ExcelManager();
                MemoryStream ms = excelServiceManager.DownloadExcel(resultFormatList);
                ms.WriteTo(System.Web.HttpContext.Current.Response.OutputStream);
                System.Web.HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename= AddMultipleStuentSheet.xlsx");
                System.Web.HttpContext.Current.Response.StatusCode = 200;
                System.Web.HttpContext.Current.Response.End();
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return View();
        }
        public ActionResult DownloadMedicalExcelFormat()
        {
            try
            {

                List<MedicalStudentFormat> resultFormatList = new List<MedicalStudentFormat>();
                MedicalStudentFormat studentFormat = new MedicalStudentFormat { SN = 1 };
                {
                    studentFormat.SN = 1;
                    studentFormat.MBBS = 1;
                    studentFormat.CourseTitle = "INTRODUCTION TO Medicine";
                    studentFormat.Grade = 70;

                }
                resultFormatList.Add(studentFormat);

                IExcelManager excelServiceManager = new ExcelManager();
                MemoryStream ms = excelServiceManager.DownloadExcel(resultFormatList);
                ms.WriteTo(System.Web.HttpContext.Current.Response.OutputStream);
                System.Web.HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename = MedicalsStudents.xlsx");
                System.Web.HttpContext.Current.Response.StatusCode = 200;
                System.Web.HttpContext.Current.Response.End();


                //return new DownloadFileActionResult(gv, "MedicalsStudents.xls");
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return View();
        }

        public ActionResult UploadStudentResult()
        {
            try
            {
                UploadResultViewModel uploadResultViewModel = new UploadResultViewModel();
                ViewBag.ProgrammeTypeId = uploadResultViewModel.ProgrammeTypeSelectListItem;
                ViewBag.DepartmentId = new SelectList(new List<Department>(), ID, NAME);
                return View();
            }
            catch (Exception)
            {

                throw;
            }

        }

        [HttpPost]
        [NoAsyncTimeout]
        public async Task<ActionResult> UploadStudentResult(UploadResultViewModel viewModel)
        {

            try
            {

                List<StudentData> studentDataList = new List<StudentData>();
                List<string> regNos = new List<string>();
                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    string pathForSaving = Server.MapPath("~/Content/ExcelUploads");

                    FileInfo fileInfo = new FileInfo(hpf.FileName);
                    string fileExtension = fileInfo.Extension;
                    string newFile = hpf.FileName + "__";
                    string newFileName = newFile +
                                         DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") +
                                         fileExtension;

                    string savedFileName = Path.Combine(pathForSaving, newFileName);
                    hpf.SaveAs(savedFileName);
                    DataSet studentList = await ReadExcel(savedFileName);
                    viewModel.FileUrl = savedFileName;

                    if (studentList != null && studentList.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < studentList.Tables[0].Rows.Count; i++)
                        {
                            StudentData studentData = new StudentData();
                            studentData.SN = studentList.Tables[0].Rows[i][0].ToString().Trim();
                            studentData.RegNo = studentList.Tables[0].Rows[i][1].ToString().Trim();
                            studentData.LastName = studentList.Tables[0].Rows[i][2].ToString().Trim();
                            studentData.FirstName = studentList.Tables[0].Rows[i][3].ToString().Trim();
                            studentData.MiddleName = studentList.Tables[0].Rows[i][4].ToString().Trim();
                            studentData.Sex = new Sex { Name = studentList.Tables[0].Rows[i][5].ToString().Trim() };
                            studentData.DateOfBirth = studentList.Tables[0].Rows[i][6].ToString().Trim();
                            studentData.Nationality = studentList.Tables[0].Rows[i][7].ToString().Trim();
                            studentData.YearOfEntry = studentList.Tables[0].Rows[i][8].ToString().Trim();
                            studentData.YearOfGraduation = studentList.Tables[0].Rows[i][9].ToString().Trim();
                            studentData.Address = studentList.Tables[0].Rows[i][10].ToString().Trim();
                            studentData.Level = studentList.Tables[0].Rows[i][11].ToString().Trim();
                            studentData.Session = studentList.Tables[0].Rows[i][12].ToString().Trim();
                            studentData.Semester = studentList.Tables[0].Rows[i][13].ToString().Trim();
                            studentData.CourseCode = studentList.Tables[0].Rows[i][14].ToString().Trim();
                            studentData.CourseTitle = studentList.Tables[0].Rows[i][15].ToString().Trim();
                            studentData.CreditUnit = studentList.Tables[0].Rows[i][16].ToString().Trim();
                            studentData.Grade = studentList.Tables[0].Rows[i][17].ToString().Trim();
                            studentDataList.Add(studentData);
                            regNos.Add(studentData.RegNo);

                        }

                        List<string> newRegNos = regNos.Distinct().ToList();
                        List<StudentData> studentDataSortedList = new List<StudentData>();
                        for (int i = 0; i < newRegNos.Count(); i++)
                        {
                            if (!string.IsNullOrEmpty(newRegNos[i]))
                            {
                                string regNo = newRegNos[i];
                                StudentData studentData = new StudentData();
                                studentData = studentDataList.Where(p => p.RegNo == regNo).FirstOrDefault();
                                studentDataSortedList.Add(studentData);
                            }

                        }
                        viewModel.StudentDataList = studentDataSortedList;
                        viewModel.AllStudentDataList = studentDataList;
                        viewModel.NewRegNos = newRegNos;
                        TempData["UploadResultViewModel"] = viewModel;
                        keepDropDownState(viewModel);

                    }
                }

            }
            catch (Exception ex)
            {
                keepDropDownState(viewModel);
                SetMessage("Error Occured " + ex.Message, Message.Category.Error);
                return View();
            }

            return View(viewModel);
        }

        public ActionResult StaffResultSheet()
        {
            UploadResultViewModel viewModel = new UploadResultViewModel();
            ViewBag.Session = viewModel.SessionSelectList;
            ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
            ViewBag.Level = viewModel.LevelSelectList;
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult StaffResultSheet(UploadResultViewModel viewModel)
        {
            try
            {
                CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
                UserLogic userLogic = new UserLogic();
                Model.Model.User user = userLogic.GetModelBy(u => u.User_Name == User.Identity.Name);
                if (user.Role.Id == 2 || user.Role.Id == 7)
                {
                    viewModel.CourseAllocations =
                        courseAllocationLogic.GetModelsBy(
                            p =>
                                p.Level_Id == viewModel.Level.Id && p.Semester_Id == viewModel.Semester.Id &&
                                p.Session_Id == viewModel.Session.Id);
                }
                else
                {
                    viewModel.CourseAllocations =
                        courseAllocationLogic.GetModelsBy(
                            p =>
                                p.Level_Id == viewModel.Level.Id && p.Semester_Id == viewModel.Semester.Id &&
                                p.Session_Id == viewModel.Session.Id && p.USER.User_Name == User.Identity.Name);
                }

                RetainDropDownState(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
            return View(viewModel);
        }

        public ActionResult ResultUploadSheet(UploadResultViewModel viewModel)
        {
            try
            {
                CourseAllocation courseAllocation = new CourseAllocation();
                CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
                courseAllocation =
                    courseAllocationLogic.GetModelBy(p => p.Course_Allocation_Id == viewModel.CourseAllocationId);

                List<ResultFormat> resultFormatList = new List<ResultFormat>();
                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    string pathForSaving = Server.MapPath("~/Content/ExcelUploads");
                    string savedFileName = Path.Combine(pathForSaving, hpf.FileName);
                    hpf.SaveAs(savedFileName);
                    DataSet studentSet = ReadExcelResult(savedFileName);

                    if (studentSet != null && studentSet.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < studentSet.Tables[0].Rows.Count; i++)
                        {
                            ResultFormat resultFormat = new ResultFormat();
                            resultFormat.SN = Convert.ToInt32(studentSet.Tables[0].Rows[i][0].ToString().Trim());
                            resultFormat.Name = studentSet.Tables[0].Rows[i][1].ToString().Trim();
                            resultFormat.Matric_NO = Convert.ToString(studentSet.Tables[0].Rows[i][2].ToString().Trim());
                            resultFormat.Test_Score = Convert.ToString(studentSet.Tables[0].Rows[i][3].ToString().Trim());
                            resultFormat.Exam_Score = Convert.ToString(studentSet.Tables[0].Rows[i][4].ToString().Trim());
                            resultFormat.Programme = Convert.ToString(studentSet.Tables[0].Rows[i][5].ToString().Trim());
                            resultFormat.Department = Convert.ToString(studentSet.Tables[0].Rows[i][6].ToString().Trim());
                            resultFormat.Level = Convert.ToString(studentSet.Tables[0].Rows[i][7].ToString().Trim());
                            resultFormat.Session = Convert.ToString(studentSet.Tables[0].Rows[i][8].ToString().Trim());
                            resultFormat.Semester = Convert.ToString(studentSet.Tables[0].Rows[i][9].ToString().Trim());
                            resultFormat.CourseCode =
                                Convert.ToString(studentSet.Tables[0].Rows[i][10].ToString().Trim());

                            if (resultFormat.Matric_NO != "")
                            {
                                resultFormatList.Add(resultFormat);
                            }

                        }

                        resultFormatList.OrderBy(p => p.Matric_NO).ToList();
                        viewModel.resultFormatList = resultFormatList;
                        viewModel.CourseAllocation = courseAllocation;
                        TempData["UploadResultViewModel"] = viewModel;
                        string uploadedFileUrl = "/Content/ExcelUploads/" + hpf.FileName;
                        TempData["UploadedFileUrl"] = uploadedFileUrl;
                    }

                }
            }
            catch (Exception ex)
            {
                SetMessage("Error occurred! " + ex.Message, Message.Category.Error);
            }
            RetainDropDownState(viewModel);
            return View(viewModel);
        }

        //public ActionResult SaveUploadedResult()
        //{
        //    try
        //    {
        //        UploadResultViewModel uploadResultViewModel = new UploadResultViewModel();
        //        uploadResultViewModel = (UploadResultViewModel)TempData["UploadResultViewModel"];
        //        PROGRAMME programme = new PROGRAMME();
        //        ProgrammeLogic programmeLogic = new ProgrammeLogic();

        //        programme =
        //       programmeLogic.GetEntityBy(
        //           p =>
        //               p.ProgrammeTypeId == uploadResultViewModel.CourseAllocation.ProgrammeType.Id &&
        //               p.DepartmentId == uploadResultViewModel.CourseAllocation.Department.Id);


        //        DepartmentLogic departmentLogic = new DepartmentLogic();
        //        DEPARTMENT department = new DEPARTMENT();

        //        department =
        //       departmentLogic.GetEntityBy(
        //           p => p.DepartmentId == uploadResultViewModel.CourseAllocation.Department.Id);



        //        foreach (ResultFormat resultFormat in uploadResultViewModel.resultFormatList)
        //        {
        //            using (TransactionScope scope = new TransactionScope())
        //            {
        //                STUDENT student = new STUDENT();
        //                if (resultFormat.Matric_NO != null || resultFormat.Matric_NO != "")
        //                {
        //                    StudentLogic studentLogic = new StudentLogic();


        //                    student = studentLogic.GetEntityBy(p => p.EntryRegNo == resultFormat.Matric_NO);
        //                    PERSON person = new PERSON();
        //                    if (student == null)
        //                    {
        //                        PersonLogic personLogic = new PersonLogic();
        //                        STATE state = new STATE() { StateId = "AB" };
        //                        LgaLogic lgaLogic = new LgaLogic();
        //                        COUNTRY country = new COUNTRY() { CountryId = 130 };
        //                        PERSON_TYPE personType = new PERSON_TYPE() { PersonTypeId = 2 };
        //                        LGA lga = new LGA() { LGAId = 1 };
        //                        MaritalStatusLogic maritalStatusLogic = new MaritalStatusLogic();
        //                        MARITAL_STATUS maritalStatus = new MARITAL_STATUS() { Marital_StatusId = 1 };
        //                        ReligionLogic religionLogic = new ReligionLogic();
        //                        RELIGION religion = new RELIGION() { ReligionId = 1 };
        //                        STUDENT_CATEGORY studentCategory = new STUDENT_CATEGORY() { CategoryId = 1 };
        //                        StateLogic stateLogic = new StateLogic();
        //                        SPONSOR sponsor = new SPONSOR();
        //                        SponsorLogic sponsorLogic = new SponsorLogic();

        //                        person.CountryId = country.CountryId;
        //                        person.DateFilled = DateTime.Now;
        //                        person.DateofBirth = "";
        //                        string[] nameArray = resultFormat.Name.Split(' ');
        //                        if (nameArray.Count() > 1)
        //                        {
        //                            person.FirstName = nameArray[1];
        //                        }
        //                        person.LGAId = lga.LGAId;
        //                        person.Marital_StatusId = maritalStatus.Marital_StatusId;
        //                        if (nameArray.Count() > 2)
        //                        {
        //                            person.MiddleName = nameArray[2];
        //                        }
        //                        person.PersonTypeId = personType.PersonTypeId;
        //                        person.PlaceofBirth = "";
        //                        person.ReligionId = religion.ReligionId;
        //                        person.Sex = "";
        //                        person.StateId = state.StateId;
        //                        person.contact_address = "";
        //                        person.Surname = nameArray[0];
        //                        person = db.PERSON.Add(person);
        //                        db.SaveChanges();



        //                        student = new STUDENT();
        //                        student.StudentId = person.PersonId;
        //                        student.ProgrammeId = programme.ProgrammeId;
        //                        student.YearOfEntry = "";
        //                        student.DepartmentId = department.DepartmentId;
        //                        student.CurrentSessionId = uploadResultViewModel.CourseAllocation.Session.Id;
        //                        student.CurrentLevelId = uploadResultViewModel.CourseAllocation.Level.Id;
        //                        student.CategoryId = studentCategory.CategoryId;
        //                        student.EntryRegNo = resultFormat.Matric_NO;
        //                        student.BasisOfAdmission = "";
        //                        student.LastSchoolAttended = "";
        //                        student.StudentSponsorId = null; //sponsor.SponsorId;
        //                        student = db.STUDENT.Add(student);
        //                        db.SaveChanges();


        //                    }


        //                    if (uploadResultViewModel.CourseAllocation != null)
        //                    {
        //                        CourseLogic courseLogic = new CourseLogic();
        //                        COURSE course = new COURSE();
        //                        //COURSE_TYPE courseType = new COURSE_TYPE() {CourseTypeId = 2};
        //                        course =
        //                            courseLogic.GetEntityBy(
        //                                p => p.CourseId == uploadResultViewModel.CourseAllocation.Course.Id);
        //                        PROGRAMME_COURSES programmeCourse = new PROGRAMME_COURSES();
        //                        ProgrammeCourseLogic programmeCourseLogic = new ProgrammeCourseLogic();

        //                        programmeCourse =
        //                            programmeCourseLogic.GetEntityBy(
        //                                p =>
        //                                    p.ProgrammeId == programme.ProgrammeId && p.CourseId == course.CourseId &&
        //                                    p.SemesterId == uploadResultViewModel.CourseAllocation.Semester.Id &&
        //                                    p.LevelId == uploadResultViewModel.CourseAllocation.Level.Id);

        //                        PROGRAMME_COURSE_REGISTRATION programmeCourseRegistration =
        //                            new PROGRAMME_COURSE_REGISTRATION();
        //                        SESSION_SEMESTER sessionSemester = new SESSION_SEMESTER();
        //                        ProgrammeCourseRegistrationLogic programmeCourseRegistrationLogic =
        //                            new ProgrammeCourseRegistrationLogic();
        //                        SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
        //                        if (programmeCourse != null)
        //                        {
        //                            sessionSemester =
        //                                sessionSemesterLogic.GetEntityBy(
        //                                    ss =>
        //                                        ss.Session_Id == uploadResultViewModel.CourseAllocation.Session.Id &&
        //                                        ss.Semester_Id == uploadResultViewModel.CourseAllocation.Session.Id);
        //                            programmeCourseRegistration =
        //                                programmeCourseRegistrationLogic.GetEntityBy(
        //                                    p =>
        //                                        p.ProgrammeCourseId == programmeCourse.ProgrammeCourseId &&
        //                                        p.StudentId == student.StudentId &&
        //                                        p.SessionSemesterId == sessionSemester.Session_Semester_Id);
        //                            if (programmeCourseRegistration == null)
        //                            {
        //                                programmeCourseRegistration = new PROGRAMME_COURSE_REGISTRATION();
        //                                programmeCourseRegistration.ProgrammeCourseId =
        //                                    programmeCourse.ProgrammeCourseId;
        //                                programmeCourseRegistration.StudentId = student.StudentId;
        //                                programmeCourseRegistration.IsCarriedOverCourses = false;
        //                                programmeCourseRegistration.SessionSemesterId =
        //                                    sessionSemester.Session_Semester_Id;
        //                                // programmeCourseRegistration.SemesterId = uploadResultViewModel.CourseAllocation.Semester.Id;
        //                                programmeCourseRegistration.LevelId =
        //                                    uploadResultViewModel.CourseAllocation.Level.Id;
        //                                programmeCourseRegistration.CourseUnit = programmeCourse.CourseUnit;
        //                                programmeCourseRegistration.DateReg = DateTime.Now;
        //                                programmeCourseRegistration.DeptCourse = "";
        //                                programmeCourseRegistration =
        //                                    db.PROGRAMME_COURSE_REGISTRATION.Add(programmeCourseRegistration);
        //                                db.SaveChanges();

        //                                if (programmeCourseRegistration != null)
        //                                {
        //                                    STUDENT_COURSE_MARK studentCourseMark = new STUDENT_COURSE_MARK();
        //                                    StudentCourseMarkLogic studentCourseMarkLogic = new StudentCourseMarkLogic();
        //                                    studentCourseMark.CourseUnit = programmeCourseRegistration.CourseUnit;
        //                                    studentCourseMark.DateRecorded = DateTime.Now;
        //                                    studentCourseMark.ExamScore = Convert.ToDecimal(resultFormat.Exam_Score);
        //                                    studentCourseMark.TestScore = Convert.ToDecimal(resultFormat.Test_Score);
        //                                    studentCourseMark.Total = studentCourseMark.ExamScore +
        //                                                              studentCourseMark.TestScore;
        //                                    studentCourseMark.Grade = GetGrade(studentCourseMark.Total);
        //                                    studentCourseMark.GradePoint = getGradePoint(studentCourseMark.Grade);
        //                                    studentCourseMark.CourseRegId = programmeCourseRegistration.CourseRegId;
        //                                    studentCourseMark.IsMarkOmitted = true;
        //                                    studentCourseMark = db.STUDENT_COURSE_MARK.Add(studentCourseMark);
        //                                    db.SaveChanges();
        //                                }

        //                            }
        //                        }

        //                    }
        //                }


        //                SetMessage("Result Upload Successful", Message.Category.Information);
        //                scope.Complete();
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        SetMessage("Error! " + ex.Message, Message.Category.Error);
        //    }

        //    return RedirectToAction("StaffResultSheet");
        //}

        private string GetGrade(decimal? total)
        {
            try
            {
                string grade = "";
                if (total != null)
                {
                    if (total >= 70M && total <= 100M)
                    {
                        grade = "A";
                    }
                    if (total >= 60M && total <= 69M)
                    {
                        grade = "B";
                    }
                    if (total >= 50M && total <= 59M)
                    {
                        grade = "C";
                    }
                    if (total >= 45M && total <= 49M)
                    {
                        grade = "D";
                    }
                    if (total >= 40M && total <= 44M)
                    {
                        grade = "E";
                    }
                    if (total >= 0 && total <= 39M)
                    {
                        grade = "D";
                    }
                }

                return grade;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void RetainDropDownState(UploadResultViewModel viewModel)
        {
            try
            {
                ViewBag.ProgrammeType = viewModel.ProgrammeTypeSelectListItem;
                if (viewModel.ProgrammeType != null && viewModel.Department != null)
                {
                    DepartmentLogic departmentLogic = new DepartmentLogic();
                    List<DEPARTMENT> departmentList = departmentLogic.GetBy(viewModel.ProgrammeType);
                    ViewBag.Department = new SelectList(departmentList, ID, NAME, viewModel.Department.DepartmentId);
                }
                else
                {
                    ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
                }
                ViewBag.Session = viewModel.SessionSelectList;
                if (viewModel.Session != null && viewModel.Semester != null)
                {
                    List<SessionSemester> sessionSemesterList = new List<SessionSemester>();
                    SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                    sessionSemesterList = sessionSemesterLogic.GetModelsBy(p => p.Session_Id == viewModel.Session.Id);

                    List<Semester> semesters = new List<Semester>();
                    foreach (SessionSemester sessionSemester in sessionSemesterList)
                    {
                        semesters.Add(sessionSemester.Semester);
                    }
                    ViewBag.Semester = new SelectList(semesters, "Id", "Name", viewModel.Semester.Id);
                }
                else
                {
                    ViewBag.Semester = new SelectList(new List<Semester>(), "Id", "Name");
                }
                ViewBag.Level = viewModel.LevelSelectList;
                if (viewModel.Level != null && viewModel.Semester != null && viewModel.Course != null)
                {
                    List<Course> courseList = Utility.GetCoursesByLevelDepartmentAndSemester(viewModel.Level,
                        viewModel.DepartmentModel, viewModel.Semester);
                    ViewBag.Course = new SelectList(courseList, "Id", "CodeName", viewModel.Course.Id);
                }
                else
                {
                    ViewBag.Course = new SelectList(new List<Course>(), "Id", "CodeName");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult SaveUpload()
        {
            try
            {
                UploadResultViewModel uploadResultViewModel = new UploadResultViewModel();
                uploadResultViewModel = (UploadResultViewModel)TempData["UploadResultViewModel"];
                Programme programme = new Programme();
                ProgrammeLogic programmeLogic = new ProgrammeLogic();
                CourseLogic courseLogic = new CourseLogic();
                programme = programmeLogic.GetModelBy(p => p.ProgrammeTypeId == uploadResultViewModel.ProgrammeType.ProgrammeTypeId && p.DepartmentId == uploadResultViewModel.Department.DepartmentId);
                DepartmentLogic departmentLogic = new DepartmentLogic();
                Department department = new Department();
                department = departmentLogic.GetModelBy(p => p.DepartmentId == uploadResultViewModel.Department.DepartmentId);
                if (department != null && programme != null)
                {
                    List<string> exisitngResult = new List<string>();
                    string failedMatricNumber = null;

                    for (int i = 0; i < uploadResultViewModel.NewRegNos.Count(); i++)
                    {
                        List<StudentData> sort =
                            uploadResultViewModel.AllStudentDataList.Where(
                                p => p.RegNo == uploadResultViewModel.NewRegNos[i])
                                .ToList();
                        Model.Model.Student student = new Model.Model.Student();


                        foreach (StudentData studentDataItem in sort)
                        {
                            using (TransactionScope scope = new TransactionScope())
                            {

                                if (!string.IsNullOrEmpty(studentDataItem.RegNo))
                                {

                                    LevelLogic levelLogic = new LevelLogic();
                                    SessionLogic sessionLogic = new SessionLogic();
                                    StudentLogic studentLogic = new StudentLogic();
                                    Course course = new Course();
                                    var studentDataItemCopy = studentDataItem;
                                    //student = studentLogic.GetEntityBy(p => p.EntryRegNo == uploadResultViewModel.StudentData.RegNo);
                                    Level level = levelLogic.GetModelBy(l => l.Name == studentDataItemCopy.Level) ?? new Level() { Id = 1 };
                                    student = studentLogic.GetModelBy(p => p.EntryRegNo == studentDataItemCopy.RegNo);
                                    if (student == null)
                                    {
                                        CreateStudent(studentDataItemCopy);

                                    }

                                    if (!string.IsNullOrEmpty(studentDataItem.CourseCode) && !string.IsNullOrEmpty(studentDataItem.CourseTitle) && !string.IsNullOrEmpty(studentDataItem.Session) && !string.IsNullOrEmpty(studentDataItem.Semester) && level.Id > 0)
                                    {
                                        var semesterId = Convert.ToInt32(studentDataItem.Semester);
                                        ProgrammeCourse programmeCourse;
                                        ProgrammeCourseRegistrationLogic programmeCourseRegistrationLogic = new ProgrammeCourseRegistrationLogic();
                                        SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                                        ProgrammeCourseLogic programmeCourseLogic = new ProgrammeCourseLogic();
                                        CourseType courseType = new CourseType() { Id = 2 };
                                        string courseCodeTrim = studentDataItem.CourseCode.Trim();
                                        course = courseLogic.GetModelBy(p => p.Course_Code == courseCodeTrim);
                                        if (course != null)
                                        {
                                            Semester semester = new Semester() { Id = semesterId };
                                            programmeCourse =
                                                programmeCourseLogic.GetModelBy(p => p.ProgrammeId == programme.Id && p.CourseId == course.Id && p.CourseTypeId == courseType.Id && p.SemesterId == semester.Id && p.LevelId == level.Id);
                                            if (programmeCourse == null)
                                            {
                                                ProgrammeCourse addExistingCourseCode = new ProgrammeCourse();
                                                addExistingCourseCode.Course = course;
                                                addExistingCourseCode.Programme = programme;
                                                addExistingCourseCode.CourseType = courseType;
                                                addExistingCourseCode.Semester = semester;
                                                addExistingCourseCode.Level = new Level() { Id = level.Id };
                                                addExistingCourseCode.Activated = true;
                                                addExistingCourseCode.CourseUnit = Convert.ToInt32(studentDataItem.CreditUnit);
                                                addExistingCourseCode.Pass_mark = 40;
                                                addExistingCourseCode.IsRemoveRequested = false;
                                                var createdProgrammeCourse = programmeCourseLogic.Create(addExistingCourseCode);

                                                if (createdProgrammeCourse != null)
                                                {
                                                    programmeCourse = programmeCourseLogic.GetModelBy(p => p.ProgrammeId == programme.Id && p.CourseId == course.Id && p.CourseTypeId == courseType.Id && p.SemesterId == semester.Id && p.LevelId == level.Id);
                                                }

                                            }

                                        }
                                        else
                                        {

                                            Semester semester = new Semester() { Id = semesterId };
                                            Course courseToInsert = new Course();
                                            courseToInsert.Course_Code = studentDataItem.CourseCode.Trim();
                                            courseToInsert.Name = studentDataItem.CourseTitle.Trim();
                                            courseToInsert.IsRemoveRequested = false;
                                            courseLogic.Create(courseToInsert); ;

                                            ProgrammeCourse programmeCourseToInsert = new ProgrammeCourse();
                                            programmeCourseToInsert.Course = courseToInsert;
                                            programmeCourseToInsert.Programme = programme;
                                            programmeCourseToInsert.CourseType = courseType;
                                            programmeCourseToInsert.Semester = semester;
                                            programmeCourseToInsert.Level = level;
                                            programmeCourseToInsert.Activated = true;
                                            programmeCourseToInsert.Pass_mark = 40;
                                            programmeCourseToInsert.CourseUnit = Convert.ToInt32(studentDataItem.CreditUnit);
                                            programmeCourseToInsert.IsRemoveRequested = false;
                                            programmeCourseLogic.Create(programmeCourseToInsert);
                                            programmeCourse = programmeCourseLogic.GetModelBy(p => p.ProgrammeId == programme.Id && p.CourseId == courseToInsert.Id && p.CourseTypeId == courseType.Id && p.SemesterId == semester.Id && p.LevelId == level.Id);

                                        }

                                        if (programmeCourse != null)
                                        {

                                            Session session = sessionLogic.GetModelBy(s => s.Name == studentDataItem.Session);
                                            Semester semester = new Semester() { Id = semesterId };
                                            SessionSemester sessionSemester = sessionSemesterLogic.GetModelBy(ss => ss.Session_Id == session.Id && ss.Semester_Id == semester.Id);

                                            //check out for duplicate here
                                            var studentCopy = student;
                                            var courseCopy = programmeCourse;

                                            ProgrammeCourseRegistration programmeCourseRegistration = programmeCourseRegistrationLogic.GetModelBy(p => p.ProgrammeCourseId == courseCopy.Id && p.StudentId == studentCopy.Id && p.SessionSemesterId == sessionSemester.Id && p.LevelId == level.Id);
                                            if (programmeCourseRegistration == null)
                                            {

                                                programmeCourseRegistration = new ProgrammeCourseRegistration();
                                                programmeCourseRegistration.ProgrammeCourse = programmeCourse;
                                                programmeCourseRegistration.Student = student;
                                                programmeCourseRegistration.IsCarriedOverCourses = false;
                                                programmeCourseRegistration.SessionSemester = sessionSemester;
                                                programmeCourseRegistration.Level = level;
                                                programmeCourseRegistration.CourseUnit = Convert.ToInt32(studentDataItem.CreditUnit);
                                                programmeCourseRegistration.DateReg = DateTime.Now;
                                                programmeCourseRegistration = programmeCourseRegistrationLogic.Create(programmeCourseRegistration);

                                                if (programmeCourseRegistration != null)
                                                {
                                                    StudentCourseMark studentCourseMark = new StudentCourseMark();
                                                    StudentCourseMarkLogic studentCourseMarkLogic = new StudentCourseMarkLogic();
                                                    studentCourseMark.CourseUnit = programmeCourseRegistration.CourseUnit;
                                                    studentCourseMark.DateRecorded = DateTime.Now;
                                                    studentCourseMark.ExamScore = 0.0M;
                                                    studentCourseMark.Grade = studentDataItem.Grade;
                                                    if (string.IsNullOrEmpty(studentCourseMark.Grade))
                                                    {
                                                        studentDataItem.Grade = "F";
                                                    }
                                                    studentCourseMark.GradePoint = GetGradePoint(studentDataItem.Grade, student);
                                                    studentCourseMark.ProgrammeCourseRegistration = programmeCourseRegistration;
                                                    studentCourseMark.IsMarkOmitted = true;
                                                    studentCourseMark.TestScore = 0.0M;
                                                    studentCourseMark = studentCourseMarkLogic.Create(studentCourseMark);
                                                }
                                            }
                                            else
                                            {
                                                exisitngResult.Add(studentDataItem.SN);

                                            }
                                        }
                                    }
                                }
                                //here
                                scope.Complete();

                            }

                            UploadedResult uploadedResult = new UploadedResult();
                            UploadedResultLogic uploadedResultLogic = new UploadedResultLogic();

                            uploadedResult = uploadedResultLogic.GetModelBy(up => up.StudentId == student.Id);
                            if (uploadedResult == null && !string.IsNullOrEmpty(studentDataItem.RegNo))
                            {
                                UserLogic userLogic = new UserLogic();
                                User user = userLogic.GetModelBy(u => u.User_Name == User.Identity.Name);
                                uploadedResult = new UploadedResult
                                {
                                    UploadedBy = user.Id,
                                    FileUrl = uploadResultViewModel.FileUrl,
                                    Programme = new Programme() { Id = programme.Id },
                                    DateUploaded = DateTime.Now,
                                    WorkSheetIndex = 1,
                                    Student = new Model.Model.Student { Id = student.Id },
                                    Client =
                                        Request.LogonUserIdentity.Name + " (" + HttpContext.Request.UserHostAddress +
                                        ")"
                                };
                                uploadedResultLogic.Create(uploadedResult);
                            }

                        }

                    }

                    if (exisitngResult.Count >= 1)
                    {
                        var sorted = exisitngResult.Distinct().ToList();
                        for (int j = 0; j < sorted.Count; j++)
                        {
                            failedMatricNumber += sorted[j] + ",";
                        }

                        SetMessage(
                            failedMatricNumber + " " +
                            "Some Records could not be added its either they exists or contains duplicate",
                            Message.Category.Error);

                    }
                    else
                    {
                        SetMessage(uploadResultViewModel.NewRegNos.Count + " " + "Results Uplaoded Successfully ",
                            Message.Category.Information);

                    }
                }
                else
                {
                    SetMessage("One or more Parameters not set please kindly cross-check and try again",
                        Message.Category.Warning);
                }

            }
            catch (Exception)
            {

                throw;
            }
            return RedirectToAction("UploadStudentResult");
        }

        private void keepDropDownState(UploadResultViewModel viewModel)
        {
            try
            {

                if (viewModel.ProgrammeType != null && viewModel.ProgrammeType.ProgrammeTypeId > 0)
                {
                    ViewBag.ProgrammeTypeId = new SelectList(viewModel.ProgrammeTypeSelectListItem, Utility.VALUE,
                        Utility.TEXT,
                        viewModel.ProgrammeType.ProgrammeTypeId);
                }
                else
                {
                    ViewBag.ProgrammeTypeId = viewModel.ProgrammeTypeSelectListItem;
                }

                if (viewModel.Department != null && viewModel.Department.DepartmentId > 0)
                {
                    ViewBag.DepartmentId = new SelectList(viewModel.DepartmentSelectListItem, Utility.VALUE,
                        Utility.TEXT,
                        viewModel.Department.DepartmentId);
                }
                else
                {
                    ViewBag.DepartmentId = viewModel.DepartmentSelectListItem;
                }
                if (viewModel.Country != null && viewModel.Country.Id > 0)
                {
                    ViewBag.CountryId = new SelectList(viewModel.CountrySelectListItem, Utility.VALUE, Utility.TEXT,
                        viewModel.Country.Id);
                }
                else
                {
                    ViewBag.CountryId = viewModel.CountrySelectListItem;
                }


                if (viewModel.Level != null && viewModel.Level.Id > 0)
                {
                    ViewBag.LevelId = new SelectList(viewModel.LevelSelectList, Utility.VALUE, Utility.TEXT,
                        viewModel.Level.Id);
                }
                else
                {
                    ViewBag.LevelId = viewModel.LevelSelectList;
                }

                if (viewModel.StudentData != null && viewModel.Person != null)
                {
                    if (viewModel.Person.MonthOfBirth != null && viewModel.Person.MonthOfBirth.Id > 0)
                    {
                        ViewBag.MonthOfBirthId = new SelectList(viewModel.MonthOfBirthSelectList, Utility.VALUE,
                            Utility.TEXT,
                            viewModel.Person.MonthOfBirth.Id);
                    }
                    else
                    {
                        ViewBag.MonthOfBirthId = viewModel.MonthOfBirthSelectList;
                    }

                    if (viewModel.Person.YearOfBirth != null && viewModel.Person.YearOfBirth.Id > 0)
                    {
                        ViewBag.YearOfBirthId = new SelectList(viewModel.YearOfBirthSelectList, Utility.VALUE,
                            Utility.TEXT,
                            viewModel.Person.YearOfBirth.Id);
                    }
                    else
                    {
                        ViewBag.YearOfBirthId = viewModel.YearOfBirthSelectList;
                    }

                    if (viewModel.Person.DayOfBirth != null && viewModel.Person.DayOfBirth.Id > 0)
                    {

                        if (viewModel.Person.YearOfBirth != null && viewModel.Person.MonthOfBirth != null)
                        {
                            var year = new Value { Id = Convert.ToInt32(viewModel.Person.YearOfBirth.Id) };
                            var month = new Value { Id = Convert.ToInt32(viewModel.Person.MonthOfBirth.Id) };
                            var daySelectListItem = Utility.PopulateDaySelectListItem(month, year);

                            ViewBag.DayOfBirthId = new SelectList(daySelectListItem, Utility.VALUE, Utility.TEXT,
                                viewModel.Person.DayOfBirth.Id);
                        }
                    }
                    else
                    {
                        ViewBag.DayOfBirthId = new SelectList(new List<Value>(), ID, NAME);
                    }

                    if (viewModel.StudentData.SessionOfEntry != null && viewModel.StudentData.SessionOfEntry.Id > 0)
                    {
                        ViewBag.SessionOfEntryId = new SelectList(viewModel.SessionOfEntrySelectListItem, Utility.VALUE,
                            Utility.TEXT,
                            viewModel.StudentData.SessionOfEntry.Id);
                    }
                    else
                    {
                        ViewBag.SessionOfEntryId = viewModel.SessionOfEntrySelectListItem;
                    }

                    if (viewModel.StudentData.SessionOfGraduation != null &&
                        viewModel.StudentData.SessionOfGraduation.Id > 0)
                    {
                        ViewBag.SessionOfGraduationId = new SelectList(viewModel.SessionOfEntrySelectListItem,
                            Utility.VALUE, Utility.TEXT,
                            viewModel.StudentData.SessionOfGraduation.Id);
                    }

                    else
                    {
                        ViewBag.SessionOfGraduationId = new SelectList(new List<Value>(), ID, NAME);
                    }
                    if (viewModel.StudentData.Sex != null && viewModel.StudentData.Sex.Id > 0)
                    {
                        ViewBag.SexId = new SelectList(viewModel.SexSelectListItem, Utility.VALUE, Utility.TEXT,
                            viewModel.StudentData.Sex.Id);
                    }
                    else
                    {
                        ViewBag.SexId = viewModel.SexSelectListItem;
                    }
                }




            }
            catch (Exception)
            {

                throw;
            }
        }

        private static async Task<DataSet> ReadExcel(string filepath)
        {
            DataSet Result = null;
            try
            {
                string xConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + filepath + ";" +
                                  "Extended Properties=Excel 8.0;";
                OleDbConnection connection = new OleDbConnection(xConnStr);

                connection.Open();
                DataTable sheet = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                foreach (DataRow dataRow in sheet.Rows)
                {
                    string sheetName = dataRow[2].ToString().Replace("'", "");
                    OleDbCommand command = new OleDbCommand("Select * FROM [" + sheetName + "]", connection);
                    // Create DbDataReader to Data Worksheet

                    OleDbDataAdapter MyData = new OleDbDataAdapter();
                    MyData.SelectCommand = command;
                    DataSet ds = new DataSet();
                    ds.Clear();
                    MyData.Fill(ds);
                    connection.Close();

                    Result = ds;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;

        }

        private DataSet ReadExcelResult(string filepath)
        {
            DataSet Result = null;
            try
            {
                string xConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + filepath + ";" +
                                  "Extended Properties=Excel 8.0;";
                OleDbConnection connection = new OleDbConnection(xConnStr);

                connection.Open();
                DataTable sheet = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                DataRow dataRow = sheet.Rows[0];

                string sheetName = dataRow[2].ToString().Replace("'", "");
                OleDbCommand command = new OleDbCommand("Select * FROM [" + sheetName + "]", connection);
                // Create DbDataReader to Data Worksheet

                OleDbDataAdapter MyData = new OleDbDataAdapter();
                MyData.SelectCommand = command;
                DataSet ds = new DataSet();
                ds.Clear();
                MyData.Fill(ds);
                connection.Close();

                Result = ds;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;

        }

        public JsonResult GetDepartments(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }

                PROGRAMME_TYPE programmeType = new PROGRAMME_TYPE() { ProgrammeTypeId = Convert.ToInt32(id) };
                DepartmentLogic departmentLogic = new DepartmentLogic();
                List<DEPARTMENT> departments = departmentLogic.GetBy(programmeType);

                return Json(new SelectList(departments, "DepartmentId", "Name"), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private string trim(string trimValue)
        {
            string[] arrayString = trimValue.Split(' ');
            string trimmedValue = "";
            for (int i = 0; i < arrayString.Length; i++)
            {
                trimmedValue += arrayString[i];
                trimmedValue.Trim();

            }
            return trimmedValue;
        }


        public double GetGradePoint(string grade, Model.Model.Student student)
        {
            double gradePoint = 0.0;
            try
            {
                string[] yearSplit = student.YearOfEntry.Split('/');
                int compareYear = Convert.ToInt32(yearSplit.FirstOrDefault());

                ScoreGradeLogic scoreGradeLogic = new ScoreGradeLogic();
                ScoreGrade scoreGrade = scoreGradeLogic.GetModelsBy(sg => (compareYear >= sg.Session_Start && compareYear <= sg.Session_End) && sg.Grade == grade).LastOrDefault();
                if (scoreGrade != null) gradePoint = Convert.ToDouble(scoreGrade.GradePoint);
            }
            catch (Exception ex)
            {
                throw;
            }
            return gradePoint;
        }

        public JsonResult GetSemester(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }

                Session session = new Session() { Id = Convert.ToInt32(id) };
                List<SessionSemester> sessionSemesterList = new List<SessionSemester>();
                SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                sessionSemesterList = sessionSemesterLogic.GetModelsBy(p => p.Session_Id == session.Id);

                List<Semester> semesters = new List<Semester>();
                foreach (SessionSemester sessionSemester in sessionSemesterList)
                {
                    semesters.Add(sessionSemester.Semester);
                }

                return Json(new SelectList(semesters, ID, NAME), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetCourses(int[] ids)
        {
            try
            {
                if (ids.Count() == 0)
                {
                    return null;
                }
                Level level = new Level() { Id = Convert.ToInt32(ids[1]) };
                Department department = new Department() { Id = Convert.ToInt32(ids[0]) };
                Semester semester = new Semester() { Id = Convert.ToInt32(ids[2]) };
                List<Course> courseList = Utility.GetCoursesByLevelDepartmentAndSemester(level, department, semester);

                return Json(new SelectList(courseList, "Id", "CodeName"), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult AddSingleStudent(string RegNo)
        {
            UploadResultViewModel viewModel = new UploadResultViewModel();
            try
            {

                TranscriptRequestLogic transcriptRequestLogic = new TranscriptRequestLogic();
                StudentLogic studentLogic = new StudentLogic();
                UploadedResultLogic uploadedResultLogic = new UploadedResultLogic();
                ProgrammeLogic programmeLogic = new ProgrammeLogic();
                DepartmentLogic departmentLogic = new DepartmentLogic();

                if (RegNo == null)
                {
                    PopulateDropDownstate(viewModel);
                }
                else
                {
                    Model.Model.Student student = studentLogic.GetModelBy(s => s.EntryRegNo == RegNo);

                    if (student != null)
                    {
                        UploadedResult uploadedResult = uploadedResultLogic.GetModelBy(u => u.StudentId == student.Id);

                        if (uploadedResult != null)
                        {
                            string id = student.Id.ToString();
                            return RedirectToAction("TranscriptReport", "Report", new { Id = id });
                        }
                    }


                    viewModel.TranscriptRequest = transcriptRequestLogic.GetModelsBy(tr => tr.Matric_Number == RegNo).LastOrDefault();
                    Programme programme = programmeLogic.GetModelBy(p => p.ProgrammeId == viewModel.TranscriptRequest.Programme.Id);
                    if (viewModel.TranscriptRequest != null)
                    {
                        string[] nameSlpit = viewModel.TranscriptRequest.StudentName.Split(' ');
                        viewModel.StudentData = new StudentData()
                        {
                            LastName = nameSlpit[1],
                            FirstName = nameSlpit[0],
                            MiddleName = nameSlpit[2],
                            RegNo = RegNo
                        };
                        viewModel.Person = new Person()
                        {
                            contact_address = viewModel.TranscriptRequest.DestinationAddress
                        };

                        ViewBag.ProgrammeTypeId = new SelectList(viewModel.ProgrammeTypeSelectListItem, Utility.VALUE,
                            Utility.TEXT, programme.ProgrammeType.Id);
                        ViewBag.DepartmentId = new SelectList(departmentLogic.GetAll(), ID, NAME,
                            programme.department.Id);
                        ViewBag.CountryId = new SelectList(viewModel.CountrySelectListItem, Utility.VALUE, Utility.TEXT,
                            viewModel.TranscriptRequest.Country.Id);
                        ViewBag.SexId = viewModel.SexSelectListItem;
                        ViewBag.DayOfBirthId = new SelectList(new List<Value>(), ID, NAME);
                        ViewBag.MonthOfBirthId = viewModel.MonthOfBirthSelectList;
                        ViewBag.YearOfBirthId = viewModel.YearOfBirthSelectList;
                        ViewBag.SessionOfEntryId = viewModel.SessionOfEntrySelectListItem;
                        ViewBag.SessionOfGraduationId = new SelectList(new List<Value>(), ID, NAME);
                        ViewBag.LevelId = viewModel.LevelSelectList;

                    }

                }

            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred!" + ex.Message + "Please kindly Try Again", Message.Category.Error);
                return View(viewModel);

            }
            return View(viewModel);
        }

        [HttpPost]
        [NoAsyncTimeout]
        public async Task<ActionResult> AddSingleStudent(UploadResultViewModel viewModel)
        {
            try
            {
                List<StudentData> studentDataList = new List<StudentData>();
                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    string pathForSaving = Server.MapPath("~/Content/ExcelUploads");
                    string savedFileName = Path.Combine(pathForSaving, hpf.FileName);
                    hpf.SaveAs(savedFileName);
                    IExcelManager excelManager = new ExcelManager();

                    //DataSet studentList = await ReadExcel(savedFileName);
                    DataSet studentList = excelManager.ReadExcel(savedFileName);
                    viewModel.FileUrl = pathForSaving + hpf.FileName;
                    if (studentList != null && studentList.Tables[0].Rows.Count > 0)
                    {

                        if (viewModel.Department.DepartmentId == 50)
                        {

                            for (int i = 0; i < studentList.Tables[0].Rows.Count; i++)
                            {
                                StudentData studentData = new StudentData();
                                studentData.SN = studentList.Tables[0].Rows[i][0].ToString().Trim();
                                studentData.Mbbs = studentList.Tables[0].Rows[i][1].ToString().Trim();
                                studentData.CourseTitle = studentList.Tables[0].Rows[i][2].ToString().Trim();
                                studentData.Grade = studentList.Tables[0].Rows[i][3].ToString().Trim();
                                studentData.Semester = studentData.Mbbs;
                                studentData.Session = viewModel.StudentData.SessionOfEntry.Id.ToString();
                                studentData.Level = viewModel.Level.Id.ToString();
                                studentDataList.Add(studentData);

                            }
                        }
                        else
                        {
                            for (int i = 0; i < studentList.Tables[0].Rows.Count; i++)
                            {
                                StudentData studentData = new StudentData();

                                studentData.SN = studentList.Tables[0].Rows[i][0].ToString().Trim();
                                studentData.Level = studentList.Tables[0].Rows[i][1].ToString().Trim();
                                studentData.Session = studentList.Tables[0].Rows[i][2].ToString().Trim();
                                studentData.Semester = studentList.Tables[0].Rows[i][3].ToString().Trim();
                                studentData.CourseCode = studentList.Tables[0].Rows[i][4].ToString().Trim();
                                studentData.CourseTitle = studentList.Tables[0].Rows[i][5].ToString().Trim();
                                studentData.CreditUnit = studentList.Tables[0].Rows[i][6].ToString().Trim();
                                studentData.Grade = studentList.Tables[0].Rows[i][7].ToString().Trim();

                                studentDataList.Add(studentData);


                            }
                        }

                        viewModel.StudentDataList = studentDataList;
                        TempData["UploadResultViewModel"] = viewModel;

                    }

                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred!" + ex.Message + "Please kindly Try Again", Message.Category.Error);
                keepDropDownState(viewModel);
                return View(viewModel);
            }
            keepDropDownState(viewModel);
            return View(viewModel);
        }

        [NoAsyncTimeout]
        public async Task<ActionResult> SaveSingleStudentUpload()
        {
            Model.Model.Student student = new Model.Model.Student();
            try
            {
                UploadResultViewModel uploadResultViewModel = new UploadResultViewModel();
                uploadResultViewModel = (UploadResultViewModel)TempData["UploadResultViewModel"];
                if (uploadResultViewModel != null)
                {
                    using (TransactionScope scope = new TransactionScope())
                    {
                        Department department;
                        Programme programme;
                        ProgrammeCourse programmeCourse;
                        Course course = new Course();
                        ProgrammeLogic programmeLogic = new ProgrammeLogic();
                        ProgrammeCourseRegistrationLogic programmeCourseRegistrationLogic = new ProgrammeCourseRegistrationLogic();
                        SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                        SessionLogic sessionLogic = new SessionLogic();
                        DepartmentLogic departmentLogic = new DepartmentLogic();
                        LevelLogic levelLogic = new LevelLogic();
                        StudentLogic studentLogic = new StudentLogic();
                        ProgrammeCourseLogic programmeCourseLogic = new ProgrammeCourseLogic();
                        CourseLogic courseLogic = new CourseLogic();
                        if (uploadResultViewModel.Department != null && uploadResultViewModel.Department.DepartmentId > 0 && uploadResultViewModel.ProgrammeType != null && uploadResultViewModel.ProgrammeType.ProgrammeTypeId > 0)
                        {
                            department = departmentLogic.GetModelBy(p => p.DepartmentId == uploadResultViewModel.Department.DepartmentId);
                            programme = programmeLogic.GetModelBy(p => p.ProgrammeTypeId == uploadResultViewModel.ProgrammeType.ProgrammeTypeId && p.DepartmentId == uploadResultViewModel.Department.DepartmentId);
                        }
                        else
                        {
                            throw new ArgumentNullException();
                        }
                        var studentData = uploadResultViewModel.StudentDataList.LastOrDefault();
                        student = studentLogic.GetModelBy(p => p.EntryRegNo == uploadResultViewModel.StudentData.RegNo) ?? CreateStudent(studentData);

                        int count = 0;

                        foreach (StudentData studentDataItem in uploadResultViewModel.StudentDataList)
                        {


                            if (uploadResultViewModel.StudentData.RegNo != null || uploadResultViewModel.StudentData.RegNo != "")
                            {
                                var studentDataItemCopy = uploadResultViewModel.StudentDataList.LastOrDefault();
                                Level level;
                                if (department.Id == 50)
                                {
                                    int levelId = Convert.ToInt32(studentDataItemCopy.Level);
                                    level = levelLogic.GetModelBy(l => l.LevelId == levelId);
                                }
                                else
                                {
                                    level = levelLogic.GetModelBy(l => l.Name == studentDataItemCopy.Level);
                                }

                                // For Medical Students
                                if (!string.IsNullOrEmpty(studentDataItem.CourseTitle) && !string.IsNullOrEmpty(studentDataItem.Mbbs) && department.Id == 50)
                                {

                                    CourseType courseType = new CourseType() { Id = 2 };
                                    course = courseLogic.GetModelsBy(p => p.Title.Contains(studentDataItem.CourseTitle)).LastOrDefault();
                                    var semesterId = studentDataItem.Mbbs + " " + "MBBS";
                                    SemesterLogic semesterLogic = new SemesterLogic();
                                    Semester semester = semesterLogic.GetModelBy(s => s.SemesterCode == semesterId);
                                    if (course != null)
                                    {
                                        programmeCourse = programmeCourseLogic.GetModelBy(p => p.ProgrammeId == programme.Id && p.CourseId == course.Id && p.CourseTypeId == courseType.Id && p.SemesterId == semester.Id && p.LevelId == level.Id);

                                        if (programmeCourse == null)
                                        {
                                            ProgrammeCourse addExistingCourseCode = new ProgrammeCourse();
                                            addExistingCourseCode.Course = course;
                                            addExistingCourseCode.Programme = programme;
                                            addExistingCourseCode.CourseType = courseType;
                                            addExistingCourseCode.Semester = new Semester() { Id = semester.Id };
                                            addExistingCourseCode.Level = new Level() { Id = level.Id };
                                            addExistingCourseCode.Activated = true;
                                            addExistingCourseCode.CourseUnit = 1;
                                            addExistingCourseCode.Pass_mark = 40;
                                            addExistingCourseCode.IsRemoveRequested = false;
                                            var createdProgrammeCourse = programmeCourseLogic.Create(addExistingCourseCode);
                                            if (createdProgrammeCourse != null)
                                            {
                                                programmeCourse = createdProgrammeCourse;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        programmeCourse = new ProgrammeCourse();
                                        course = new Course();
                                        course.Course_Code = "Med 101";
                                        course.Name = studentDataItem.CourseTitle.Trim();
                                        course.IsRemoveRequested = false;
                                        course = courseLogic.Create(course);

                                        ProgrammeCourse programmeCourseToInsert = new ProgrammeCourse();
                                        programmeCourseToInsert.Course = course;
                                        programmeCourseToInsert.Programme = programme;
                                        programmeCourseToInsert.CourseType = courseType;
                                        programmeCourseToInsert.Semester = semester;
                                        programmeCourseToInsert.Level = level;
                                        programmeCourseToInsert.Activated = true;
                                        programmeCourseToInsert.Pass_mark = 40;
                                        programmeCourseToInsert.CourseUnit = 1;
                                        programmeCourseToInsert.IsRemoveRequested = false;
                                        var createprogrammeCourse = programmeCourseLogic.Create(programmeCourseToInsert);
                                        if (createprogrammeCourse != null)
                                        {
                                            programmeCourse = createprogrammeCourse;
                                        }

                                    }
                                    if (programmeCourse != null)
                                    {

                                        SessionSemester sessionSemester = sessionSemesterLogic.GetModelBy(ss => ss.SEMESTER.SemesterCode == semesterId && ss.Semester_Id == semester.Id);
                                        //check out for duplicate here
                                        var studentCopy = student;
                                        var courseCopy = programmeCourse;
                                        ProgrammeCourseRegistration programmeCourseRegistration = programmeCourseRegistrationLogic.GetModelBy(p => p.ProgrammeCourseId == courseCopy.Id && p.StudentId == studentCopy.Id && p.SessionSemesterId == sessionSemester.Id);

                                        if (programmeCourseRegistration == null)
                                        {

                                            programmeCourseRegistration = new ProgrammeCourseRegistration();
                                            programmeCourseRegistration.ProgrammeCourse = programmeCourse;
                                            programmeCourseRegistration.Student = student;
                                            programmeCourseRegistration.IsCarriedOverCourses = false;
                                            programmeCourseRegistration.SessionSemester = sessionSemester;
                                            programmeCourseRegistration.Level = level;
                                            programmeCourseRegistration.CourseUnit = Convert.ToInt32(studentDataItem.CreditUnit);
                                            programmeCourseRegistration.DateReg = DateTime.Now;
                                            programmeCourseRegistration = programmeCourseRegistrationLogic.Create(programmeCourseRegistration);

                                            if (programmeCourseRegistration != null)
                                            {
                                                StudentCourseMark studentCourseMark = new StudentCourseMark();
                                                StudentCourseMarkLogic studentCourseMarkLogic = new StudentCourseMarkLogic();
                                                studentCourseMark.CourseUnit = programmeCourseRegistration.CourseUnit;
                                                studentCourseMark.DateRecorded = DateTime.Now;
                                                studentCourseMark.ExamScore = 0.0M;
                                                studentCourseMark.Grade = "Nil";
                                                studentCourseMark.GradePoint = Convert.ToDouble(studentDataItem.Grade);
                                                studentCourseMark.ProgrammeCourseRegistration = programmeCourseRegistration;
                                                studentCourseMark.IsMarkOmitted = true;
                                                studentCourseMark.TestScore = 0.0M;
                                                studentCourseMarkLogic.Create(studentCourseMark);
                                            }
                                        }
                                    }
                                }
                                // For Non Medical Students

                                else if (!string.IsNullOrEmpty(studentDataItem.CourseCode) && !string.IsNullOrEmpty(studentDataItem.CourseTitle) && !string.IsNullOrEmpty(studentDataItem.Session) && !string.IsNullOrEmpty(studentDataItem.Semester) && level.Id > 0)
                                {

                                    CourseType courseType = new CourseType() { Id = 2 };
                                    string courseCodeTrim = studentDataItem.CourseCode.Trim();

                                    course = courseLogic.GetModelsBy(p => p.Course_Code.Contains(courseCodeTrim)).FirstOrDefault();
                                    var semesterId = Convert.ToInt32(studentDataItem.Semester);
                                    string levelName = studentDataItem.Level;
                                    level = levelLogic.GetModelBy(s => s.Name == levelName);

                                    if (course != null)
                                    {
                                        Semester semester = new Semester() { Id = semesterId };
                                        programmeCourse = programmeCourseLogic.GetModelBy(p => p.ProgrammeId == programme.Id && p.CourseId == course.Id && p.CourseTypeId == courseType.Id && p.SemesterId == semester.Id && p.LevelId == level.Id);

                                        if (programmeCourse == null)
                                        {
                                            ProgrammeCourse addExistingCourseCode = new ProgrammeCourse();
                                            addExistingCourseCode.Course = course;
                                            addExistingCourseCode.Programme = programme;
                                            addExistingCourseCode.CourseType = courseType;
                                            addExistingCourseCode.Semester = new Semester() { Id = semester.Id };
                                            addExistingCourseCode.Level = new Level() { Id = level.Id };
                                            addExistingCourseCode.Activated = true;
                                            addExistingCourseCode.CourseUnit = Convert.ToInt32(studentDataItem.CreditUnit);
                                            addExistingCourseCode.Pass_mark = 40;
                                            addExistingCourseCode.IsRemoveRequested = false;
                                            var createdProgrammeCourse = programmeCourseLogic.Create(addExistingCourseCode);
                                            if (createdProgrammeCourse != null)
                                            {
                                                programmeCourse = createdProgrammeCourse;
                                            }
                                        }
                                    }
                                    else
                                    {

                                        Semester semester = new Semester() { Id = semesterId };
                                        Course courseToInsert = new Course();
                                        programmeCourse = new ProgrammeCourse();
                                        courseToInsert.Course_Code = studentDataItem.CourseCode.Trim();
                                        courseToInsert.Name = studentDataItem.CourseTitle.Trim();
                                        courseToInsert.IsRemoveRequested = false;
                                        var createdCourse = courseLogic.Create(courseToInsert);
                                        ProgrammeCourse programmeCourseToInsert = new ProgrammeCourse();
                                        programmeCourseToInsert.Course = createdCourse;
                                        programmeCourseToInsert.Programme = programme;
                                        programmeCourseToInsert.CourseType = courseType;
                                        programmeCourseToInsert.Semester = semester;
                                        programmeCourseToInsert.Level = level;
                                        programmeCourseToInsert.Activated = true;
                                        programmeCourseToInsert.Pass_mark = 40;
                                        programmeCourseToInsert.CourseUnit = Convert.ToInt32(studentDataItem.CreditUnit);
                                        programmeCourseToInsert.IsRemoveRequested = false;
                                        var createdProgrammeCourse = programmeCourseLogic.Create(programmeCourseToInsert);
                                        if (createdProgrammeCourse != null)
                                        {
                                            programmeCourse = createdProgrammeCourse;
                                        }
                                    }

                                    if (programmeCourse != null)
                                    {

                                        Session session = sessionLogic.GetModelBy(s => s.Name == studentDataItem.Session);
                                        Semester semester = new Semester() { Id = semesterId };
                                        SessionSemester sessionSemester = sessionSemesterLogic.GetModelBy(ss => ss.Session_Id == session.Id && ss.Semester_Id == semester.Id);
                                        //check out for duplicate here
                                        var studentCopy = student;
                                        var courseCopy = programmeCourse;
                                        ProgrammeCourseRegistration programmeCourseRegistration = programmeCourseRegistrationLogic.GetModelBy(p => p.ProgrammeCourseId == courseCopy.Id && p.StudentId == studentCopy.Id && p.SessionSemesterId == sessionSemester.Id);
                                        if (programmeCourseRegistration == null)
                                        {

                                            programmeCourseRegistration = new ProgrammeCourseRegistration(); ;
                                            programmeCourseRegistration.ProgrammeCourse = programmeCourse;
                                            programmeCourseRegistration.Student = student;
                                            programmeCourseRegistration.IsCarriedOverCourses = false;
                                            programmeCourseRegistration.SessionSemester = sessionSemester;
                                            programmeCourseRegistration.Level = level;
                                            programmeCourseRegistration.CourseUnit = Convert.ToInt32(studentDataItem.CreditUnit);
                                            programmeCourseRegistration.DateReg = DateTime.Now;
                                            programmeCourseRegistration = programmeCourseRegistrationLogic.Create(programmeCourseRegistration);

                                            if (programmeCourseRegistration != null)
                                            {
                                                StudentCourseMark studentCourseMark = new StudentCourseMark();
                                                StudentCourseMarkLogic studentCourseMarkLogic = new StudentCourseMarkLogic();
                                                studentCourseMark.CourseUnit = programmeCourseRegistration.CourseUnit;
                                                studentCourseMark.DateRecorded = DateTime.Now;
                                                studentCourseMark.ExamScore = 0.0M;
                                                studentCourseMark.Grade = studentDataItem.Grade;
                                                if (string.IsNullOrEmpty(studentCourseMark.Grade))
                                                {
                                                    studentDataItem.Grade = "Nil";
                                                    studentCourseMark.GradePoint = 0.00;
                                                }
                                                else
                                                {
                                                    studentCourseMark.GradePoint = GetGradePoint(studentDataItem.Grade, student);
                                                }

                                                studentCourseMark.ProgrammeCourseRegistration = programmeCourseRegistration;
                                                studentCourseMark.IsMarkOmitted = true;
                                                studentCourseMark.TestScore = 0.0M;
                                                studentCourseMarkLogic.Create(studentCourseMark);
                                            }
                                        }
                                    }
                                }

                            }
                            // Add Result To uploaded list
                            UploadedResultLogic uploadedResultLogic = new UploadedResultLogic();
                            UploadedResult uploadedResult = uploadedResultLogic.GetModelBy(up => up.StudentId == student.Id);
                            if (uploadedResult == null && !string.IsNullOrEmpty(uploadResultViewModel.StudentData.RegNo))
                            {
                                UserLogic userLogic = new UserLogic();
                                User user = userLogic.GetModelBy(u => u.User_Name == User.Identity.Name);
                                uploadedResult = new UploadedResult
                                {
                                    UploadedBy = user.Id,
                                    FileUrl = uploadResultViewModel.FileUrl,
                                    Programme = new Programme() { Id = programme.Id },
                                    DateUploaded = DateTime.Now,
                                    WorkSheetIndex = 1,
                                    Student = student,
                                    Client = Request.LogonUserIdentity.Name + " (" + HttpContext.Request.UserHostAddress + ")"
                                };
                                uploadedResultLogic.Create(uploadedResult);
                            }

                            // check if Upload was successful
                            count += 1;
                        }

                        if (count == uploadResultViewModel.StudentDataList.Count)
                        {
                            scope.Complete();
                            SetMessage("Result Upload was Successful ", Message.Category.Information);
                        }
                    }
                }
                else
                {
                    SetMessage("Error Occured! One or more parameters not set", Message.Category.Error);
                }
            }
            catch (Exception ex)
            {

                SetMessage("Error Occurred!" + ex.Message + "Please kindly Try Again", Message.Category.Error);
                return RedirectToAction("AddSingleStudent");
            }

            return RedirectToAction("TranscriptReport", "Report", new { area = "Admin", id = Utility.Encrypt(student.Id.ToString()) });

        }

        public ActionResult EditStudent()
        {
            UploadResultViewModel viewModel = new UploadResultViewModel();
            return View(viewModel);
        }


        [HttpPost]
        public ActionResult EditStudent(UploadResultViewModel viewModel)
        {

            StudentLogic studentLogic = new StudentLogic();
            SessionLogic sessionLogic = new SessionLogic();

            try
            {
                if (viewModel.StudentData.RegNo != null)
                {
                    Model.Model.Student student =
                        studentLogic.GetModelBy(s => s.EntryRegNo == viewModel.StudentData.RegNo);

                    if (student != null)
                    {
                        viewModel.StudentData.StudentId = student.Id;
                        viewModel.StudentData.LastName = student.Person.Surname;
                        viewModel.StudentData.FirstName = student.Person.FirstName;
                        viewModel.StudentData.MiddleName = student.Person.MiddleName;
                        viewModel.StudentData.Sex = new Sex { Id = (byte)(student.Person.Sex == "MALE" ? 1 : 2) };
                        viewModel.StudentData.Address = student.Person.contact_address;
                        viewModel.StudentData.CGPA = student.CGPA;
                        if (student.YearOfEntry != null && student.GraduationDate != null)
                        {
                            viewModel.StudentData.SessionOfEntry = new Value()
                            {
                                Id = sessionLogic.GetModelBy(s => s.Name == student.YearOfEntry).Id
                            };
                            if (viewModel.StudentData.SessionOfEntry == null)
                            {
                                ViewBag.SessionOfEntryId = viewModel.SessionOfEntrySelectListItem;
                            }
                            viewModel.StudentData.SessionOfGraduation = new Value()
                            {
                                Id = sessionLogic.GetModelBy(s => s.Name == student.GraduationDate).Id
                            };
                            if (viewModel.StudentData.SessionOfGraduation == null)
                            {
                                ViewBag.SessionOfGraduationId = new SelectList(new List<Value>(), ID, NAME);
                            }
                        }
                        else
                        {

                            ViewBag.SessionOfGraduationId = new SelectList(new List<Value>(), ID, NAME);
                        }

                        ViewBag.ProgrammeTypeId = new SelectList(viewModel.ProgrammeTypeSelectListItem, Utility.VALUE,
                            Utility.TEXT, student.Programme.ProgrammeType.Id);
                        ViewBag.DepartmentId = new SelectList(viewModel.DepartmentSelectListItem, Utility.VALUE,
                            Utility.TEXT, student.Department.Id);
                        ViewBag.CountryId = new SelectList(viewModel.CountrySelectListItem, Utility.VALUE, Utility.TEXT,
                            student.Person.Country.Id);

                        ViewBag.SexId = new SelectList(viewModel.SexSelectListItem, Utility.VALUE, Utility.TEXT,
                            viewModel.StudentData.Sex.Id);

                        ViewBag.SessionOfEntryId = new SelectList(viewModel.SessionOfEntrySelectListItem, Utility.VALUE,
                            Utility.TEXT, viewModel.StudentData.SessionOfEntry.Id);
                        ViewBag.SessionOfGraduationId = new SelectList(viewModel.SessionOfEntrySelectListItem,
                            Utility.VALUE, Utility.TEXT, viewModel.StudentData.SessionOfGraduation.Id);

                    }
                    else
                    {
                        SetMessage("Student was not Found", Message.Category.Error);
                        return RedirectToAction("EditStudent");
                    }

                    TempData["OldStudent"] = student;
                }


                else
                {
                    SetMessage("No User input Found", Message.Category.Error);
                    return RedirectToAction("EditStudent");
                }

            }
            catch (Exception ex)
            {

                SetMessage("Error Occurred!" + ex.Message + "Please kindly Try Again", Message.Category.Error);
                return View(viewModel);
            }
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult SaveEditedStudent(UploadResultViewModel viewModel)
        {
            try
            {
                Model.Model.Student studentOldData = (Model.Model.Student)TempData["OldStudent"];

                StudentLogic studentLogic = new StudentLogic();
                SessionLogic sessionLogic = new SessionLogic();
                PersonLogic personLogic = new PersonLogic();
                ProgrammeLogic programmeLogic = new ProgrammeLogic();
                ProgrammeCourseLogic programmeCourseLogic = new ProgrammeCourseLogic();

                if (viewModel.StudentData != null && viewModel.ProgrammeType.ProgrammeTypeId > 0 &&
                    viewModel.Department.DepartmentId > 0)
                {
                    Model.Model.Student student = new Model.Model.Student();
                    student.Id = viewModel.StudentData.StudentId;
                    student.Person = new Person
                    {
                        FirstName = viewModel.StudentData.FirstName,
                        MiddleName = viewModel.StudentData.MiddleName,
                        Surname = viewModel.StudentData.LastName,
                        Sex = (viewModel.StudentData.Sex.Id == 1 ? "MALE" : "FEMALE"),
                        Country = new Country() { Id = viewModel.Country.Id },
                        contact_address = viewModel.StudentData.Address
                    };

                    Programme programme =
                        programmeLogic.GetModelBy(p => p.ProgrammeTypeId == viewModel.ProgrammeType.ProgrammeTypeId && p.DepartmentId == viewModel.Department.DepartmentId);
                    if (programme != null && studentOldData != null)
                    {
                        SESSION sessionOfENtry = sessionLogic.GetEntityBy(s => s.SessionId == viewModel.StudentData.SessionOfEntry.Id);
                        SESSION sessionOfGraduation = sessionLogic.GetEntityBy(s => s.SessionId == viewModel.StudentData.SessionOfGraduation.Id);
                        student.YearOfEntry = sessionOfENtry.Name;
                        student.GraduationDate = sessionOfGraduation.Name;
                        student.Programme = programme;
                        student.Department = programme.department;
                        student.CGPA = viewModel.StudentData.CGPA;
                        bool modifiedStudent = studentLogic.Modify(student);
                        bool modifiedPerson = personLogic.Modify(student);
                        bool programmeCourseModified = programmeCourseLogic.Modify(student);

                        if (modifiedStudent || modifiedPerson || programmeCourseModified)
                        {

                            PersonAuditLogic personAuditLogic = new PersonAuditLogic();
                            string action = "EDITING STUDENT DETAILS";
                            string client = Request.LogonUserIdentity.Name + " (" + HttpContext.Request.UserHostAddress + ")";
                            UserLogic userLogic = new UserLogic();
                            var user = userLogic.GetModelsBy(u => u.User_Name == User.Identity.Name).LastOrDefault();
                            PersonAudit personAudit = new PersonAudit();
                            personAudit.Operation = "INSERT";
                            personAudit.Action = action;
                            personAudit.Client = client;
                            personAudit.Time = DateTime.Now;
                            personAudit.User = user;
                            personAudit.Person = studentOldData.Person;
                            personAudit.NewFirstName = student.Person.FirstName;
                            personAudit.NewLastName = student.Person.Surname;
                            personAudit.NewMiddleName = student.Person.MiddleName;
                            personAudit.NewYearOfEntry = student.YearOfEntry;
                            personAudit.NewYearOfGraduation = student.GraduationDate;
                            personAudit.NewCountryId = student.Person.Country.Id;
                            personAudit.NewSex = student.Person.Sex;
                            personAudit.NewCGPA = student.CGPA;
                            personAudit.NewDepartmentId = student.Programme.department.Id;
                            personAudit.NewProgrammeId = student.Programme.Id;
                            personAudit.OldFirstName = studentOldData.Person.FirstName;
                            personAudit.OldLastName = studentOldData.Person.Surname;
                            personAudit.OldMiddleName = studentOldData.Person.MiddleName;
                            personAudit.OldYearOfEntry = studentOldData.YearOfEntry;
                            personAudit.OldYearOfGraduation = studentOldData.GraduationDate;
                            personAudit.OldCountryId = studentOldData.Person.Country.Id;
                            personAudit.OldSex = studentOldData.Person.Sex;
                            personAudit.OldDepartmentId = studentOldData.Programme.department.Id;
                            personAudit.OldProgrammeId = studentOldData.Programme.Id;
                            personAudit.OldCGPA = studentOldData.CGPA;

                            var createdAudit = personAuditLogic.Create(personAudit);

                            if (createdAudit != null)
                            {
                                SetMessage("Operation SuccessFul", Message.Category.Information);
                            }
                            else
                            {
                                throw new ArgumentNullException();
                            }

                        }
                    }

                    else
                    {
                        SetMessage("No Item Found to be Modified", Message.Category.Error);
                        RedirectToAction("EditStudent", viewModel);
                    }
                }
                else
                {
                    SetMessage("No User Input Found", Message.Category.Error);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred!" + ex.Message + "Please kindly Try Again", Message.Category.Error);
                return RedirectToAction("EditStudent");
            }

            return RedirectToAction("EditStudent");
        }

        public JsonResult GetDayOfBirthBy(string monthId, string yearId)
        {
            try
            {
                if (string.IsNullOrEmpty(monthId) || string.IsNullOrEmpty(yearId))
                {
                    return null;
                }

                var month = new Value { Id = Convert.ToInt32(monthId) };
                var year = new Value { Id = Convert.ToInt32(yearId) };
                List<Value> days = Utility.GetNumberOfDaysInMonth(month, year);

                return Json(new SelectList(days, ID, NAME), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public JsonResult GetYearOfGraduation(string yearId)
        {
            List<Session> sessions = new List<Session>();
            try
            {
                int Id = Convert.ToInt32(yearId);
                SessionLogic sessionLogic = new SessionLogic();
                List<Session> sessionList = new List<Session>();
                Session session = new Session();
                session = sessionLogic.GetModelBy(s => s.SessionId == Id);
                string[] compareslipt = session.Name.Split('/');
                int comparesliptSession = Convert.ToInt32(compareslipt.LastOrDefault()) + 8;
                string sessionRange = Convert.ToString(comparesliptSession);
                string[] sessionArray = new string[2];
                sessionArray[0] = (Convert.ToInt32(compareslipt.FirstOrDefault()) + 8).ToString();
                sessionArray[1] = sessionRange;
                string sessionValue = string.Join("/", sessionArray);
                Session currentSession = sessionLogic.GetModelBy(s => s.Name == sessionValue);
                if (currentSession != null)
                {
                    sessionList = sessionLogic.GetModelsBy(s => (s.SessionId > Id && s.SessionId <= currentSession.Id));
                    sessions = sessionList.Take(8).ToList();
                }
                else
                {
                    Session lastSession = sessionLogic.GetAll().LastOrDefault();
                    sessionList =
                        sessionLogic.GetModelsBy(s => (s.SessionId > session.Id && s.SessionId <= lastSession.Id));
                    sessions = sessionList.Take(8).ToList();
                }

            }
            catch (Exception ex)
            {

                throw;
            }
            return Json(new SelectList(sessions, ID, NAME), JsonRequestBehavior.AllowGet);
        }


        private Model.Model.Student CreateStudent(StudentData studentDataItemCopy)
        {
            var createdStudent = new Model.Model.Student();
            try
            {
                UploadResultViewModel uploadResultViewModel = (UploadResultViewModel)TempData["UploadResultViewModel"];
                if (uploadResultViewModel != null)
                {
                    ProgrammeLogic programmeLogic = new ProgrammeLogic();
                    SessionLogic sessionLogic = new SessionLogic();
                    DepartmentLogic departmentLogic = new DepartmentLogic();
                    StudentLogic studentLogic = new StudentLogic();
                    Model.Model.Student student = new Model.Model.Student();
                    SexLogic sexLogic = new SexLogic();
                    LevelLogic levelLogic = new LevelLogic();
                    Person person = new Person();
                    PersonLogic personLogic = new PersonLogic();
                    Session sessionOfENtry = sessionLogic.GetModelBy(s => s.SessionId == uploadResultViewModel.StudentData.SessionOfEntry.Id);
                    Session sessionOfGraduation = sessionLogic.GetModelBy(s => s.SessionId == uploadResultViewModel.StudentData.SessionOfGraduation.Id);
                    Department department = departmentLogic.GetModelBy(p => p.DepartmentId == uploadResultViewModel.Department.DepartmentId);
                    Programme programme = programmeLogic.GetModelBy(p => p.ProgrammeTypeId == uploadResultViewModel.ProgrammeType.ProgrammeTypeId && p.DepartmentId == uploadResultViewModel.Department.DepartmentId);
                    Level level;
                    Country country = new Country() { Id = uploadResultViewModel.Country.Id };
                    PersonType personType = new PersonType() { Id = 2 };
                    StudentCategory studentCategory = new StudentCategory() { Id = 1 };

                    if (department.Id == 50)
                    {

                        int leveld = Convert.ToInt32(studentDataItemCopy.Mbbs);
                        level = levelLogic.GetModelBy(l => l.LevelId == leveld);
                        SEX sex = sexLogic.GetEntityBy(s => s.Sex_Id == uploadResultViewModel.StudentData.Sex.Id);
                        uploadResultViewModel.StudentData.Sex = new Sex() { Name = sex.Sex_Name };
                        person.Country = country;
                        person.DateFilled = DateTime.Now;
                        string year = Convert.ToString(uploadResultViewModel.Person.YearOfBirth.Id);
                        string check = uploadResultViewModel.YearOfBirthSelectList.LastOrDefault(s => s.Value == year).Text;
                        int yearId = Convert.ToInt32(check);
                        int monthId = Convert.ToInt32(uploadResultViewModel.Person.MonthOfBirth.Id);
                        int dayId = Convert.ToInt32(uploadResultViewModel.Person.DayOfBirth.Id);
                        string[] dob = new DateTime(yearId, monthId, dayId).GetDateTimeFormats();
                        person.DateofBirth = dob[3];
                        if (uploadResultViewModel.StudentData != null && uploadResultViewModel.StudentData.LastName != null)
                        {
                            person.Surname = uploadResultViewModel.StudentData.LastName.Trim();
                        }
                        if (uploadResultViewModel.StudentData.MiddleName != null)
                        {
                            person.MiddleName = uploadResultViewModel.StudentData.MiddleName.Trim();
                        }
                        person.PersonType = personType;
                        person.Sex = uploadResultViewModel.StudentData.Sex.Name.Trim();
                        person.FirstName = uploadResultViewModel.StudentData.FirstName.Trim();
                        person.contact_address = uploadResultViewModel.Person.contact_address;
                        if (uploadResultViewModel.ModeOfEntry == true)
                        {
                            person.ModeOfEntry = "Direct Entry";
                        }
                        person = personLogic.Create(person);
                        student = new Model.Model.Student();
                        student.Id = person.Id;
                        student.Programme = new Programme()
                        {
                            Id = programme.Id
                        };
                        student.YearOfEntry = sessionOfENtry.Name;
                        student.GraduationDate = sessionOfGraduation.Name;
                        student.Department = department;
                        student.CurrentSession = sessionOfENtry;
                        student.CurrentLevel = level;
                        student.StudentCategory = studentCategory;
                        student.EntryRegNo = uploadResultViewModel.StudentData.RegNo;
                        createdStudent = studentLogic.Create(student);
                    }
                    else
                    {

                        SEX sex = sexLogic.GetEntityBy(s => s.Sex_Id == uploadResultViewModel.StudentData.Sex.Id);

                        level = levelLogic.GetModelBy(l => l.Name == studentDataItemCopy.Level);
                        uploadResultViewModel.StudentData.Sex = uploadResultViewModel.StudentData.Sex;
                        person.Country = country;
                        person.DateFilled = DateTime.Now;
                        string year = Convert.ToString(uploadResultViewModel.Person.YearOfBirth.Id);
                        string check = uploadResultViewModel.YearOfBirthSelectList.LastOrDefault(s => s.Value == year).Text;
                        int yearId = Convert.ToInt32(check);
                        int monthId = Convert.ToInt32(uploadResultViewModel.Person.MonthOfBirth.Id);
                        int dayId = Convert.ToInt32(uploadResultViewModel.Person.DayOfBirth.Id);
                        string[] dob = new DateTime(yearId, monthId, dayId).GetDateTimeFormats();
                        person.DateofBirth = dob[3];
                        if (uploadResultViewModel.StudentData != null && uploadResultViewModel.StudentData.LastName != null)
                        {
                            person.Surname = uploadResultViewModel.StudentData.LastName.Trim();
                        }
                        if (uploadResultViewModel.StudentData.MiddleName != null)
                        {
                            person.MiddleName = uploadResultViewModel.StudentData.MiddleName.Trim();
                        }
                        person.PersonType = personType;
                        person.Sex = sex.Sex_Name;
                        person.FirstName = uploadResultViewModel.StudentData.FirstName.Trim();
                        person.contact_address = uploadResultViewModel.Person.contact_address;
                        person = personLogic.Create(person);
                        student = new Model.Model.Student();
                        student.Id = person.Id;
                        student.Programme = new Programme()
                        {
                            Id = programme.Id
                        };

                        student.YearOfEntry = sessionOfENtry.Name;
                        student.GraduationDate = sessionOfGraduation.Name;
                        student.Department = new Department()
                        {
                            Id = department.Id
                        };
                        student.CurrentSession = new Session()
                        {
                            Id = sessionOfENtry.Id
                        };
                        student.CurrentLevel = level;
                        student.StudentCategory = studentCategory;
                        student.EntryRegNo = uploadResultViewModel.StudentData.RegNo;
                        createdStudent = studentLogic.Create(student);


                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return createdStudent;
        }

        public ActionResult AddStudentResult()
        {
            UploadResultViewModel viewModel = new UploadResultViewModel();

            return View(viewModel);
        }
        public ActionResult AddResultByTranscriptId(string RegNo)
        {
            UploadResultViewModel viewModel = new UploadResultViewModel();
            try
            {
                viewModel.StudentData = new StudentData();
                viewModel.StudentData.RegNo = RegNo;
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred!" + ex.Message + "Please kindly Try Again", Message.Category.Error);
            }

            return View("AddStudentResult", viewModel);
        }
        [HttpPost]
        public ActionResult AddStudentResult(UploadResultViewModel viewModel)
        {
            try
            {
                StudentLogic studentLogic = new StudentLogic();
                DepartmentLogic departmentLogic = new DepartmentLogic();
                SessionLogic sessionLogic = new SessionLogic();
                ProgrammeLogic programmeLogic = new ProgrammeLogic();
                TranscriptRequestLogic transcriptRequestLogic = new TranscriptRequestLogic();
                SexLogic sexlogic = new SexLogic();
                UploadedResultLogic uploadedResultLogic = new UploadedResultLogic();
                viewModel.Student = new Model.Model.Student();
                Model.Model.Student student = studentLogic.GetModelBy(s => s.EntryRegNo == viewModel.StudentData.RegNo);
                viewModel.ModeOfEntry = false;

                if (student != null)
                {
                    UploadedResult uploadedResult = uploadedResultLogic.GetModelBy(u => u.StudentId == student.Id);

                    //if (uploadedResult != null)
                    //{
                    //    string id = student.Id.ToString();
                    //    return RedirectToAction("TranscriptReport", "Report", new { Id = id });
                    //}

                    Programme programme = programmeLogic.GetModelBy(p => p.ProgrammeId == student.Programme.Id);
                    Session entryYear = sessionLogic.GetModelsBy(s => s.Name == student.YearOfEntry).LastOrDefault();
                    Session graduationYear = sessionLogic.GetModelsBy(s => s.Name == student.GraduationDate).LastOrDefault();
                    Sex sex = sexlogic.GetModelBy(s => s.Sex_Name == student.Person.Sex);
                    viewModel.Student = student;
                    ViewBag.ProgrammeTypeId = new SelectList(viewModel.ProgrammeTypeSelectListItem,
                        Utility.VALUE,
                        Utility.TEXT, programme.ProgrammeType.Id);
                    ViewBag.DepartmentId = new SelectList(departmentLogic.GetAll(), ID, NAME,
                        programme.department.Id);
                    ViewBag.CountryId = new SelectList(viewModel.CountrySelectListItem, Utility.VALUE,
                        Utility.TEXT,
                        viewModel.Student.Person.Country.Id);
                    ViewBag.SexId = viewModel.SexSelectListItem;

                    DateTime dob = new DateTime();
                    if (DateTime.TryParse(student.Person.DateofBirth, out dob))
                    {

                        var year = viewModel.YearOfBirthSelectList.LastOrDefault(s => s.Text.Contains(dob.Year.ToString()));
                        var monthId = new Value { Id = dob.Month };
                        if (year != null)
                        {
                            var yearId = new Value { Id = Convert.ToInt32(year.Value) };
                            ViewBag.DayOfBirthId = new SelectList(Utility.GetNumberOfDaysInMonth(monthId, yearId), ID, NAME, dob.Day);
                            ViewBag.YearOfBirthId = new SelectList(viewModel.YearOfBirthSelectList, "Value", "Text", yearId.Id);
                            ViewBag.MonthOfBirthId = new SelectList(viewModel.MonthOfBirthSelectList, "Value", "Text", dob.Month);
                        }
                        else
                        {
                            ViewBag.DayOfBirthId = new SelectList(new List<Value>(), ID, NAME);
                            ViewBag.MonthOfBirthId = viewModel.MonthOfBirthSelectList;
                            ViewBag.YearOfBirthId = viewModel.YearOfBirthSelectList;
                        }
                    }
                    else
                    {
                        ViewBag.DayOfBirthId = new SelectList(new List<Value>(), ID, NAME);
                        ViewBag.MonthOfBirthId = viewModel.MonthOfBirthSelectList;
                        ViewBag.YearOfBirthId = viewModel.YearOfBirthSelectList;
                    }

                    if (graduationYear != null)
                    {
                        ViewBag.SessionOfGraduationId = new SelectList(viewModel.SessionOfEntrySelectListItem, Utility.VALUE, Utility.TEXT, graduationYear.Id);
                    }
                    else
                    {
                        ViewBag.SessionOfGraduationId = viewModel.SessionOfEntrySelectListItem;
                    }
                    if (entryYear != null)
                    {
                        ViewBag.SessionOfEntryId = new SelectList(viewModel.SessionOfEntrySelectListItem, Utility.VALUE, Utility.TEXT, entryYear.Id);
                    }
                    else
                    {
                        ViewBag.SessionOfEntryId = viewModel.SessionOfEntrySelectListItem;
                    }
                    if (sex != null)
                    {
                        ViewBag.SexId = new SelectList(viewModel.SexSelectListItem, Utility.VALUE, Utility.TEXT, sex.Id);
                    }
                    else
                    {
                        ViewBag.SexId = viewModel.SexSelectListItem;
                    }
                    if (viewModel.Student.Department != null && viewModel.Student.Department.Id == 50)
                    {
                        ViewBag.LevelId = new SelectList(viewModel.LevelSelectList, Utility.VALUE, Utility.TEXT, viewModel.Student.CurrentLevel.Id);
                    }
                    else
                    {
                        ViewBag.LevelId = new SelectList(viewModel.LevelSelectList, Utility.VALUE, Utility.TEXT);
                    }
                    if (!string.IsNullOrEmpty(student.Person.ModeOfEntry))
                    {
                        viewModel.ModeOfEntry = true;
                    }
                    viewModel.StudentData.LastName = student.Person.Surname;
                    viewModel.StudentData.FirstName = student.Person.FirstName;
                    viewModel.StudentData.MiddleName = student.Person.MiddleName;
                    viewModel.StudentData.WesVerificationNumber = student.WesVerificationNumber;
                    viewModel.ModeOfEntry = student.Person.ModeOfEntry != null;
                    viewModel.Person = student.Person;


                }
                else
                {
                    viewModel.TranscriptRequest = transcriptRequestLogic.GetModelsBy(tr => tr.Matric_Number == viewModel.StudentData.RegNo).LastOrDefault();

                    if (viewModel.TranscriptRequest != null)
                    {
                        Programme programme = programmeLogic.GetModelBy(p => p.ProgrammeId == viewModel.TranscriptRequest.Programme.Id);
                        string[] nameSlpit = viewModel.TranscriptRequest.StudentName.Split(' ');

                        Person person = new Person();
                        if (nameSlpit.Length == 3)
                        {
                            person = new Person()
                            {
                                Surname = nameSlpit[1],
                                FirstName = nameSlpit[0],
                                MiddleName = nameSlpit[2],
                                contact_address = viewModel.TranscriptRequest.DestinationAddress
                            };
                        }
                        else
                        {
                            person = new Person()
                            {
                                Surname = nameSlpit[1],
                                FirstName = nameSlpit[0],
                                contact_address = viewModel.TranscriptRequest.DestinationAddress
                            };
                        }

                        viewModel.StudentData.LastName = person.Surname;
                        viewModel.StudentData.FirstName = person.FirstName;
                        viewModel.StudentData.MiddleName = person.MiddleName;
                        viewModel.Student.Person = person;
                        viewModel.Student.EntryRegNo = viewModel.StudentData.RegNo;
                        viewModel.Person = person;
                        ViewBag.ProgrammeTypeId = new SelectList(viewModel.ProgrammeTypeSelectListItem, Utility.VALUE, Utility.TEXT, programme.ProgrammeType.Id);
                        ViewBag.DepartmentId = new SelectList(departmentLogic.GetAll(), ID, NAME, programme.department.Id);
                        ViewBag.CountryId = new SelectList(viewModel.CountrySelectListItem, Utility.VALUE, Utility.TEXT, viewModel.TranscriptRequest.Country.Id);
                        ViewBag.SexId = viewModel.SexSelectListItem;
                        ViewBag.DayOfBirthId = new SelectList(new List<Value>(), ID, NAME);
                        ViewBag.MonthOfBirthId = viewModel.MonthOfBirthSelectList;
                        ViewBag.YearOfBirthId = viewModel.YearOfBirthSelectList;
                        ViewBag.SessionOfEntryId = viewModel.SessionOfEntrySelectListItem;
                        ViewBag.SessionOfGraduationId = new SelectList(new List<Value>(), ID, NAME);
                        ViewBag.LevelId = viewModel.LevelSelectList;
                    }
                    else
                    {
                        viewModel.Student = new Model.Model.Student();
                        viewModel.Student.EntryRegNo = viewModel.StudentData.RegNo;

                        PopulateDropDownstate(viewModel);
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View(viewModel);
        }
        public ActionResult AddExtraCourse(long personId)
        {
            UploadResultViewModel viewModel = new UploadResultViewModel();
            try
            {
                StudentLogic studentLogic = new StudentLogic();
                SessionLogic sessionLogic = new SessionLogic();
                viewModel.Student = studentLogic.GetModelBy(s => s.StudentId == personId);

                if (viewModel.Student != null)
                {
                    viewModel.Session = sessionLogic.GetModelBy(s => s.Name == viewModel.Student.YearOfEntry);
                    ViewBag.Session = new SelectList(viewModel.SessionSelectList, Utility.VALUE, Utility.TEXT, viewModel.Session.Id);
                    ViewBag.Semester = new SelectList(viewModel.MbbselectListItem, Utility.VALUE, Utility.TEXT);
                    ViewBag.Programme = new SelectList(viewModel.ProgrammeTypeSelectListItem, Utility.VALUE, Utility.TEXT, viewModel.Student.Programme.ProgrammeType.Id);
                    ViewBag.Department = new SelectList(viewModel.DepartmentSelectListItem, Utility.VALUE, Utility.TEXT, viewModel.Student.Department.Id);
                    ViewBag.Level = new SelectList(viewModel.LevelSelectList, Utility.VALUE, Utility.TEXT);
                    ViewBag.Semester = new SelectList(viewModel.MbbselectListItem, Utility.VALUE, Utility.TEXT);

                    viewModel.ProgrammeTypeModel = viewModel.Student.Programme.ProgrammeType;
                    viewModel.DepartmentModel = viewModel.Student.Department;
                    ViewBag.ScoreGrade = viewModel.GradeSelectList;
                }
                else
                {
                    SetMessage("No Student Record not Found", Message.Category.Error);
                    return RedirectToAction("AddStudentResult", "UploadResult", new { area = "Admin" });
                }

            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddExtraCourse(UploadResultViewModel viewModel)
        {
            try
            {
                if (viewModel != null)
                {
                    StudentLogic studentLogic = new StudentLogic();
                    SessionLogic sessionLogic = new SessionLogic();

                    List<Model.Model.Student> students = studentLogic.GetModelsBy(s => s.EntryRegNo == viewModel.Student.EntryRegNo);
                    if (students.Count != 1)
                    {
                        SetMessage("Duplicate Matric Number OR Matric Number does not exist!", Message.Category.Error);
                        KeepDropdownState(viewModel);
                        return View(viewModel);
                    }

                    Model.Model.Student student = students.FirstOrDefault();
                    viewModel.Student = student;
                    Session session = sessionLogic.GetModelBy(s => s.SessionId == viewModel.Session.Id);

                    if (student != null && student.Department.Id != viewModel.DepartmentModel.Id && student.Programme.ProgrammeType.Id != viewModel.ProgrammeTypeModel.Id)
                    {
                        SetMessage("Student is not in this Programme, Department!", Message.Category.Error);
                        KeepDropdownState(viewModel);
                        return View(viewModel);
                    }

                    int yearOfEntry = Convert.ToInt32(student.YearOfEntry.Split('/').FirstOrDefault());
                    int currentSession = Convert.ToInt32(session.Name.Split('/').FirstOrDefault());
                    if (yearOfEntry <= currentSession)
                    {
                        if (student.Department.Id == 50)
                        {
                            viewModel = GetStudentCourseRegistrationForMedicine(viewModel);
                        }
                        else
                        {
                            viewModel = GetStudentCourseRegistration(viewModel);
                        }
                    }
                    else
                    {
                        SetMessage("Current Session is less than Student's year of Entry", Message.Category.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
            }

            KeepDropdownState(viewModel);
            return View(viewModel);
        }


        public JsonResult SaveStudentData(string jsonData)
        {

            PersonModel personDetails;
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                personDetails = serializer.Deserialize<PersonModel>(jsonData);
                if (personDetails != null)
                {

                    using (TransactionScope scope = new TransactionScope())
                    {
                        personDetails = CreateStudentBioData(personDetails);
                        if (personDetails.IsError)
                        {
                            scope.Dispose();
                        }
                        else
                        {
                            personDetails.IsError = false;
                            personDetails.Message = "Operation Successful!";
                            if (!string.IsNullOrEmpty(personDetails.Id))
                            {
                                scope.Complete();
                            }
                        }
                    }
                }
                else
                {
                    personDetails = new PersonModel();
                    personDetails.IsError = true;
                    personDetails.Message = "Error! Kindly try again Later";
                }
            }
            catch (Exception ex)
            {
                personDetails = new PersonModel();
                personDetails.IsError = true;
                personDetails.Message = "Error! " + ex.Message;
            }
            return Json(personDetails, JsonRequestBehavior.AllowGet);
        }

        public PersonModel CreateStudentBioData(PersonModel personModel)
        {
            try
            {
                SexLogic sexLogic = new SexLogic();
                LevelLogic levelLogic = new LevelLogic();
                CountryLogic countryLogic = new CountryLogic();
                PersonLogic personLogic = new PersonLogic();
                Person person = new Person();
                Model.Model.Student student = new Model.Model.Student();
                Level level = new Level();
                SessionLogic sessionLogic = new SessionLogic();
                StudentLogic studentLogic = new StudentLogic();
                PersonType personType = new PersonType() { Id = 2 };
                StudentCategory studentCategory = new StudentCategory() { Id = 1 };
                ProgrammeLogic programmeLogic = new ProgrammeLogic();

                var existingStudent = studentLogic.GetModelsBy(s => s.EntryRegNo == personModel.regNo).LastOrDefault();

                var details = personModel;
                var sexId = Convert.ToInt32(details.sex);
                var countryId = Convert.ToInt32(details.country);
                var programmeId = Convert.ToInt32(personModel.programme);
                int yearId = personModel.yob == "--YY--" ? 0 : Convert.ToInt32(personModel.yob);
                int monthId = !string.IsNullOrEmpty(personModel.mob) ? Convert.ToInt32(personModel.mob) : 0;
                int dayId = !string.IsNullOrEmpty(personModel.dob) ? Convert.ToInt32(personModel.dob) : 0;
                string[] dob = yearId > 0 && monthId > 0 && dayId > 0 ? new DateTime(yearId, monthId, dayId).GetDateTimeFormats() : null;
                var departmentId = Convert.ToInt32(details.department);
                if (departmentId == 50)
                {
                    int leveld = Convert.ToInt32(personModel.level);
                    level = levelLogic.GetModelBy(l => l.LevelId == leveld);
                    if (personModel.modeOfEntry)
                    {
                        person.ModeOfEntry = "Direct Entry";
                    }
                    else
                    {
                        person.ModeOfEntry = null;
                    }
                }
                else
                {
                    level = levelLogic.GetModelBy(l => l.LevelId == 1);
                    if (personModel.modeOfEntry)
                    {
                        level = new Level { Id = 2 };
                        person.ModeOfEntry = "Direct Entry";
                    }
                }

                Sex sex = sexLogic.GetModelBy(s => s.Sex_Id == sexId);
                person.DateofBirth = dob != null ? dob[3] : null;
                person.MiddleName = personModel.middleName;
                person.Surname = personModel.surName;
                person.FirstName = personModel.firstName;
                person.PersonType = personType;
                person.Sex = sex.Name;
                person.contact_address = personModel.transcriptAddress;
                person.Country = countryLogic.GetModelBy(c => c.CountryId == countryId);
                person.DateFilled = DateTime.Now;
                if (existingStudent != null)
                {
                    var existingPerson = personLogic.GetModelBy(p => p.PersonId == existingStudent.Person.Id);
                    existingPerson.DateofBirth = !string.IsNullOrEmpty(person.DateofBirth) ? person.DateofBirth : existingPerson.DateofBirth;
                    existingPerson.MiddleName = person.MiddleName;
                    existingPerson.Surname = person.Surname;
                    existingPerson.FirstName = person.FirstName;
                    existingPerson.PersonType = person.PersonType;
                    existingPerson.Sex = person.Sex;
                    existingPerson.contact_address = person.contact_address;
                    existingPerson.Country = person.Country;
                    existingPerson.DateFilled = person.DateFilled;
                    existingPerson.ModeOfEntry = person.ModeOfEntry;
                    personLogic.Modify(existingPerson);
                    person = existingPerson;
                }
                else
                {
                    person = personLogic.Create(person);
                }

                student = new Model.Model.Student();
                student.Id = person.Id;
                student.Programme = programmeLogic.GetModelBy(p => p.ProgrammeTypeId == programmeId && p.DepartmentId == departmentId);
                var sessionOfEntryId = Convert.ToInt32(personModel.yearOfEntry);
                var sessionOfGraduationId = Convert.ToInt32(personModel.yearOfGraduation);
                if (sessionOfEntryId != sessionOfGraduationId)
                {
                    student.YearOfEntry = sessionLogic.GetModelBy(s => s.SessionId == sessionOfEntryId).Name;
                    student.GraduationDate = sessionLogic.GetModelBy(s => s.SessionId == sessionOfGraduationId).Name;
                    student.Department = new Department()
                    {
                        Id = Convert.ToInt32(personModel.department)
                    };
                    student.CurrentSession = new Session()
                    {
                        Id = Convert.ToInt32(personModel.yearOfEntry)
                    };
                    student.CurrentLevel = level;
                    student.StudentCategory = studentCategory;
                    student.EntryRegNo = personModel.regNo;
                    student.WesVerificationNumber = personModel.wesVerificationNumber;
                    if (existingStudent != null)
                    {
                        student.Id = existingStudent.Id;
                        student.Person = existingStudent.Person;
                        studentLogic.Modify(student);
                        personModel.Id = student.Id.ToString();
                    }
                    else
                    {
                        var createdStudent = studentLogic.Create(student);
                        personModel.Id = createdStudent.Id.ToString();
                    }
                }
                else
                {
                    personModel.IsError = true;
                    personModel.Message = "Session of Entry and Session of Graduation can not be equal";
                }

            }
            catch (Exception)
            {
                throw;
            }

            return personModel;
        }

        public ActionResult SaveAddedCourse(UploadResultViewModel viewModel)
        {
            StudentLogic studentLogic = new StudentLogic();
            List<Model.Model.Student> students = studentLogic.GetModelsBy(s => s.EntryRegNo == viewModel.Student.EntryRegNo);
            try
            {
                if (viewModel != null)
                {
                    var programmeCourses = viewModel.ProgrammeCourses;
                    SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                    StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                    ProgrammeCourseRegistrationLogic programmeCourseRegistrationLogic = new ProgrammeCourseRegistrationLogic();
                    StudentCourseMarkLogic studentCourseMarkLogic = new StudentCourseMarkLogic();
                    ScoreGradeLogic scoreGradeLogic = new ScoreGradeLogic();
                    viewModel.ProgrammeCourses = new List<ProgrammeCourse>();

                    int uploadCount = 0;

                    if (students.Count != 1)
                    {
                        SetMessage("Duplicate Matric Number OR Matric Number does not exist!", Message.Category.Error);
                        KeepDropdownState(viewModel);

                        return RedirectToAction("AddExtraCourse", new { personId = students.LastOrDefault().Id });
                    }
                    using (TransactionScope scope = new TransactionScope())
                    {
                        if (viewModel.FirstSemesterCourses != null)
                        {
                            viewModel.ProgrammeCourses.AddRange(viewModel.FirstSemesterCourses);
                        }
                        else if (viewModel.SecondSemesterCourses != null)
                        {
                            viewModel.ProgrammeCourses.AddRange(viewModel.SecondSemesterCourses);
                        }
                        else
                        {
                            viewModel.ProgrammeCourses = programmeCourses;
                        }

                        Model.Model.Student student = students.FirstOrDefault();
                        viewModel.Student = student;

                        StudentLevel studentLevel = CreateStudentLevel(viewModel);

                        for (int i = 0; i < viewModel.ProgrammeCourses.Count; i++)
                        {

                            long programmecourseId = viewModel.ProgrammeCourses[i].Id;
                            int semesterId = viewModel.ProgrammeCourses[i].Semester.Id;
                            var courseGrade = "";
                            var grade = "";


                            if (viewModel.DepartmentModel.Id == 50)
                            {

                                SessionSemester sessionSemester = sessionSemesterLogic.GetModelBy(s => s.Semester_Id == semesterId && s.Session_Id == viewModel.Session.Id);

                                if (sessionSemester != null)
                                {
                                    ProgrammeCourseRegistration courseRegistration = programmeCourseRegistrationLogic.GetModelsBy(p => p.StudentId == studentLevel.Student.Id && p.PROGRAMME_COURSES.PROGRAMME.ProgrammeTypeId == studentLevel.ProgrammeType.Id &&
                                                            p.PROGRAMME_COURSES.PROGRAMME.DepartmentId == studentLevel.Department.Id && p.SessionSemesterId == sessionSemester.Id && p.LevelId == viewModel.Level.Id && p.ProgrammeCourseId == programmecourseId).LastOrDefault();

                                    if (courseRegistration == null && !string.IsNullOrEmpty(viewModel.ProgrammeCourses[i].Grade))
                                    {
                                        int levelId = viewModel.ProgrammeCourses[i].Level.Id;
                                        var carryOverlevel = studentLevelLogic.GetModelsBy(s => s.Level_Id == levelId && s.STUDENT.StudentId == studentLevel.Student.Id).LastOrDefault();

                                        if (carryOverlevel != null)
                                        {
                                            var existingCarryOverCourse = programmeCourseRegistrationLogic.GetModelsBy(p => p.StudentId == studentLevel.Student.Id && p.PROGRAMME_COURSES.PROGRAMME.ProgrammeTypeId == studentLevel.ProgrammeType.Id &&
                                                              p.PROGRAMME_COURSES.PROGRAMME.DepartmentId == studentLevel.Department.Id && p.ProgrammeCourseId == programmecourseId && p.IsCarriedOverCourses && p.Approved == false);

                                            foreach (ProgrammeCourseRegistration carryOverCourseRegistration in existingCarryOverCourse)
                                            {
                                                if (!string.IsNullOrEmpty(viewModel.ProgrammeCourses[i].Grade))
                                                {
                                                    carryOverCourseRegistration.Approved = true;
                                                    programmeCourseRegistrationLogic.Modify(carryOverCourseRegistration);

                                                }
                                            }
                                        }

                                        viewModel.Level = studentLevel.Level;
                                        viewModel.SessionSemester = sessionSemester;
                                        viewModel.ProgrammeCourse = viewModel.ProgrammeCourses[i];
                                        courseRegistration = CreateProgrammeCourseRegistration(viewModel);
                                    }

                                    StudentCourseMark studentCourseMark = studentCourseMarkLogic.GetModelsBy(crd => crd.PROGRAMME_COURSE_REGISTRATION.ProgrammeCourseId == programmecourseId &&
                                                                                                crd.CourseRegId == courseRegistration.Id).LastOrDefault();
                                    if (studentCourseMark == null)
                                    {
                                        if (!string.IsNullOrEmpty(viewModel.ProgrammeCourses[i].Grade))
                                        {
                                            studentCourseMark = new StudentCourseMark();
                                            studentCourseMark.ProgrammeCourseRegistration = courseRegistration;
                                            studentCourseMark.DateRecorded = DateTime.Now;

                                            int examScore = Convert.ToInt32(viewModel.ProgrammeCourses[i].Grade);
                                            //AssignScoresForMedicalStudents(studentCourseMark, examScore);
                                            studentCourseMarkLogic.Create(studentCourseMark);

                                        }
                                    }
                                    else
                                    {
                                        if (viewModel.ProgrammeCourses[i].IsRegistered)
                                        {

                                            if (string.IsNullOrEmpty(viewModel.ProgrammeCourses[i].Grade))
                                            {

                                                continue;
                                            }

                                            AssignScores(studentCourseMark, courseGrade);
                                            studentCourseMark.CourseUnit = viewModel.ProgrammeCourses[i].CourseUnit;

                                            studentCourseMark.Grade = courseGrade.Trim();
                                            studentCourseMarkLogic.Modify(studentCourseMark);

                                        }
                                    }
                                    uploadCount += 1;
                                }

                            }
                            else
                            {
                                if (!viewModel.ProgrammeCourses[i].IsRegistered)
                                {
                                    continue;
                                }

                                if (viewModel.ProgrammeCourses[i].Grade != null)
                                {
                                    grade = viewModel.ProgrammeCourses[i].Grade.Trim();
                                    courseGrade = viewModel.GradeSelectList.Where(s => s.Value == grade).Select(s => s.Text).LastOrDefault();
                                }
                                SessionSemester sessionSemester = sessionSemesterLogic.GetModelBy(s => s.Session_Id == studentLevel.Session.Id && s.Semester_Id == semesterId);

                                if (sessionSemester != null)
                                {
                                    ProgrammeCourseRegistration courseRegistration = programmeCourseRegistrationLogic.GetModelsBy(p => p.StudentId == studentLevel.Student.Id && p.PROGRAMME_COURSES.PROGRAMME.ProgrammeTypeId == studentLevel.ProgrammeType.Id &&
                                                            p.PROGRAMME_COURSES.PROGRAMME.DepartmentId == studentLevel.Department.Id && p.SessionSemesterId == sessionSemester.Id && p.LevelId == viewModel.Level.Id && p.ProgrammeCourseId == programmecourseId).LastOrDefault();

                                    if (courseRegistration == null && viewModel.ProgrammeCourses[i].IsRegistered)
                                    {
                                        int levelId = viewModel.ProgrammeCourses[i].Level.Id;
                                        var carryOverlevel = studentLevelLogic.GetModelsBy(s => s.Level_Id == levelId && s.STUDENT.StudentId == studentLevel.Student.Id).LastOrDefault();

                                        if (carryOverlevel != null)
                                        {
                                            var existingCarryOverCourse = programmeCourseRegistrationLogic.GetModelsBy(p => p.StudentId == studentLevel.Student.Id && p.PROGRAMME_COURSES.PROGRAMME.ProgrammeTypeId == studentLevel.ProgrammeType.Id &&
                                                             p.PROGRAMME_COURSES.PROGRAMME.DepartmentId == studentLevel.Department.Id && p.ProgrammeCourseId == programmecourseId && p.IsCarriedOverCourses && p.Approved == false);

                                            foreach (ProgrammeCourseRegistration carryOverCourseRegistration in existingCarryOverCourse)
                                            {
                                                if (!string.IsNullOrEmpty(viewModel.ProgrammeCourses[i].Grade))
                                                {
                                                    carryOverCourseRegistration.Approved = true;
                                                    programmeCourseRegistrationLogic.Modify(carryOverCourseRegistration);

                                                }
                                            }
                                        }

                                        viewModel.Level = studentLevel.Level;
                                        viewModel.SessionSemester = sessionSemester;
                                        viewModel.ProgrammeCourse = viewModel.ProgrammeCourses[i];

                                        courseRegistration = CreateProgrammeCourseRegistration(viewModel);
                                    }

                                    StudentCourseMark studentCourseMark = studentCourseMarkLogic.GetModelsBy(crd => crd.PROGRAMME_COURSE_REGISTRATION.ProgrammeCourseId == programmecourseId &&
                                                                                                crd.CourseRegId == courseRegistration.Id).LastOrDefault();
                                    if (studentCourseMark == null)
                                    {
                                        if (viewModel.ProgrammeCourses[i].IsRegistered && !string.IsNullOrEmpty(viewModel.ProgrammeCourses[i].Grade))
                                        {

                                            if (viewModel.ProgrammeCourses[i].IsCarryOverCourse && !viewModel.ProgrammeCourses[i].IsApproved)
                                            {
                                                courseRegistration.Approved = false;
                                                courseRegistration.IsCarriedOverCourses = true;
                                                programmeCourseRegistrationLogic.Modify(courseRegistration);
                                            }

                                            studentCourseMark = new StudentCourseMark();
                                            studentCourseMark.ProgrammeCourseRegistration = courseRegistration;
                                            studentCourseMark.DateRecorded = DateTime.Now;

                                            string[] yearSplit = student.YearOfEntry.Split('/');
                                            int compareYear = Convert.ToInt32(yearSplit.FirstOrDefault());
                                            ScoreGrade scoreGrade = scoreGradeLogic.GetModelsBy(sg => (compareYear >= sg.Session_Start && compareYear <= sg.Session_End) && sg.Grade == courseGrade).LastOrDefault();
                                            if (scoreGrade != null) studentCourseMark.GradePoint = (double)scoreGrade.GradePoint;

                                            AssignScores(studentCourseMark, courseGrade);
                                            studentCourseMark.CourseUnit = viewModel.ProgrammeCourses[i].CourseUnit;
                                            studentCourseMark.Grade = courseGrade.Trim();
                                            studentCourseMarkLogic.Create(studentCourseMark);

                                        }
                                    }
                                    else
                                    {
                                        if (viewModel.ProgrammeCourses[i].IsRegistered)
                                        {

                                            if (string.IsNullOrEmpty(viewModel.ProgrammeCourses[i].Grade))
                                            {

                                                continue;
                                            }
                                            if (viewModel.ProgrammeCourses[i].IsCarryOverCourse && !viewModel.ProgrammeCourses[i].IsApproved)
                                            {
                                                courseRegistration.Approved = false;
                                                courseRegistration.IsCarriedOverCourses = true;
                                                programmeCourseRegistrationLogic.Modify(courseRegistration);
                                            }
                                            if (!viewModel.ProgrammeCourses[i].IsCarryOverCourse && !viewModel.ProgrammeCourses[i].IsApproved)
                                            {
                                                courseRegistration.Approved = true;
                                                courseRegistration.IsCarriedOverCourses = false;
                                                programmeCourseRegistrationLogic.Modify(courseRegistration);
                                            }

                                            AssignScores(studentCourseMark, courseGrade);
                                            studentCourseMark.CourseUnit = viewModel.ProgrammeCourses[i].CourseUnit;
                                            studentCourseMark.GradePoint = 0.0;
                                            studentCourseMark.Grade = courseGrade.Trim();
                                            studentCourseMarkLogic.Modify(studentCourseMark);

                                        }
                                    }
                                    uploadCount += 1;
                                }

                            }
                        }
                        if (uploadCount > 0)
                        {
                            UploadedResultLogic uploadedResultLogic = new UploadedResultLogic();
                            UploadedResult uploadedResult = uploadedResultLogic.GetModelBy(up => up.StudentId == student.Id);
                            if (uploadedResult == null)
                            {
                                UserLogic userLogic = new UserLogic();
                                User user = userLogic.GetModelBy(u => u.User_Name == User.Identity.Name);
                                uploadedResult = new UploadedResult
                                {
                                    UploadedBy = user.Id,
                                    FileUrl = "Added Result on the System",
                                    Programme = new Programme() { Id = student.Programme.Id },
                                    DateUploaded = DateTime.Now,
                                    WorkSheetIndex = 1,
                                    Student = student,
                                    Client = Request.LogonUserIdentity.Name + " (" + HttpContext.Request.UserHostAddress + ")"
                                };
                                uploadedResultLogic.Create(uploadedResult);
                            }
                        }
                        scope.Complete();
                    }

                    if (uploadCount > 0)
                    {
                        SetMessage("Operation Successful!", Message.Category.Information);
                    }
                    else
                    {
                        SetMessage("Error ocurred while Processing your request!", Message.Category.Error);
                    }
                    RetainDropDown(viewModel);

                    return RedirectToAction("AddExtraCourse", new { personId = students.LastOrDefault().Id });
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
            }

            RetainDropDown(viewModel);

            return RedirectToAction("AddExtraCourse", new { personId = students.LastOrDefault().Id });
        }
        private void AssignScores(StudentCourseMark studentCourseMark, string grade)
        {
            try
            {
                switch (grade)
                {
                    case "A":
                        studentCourseMark.TestScore = 30M;
                        studentCourseMark.ExamScore = 50M;
                        break;
                    case "AB":
                        studentCourseMark.TestScore = 30M;
                        studentCourseMark.ExamScore = 40M;
                        break;
                    case "B":
                        studentCourseMark.TestScore = 20M;
                        studentCourseMark.ExamScore = 40M;
                        break;
                    case "BC":
                        studentCourseMark.TestScore = 20M;
                        studentCourseMark.ExamScore = 30M;
                        break;
                    case "C":
                        studentCourseMark.TestScore = 20M;
                        studentCourseMark.ExamScore = 20M;
                        break;
                    case "CD":
                        studentCourseMark.TestScore = 10M;
                        studentCourseMark.ExamScore = 20M;
                        break;
                    case "D":
                        studentCourseMark.TestScore = 10M;
                        studentCourseMark.ExamScore = 35M;
                        break;
                    case "E":
                        studentCourseMark.TestScore = 10M;
                        studentCourseMark.ExamScore = 30M;
                        break;
                    case "F":
                        studentCourseMark.TestScore = 0M;
                        studentCourseMark.ExamScore = 0M;
                        break;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        //private void AssignScoresForMedicalStudents(StudentCourseMark studentCourseMark, int score)
        //{
        //    try
        //    {
        //        switch (score)
        //        {
        //            case var expression when score >= 80:
        //                studentCourseMark.TestScore = 30M;
        //                studentCourseMark.ExamScore = score;
        //                studentCourseMark.Grade = "A";
        //                break;
        //            case var expression when score >= 70:
        //                studentCourseMark.TestScore = 30M;
        //                studentCourseMark.ExamScore = score;
        //                studentCourseMark.Grade = "AB";
        //                break;
        //            case var expression when score >= 60:
        //                studentCourseMark.TestScore = 30M;
        //                studentCourseMark.ExamScore = score;
        //                studentCourseMark.Grade = "B";
        //                break;
        //            case var expression when score >= 50:
        //                studentCourseMark.TestScore = 30M;
        //                studentCourseMark.ExamScore = score;
        //                studentCourseMark.Grade = "BC";
        //                break;
        //            case var expression when score >= 40:
        //                studentCourseMark.TestScore = 30M;
        //                studentCourseMark.ExamScore = score;
        //                studentCourseMark.Grade = "C";
        //                break;
        //            case var expression when score >= 60:
        //                studentCourseMark.TestScore = 30M;
        //                studentCourseMark.ExamScore = score;
        //                studentCourseMark.Grade = "CD";
        //                break;
        //            case var expression when score >= 45:
        //                studentCourseMark.TestScore = 15M;
        //                studentCourseMark.ExamScore = score;
        //                studentCourseMark.Grade = "D";
        //                break;
        //            case var expression when score >= 30:
        //                studentCourseMark.TestScore = 10M;
        //                studentCourseMark.ExamScore = score;
        //                studentCourseMark.Grade = "E";
        //                break;
        //            case var expression when score >= 0:
        //                studentCourseMark.TestScore = 0M;
        //                studentCourseMark.ExamScore = score;
        //                studentCourseMark.Grade = "F";
        //                break;
        //        }
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}
        //private int GetStudentLevel(string sessionOfEntry, string currentOfSession)
        //{
        //    int level = 1;
        //    try
        //    {
        //        var firstSession = sessionOfEntry.Split('/').FirstOrDefault();
        //        var seecondSession = currentOfSession.Split('/').FirstOrDefault();
        //        if (Convert.ToInt32(seecondSession) != Convert.ToInt32(firstSession))
        //        {
        //            level = Convert.ToInt32(seecondSession) - Convert.ToInt32(firstSession) + 1;
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        throw;
        //    }
        //    return level;
        //}
        public void KeepDropdownState(UploadResultViewModel viewModel)
        {
            try
            {
                SemesterLogic semesterLogic = new SemesterLogic();
                if (viewModel.Session != null && viewModel.ProgrammeTypeModel != null && viewModel.DepartmentModel != null && viewModel.Level != null)
                {
                    ViewBag.Session = new SelectList(viewModel.SessionSelectList, "Value", "Text", viewModel.Session.Id);
                    //ViewBag.Semester = new SelectList(new List<Semester>(), "Id", "Name");
                    ViewBag.Programme = new SelectList(viewModel.ProgrammeTypeSelectListItem, "Value", "Text", viewModel.ProgrammeTypeModel.Id);
                    ViewBag.Department = new SelectList(viewModel.DepartmentSelectListItem, "Value", "Text", viewModel.DepartmentModel.Id);
                    ViewBag.Level = new SelectList(viewModel.LevelSelectList, "Value", "Text", viewModel.Level.Id);
                    ViewBag.Semester = new SelectList(viewModel.MbbselectListItem, "Value", "Text", viewModel.Semester.Id);
                }
                else
                {
                    ViewBag.Session = viewModel.SessionSelectList;
                    ViewBag.Programme = viewModel.ProgrammeTypeSelectListItem;
                    ViewBag.Department = new SelectList(new List<Department>(), "Id", "Name");
                    ViewBag.Level = viewModel.LevelSelectList;

                }
                ViewBag.ScoreGrade = viewModel.GradeSelectList;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void SetCoursesDropDown(UploadResultViewModel viewModel)
        {
            try
            {
                if (viewModel.ProgrammeCourses != null && viewModel.ProgrammeCourses.Count > 0)
                {
                    ProgrammeCourse programmeCourse = viewModel.ProgrammeCourses.FirstOrDefault();

                    viewModel.ProgrammeCourses = viewModel.ProgrammeCourses.OrderBy(c => c.Level.Id).ThenBy(c => c.Semester.Id).ToList();

                    viewModel.FirstSemesterCourses =
                        viewModel.ProgrammeCourses.Where(s => s.Semester.Id == 1)
                            .OrderBy(c => c.Level.Id)
                            .ThenBy(c => c.Semester.Id)
                            .ToList();

                    viewModel.SecondSemesterCourses =
                       viewModel.ProgrammeCourses.Where(s => s.Semester.Id == 2)
                           .OrderBy(c => c.Level.Id)
                           .ThenBy(c => c.Semester.Id)
                           .ToList();

                    int fIndex = 0;
                    foreach (ProgrammeCourse fCourse in viewModel.FirstSemesterCourses)
                    {
                        if (fCourse.Grade != null)
                        {
                            var gradeValue = viewModel.GradeSelectList.Where(s => s.Text == fCourse.Grade.Trim()).LastOrDefault().Value;
                            var value = Convert.ToInt32(gradeValue);
                            fCourse.IsRegistered = true;
                            ViewData["fCourse" + fIndex] = new SelectList(viewModel.GradeSelectList, Utility.VALUE, Utility.TEXT,
                           value);
                        }
                        else
                        {
                            ViewData["fCourse" + fIndex] = new SelectList(viewModel.GradeSelectList, Utility.VALUE, Utility.TEXT,
                           fCourse.Grade);
                        }


                        fIndex++;
                    }
                    int sIndex = 0;
                    foreach (ProgrammeCourse sCourse in viewModel.SecondSemesterCourses)
                    {
                        if (sCourse.Grade != null)
                        {
                            var gradeValue = viewModel.GradeSelectList.Where(s => s.Text == sCourse.Grade.Trim()).LastOrDefault().Value;
                            var value = Convert.ToInt32(gradeValue);
                            sCourse.IsRegistered = true;
                            ViewData["sCourse" + sIndex] = new SelectList(viewModel.GradeSelectList, Utility.VALUE, Utility.TEXT,
                          value);
                        }
                        else
                        {
                            ViewData["sCourse" + sIndex] = new SelectList(viewModel.GradeSelectList, Utility.VALUE, Utility.TEXT,
                           sCourse.Grade);
                        }

                        sIndex++;
                    }

                }

            }
            catch (Exception)
            {
                throw;
            }
        }
        public void PopulateDropDownstate(UploadResultViewModel viewModel)
        {

            try
            {
                ViewBag.ProgrammeTypeId = viewModel.ProgrammeTypeSelectListItem;
                ViewBag.DepartmentId = new SelectList(new List<Department>(), ID, NAME);
                ViewBag.CountryId = viewModel.CountrySelectListItem;
                ViewBag.SexId = viewModel.SexSelectListItem;
                ViewBag.DayOfBirthId = new SelectList(new List<Value>(), ID, NAME);
                ViewBag.MonthOfBirthId = viewModel.MonthOfBirthSelectList;
                ViewBag.YearOfBirthId = viewModel.YearOfBirthSelectList;
                ViewBag.SessionOfEntryId = viewModel.SessionOfEntrySelectListItem;
                ViewBag.SessionOfGraduationId = new SelectList(new List<Value>(), ID, NAME);
                ViewBag.LevelId = viewModel.LevelSelectList;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void RetainDropDown(UploadResultViewModel viewModel)
        {
            try
            {
                if (viewModel.Session != null && viewModel.ProgrammeTypeModel != null && viewModel.Department != null && viewModel.Level != null)
                {
                    ViewBag.Session = new SelectList(viewModel.SessionSelectList, "Value", "Text", viewModel.Session.Id);
                    ViewBag.Programme = new SelectList(viewModel.ProgrammeTypeSelectListItem, "Value", "Text", viewModel.ProgrammeTypeModel.Id);
                    ViewBag.Department = new SelectList(viewModel.DepartmentSelectListItem, "Value", "Text", viewModel.DepartmentModel.Id);
                    ViewBag.Level = new SelectList(viewModel.LevelSelectList, "Value", "Text", viewModel.Level.Id);
                    ViewBag.Semester = new SelectList(viewModel.MbbselectListItem, "Value", "Text", viewModel.Semester.Id);
                }
                else
                {
                    ViewBag.Session = viewModel.SessionSelectList;
                    ViewBag.Programme = viewModel.ProgrammeTypeSelectListItem;
                    ViewBag.Department = new SelectList(new List<Department>(), "Id", "Name");
                    ViewBag.Level = viewModel.LevelSelectList;
                }
                ViewBag.ScoreGrade = viewModel.GradeSelectList;
            }
            catch (Exception ex)
            {

                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
            }
        }

        public StudentLevel CreateStudentLevel(UploadResultViewModel viewModel)
        {
            StudentLevel studentLevel;
            try
            {
                Level level = new Level();
                if (viewModel.DepartmentModel.Id == 50)
                {
                    int semesterId = viewModel.ProgrammeCourses.FirstOrDefault().Semester.Id;
                    if (semesterId == 3)
                    {
                        level = new Level() { Id = 1 };
                    }
                    else
                    {
                        int levelId = semesterId - 2;
                        level = new Level() { Id = levelId };
                    }
                    viewModel.Level = level;
                }

                List<StudentLevel> studentLevelList = new List<StudentLevel>();
                StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                studentLevelList = studentLevelLogic.GetModelsBy(p => p.Person_Id == viewModel.Student.Id && p.Department_Id == viewModel.DepartmentModel.Id && p.PROGRAMME_TYPE.ProgrammeTypeId == viewModel.ProgrammeTypeModel.Id &&
                                                                          p.Session_Id == viewModel.Session.Id && p.Level_Id == viewModel.Level.Id);

                studentLevel = studentLevelList.LastOrDefault();
                if (studentLevelList.Count == 0)
                {
                    studentLevel = new StudentLevel();
                    studentLevel.Student = viewModel.Student;
                    studentLevel.Department = viewModel.DepartmentModel;
                    studentLevel.ProgrammeType = viewModel.ProgrammeTypeModel;
                    studentLevel.Level = viewModel.Level;
                    studentLevel.Session = viewModel.Session;

                    StudentLevel newStudentLevel = studentLevelLogic.Create(studentLevel);
                    studentLevel.Id = newStudentLevel.Id;
                }

            }
            catch (Exception ex)
            {

                throw;
            }
            return studentLevel;
        }

        public ProgrammeCourseRegistration CreateProgrammeCourseRegistration(UploadResultViewModel viewModel)
        {
            ProgrammeCourseRegistration courseRegistration = new ProgrammeCourseRegistration();
            try
            {

                ProgrammeCourseRegistrationLogic programmeCourseRegistrationLogic = new ProgrammeCourseRegistrationLogic();
                courseRegistration.Level = viewModel.Level;
                courseRegistration.SessionSemester = viewModel.SessionSemester;
                courseRegistration.ProgrammeCourse = viewModel.ProgrammeCourse;
                courseRegistration.Student = viewModel.Student;
                courseRegistration.CourseUnit = viewModel.ProgrammeCourse.CourseUnit;
                courseRegistration.IsCarriedOverCourses = viewModel.ProgrammeCourse.IsCarryOverCourse;
                if (viewModel.ProgrammeCourse.IsCarryOverCourse)
                {
                    courseRegistration.Approved = false;
                }
                else
                {
                    courseRegistration.Approved = true;
                }

                courseRegistration = programmeCourseRegistrationLogic.Create(courseRegistration);
            }
            catch (Exception ex)
            {

                throw;
            }
            return courseRegistration;
        }
        private UploadResultViewModel GetStudentCourseRegistration(UploadResultViewModel viewModel)
        {
            try
            {

                StudentLogic studentLogic = new StudentLogic();
                StudentCourseMarkLogic studentCourseMarkLogic = new StudentCourseMarkLogic();
                SessionLogic sessionLogic = new SessionLogic();
                var carryOverCourses = new List<ProgrammeCourse>();
                ProgrammeCourseLogic programmeCourseLogic = new ProgrammeCourseLogic();
                ProgrammeCourseRegistrationLogic programmeCourseRegistrationLogic = new ProgrammeCourseRegistrationLogic();

                Level studentLevel = viewModel.Level.Id <= 6 ? new Level() { Id = viewModel.Level.Id } : new Level() { Id = 6 };
                viewModel.Level = studentLevel;

                viewModel.StudentCourseMarks = studentCourseMarkLogic.GetModelsBy(c => c.PROGRAMME_COURSE_REGISTRATION.StudentId == viewModel.Student.Id && c.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.DepartmentId == viewModel.DepartmentModel.Id && c.PROGRAMME_COURSE_REGISTRATION.LevelId == viewModel.Level.Id);
                var programmeCourses = programmeCourseLogic.GetModelsBy(c => c.PROGRAMME.DepartmentId == viewModel.DepartmentModel.Id && c.Activated && c.LevelId == viewModel.Level.Id && c.PROGRAMME.ProgrammeTypeId == viewModel.ProgrammeTypeModel.Id && c.Session_Id == viewModel.Session.Id);

                if (viewModel.StudentCourseMarks != null && viewModel.StudentCourseMarks.Count > 0)
                {
                    var courseMark = viewModel.StudentCourseMarks.FirstOrDefault();

                    if (courseMark != null && courseMark.ProgrammeCourseRegistration.SessionSemester.Session.Id == viewModel.Session.Id)
                    {
                        for (int i = 0; i < viewModel.StudentCourseMarks.Count; i++)
                        {
                            viewModel.StudentCourseMarks[i].ProgrammeCourseRegistration.ProgrammeCourse.Course
                                .Grade = viewModel.StudentCourseMarks[i].Grade;
                            viewModel.StudentCourseMarks[i].ProgrammeCourseRegistration.ProgrammeCourse.Grade =
                                viewModel.StudentCourseMarks[i].Grade;
                            viewModel.StudentCourseMarks[i].ProgrammeCourseRegistration.ProgrammeCourse
                                .IsCarryOverCourse =
                                viewModel.StudentCourseMarks[i].ProgrammeCourseRegistration.IsCarriedOverCourses;
                            viewModel.StudentCourseMarks[i].ProgrammeCourseRegistration.ProgrammeCourse
                                .IsApproved =
                                (bool)viewModel.StudentCourseMarks[i].ProgrammeCourseRegistration.Approved;

                        }
                        carryOverCourses = studentCourseMarkLogic.GetModelsBy(
                                c =>
                                    c.PROGRAMME_COURSE_REGISTRATION.StudentId == viewModel.Student.Id &&
                                    c.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.DepartmentId ==
                                    viewModel.DepartmentModel.Id &&
                                    c.PROGRAMME_COURSE_REGISTRATION.IsCarriedOverCourses &&
                                    !(bool)c.PROGRAMME_COURSE_REGISTRATION.Approved)
                                .Select(s => s.ProgrammeCourseRegistration.ProgrammeCourse)
                                .ToList();

                        viewModel.ProgrammeCourses = viewModel.StudentCourseMarks.Select(s => s.ProgrammeCourseRegistration.ProgrammeCourse).ToList();

                        for (int i = 0; i < programmeCourses.Count; i++)
                        {
                            ProgrammeCourse departmentalCourse = programmeCourses[i];
                            if (
                                viewModel.StudentCourseMarks.LastOrDefault(
                                    c =>
                                        c.ProgrammeCourseRegistration.ProgrammeCourse.Id ==
                                        departmentalCourse.Id &&
                                        c.ProgrammeCourseRegistration.Level.Id == programmeCourses[i].Level.Id) ==
                                null)
                            {
                                viewModel.ProgrammeCourses.Add(departmentalCourse);
                            }
                        }
                    }
                    else
                    {
                        var existingStudentLevel = studentLogic.GetModelsBy(s => s.CurrentLevelId == viewModel.Level.Id && s.CurrentSessionId == viewModel.Session.Id).LastOrDefault();

                        if (existingStudentLevel == null && viewModel.Level.Id == 6)
                        {
                            ProgrammeCourseRegistration courseRegistration = programmeCourseRegistrationLogic.GetModelsBy(p => p.StudentId == viewModel.Student.Id && p.PROGRAMME_COURSES.PROGRAMME.ProgrammeTypeId == viewModel.Student.Programme.ProgrammeType.Id &&
                                                p.PROGRAMME_COURSES.PROGRAMME.DepartmentId == viewModel.Student.Department.Id && p.SESSION_SEMESTER.Session_Id == viewModel.Session.Id).LastOrDefault();

                            if (courseRegistration != null && courseRegistration.SessionSemester.Session.Id == viewModel.Session.Id && courseRegistration.Level.Id == viewModel.Level.Id)
                            {
                                viewModel.StudentCourseMarks = studentCourseMarkLogic.GetModelsBy(c => c.PROGRAMME_COURSE_REGISTRATION.StudentId == viewModel.Student.Id && c.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.DepartmentId == viewModel.DepartmentModel.Id && c.PROGRAMME_COURSE_REGISTRATION.IsCarriedOverCourses && c.PROGRAMME_COURSE_REGISTRATION.Approved == false);
                                carryOverCourses = viewModel.StudentCourseMarks.Select(s => s.ProgrammeCourseRegistration.ProgrammeCourse).ToList();
                                viewModel.ProgrammeCourses = programmeCourses;

                            }
                            else if (courseRegistration != null)
                            {
                                SetMessage("An Error Occured while processing your request You have already registered courses for this Session in a different Level " + courseRegistration.Level.Name, Message.Category.Error);
                            }
                            else
                            {
                                viewModel.StudentCourseMarks = studentCourseMarkLogic.GetModelsBy(c => c.PROGRAMME_COURSE_REGISTRATION.StudentId == viewModel.Student.Id && c.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.DepartmentId == viewModel.DepartmentModel.Id && c.PROGRAMME_COURSE_REGISTRATION.IsCarriedOverCourses && c.PROGRAMME_COURSE_REGISTRATION.Approved == false);
                                if (viewModel.StudentCourseMarks.Count > 0)
                                {
                                    carryOverCourses = viewModel.StudentCourseMarks.Select(s => s.ProgrammeCourseRegistration.ProgrammeCourse).ToList();
                                }
                                if (programmeCourses.Count > 0)
                                {
                                    viewModel.ProgrammeCourses = programmeCourses;
                                }
                                else
                                {
                                    SetMessage("No Courses found for the selected parameter", Message.Category.Error);
                                }
                            }
                        }
                        else
                        {
                            SetMessage("An Error Occured while processing your request You have already registered courses for this level in a different Session " +
                                courseMark.ProgrammeCourseRegistration.SessionSemester.Session.Name,
                                Message.Category.Error);
                        }
                    }
                }
                else
                {
                    ProgrammeCourseRegistration courseRegistration = programmeCourseRegistrationLogic.GetModelsBy(p => p.StudentId == viewModel.Student.Id && p.PROGRAMME_COURSES.PROGRAMME.ProgrammeTypeId == viewModel.Student.Programme.ProgrammeType.Id &&
                                                p.PROGRAMME_COURSES.PROGRAMME.DepartmentId == viewModel.Student.Department.Id && p.SESSION_SEMESTER.Session_Id == viewModel.Session.Id).LastOrDefault();

                    if (courseRegistration != null && courseRegistration.SessionSemester.Session.Id == viewModel.Session.Id && courseRegistration.Level.Id == viewModel.Level.Id)
                    {
                        viewModel.StudentCourseMarks = studentCourseMarkLogic.GetModelsBy(c => c.PROGRAMME_COURSE_REGISTRATION.StudentId == viewModel.Student.Id && c.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.DepartmentId == viewModel.DepartmentModel.Id && c.PROGRAMME_COURSE_REGISTRATION.IsCarriedOverCourses && c.PROGRAMME_COURSE_REGISTRATION.Approved == false);
                        carryOverCourses = viewModel.StudentCourseMarks.Select(s => s.ProgrammeCourseRegistration.ProgrammeCourse).ToList();
                        viewModel.ProgrammeCourses = programmeCourses;

                    }
                    else if (courseRegistration != null)
                    {
                        SetMessage("An Error Occured while processing your request You have already registered courses for this Session in a different Level " + courseRegistration.Level.Name, Message.Category.Error);
                    }
                    else
                    {
                        viewModel.StudentCourseMarks = studentCourseMarkLogic.GetModelsBy(c => c.PROGRAMME_COURSE_REGISTRATION.StudentId == viewModel.Student.Id && c.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.DepartmentId == viewModel.DepartmentModel.Id && c.PROGRAMME_COURSE_REGISTRATION.IsCarriedOverCourses && c.PROGRAMME_COURSE_REGISTRATION.Approved == false);
                        if (viewModel.StudentCourseMarks.Count > 0)
                        {
                            carryOverCourses = viewModel.StudentCourseMarks.Select(s => s.ProgrammeCourseRegistration.ProgrammeCourse).ToList();
                        }
                        viewModel.ProgrammeCourses = programmeCourses;
                    }
                }
                if (carryOverCourses.Count > 0)
                {
                    for (int i = 0; i < carryOverCourses.Count; i++)
                    {
                        if (carryOverCourses[i].Level.Id != viewModel.Level.Id && carryOverCourses[i].Level.Id < viewModel.Level.Id)
                        {
                            long courseId = carryOverCourses[i].Id;
                            var existingCourse = viewModel.ProgrammeCourses.Where(s => s.Id == courseId);
                            if (!existingCourse.Any())
                            {
                                viewModel.ProgrammeCourses.Add(carryOverCourses[i]);
                            }
                        }
                    }
                }
                SetCoursesDropDown(viewModel);
            }
            catch (Exception ex)
            {

                throw;
            }
            return viewModel;
        }
        private UploadResultViewModel GetStudentCourseRegistrationForMedicine(UploadResultViewModel viewModel)
        {
            try
            {

                StudentLogic studentLogic = new StudentLogic();
                StudentCourseMarkLogic studentCourseMarkLogic = new StudentCourseMarkLogic();
                SessionLogic sessionLogic = new SessionLogic();
                var carryOverCourses = new List<ProgrammeCourse>();
                ProgrammeCourseLogic programmeCourseLogic = new ProgrammeCourseLogic();
                ProgrammeCourseRegistrationLogic programmeCourseRegistrationLogic = new ProgrammeCourseRegistrationLogic();
                Level studentLevel = new Level();

                if (viewModel.Semester.Id == 3)
                {
                    studentLevel = new Level() { Id = 1 };
                }
                else
                {
                    int levelId = viewModel.Semester.Id - 2;
                    studentLevel = new Level() { Id = levelId };
                }

                viewModel.Level = studentLevel;
                viewModel.StudentCourseMarks = studentCourseMarkLogic.GetModelsBy(c => c.PROGRAMME_COURSE_REGISTRATION.StudentId == viewModel.Student.Id && c.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.DepartmentId == viewModel.DepartmentModel.Id && c.PROGRAMME_COURSE_REGISTRATION.SESSION_SEMESTER.Semester_Id == viewModel.Semester.Id);
                var programmeCourses = programmeCourseLogic.GetModelsBy(c => c.PROGRAMME.DepartmentId == viewModel.DepartmentModel.Id && c.Activated && c.SemesterId == viewModel.Semester.Id && c.PROGRAMME.ProgrammeTypeId == viewModel.ProgrammeTypeModel.Id && c.Session_Id == viewModel.Session.Id);

                if (viewModel.StudentCourseMarks != null && viewModel.StudentCourseMarks.Count > 0)
                {
                    var courseMark = viewModel.StudentCourseMarks.FirstOrDefault();

                    if (courseMark != null && courseMark.ProgrammeCourseRegistration.SessionSemester.Session.Id == viewModel.Session.Id)
                    {
                        for (int i = 0; i < viewModel.StudentCourseMarks.Count; i++)
                        {
                            viewModel.StudentCourseMarks[i].ProgrammeCourseRegistration.ProgrammeCourse.Course.Grade = viewModel.StudentCourseMarks[i].Grade;
                            viewModel.StudentCourseMarks[i].ProgrammeCourseRegistration.ProgrammeCourse.Grade = viewModel.StudentCourseMarks[i].Grade;
                            viewModel.StudentCourseMarks[i].ProgrammeCourseRegistration.ProgrammeCourse.ExamScore = (decimal)viewModel.StudentCourseMarks[i].ExamScore;
                            viewModel.StudentCourseMarks[i].ProgrammeCourseRegistration.ProgrammeCourse.IsCarryOverCourse = viewModel.StudentCourseMarks[i].ProgrammeCourseRegistration.IsCarriedOverCourses;
                            viewModel.StudentCourseMarks[i].ProgrammeCourseRegistration.ProgrammeCourse.IsApproved = (bool)viewModel.StudentCourseMarks[i].ProgrammeCourseRegistration.Approved;

                        }
                        carryOverCourses = studentCourseMarkLogic.GetModelsBy(
                                c =>
                                    c.PROGRAMME_COURSE_REGISTRATION.StudentId == viewModel.Student.Id &&
                                    c.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.DepartmentId ==
                                    viewModel.DepartmentModel.Id &&
                                    c.PROGRAMME_COURSE_REGISTRATION.IsCarriedOverCourses &&
                                    !(bool)c.PROGRAMME_COURSE_REGISTRATION.Approved)
                                .Select(s => s.ProgrammeCourseRegistration.ProgrammeCourse)
                                .ToList();

                        viewModel.ProgrammeCourses = viewModel.StudentCourseMarks.Select(s => s.ProgrammeCourseRegistration.ProgrammeCourse).ToList();

                        for (int i = 0; i < programmeCourses.Count; i++)
                        {
                            ProgrammeCourse departmentalCourse = programmeCourses[i];
                            if (
                                viewModel.StudentCourseMarks.LastOrDefault(
                                    c =>
                                        c.ProgrammeCourseRegistration.ProgrammeCourse.Id ==
                                        departmentalCourse.Id &&
                                        c.ProgrammeCourseRegistration.SessionSemester.Semester.Id == programmeCourses[i].Semester.Id) ==
                                null)
                            {
                                viewModel.ProgrammeCourses.Add(departmentalCourse);
                            }
                        }
                    }
                    else
                    {
                        var existingStudentLevel = studentLogic.GetModelsBy(s => s.CurrentLevelId == viewModel.Level.Id && s.CurrentSessionId == viewModel.Session.Id).LastOrDefault();

                        if (existingStudentLevel == null && viewModel.Level.Id == 6)
                        {
                            ProgrammeCourseRegistration courseRegistration = programmeCourseRegistrationLogic.GetModelsBy(p => p.StudentId == viewModel.Student.Id && p.PROGRAMME_COURSES.PROGRAMME.ProgrammeTypeId == viewModel.Student.Programme.ProgrammeType.Id &&
                                                p.PROGRAMME_COURSES.PROGRAMME.DepartmentId == viewModel.Student.Department.Id && p.SESSION_SEMESTER.Session_Id == viewModel.Session.Id).LastOrDefault();

                            if (courseRegistration != null && courseRegistration.SessionSemester.Session.Id == viewModel.Session.Id && courseRegistration.SessionSemester.Semester.Id == viewModel.Semester.Id)
                            {
                                viewModel.StudentCourseMarks = studentCourseMarkLogic.GetModelsBy(c => c.PROGRAMME_COURSE_REGISTRATION.StudentId == viewModel.Student.Id && c.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.DepartmentId == viewModel.DepartmentModel.Id && c.PROGRAMME_COURSE_REGISTRATION.IsCarriedOverCourses && c.PROGRAMME_COURSE_REGISTRATION.Approved == false);
                                carryOverCourses = viewModel.StudentCourseMarks.Select(s => s.ProgrammeCourseRegistration.ProgrammeCourse).ToList();
                                viewModel.ProgrammeCourses = programmeCourses;

                            }
                            else if (courseRegistration != null)
                            {
                                SetMessage("An Error Occured while processing your request You have already registered courses for this Session in a different Level " + courseRegistration.Level.Name, Message.Category.Error);
                            }
                            else
                            {
                                viewModel.StudentCourseMarks = studentCourseMarkLogic.GetModelsBy(c => c.PROGRAMME_COURSE_REGISTRATION.StudentId == viewModel.Student.Id && c.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.DepartmentId == viewModel.DepartmentModel.Id && c.PROGRAMME_COURSE_REGISTRATION.IsCarriedOverCourses && c.PROGRAMME_COURSE_REGISTRATION.Approved == false);
                                if (viewModel.StudentCourseMarks.Count > 0)
                                {
                                    carryOverCourses = viewModel.StudentCourseMarks.Select(s => s.ProgrammeCourseRegistration.ProgrammeCourse).ToList();
                                }
                                if (programmeCourses.Count > 0)
                                {
                                    viewModel.ProgrammeCourses = programmeCourses;
                                }
                                else
                                {
                                    SetMessage("No Courses found for the selected parameter", Message.Category.Error);
                                }
                            }
                        }
                        else
                        {
                            SetMessage("An Error Occured while processing your request You have already registered courses for this level in a different Session " +
                                courseMark.ProgrammeCourseRegistration.SessionSemester.Session.Name,
                                Message.Category.Error);
                        }
                    }
                }
                else
                {
                    ProgrammeCourseRegistration courseRegistration = programmeCourseRegistrationLogic.GetModelsBy(p => p.StudentId == viewModel.Student.Id && p.PROGRAMME_COURSES.PROGRAMME.ProgrammeTypeId == viewModel.Student.Programme.ProgrammeType.Id &&
                                                p.PROGRAMME_COURSES.PROGRAMME.DepartmentId == viewModel.Student.Department.Id && p.SESSION_SEMESTER.Session_Id == viewModel.Session.Id).LastOrDefault();

                    if (courseRegistration != null && courseRegistration.SessionSemester.Session.Id == viewModel.Session.Id && courseRegistration.SessionSemester.Semester.Id == viewModel.Semester.Id)
                    {
                        viewModel.StudentCourseMarks = studentCourseMarkLogic.GetModelsBy(c => c.PROGRAMME_COURSE_REGISTRATION.StudentId == viewModel.Student.Id && c.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.DepartmentId == viewModel.DepartmentModel.Id && c.PROGRAMME_COURSE_REGISTRATION.IsCarriedOverCourses && c.PROGRAMME_COURSE_REGISTRATION.Approved == false);
                        carryOverCourses = viewModel.StudentCourseMarks.Select(s => s.ProgrammeCourseRegistration.ProgrammeCourse).ToList();
                        viewModel.ProgrammeCourses = programmeCourses;

                    }
                    else if (courseRegistration != null)
                    {
                        SetMessage("An Error Occured while processing your request You have already registered courses for this Session in a different Level " + courseRegistration.Level.Name, Message.Category.Error);
                    }
                    else
                    {
                        viewModel.StudentCourseMarks = studentCourseMarkLogic.GetModelsBy(c => c.PROGRAMME_COURSE_REGISTRATION.StudentId == viewModel.Student.Id && c.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.DepartmentId == viewModel.DepartmentModel.Id && c.PROGRAMME_COURSE_REGISTRATION.IsCarriedOverCourses && c.PROGRAMME_COURSE_REGISTRATION.Approved == false);
                        if (viewModel.StudentCourseMarks.Count > 0)
                        {
                            carryOverCourses = viewModel.StudentCourseMarks.Select(s => s.ProgrammeCourseRegistration.ProgrammeCourse).ToList();
                        }
                        viewModel.ProgrammeCourses = programmeCourses;
                    }
                }
                if (carryOverCourses.Count > 0)
                {
                    for (int i = 0; i < carryOverCourses.Count; i++)
                    {
                        if (carryOverCourses[i].Semester.Id != viewModel.Semester.Id && carryOverCourses[i].Semester.Id < viewModel.Semester.Id)
                        {
                            long courseId = carryOverCourses[i].Id;
                            var existingCourse = viewModel.ProgrammeCourses.Where(s => s.Id == courseId);
                            if (!existingCourse.Any())
                            {
                                viewModel.ProgrammeCourses.Add(carryOverCourses[i]);
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {

                throw;
            }
            return viewModel;
        }

      
    }
}
