﻿using Abundance_Rm.Areas.Admin.ViewModel;
using Abundance_Rm.Business;
using Abundance_Rm.Controllers;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace Abundance_Rm.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UserController : BaseController
    {
        private UserViewModel viewModel;
        private User user;
        private Person person;
        private PersonType personType;
        private UserLogic userLogic;
        private PersonLogic personLogic;
        private PersonTypeLogic personTypeLogic;
        public UserController()
        {
            viewModel = new UserViewModel();
            user = new User();
            person = new Person();
            personType = new PersonType();
            personTypeLogic = new PersonTypeLogic();
            userLogic = new UserLogic();
            personLogic = new PersonLogic();
        }

        public ActionResult Index()
        {
            try
            {
                List<User> userList = new List<User>();
                userList = userLogic.GetAll().ToList();
                viewModel.Users = userList;
            }
            catch (Exception e)
            {
                SetMessage("Error Occured " + e.Message, Message.Category.Error);
            }
            return View(viewModel);
        }
        public ActionResult CreateUser()
        {
            
            PopulateDropdown();
            return View();
        }
        [HttpPost]
        public ActionResult CreateUser(UserViewModel viewModel)
        {
            try
            {
                Person person = null;
                if (viewModel != null)
                {
                    using (TransactionScope trans = new TransactionScope())
                    {
                        person = new Person();
                        personType = personTypeLogic.GetModelBy(p => p.PersonTypeId == 1);
                        person = viewModel.User.person;
                        person.Role = viewModel.User.Role;
                        person.PersonType = personType;
                        Person newPerson = personLogic.Create(person);

                        viewModel.User.person = newPerson;
                        viewModel.User.LastLoginDate = DateTime.Now;
                      var createdUser =  userLogic.Create(viewModel.User);
                        if (viewModel.User.Role.Id == (int)Roles.Records)
                        {
                           StaffLogic staffLogic = new StaffLogic();
                            Staff staff = new Staff
                            {
                                User = createdUser,
                                Activated = false
                            };
                            staffLogic.Create(staff);
                        }
                        trans.Complete();
                    }
                    PopulateDropdown();
                    SetMessage("User Created Succesfully", Message.Category.Information);
                    return RedirectToAction("Index", "User");
                }
                else
                {
                    SetMessage("Input is null", Message.Category.Error);
                    PopulateDropdown();
                    return RedirectToAction("CreateUser");
                }
                
            }
            catch (Exception e)
            {
                PopulateDropdown();
                SetMessage("Error Occured " + e.Message, Message.Category.Error);
            }
            return View("CreateUser",viewModel);
        }
        public ActionResult ViewUserDetails(int? id)
        {

            UserViewModel userViewModel = null;
            try
            {
                if (id != null)
                {
                    user = userLogic.GetModelBy(p => p.User_Id == id);
                    userViewModel = new UserViewModel();
                    userViewModel.User = user;
                }
                else
                {
                    SetMessage("Error Occured: Select a User", Message.Category.Error);
                    return RedirectToAction("Index");
               }
            }
            catch (Exception e)
            {
                SetMessage("Error Occured " + e.Message, Message.Category.Error);
            }
            return View(userViewModel);
        }
        public ActionResult EditUser(int? id)
        {
            UserViewModel userViewModel = null;
            try
            {
                if (id != null)
                {
                    TempData["userId"] = id;
                    user = userLogic.GetModelBy(p => p.User_Id == id);
                    userViewModel = new UserViewModel();
                    userViewModel.User = user;
                }
                else
                {
                    SetMessage("Select a User", Message.Category.Error);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                SetMessage("Error Occured " + e.Message, Message.Category.Error);
            }

            ViewBag.SexList = viewModel.SexSelectList;

            ViewBag.SexList = new SelectList(Utility.PopulateSexValue(), "Id", "Name", user.person.Sex);
            ViewBag.RoleList = viewModel.RoleSelectList;
            ViewBag.SecurityQuestionList = viewModel.SecurityQuestionSelectList;
            return View(userViewModel);
        }
        [HttpPost]
        public ActionResult EditUser(UserViewModel viewModel)
        {
            try
            {
                if (viewModel != null)
                {
                    viewModel.User.Id = (int)TempData["userId"];
                    using(TransactionScope transaction = new TransactionScope())
                    {
                        Person person = new Person();
                        person.Sex = viewModel.value.Id;
                        viewModel.User.person.Sex = person.Sex;
                        userLogic.Update(viewModel.User);
                        personLogic.Update(viewModel.User);
                        transaction.Complete();
                    }
                    SetMessage("User Edited Successfully", Message.Category.Information);
                }
                else
                {
                    SetMessage("Input is null", Message.Category.Warning);
                    return RedirectToAction("EditUser");
                }
            }
            catch (Exception e)
            {
                SetMessage("Error Occured " + e.Message, Message.Category.Error);
            }
            return RedirectToAction("Index");
        }
        private void PopulateDropdown()
        {
            try
            {
                ViewBag.SexList = viewModel.SexSelectList;
                ViewBag.RoleList = viewModel.RoleSelectList;
                ViewBag.SecurityQuestionList = viewModel.SecurityQuestionSelectList;
                ViewBag.DepartmentList = Utility.PopulateDepartmentListItem();
                ViewBag.StaffList = Utility.PopulateStaffSelectListItem();

            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public ActionResult AssignDepartment()
        {
            PopulateDropdown();
            return View();
        }

        [HttpPost]
        public ActionResult AssignDepartment(UserViewModel model)
        {
            try
            {
                if (model.Department != null && model.Staff != null)
                {
                    StaffLogic staffLogic = new StaffLogic();
                    Staff staff = new Staff();
                    staff = model.Staff;
                    staff.Department = model.Department;
                    staff.Activated = model.User.Activated;
                   var modifiedstaff = staffLogic.Modify(staff);
                    if (modifiedstaff)
                    {
                        SetMessage("Operation Successful",Message.Category.Information);
                        ModelState.Clear();
                    }
                    else
                    {
                        SetMessage("Operation Failed Please Try Again", Message.Category.Error);
                    }
                }
                PopulateDropdown();
            }
            catch (Exception)
            {
                
                throw;
            }
            return View();
        }
    }

}