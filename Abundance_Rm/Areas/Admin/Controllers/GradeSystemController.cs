﻿using Abundance_Rm.Areas.Admin.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Abundance_Rm.Areas.Admin.Controllers
{
    public class GradeSystemController : Controller
    {
        // GET: Admin/GradeSystem
        public ActionResult Index()
        {
            GradeSystemViewModel viewModel = new GradeSystemViewModel();
            ViewBag.level = viewModel.LevelSelectList;
            ViewBag.grade = viewModel.GradeSelectList;
            ViewBag.gradepoint = viewModel.GradePointSelectList;
            ViewBag.startmark = viewModel.StartMarkSelectList;
            ViewBag.endmark = viewModel.EndMarkSelectList;
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddGrade(GradeSystemViewModel viewModel)
        {
            return RedirectToAction("Index");
        }
    }
}