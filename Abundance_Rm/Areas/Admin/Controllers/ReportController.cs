﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Abundance_Rm.Models;

namespace Abundance_Rm.Areas.Admin.Controllers
{
    public class ReportController : Controller
    {
        // GET: Admin/Report
        public ActionResult TranscriptReport(string id)
        {
            try
            {
                string studentId =  Utility.Decrypt(id);
                if (studentId != "1")
                {
                    ViewBag.StudentId = studentId; 
                }
                else if(studentId == "1")
                {
                    ViewBag.StudentId = id;
                }
                
            }
            catch (Exception)
            {
                
                throw;
            }
            
            return View();
        }
        public ActionResult ClassListReport(string deptId, string progId, string yearOfGraduation)
        {
            ViewBag.deptId = deptId;
            ViewBag.progId = progId;
            ViewBag.yearOfGraduation = yearOfGraduation;
            return View();
        }
        public ActionResult ResultSheet(string levelId, string semesterId, string progId, string deptId, string sessionId, string courseId)
        {
            ViewBag.levelId = levelId;
            ViewBag.semesterId = semesterId;
            ViewBag.progId = progId;
            ViewBag.deptId = deptId;
            ViewBag.sessionId = sessionId;
            ViewBag.courseId = courseId;
            return View();
        }
        public ActionResult MasterGradeSheet()
        {
            return View();
        }
        public ActionResult ExamPassList()
        {
            return View();
        }
        public ActionResult ExamReferenceList()
        {
            return View();
        }

        public ActionResult ProcessedStudentList()
        {
            return View();
        }

        public ActionResult ProcessedStudentListByProgramme()
        {
            return View();
        }
    }
}