﻿using Abundance_Rm.Business;
using Abundance_Rm.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Abundance_Rm.Areas.Admin.Controllers
{
    public class FacultyController : Controller
    {
        private FacultyLogic facultyLogic;
        public FacultyController()
        {
            facultyLogic = new FacultyLogic();
        }
        // GET: Admin/Faculty
        public ActionResult Index()
        {
            List<Faculty> facultyList = new List<Faculty>();
            Faculty faculty = new Faculty();
            facultyList = facultyLogic.GetAll().Where(p=>p.IsRemoveRequested==false).ToList();
            faculty.FacultyList = facultyList;
            return View(faculty);
        }
        [HttpPost]
        public ActionResult AddFaculty(Faculty faculty)
        {
            try
            {
                faculty.IsRemoveRequested = false;
                facultyLogic.Create(faculty);
                TempData["msg"] = "Operation Successful";
            }
            catch (Exception)
            {
                TempData["msg"] = "Operation Failed";
 
                
            }
            

            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                facultyLogic.Delete(id);
                TempData["msg"] = "Delete Successful";
            }
            catch (Exception)
            {

                TempData["msg"] = "Error Occured While Deleting Item";
            }
            return RedirectToAction("Index");
        }
    }
}