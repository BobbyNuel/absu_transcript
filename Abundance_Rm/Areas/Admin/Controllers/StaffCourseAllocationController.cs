﻿using Abundance_Rm.Business;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Areas.Admin.ViewModel;
using Abundance_Rm.Controllers;
using Abundance_Rm.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Abundance_Rm.Areas.Admin.Controllers
{
    [Authorize]
    public class StaffCourseAllocationController : BaseController
    {
        private const string ID = "Id";
        private const string NAME = "Name";
        private StaffCourseAllocationViewModel viewModel;
        public ActionResult AllocatedCourses()
        {
            try
            {
                viewModel = new StaffCourseAllocationViewModel();
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                ViewBag.ProgrammeType = viewModel.ProgrammeTypeSelectList;
                ViewBag.Level = viewModel.LevelSelectList;
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured" + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult AllocatedCourses(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                if (viewModel != null)
                {
                    CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
                    viewModel.CourseAllocationList = courseAllocationLogic.GetModelsBy(p => p.Level_Id == viewModel.CourseAllocation.Level.Id &&
                                                                                            p.Programme_Type_Id == viewModel.CourseAllocation.ProgrammeType.Id &&
                                                                                            p.Semester_Id == viewModel.CourseAllocation.Semester.Id &&
                                                                                            p.Session_Id == viewModel.CourseAllocation.Session.Id
                                                                                        );
                }

                KeepDropDownState(viewModel);
            }
            catch (Exception ex)
            {
                SetMessage("Error: " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
       
        public ActionResult AllocateCourse()
        {
            try
            {
                viewModel = new StaffCourseAllocationViewModel();
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                ViewBag.ProgrammeType = viewModel.ProgrammeTypeSelectList;
                ViewBag.User = viewModel.StaffSelectList;
                ViewBag.Level = viewModel.LevelSelectList;
                ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
                ViewBag.Course = new SelectList(new List<Course>(), ID, "CodeName");
            }
            catch (Exception ex)
            {
                SetMessage("Error: " + ex.Message, Message.Category.Error);
            }
            return View();
        }
        [HttpPost]
        public ActionResult AllocateCourse(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                if (viewModel.CourseAllocation != null)
                {
                    CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
                    List<CourseAllocation> courseAllocationList = courseAllocationLogic.GetModelsBy(p => p.Level_Id == viewModel.CourseAllocation.Level.Id &&
                                                                                                        p.Programme_Type_Id == viewModel.CourseAllocation.ProgrammeType.Id &&
                                                                                                        p.Semester_Id == viewModel.CourseAllocation.Semester.Id &&
                                                                                                        p.Session_Id == viewModel.CourseAllocation.Session.Id &&
                                                                                                        p.Course_Id == viewModel.CourseAllocation.Course.Id &&
                                                                                                        p.Department_Id == viewModel.CourseAllocation.Department.Id &&
                                                                                                        p.User_Id == viewModel.CourseAllocation.User.Id
                                                                                                        );
                    if (courseAllocationList.Count == 0)
                    {
                        courseAllocationLogic.Create(viewModel.CourseAllocation);
                        SetMessage("Course Allocated", Message.Category.Information);
                        return RedirectToAction("AllocateCourse");
                    }
                    else
                    {
                        SetMessage("Another Staff has already been assigned to this Course", Message.Category.Error);
                        KeepAllDropDownState(viewModel);
                        return View();
                    }
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error: " + ex.Message, Message.Category.Error);
            }
            return View();
        }
        public ActionResult StaffReportSheet()
        {
            try
            {
                viewModel = new StaffCourseAllocationViewModel();
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                ViewBag.Level = viewModel.LevelSelectList;
                
            }
            catch (Exception ex)
            {       
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult StaffReportSheet(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                if (viewModel != null)
                {
                    CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
                    UserLogic userLogic = new UserLogic();
                    Model.Model.User user = userLogic.GetModelBy(u => u.User_Name == User.Identity.Name);
                    if (user.Role.Id == 2)
                    {
                        viewModel.CourseAllocationList = courseAllocationLogic.GetModelsBy(p => p.Level_Id == viewModel.Level.Id && p.Semester_Id == viewModel.Semester.Id && p.Session_Id == viewModel.Session.Id ); 
                 
                    }
                    else
                    {
                        viewModel.CourseAllocationList = courseAllocationLogic.GetModelsBy(p => p.Level_Id == viewModel.Level.Id && p.Semester_Id == viewModel.Semester.Id && p.Session_Id == viewModel.Session.Id && p.USER.User_Name == User.Identity.Name);
                    } 
                }
                if (viewModel.CourseAllocationList.Count == 0)
                {
                   SetMessage("No Course To Display For User", Message.Category.Error); 
                }                                    

                KeepDropDownState(viewModel);
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }
            
            return View(viewModel);
        }
        public ActionResult StaffDownloadReportSheet(int Session_Id, int Semester_Id, int Programme_Id, int Department_Id, int Level_Id, int Course_Id)
        {
            try
            { 
                return RedirectToAction("ResultSheet", new { area = "admin", controller = "Report", levelId = Level_Id, semesterId = Semester_Id, progId = Programme_Id, deptId = Department_Id, sessionId = Session_Id, courseId = Course_Id });

            }
            catch (Exception)
            {  
                throw;
            }

        }
        public JsonResult GetDepartments(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }
                ProgrammeTypeLogic programmeTypeLogic = new ProgrammeTypeLogic();
                ProgrammeType programmeType = new ProgrammeType() { Id = Convert.ToInt32(id) };
                PROGRAMME_TYPE entityProgrammeType = programmeTypeLogic.GetEntityBy(pt => pt.ProgrammeTypeId == programmeType.Id);
                DepartmentLogic departmentLogic = new DepartmentLogic();
                List<DEPARTMENT> departments = departmentLogic.GetBy(entityProgrammeType);

                return Json(new SelectList(departments, "DepartmentId", NAME), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetSemester(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }

                Session session = new Session() { Id = Convert.ToInt32(id) };
                List<SessionSemester> sessionSemesterList = new List<SessionSemester>();
                SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                sessionSemesterList = sessionSemesterLogic.GetModelsBy(p => p.Session_Id == session.Id);

                List<Semester> semesters = new List<Semester>();
                foreach (SessionSemester sessionSemester in sessionSemesterList)
                {
                    semesters.Add(sessionSemester.Semester);
                }

                return Json(new SelectList(semesters, ID, NAME), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetCourses(int[] ids)
        {
            try
            {
                if (ids.Count() == 0)
                {
                    return null;
                }
                Level level = new Level() { Id = Convert.ToInt32(ids[1]) };
                Department department = new Department() { Id = Convert.ToInt32(ids[0]) };
                Semester semester = new Semester() { Id = Convert.ToInt32(ids[2]) };
                List<Course> courseList = Utility.GetCoursesByLevelDepartmentAndSemester(level, department, semester);

                return Json(new SelectList(courseList, ID, "CodeName"), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void KeepDropDownState(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.ProgrammeType = viewModel.ProgrammeTypeSelectList;
                ViewBag.Level = viewModel.LevelSelectList;
                if (viewModel.Session != null && viewModel.Session.Id > 0)
                {
                    List<SessionSemester> sessionSemesterList = new List<SessionSemester>();
                    SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                    sessionSemesterList = sessionSemesterLogic.GetModelsBy(p => p.Session_Id == viewModel.Session.Id);

                    List<Semester> semesters = new List<Semester>();
                    foreach (SessionSemester sessionSemester in sessionSemesterList)
                    {
                        semesters.Add(sessionSemester.Semester);
                    }

                    ViewBag.Semester = new SelectList(semesters, ID, NAME, viewModel.Semester.Id);
                }
                else
                {
                    List<SessionSemester> sessionSemesterList = new List<SessionSemester>();
                    SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                    sessionSemesterList = sessionSemesterLogic.GetModelsBy(p => p.Session_Id == viewModel.CourseAllocation.Session.Id);

                    List<Semester> semesters = new List<Semester>();
                    foreach (SessionSemester sessionSemester in sessionSemesterList)
                    {
                        semesters.Add(sessionSemester.Semester);
                    }

                    ViewBag.Semester = new SelectList(semesters, ID, NAME, viewModel.CourseAllocation.Semester.Id);
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public void KeepAllDropDownState(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.ProgrammeType = viewModel.ProgrammeTypeSelectList;
                ViewBag.User = viewModel.StaffSelectList;
                ViewBag.Level = new SelectList(viewModel.LevelSelectList, ID, NAME);
                if (viewModel.CourseAllocation.Session != null && viewModel.CourseAllocation.Session.Id > 0)
                {
                    List<SessionSemester> sessionSemesterList = new List<SessionSemester>();
                    SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                    sessionSemesterList = sessionSemesterLogic.GetModelsBy(p => p.Session_Id == viewModel.CourseAllocation.Session.Id);

                    List<Semester> semesters = new List<Semester>();
                    foreach (SessionSemester sessionSemester in sessionSemesterList)
                    {
                        semesters.Add(sessionSemester.Semester);
                    }

                    ViewBag.Semester = new SelectList(semesters, ID, NAME, viewModel.CourseAllocation.Semester.Id);
                }
                else
                {
                    ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                }
                if (viewModel.CourseAllocation.ProgrammeType != null && viewModel.CourseAllocation.Department.Id > 0)
                {
                    DepartmentLogic departmentLogic = new DepartmentLogic();
                    ProgrammeTypeLogic programmeTypeLogic = new ProgrammeTypeLogic();
                    List<DEPARTMENT> departments = new List<DEPARTMENT>();
                    PROGRAMME_TYPE entityProgrammeType = programmeTypeLogic.GetEntityBy(pt => pt.ProgrammeTypeId == viewModel.ProgrammeType.Id);
                    departments = departmentLogic.GetBy(entityProgrammeType);

                    ViewBag.Department = new SelectList(departments, ID, NAME, viewModel.CourseAllocation.Department.Id);
                }
                else
                {
                    ViewBag.Department = new SelectList(new List<Department>(), ID, NAME); 
                }
                if (viewModel.CourseAllocation.Level != null && viewModel.CourseAllocation.Level.Id > 0 && viewModel.CourseAllocation.Semester != null && viewModel.CourseAllocation.Semester.Id > 0 && viewModel.CourseAllocation.Course != null && viewModel.CourseAllocation.Course.Id > 0)
                {
                    List<Course> courseList = new List<Course>();
                    courseList = Utility.GetCoursesByLevelDepartmentAndSemester(viewModel.CourseAllocation.Level,
                                                                                             viewModel.CourseAllocation.Department,
                                                                                             viewModel.CourseAllocation.Semester
                                                                                             );

                    ViewBag.Course = new SelectList(courseList, ID, NAME, viewModel.CourseAllocation.Course.Id);
                }
                else
                {
                    ViewBag.Course = new SelectList(new List<Course>(), ID, NAME); 
                }
            }
            catch (Exception)
            {  
                throw;
            }
        }
        private DataSet ReadExcel(string filepath)
        {
            DataSet Result = null;
            try
            {
                string xConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + filepath + ";" + "Extended Properties=Excel 8.0;";
                OleDbConnection connection = new OleDbConnection(xConnStr);

                connection.Open();
                DataTable sheet = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                foreach (DataRow dataRow in sheet.Rows)
                {
                    string sheetName = dataRow[2].ToString().Replace("'", "");
                    OleDbCommand command = new OleDbCommand("Select * FROM [" + sheetName + "]", connection);
                    // Create DbDataReader to Data Worksheet

                    OleDbDataAdapter MyData = new OleDbDataAdapter();
                    MyData.SelectCommand = command;
                    DataSet ds = new DataSet();
                    ds.Clear();
                    MyData.Fill(ds);
                    connection.Close();

                    Result = ds;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;

        }

    
    }
}