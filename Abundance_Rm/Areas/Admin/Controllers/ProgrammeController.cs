﻿using Abundance_Rm.Areas.Admin.ViewModel;
using Abundance_Rm.Business;
using Abundance_Rm.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Abundance_Rm.Controllers;

namespace Abundance_Rm.Areas.Admin.Controllers
{
    public class ProgrammeController :BaseController
    {
        private ProgrammeLogic programmeLogic;
        private DepartmentLogic departmentLogic;
        private CertificateLogic certificateLogic;
        private ProgrammeTypeLogic programmeTypeLogic;
        public ProgrammeController()
        {
            programmeLogic = new ProgrammeLogic();
            departmentLogic = new DepartmentLogic();
            certificateLogic = new CertificateLogic();
            programmeTypeLogic = new ProgrammeTypeLogic();
        }
        // GET: Admin/Programme
        public ActionResult Index()
        {
            ProgrammeViewModel programmeViewModel = new ProgrammeViewModel();
            return View(programmeViewModel);
        }
          [HttpPost]
        public ActionResult AddProgramme(ProgrammeViewModel viewModel)
        {
              try
              {
                  Programme programme = new Programme();
                  programme.department = viewModel.department;
                  programme.certificate = viewModel.certificate;
                  programme.ProgrammeType = viewModel.programmeType;
                  programme.Duration = viewModel.Duration;
                  programme.IsRemoveRequested = false;
                  programme.ProgrammeName = viewModel.ProgrammeName;
                  programme.Activated = true;
                  programme.UnitsRequired = 0;

                  var existingProgramme =
                      programmeLogic.GetModelBy(
                          s =>
                              s.ProgrammeTypeId == viewModel.programmeType.Id &&
                              s.DepartmentId == viewModel.department.Id);
                 if (existingProgramme == null)
                  {

                      var createdProgramme = programmeLogic.Create(programme);
                      if (createdProgramme != null)
                      {
                          SetMessage("Operation Successful", Message.Category.Information);
                      }
                      else
                      {
                          SetMessage("Error occured while processing you request", Message.Category.Information);
                      }
                  }
                
              }
              catch (Exception)
              {
                  SetMessage("Error Sucessful", Message.Category.Information);
              }
            return RedirectToAction("Index");
        }
    }
}