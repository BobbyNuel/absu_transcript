﻿using Abundance_Rm.Business;
using Abundance_Rm.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Abundance_Rm.Areas.Admin.Controllers
{
    public class DepartmentController : Controller
    {
        private DepartmentLogic departmentLogic;
        private FacultyLogic facultyLogic;
        public DepartmentController()
        {
            departmentLogic = new DepartmentLogic();
            facultyLogic = new FacultyLogic();
        }
        // GET: Admin/Department
        public ActionResult Index()
        {
            List<Department> departmentList = new List<Department>();
            Department department = new Department();
            departmentList = departmentLogic.GetAll().Where(p => p.IsRemoveRequested == false).ToList();
            department.DepartmentList = departmentList;
            List<Faculty> faculty = facultyLogic.GetAll().Where(p=>p.IsRemoveRequested == false).ToList();
            ViewBag.faculty = new SelectList(faculty, "Id", "Name");
            return View(department);
        }

        public ActionResult AddDepartment(Department department)
        {
            try
            {
                department.IsRemoveRequested = false;
                department.Activated = false;
                departmentLogic.Create(department);
                TempData["msg"] = "Operation Successful";
            }
            catch (Exception)
            {
                TempData["msg"] = "Operation Failed";


            }


            return RedirectToAction("Index");
        }
    }
}