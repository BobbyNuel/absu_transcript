﻿using Abundance_Rm.Areas.Admin.ViewModel;
using Abundance_Rm.Business;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using Abundance_Rm.Controllers;
using Abundance_Rm.Models;
using Newtonsoft.Json;


namespace Abundance_Rm.Areas.Admin.Controllers
{

    [Authorize(Roles = "Admin,Records")]
    public class TranscriptController : BaseController
    {
        public const string ID = "Id";
        public const string NAME = "Name";
        //SqlConnection conn = new SqlConnection("data source=.;initial catalog=VS_ABSU;integrated security=True;");
        SqlConnection conn = new SqlConnection("data source=97.74.6.243;initial catalog=VS_ABSU;User ID=support;Password=8n4H!Lo65");



        // GET: Admin/Transcript
        public ActionResult StudentDisplay()
        {
            try
            {
                TranscriptViewModel tanscriptViewModel = new TranscriptViewModel();
                ViewBag.ProgrammeTypeId = tanscriptViewModel.ProgrammeTypeSelectListItem;
                ViewBag.DepartmentId = new SelectList(new List<Department>(), ID, NAME);
                ViewBag.SessionId = tanscriptViewModel.YearOfEntrySelectListItem;
                return View();
            }
            catch (Exception)
            {

                throw;
            }
        }
        [HttpPost]
        public ActionResult StudentDisplay(TranscriptViewModel viewModel)
        {
            try
            {
                SessionLogic sessionLogic = new SessionLogic();
                SESSION session = new SESSION();
                StudentLogic studentLogic = new StudentLogic();
                ProgrammeTypeLogic programmeTypeLogic = new ProgrammeTypeLogic();
                DepartmentLogic departmentLogic = new DepartmentLogic();
                List<DEPARTMENT> departments = departmentLogic.GetBy(viewModel.ProgrammeType);
                session = sessionLogic.GetEntityBy(p => p.SessionId == viewModel.Session.SessionId);
                string yearOfGraduation = session.Name.Trim();
                List<STUDENT> studentList = studentLogic.GetBy(yearOfGraduation, viewModel.Department, viewModel.ProgrammeType);
                viewModel.StudentList = studentList;
                viewModel.YearOfEntry = yearOfGraduation;
                ViewBag.ProgrammeTypeId = new SelectList(programmeTypeLogic.GetAll(), ID, NAME, viewModel.ProgrammeType.ProgrammeTypeId);
                ViewBag.DepartmentId = new SelectList(departments, "DepartmentId", NAME, viewModel.Department.DepartmentId);
                ViewBag.SessionId = new SelectList(sessionLogic.GetAll(), ID, NAME, viewModel.Session.SessionId);
                return View(viewModel);
            }
            catch (Exception)
            {

                throw;
            }

        }
        public ActionResult StudentClassList()
        {
            try
            {
                TranscriptViewModel tanscriptViewModel = new TranscriptViewModel();
                ViewBag.ProgrammeTypeId = tanscriptViewModel.ProgrammeTypeSelectListItem;
                ViewBag.DepartmentId = new SelectList(new List<DEPARTMENT>(), ID, NAME);
                ViewBag.SessionId = tanscriptViewModel.YearOfEntrySelectListItem;
                return View();
            }
            catch (Exception)
            {

                throw;
            }

        }
        [HttpPost]
        public ActionResult StudentClassList(TranscriptViewModel viewModel)
        {
            try
            {
                SessionLogic sessionLogic = new SessionLogic();
                SESSION session = new SESSION();
                long deptId = viewModel.Department.DepartmentId;
                long progId = viewModel.ProgrammeType.ProgrammeTypeId;
                session = sessionLogic.GetEntityBy(p => p.SessionId == viewModel.Session.SessionId);
                string yearOfGraduation = session.Name.Trim();
                return RedirectToAction("ClassListReport", new { area = "admin", controller = "Report", deptId = deptId, progId = progId, yearOfGraduation = yearOfGraduation });
                // Response.Redirect(@"~/Report/Presenter/StudentClassList.aspx?deptId=" + deptId + "&progId=" + progId + "&yearOfEntry=" + yearOfEntry);
                // return new EmptyResult();
            }
            catch (Exception)
            {

                throw;
            }

        }
        public ActionResult TranscriptReport(string Id)
        {
            return RedirectToAction("TranscriptReport", new { area = "admin", controller = "Report", id = Id });
            // Response.Redirect(@"~/Report/Presenter/ResultTranscript.aspx?id=" + Id);
            //return new EmptyResult();

        }
        public JsonResult GetDepartments(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }

                PROGRAMME_TYPE programmeType = new PROGRAMME_TYPE() { ProgrammeTypeId = Convert.ToInt32(id) };
                DepartmentLogic departmentLogic = new DepartmentLogic();
                List<DEPARTMENT> departments = departmentLogic.GetBy(programmeType);

                return Json(new SelectList(departments, "DepartmentId", NAME), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult CreateSession()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateSession(Session model)
        {
            try
            {
                if (model != null && model.Name.Contains('/'))
                {

                    SessionLogic sessionLogic = new SessionLogic();

                    model.Description = model.Name + " " + "SESSION";
                    var duplicateSession = sessionLogic.GetModelBy(s => s.Name.Contains(model.Name));
                    if (duplicateSession == null)
                    {
                        Session createdSession = sessionLogic.Create(model);
                        if (createdSession != null)
                        {
                            SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                            var lastSessionSemester = sessionSemesterLogic.GetAll().LastOrDefault();

                            if (lastSessionSemester != null)
                            {
                                for (int i = 1; i < 3; i++)
                                {

                                    SessionSemester sessionSemester = new SessionSemester();
                                    sessionSemester.SequenceNumber = lastSessionSemester.SequenceNumber + i;
                                    sessionSemester.Session = createdSession;
                                    sessionSemester.Semester = new Semester()
                                    {
                                        Id = i
                                    };
                                    sessionSemester.StartDate = DateTime.Now;
                                    sessionSemester.EndDate = DateTime.Now;
                                    var createdSessionSemester = sessionSemesterLogic.Create(sessionSemester);
                                }
                                SetMessage("Operation Succesful", Message.Category.Information);
                            }
                            else
                            {
                                SetMessage("Input String was not in the correct Format", Message.Category.Error);
                            }

                        }
                    }
                    else
                    {
                        SetMessage("Error Occured! Session Already Exist", Message.Category.Error);
                    }

                }
            }
            catch (Exception ex)
            {

                throw;
            }
            ModelState.Clear();
            return View();
        }
        public ActionResult EditSession(int? editId)
        {
            Session session = new Session();
            try
            {
                if (editId > 0)
                {
                    SessionLogic courseLogic = new SessionLogic();
                    session = courseLogic.GetModelBy(s => s.SessionId == editId);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return View(session);
        }
        public ActionResult SaveSession(Session session)
        {
            try
            {

                if (session != null)
                {


                    SessionLogic sessionLogic = new SessionLogic();

                    var checkduplicate = sessionLogic.GetModelBy(da => da.Name == session.Name);
                    if (checkduplicate == null)
                    {
                        bool modifiedSession = sessionLogic.Modify(session);
                        if (modifiedSession)
                        {
                            SetMessage("Operation Successful", Message.Category.Information);

                        }
                        else
                        {
                            SetMessage("Error Occured Session Could not be Modified", Message.Category.Error);
                        }
                    }
                    else
                    {
                        SetMessage("Error Occured Session Already Exist", Message.Category.Error);
                    }
                }

            }
            catch (Exception ex)
            {
                SetMessage("Record could not be Modified please contact your System Administrator for details", Message.Category.Error);
                return RedirectToAction("CreateSession");
            }

            return RedirectToAction("CreateSession");
        }
        public ActionResult DeleteSession(string deleteId)
        {
            try
            {

                if (deleteId != null)
                {
                    int id = Convert.ToInt32(deleteId);
                    SessionLogic sessionLogic = new SessionLogic();
                    bool deletedCourse = sessionLogic.Delete(da => da.SessionId == id);
                    if (deletedCourse)
                    {

                        SetMessage("Record has been Succesfully Deleted", Message.Category.Information);
                    }
                    else
                    {
                        SetMessage("Record Could not be Deleted", Message.Category.Error);
                    }
                }

            }
            catch (Exception ex)
            {
                SetMessage("Record could not be deleted please contact your System Administrator for details", Message.Category.Error);
                return RedirectToAction("ViewCourses");
            }

            return RedirectToAction("ViewCourses");
        }
        public ActionResult CreateDegreeAward()
        {
            DegreeAwardedViewModel viewModel = new DegreeAwardedViewModel();
            ViewBag.ProgrammeTypeId = viewModel.ProgrammeTypeSelectListItem;
            ViewBag.DepartmentId = new SelectList(new List<Department>(), ID, NAME);
            return View();
        }

        [HttpPost]
        public ActionResult CreateDegreeAward(UploadResultViewModel viewModel)
        {
            try
            {
                if (viewModel.DegreeAward.Department.Id > 0 && viewModel.DegreeAward.Programme.Id > 0)
                {
                    DegreeAwardsByProgrammeDepartmentLogic degreeAwardLogic = new DegreeAwardsByProgrammeDepartmentLogic();
                    ProgrammeLogic programmeLogic = new ProgrammeLogic();
                    Programme programme = programmeLogic.GetModelBy(p => p.ProgrammeTypeId == viewModel.DegreeAward.Programme.Id && p.DepartmentId == viewModel.DegreeAward.Department.Id);
                    DegreeAwardsByProgrammeDepartment degreeAward = degreeAwardLogic.GetModelBy(dA => dA.Degree_Name.Contains(viewModel.DegreeAward.DegreeName) && dA.Department_Id == viewModel.DegreeAward.Department.Id && dA.Programme_Id == programme.Id);

                    if (degreeAward == null)
                    {
                        viewModel.DegreeAward.Programme = programme;
                        degreeAwardLogic.Create(viewModel.DegreeAward);
                        SetMessage("Operation Successful", Message.Category.Information);

                    }
                    else
                    {
                        SetMessage("Error Occured! Degree Already Exist", Message.Category.Error);
                    }
                }

                ModelState.Clear();

            }
            catch (Exception ex)
            {

                throw;
            }

            ViewBag.ProgrammeTypeId = viewModel.ProgrammeTypeSelectListItem;
            ViewBag.DepartmentId = new SelectList(new List<Department>(), ID, NAME);

            return View();
        }

        public ActionResult ViewDegreeAwarded()
        {
            UploadResultViewModel degreeAwardedViewModel = new UploadResultViewModel();
            try
            {
                DegreeAwardsByProgrammeDepartmentLogic degreeAwardeLogic = new DegreeAwardsByProgrammeDepartmentLogic();
                degreeAwardedViewModel.DegreeAwards = degreeAwardeLogic.GetAll();

            }
            catch (Exception ex)
            {
                SetMessage("Error Occured " + ex.Message, Message.Category.Error);
            }
            return View(degreeAwardedViewModel);
        }


        public ActionResult EditDegreeAwarded(int? editId)
        {
            DegreeAwardedViewModel viewModel = new DegreeAwardedViewModel();
            try
            {
                if (editId > 0)
                {
                    DegreeAwardsByProgrammeDepartmentLogic degreeAwardedlogic = new DegreeAwardsByProgrammeDepartmentLogic();
                    viewModel.DegreeAwardsByProgrammeDepartment = degreeAwardedlogic.GetModelBy(da => da.Id == editId);
                    ViewBag.ProgrammeTypeId = new SelectList(viewModel.ProgrammeTypeSelectListItem, Utility.VALUE, Utility.TEXT, viewModel.DegreeAwardsByProgrammeDepartment.Programme.Id);
                    ViewBag.DepartmentId = new SelectList(viewModel.DepartmentSelectListItem, Utility.VALUE, Utility.TEXT, viewModel.DegreeAwardsByProgrammeDepartment.Department.Id);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
            return View(viewModel);
        }

        public ActionResult SaveEditedDegreeAward(DegreeAwardedViewModel viewModel)
        {
            try
            {

                if (viewModel.DegreeAwardsByProgrammeDepartment.Programme.ProgrammeType.Id > 0 && viewModel.DegreeAwardsByProgrammeDepartment.Department.Id > 0 && viewModel.DegreeAwardsByProgrammeDepartment.DegreeName != null)
                {

                    ProgrammeLogic programmeLogic = new ProgrammeLogic();
                    Programme programme = programmeLogic.GetModelBy(p => p.ProgrammeTypeId == viewModel.DegreeAwardsByProgrammeDepartment.Programme.ProgrammeType.Id && p.DepartmentId == viewModel.DegreeAwardsByProgrammeDepartment.Department.Id);

                    DegreeAwardsByProgrammeDepartmentLogic degreeAwardLogic = new DegreeAwardsByProgrammeDepartmentLogic();

                    var checkduplicate = degreeAwardLogic.GetModelBy(da => da.Department_Id == viewModel.DegreeAwardsByProgrammeDepartment.Department.Id && da.Programme_Id ==
                        viewModel.DegreeAwardsByProgrammeDepartment.Programme.ProgrammeType.Id && da.Degree_Name == viewModel.DegreeAwardsByProgrammeDepartment.DegreeName);
                    viewModel.DegreeAwardsByProgrammeDepartment.Programme = programme;
                    if (checkduplicate == null)
                    {
                        bool modifiedDegreeAwarded = degreeAwardLogic.Modify(viewModel.DegreeAwardsByProgrammeDepartment);
                        if (modifiedDegreeAwarded)
                        {
                            SetMessage("Operation Successful", Message.Category.Information);

                        }
                        else
                        {
                            SetMessage("Error Occured Degree Could not be Modified", Message.Category.Error);
                        }
                    }
                    else
                    {
                        SetMessage("Error Occured Degree Already Exist", Message.Category.Error);
                    }
                }



            }
            catch (Exception ex)
            {
                throw;
            }

            return RedirectToAction("ViewDegreeAwarded");
        }

        public ActionResult DeleteDegreeAwarded(string deleteId)
        {
            try
            {

                if (deleteId != null)
                {
                    int id = Convert.ToInt32(deleteId);
                    DegreeAwardsByProgrammeDepartmentLogic degreeAwardLogic = new DegreeAwardsByProgrammeDepartmentLogic();

                    bool deletedDegreeAwarded = degreeAwardLogic.Delete(da => da.Id == id);
                    if (deletedDegreeAwarded)
                    {
                        SetMessage("Record has been Succesfully Deleted", Message.Category.Information);
                    }
                    else
                    {
                        SetMessage("Record Could not be Deleted", Message.Category.Error);
                    }
                }



            }
            catch (Exception ex)
            {
                throw;
            }

            return RedirectToAction("ViewDegreeAwarded");
        }

        public ActionResult ViewTranscriptRequest()
        {
            TranscriptViewModel viewModel = new TranscriptViewModel();

            try
            {

                //UserLogic userLogic = new UserLogic();
                //User user = userLogic.GetModelBy(u => u.User_Name == User.Identity.Name);
                //if (user.Role.Name == "Admin")
                //{
                //    TranscriptRequestLogic transcriptRequestLogic = new TranscriptRequestLogic();
                //    viewModel.TranscriptRequests = transcriptRequestLogic.GetAll().Where(s => s.Processed == false).ToList();
                //}
                //else
                //{

                //    StaffLogic staffLogic = new StaffLogic();
                //    TranscriptRequestLogic transcriptRequestLogic = new TranscriptRequestLogic();
                //    Staff staff = staffLogic.GetModelBy(s => s.User_Id == user.Id);
                //    if (staff.Department.Id > 0 && staff.Activated == true)
                //    {
                //        viewModel.TranscriptRequests = transcriptRequestLogic.GetModelsBy(tr => tr.PROGRAMME.DepartmentId == staff.Department.Id && tr.Processed == false );
                //    }
                //    else
                //    {
                //        SetMessage("Sorry you have not been assigned any department yet", Message.Category.Error);
                //    }
                //}
                viewModel.TranscriptRequests = new List<TranscriptRequest>();
                ViewBag.Department = new SelectList(viewModel.DepartmentSelectListItem, Utility.VALUE, Utility.TEXT);
            }
            catch (Exception ex)
            {

                throw;
            }
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult ViewTranscriptRequest(TranscriptViewModel viewModel)
        {
            try
            {
                TranscriptRequestLogic transcriptRequestLogic = new TranscriptRequestLogic();
                viewModel.TranscriptRequests = transcriptRequestLogic.GetModelsBy(tr => tr.PROGRAMME.DepartmentId == viewModel.Department.DepartmentId && tr.Processed == false);
                ViewBag.Department = new SelectList(viewModel.DepartmentSelectListItem, Utility.VALUE, Utility.TEXT, viewModel.Department.DepartmentId);
            }
            catch (Exception ex)
            {

                throw;
            }
            return View(viewModel);
        }

        public ActionResult UpdateTransciptRequestRecord()
        {

            try
            {
                List<TranscriptRequest> requestTobeUpdated = new List<TranscriptRequest>();
                TranscriptViewModel viewModel = new TranscriptViewModel();
                UploadedResultLogic uploadedResultLogic = new UploadedResultLogic();
                TranscriptRequestLogic transcriptRequestLogic = new TranscriptRequestLogic();

                List<UploadedResult> uploadedResults = uploadedResultLogic.GetAll();

                List<long> processedTranscriptId = new List<long>();
                for (int i = 0; i < uploadedResults.Count; i++)
                {
                    string regNo = uploadedResults[i].Student.EntryRegNo;
                    List<TranscriptRequest> transcriptRequest = transcriptRequestLogic.GetModelsBy(tr => tr.Matric_Number == regNo);

                    if (transcriptRequest != null && transcriptRequest.Count > 0)
                    {
                        foreach (TranscriptRequest requestedTranscript in transcriptRequest)
                        {
                            if (requestedTranscript.Processed == false)
                            {
                                requestedTranscript.Person = new Person
                                {
                                    Id = uploadedResults[i].Student.Id
                                };
                                requestedTranscript.ProcessedDate = uploadedResults[i].DateUploaded;
                                requestTobeUpdated.Add(requestedTranscript);
                                requestedTranscript.DateRequested = uploadedResults[i].DateUploaded;
                                processedTranscriptId.Add(requestedTranscript.Id);
                            }

                        }

                    }

                }

                TranscriptRequest request = transcriptRequestLogic.GetAll().LastOrDefault();

                conn.Open();

                if (processedTranscriptId.Count > 0)
                {
                    // update uploaded transcript status online
                    UpdateTranscriptRequest(processedTranscriptId);

                }
                viewModel.TranscriptDataTable = GetTranscriptRequest(request);
                if (viewModel.TranscriptDataTable.Rows.Count > 0)
                {
                    List<TranscriptRequestFormat> requestFormats = new List<TranscriptRequestFormat>();
                    for (int i = 0; i < viewModel.TranscriptDataTable.Rows.Count; i++)
                    {

                        if (viewModel.TranscriptDataTable.Rows.Count > 0 && !string.IsNullOrEmpty(viewModel.TranscriptDataTable.Rows[i][0].ToString().Trim()))
                        {
                            TranscriptRequestFormat requestFormat = new TranscriptRequestFormat();
                            requestFormat.Id = viewModel.TranscriptDataTable.Rows[i][0].ToString().Trim();
                            requestFormat.PaymentId = viewModel.TranscriptDataTable.Rows[i][1].ToString().Trim();
                            requestFormat.StudentId = viewModel.TranscriptDataTable.Rows[i][2].ToString().Trim();
                            requestFormat.StudentName = viewModel.TranscriptDataTable.Rows[i][3].ToString().Trim();
                            requestFormat.MatricNumber = viewModel.TranscriptDataTable.Rows[i][4].ToString().Trim();
                            requestFormat.EmailAddress = viewModel.TranscriptDataTable.Rows[i][5].ToString().Trim();
                            requestFormat.DepartmentName = viewModel.TranscriptDataTable.Rows[i][6].ToString().Trim();
                            requestFormat.DepartmentId = viewModel.TranscriptDataTable.Rows[i][7].ToString().Trim();
                            requestFormat.ProgrammeId = viewModel.TranscriptDataTable.Rows[i][8].ToString().Trim();
                            requestFormat.LevelId = viewModel.TranscriptDataTable.Rows[i][9].ToString().Trim();
                            requestFormat.FacultyId = viewModel.TranscriptDataTable.Rows[i][10].ToString().Trim();
                            requestFormat.DateRequested = viewModel.TranscriptDataTable.Rows[i][11].ToString().Trim();
                            requestFormat.DestinationAddress = viewModel.TranscriptDataTable.Rows[i][12].ToString().Trim();
                            requestFormat.State = viewModel.TranscriptDataTable.Rows[i][13].ToString().Trim();
                            requestFormat.Country = viewModel.TranscriptDataTable.Rows[i][14].ToString().Trim();
                            requestFormat.TranscriptclearanceStatusId = viewModel.TranscriptDataTable.Rows[i][15].ToString().Trim();
                            requestFormat.TranscriptStatusId = viewModel.TranscriptDataTable.Rows[i][16].ToString().Trim();
                            requestFormat.Status = viewModel.TranscriptDataTable.Rows[i][17].ToString().Trim();
                            requestFormat.DeliveryZone = viewModel.TranscriptDataTable.Rows[i][18].ToString().Trim();
                            requestFormat.DeliveryService = viewModel.TranscriptDataTable.Rows[i][19].ToString().Trim();
                            requestFormats.Add(requestFormat);
                        }

                    }

                    conn.Close();
                    // Save collectd transcript Request to local server
                    SaveTranscriptRequest(requestFormats);
                }
                else
                {
                    SetMessage("No New Request Found", Message.Category.Warning);
                    conn.Close();
                }
                for (int i = 0; i < requestTobeUpdated.Count; i++)
                {
                    transcriptRequestLogic.Modify(requestTobeUpdated[i]);

                }
                //send Email Notification
                // IEmailSender emailSender = new EmailSender();
                // emailSender.SendEmail(requestTobeUpdated);
            }
            catch (Exception ex)
            {

                SetMessage("Could not Establish a Strong connection Please try again", Message.Category.Error);
                conn.Close();
                return RedirectToAction("ViewTranscriptRequest");

            }
            return RedirectToAction("ViewTranscriptRequest");
        }
        public DataTable GetTranscriptRequest(TranscriptRequest model)
        {
            DataTable dataTableForEtranzact = new DataTable();
            DataTable dataTableForInterswitch = new DataTable();
            try
            {

                if (model != null)
                {
                    string dateString = Convert.ToString(model.DateRequested, CultureInfo.InvariantCulture);
                    string CommandString;

                    CommandString = String.Format("SELECT  Transcript_Request_Id,TS.Payment_Id,Student_id,UPPER( PS.First_Name + ' ' + PS.Last_Name + ' ' + ISNULL(PS.Other_Name, '')) AS NAME, S.Matric_Number AS RegNo,ISNULL(PS.Email, ''),D.Department_Name,SL.Department_Id,SL.Programme_Id,SL.Level_Id,D.Faculty_Id,Date_Requested,Destination_Address, Destination_State_Id, Destination_Country_Id, Transcript_clearance_Status_Id,TS.Transcript_Status_Id,TRS.Transcript_Status_Name " +
                   ",GZ.Name AS Delivery_Zone,DS.Name AS Delivery_Service " +
                   "FROM  TRANSCRIPT_REQUEST TS INNER JOIN " +
                   "PERSON PS ON PS.Person_Id = TS.Student_id INNER JOIN " +
                   "STUDENT S ON PS.Person_Id  = S.Person_Id INNER JOIN " +
                   "STUDENT_LEVEL SL ON SL.Person_Id = s.Person_Id INNER JOIN " +
                   "DEPARTMENT D ON D.Department_Id =   SL.Department_Id INNER JOIN " +
                   "PAYMENT_ETRANZACT PE ON PE.Payment_Id = TS.Payment_Id INNER JOIN " +
                   "TRANSCRIPT_STATUS TRS ON TRS.Transcript_Status_Id = TS.Transcript_Status_Id LEFT OUTER JOIN " +
                   "DELIVERY_SERVICE_ZONE DSZ ON  DSZ.Delivery_Service_Zone_Id = TS.Delivery_Service_Zone_Id LEFT OUTER JOIN " +
                   "GEO_ZONE GZ ON GZ.Geo_Zone_Id = DSZ.Geo_Zone_Id LEFT OUTER JOIN " +
                   "DELIVERY_SERVICE DS ON DS.Delivery_Service_Id = DSZ.Delivery_Service_Id WHERE TS.Date_Requested > '{0}'", dateString);

                    SqlDataAdapter dataAdapterForEtranzact = new SqlDataAdapter(CommandString, conn);
                    dataAdapterForEtranzact.Fill(dataTableForEtranzact);

                    CommandString = String.Format("SELECT  Transcript_Request_Id,TS.Payment_Id,Student_id,UPPER( PS.First_Name + ' ' + PS.Last_Name + ' ' + ISNULL(PS.Other_Name, '')) AS NAME, S.Matric_Number AS RegNo,ISNULL(PS.Email, ''),D.Department_Name,SL.Department_Id,SL.Programme_Id,SL.Level_Id,D.Faculty_Id,Date_Requested,Destination_Address, Destination_State_Id, Destination_Country_Id, Transcript_clearance_Status_Id,TS.Transcript_Status_Id,TRS.Transcript_Status_Name " +
                  ",GZ.Name AS Delivery_Zone,DS.Name AS Delivery_Service " +
                  "FROM  TRANSCRIPT_REQUEST TS INNER JOIN " +
                  "PERSON PS ON PS.Person_Id = TS.Student_id INNER JOIN " +
                  "STUDENT S ON PS.Person_Id  = S.Person_Id INNER JOIN " +
                  "STUDENT_LEVEL SL ON SL.Person_Id = s.Person_Id INNER JOIN " +
                  "DEPARTMENT D ON D.Department_Id =   SL.Department_Id INNER JOIN " +
                   "PAYMENT_INTERSWITCH PIS ON PIS.Payment_Id = TS.Payment_Id INNER JOIN " +
                  "TRANSCRIPT_STATUS TRS ON TRS.Transcript_Status_Id = TS.Transcript_Status_Id LEFT OUTER JOIN " +
                  "DELIVERY_SERVICE_ZONE DSZ ON  DSZ.Delivery_Service_Zone_Id =TS.Delivery_Service_Zone_Id LEFT OUTER JOIN " +
                  "GEO_ZONE GZ ON GZ.Geo_Zone_Id = DSZ.Geo_Zone_Id LEFT OUTER JOIN " +
                  "DELIVERY_SERVICE DS ON DS.Delivery_Service_Id = DSZ.Delivery_Service_Id WHERE PIS.ResponseCode = '00' AND TS.Date_Requested >'{0}'", dateString);

                    SqlDataAdapter dataAdapterForInterwitch = new SqlDataAdapter(CommandString, conn);
                    dataAdapterForInterwitch.Fill(dataTableForInterswitch);

                    if (dataTableForEtranzact.Rows.Count > 0)
                    {
                        if (dataTableForInterswitch.Rows.Count > 0)
                        {
                            dataTableForEtranzact.Rows.Remove(dataTableForEtranzact.Rows[0]);
                            dataTableForInterswitch.Rows.Remove(dataTableForInterswitch.Rows[0]);

                            dataTableForEtranzact.Merge(dataTableForInterswitch);
                        }
                        else
                        {
                            dataTableForEtranzact.Rows.Remove(dataTableForEtranzact.Rows[0]);
                        }

                    }
                }
                else
                {
                    string CommandString;
                    CommandString = "SELECT  Transcript_Request_Id,TS.Payment_Id,Student_id,UPPER( PS.First_Name + ' ' + PS.Last_Name + ' ' + ISNULL(PS.Other_Name, '')) AS NAME, S.Matric_Number AS RegNo,ISNULL(PS.Email, ''),D.Department_Name,SL.Department_Id,SL.Programme_Id,SL.Level_Id,D.Faculty_Id,Date_Requested,Destination_Address, Destination_State_Id, Destination_Country_Id, Transcript_clearance_Status_Id,TS.Transcript_Status_Id,TRS.Transcript_Status_Name " +
                     ",GZ.Name AS Delivery_Zone,DS.Name AS Delivery_Service" +
                    " FROM TRANSCRIPT_REQUEST TS INNER JOIN" +
                    " PERSON PS ON PS.Person_Id = TS.Student_id INNER JOIN" +
                    " STUDENT S ON PS.Person_Id = S.Person_Id INNER JOIN" +
                    " STUDENT_LEVEL SL ON SL.Person_Id = s.Person_Id INNER JOIN" +
                    " DEPARTMENT D ON D.Department_Id = SL.Department_Id INNER JOIN" +
                    " PAYMENT_ETRANZACT PE ON PE.Payment_Id = TS.Payment_Id INNER JOIN" +
                    " TRANSCRIPT_STATUS TRS ON TRS.Transcript_Status_Id = TS.Transcript_Status_Id LEFT OUTER JOIN" +
                    " DELIVERY_SERVICE_ZONE DSZ ON  DSZ.Delivery_Service_Zone_Id = TS.Delivery_Service_Zone_Id LEFT OUTER JOIN" +
                    " GEO_ZONE GZ ON GZ.Geo_Zone_Id = DSZ.Geo_Zone_Id LEFT OUTER JOIN" +
                    " DELIVERY_SERVICE DS ON DS.Delivery_Service_Id = DSZ.Delivery_Service_Id";

                    SqlDataAdapter dataAdapterForEtranzact = new SqlDataAdapter(CommandString, conn);
                    dataAdapterForEtranzact.Fill(dataTableForEtranzact);

                    CommandString =
                        "SELECT  Transcript_Request_Id,TS.Payment_Id,Student_id,UPPER( PS.First_Name + ' ' + PS.Last_Name + ' ' + ISNULL(PS.Other_Name, '')) AS NAME, S.Matric_Number AS RegNo,ISNULL(PS.Email, ''),D.Department_Name,SL.Department_Id,SL.Programme_Id,SL.Level_Id,D.Faculty_Id,Date_Requested,Destination_Address, Destination_State_Id, Destination_Country_Id, Transcript_clearance_Status_Id,TS.Transcript_Status_Id,TRS.Transcript_Status_Name " +
                        ",GZ.Name AS Delivery_Zone,DS.Name AS Delivery_Service" +
                        " FROM  TRANSCRIPT_REQUEST TS INNER JOIN " +
                        " PERSON PS ON PS.Person_Id = TS.Student_id INNER JOIN " +
                        "STUDENT S ON PS.Person_Id  = S.Person_Id INNER JOIN " +
                        "STUDENT_LEVEL SL ON SL.Person_Id = s.Person_Id INNER JOIN " +
                        "DEPARTMENT D ON D.Department_Id =   SL.Department_Id INNER JOIN " +
                        "PAYMENT_INTERSWITCH PIS ON PIS.Payment_Id = TS.Payment_Id INNER JOIN " +
                        "TRANSCRIPT_STATUS TRS ON TRS.Transcript_Status_Id = TS.Transcript_Status_Id LEFT OUTER JOIN " +
                        "DELIVERY_SERVICE_ZONE DSZ ON  DSZ.Delivery_Service_Zone_Id =TS.Delivery_Service_Zone_Id LEFT OUTER JOIN " +
                        "GEO_ZONE GZ ON GZ.Geo_Zone_Id = DSZ.Geo_Zone_Id LEFT OUTER JOIN " +
                        "DELIVERY_SERVICE DS ON DS.Delivery_Service_Id = DSZ.Delivery_Service_Id " +
                        "WHERE PIS.ResponseCode = '00'";

                    SqlDataAdapter dataAdapter = new SqlDataAdapter(CommandString, conn);
                    dataAdapter.Fill(dataTableForInterswitch);

                    if (dataTableForInterswitch.Rows.Count > 0)
                    {
                        dataTableForEtranzact.Merge(dataTableForInterswitch);
                    }
                }
            }

            catch (Exception ex)
            {
                throw;

            }

            return dataTableForEtranzact;
        }
        public List<DataTable> UpdateTranscriptRequest(List<long> processedTranscripts)
        {
            DataTable dataTable = new DataTable();
            List<DataTable> dataTableList = new List<DataTable>();

            try
            {
                for (int i = 0; i < processedTranscripts.Count; i++)
                {
                    string CommandString;
                    CommandString = String.Format("UPDATE TRANSCRIPT_REQUEST SET Transcript_Status_Id = 4 WHERE Transcript_Request_Id = {0}; ", processedTranscripts[i]);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter(CommandString, conn);
                    dataAdapter.Fill(dataTable);
                    dataTableList.Add(dataTable);
                }

            }

            catch (Exception)
            {
                throw;

            }

            return dataTableList;
        }
        public bool SaveTranscriptRequest(List<TranscriptRequestFormat> format)
        {
            try
            {
                List<TranscriptRequest> transcriptRequests = new List<TranscriptRequest>();
                TranscriptRequestLogic requestLogic = new TranscriptRequestLogic();
                CountryLogic countryLogic = new CountryLogic();
                StudentLogic studentLogic = new StudentLogic();
                DepartmentLogic departmentLogic = new DepartmentLogic();
                ProgrammeLogic programmeLogic = new ProgrammeLogic();
                FacultyLogic facultyLogic = new FacultyLogic();

                if (format.Count > 0)
                {
                    for (int i = 0; i < format.Count; i++)
                    {

                        TranscriptRequest transcriptRequest = new TranscriptRequest();
                        transcriptRequest.Id = Convert.ToInt32(format[i].Id);
                        var existingRequest = requestLogic.GetModelBy(s => s.Transcript_Request_Id == transcriptRequest.Id);
                        if (existingRequest == null)
                        {
                            transcriptRequest.StudentId = Convert.ToInt32(format[i].StudentId);
                            transcriptRequest.StudentName = format[i].StudentName;
                            string regNo = format[i].MatricNumber;
                            STUDENT student = studentLogic.GetEntityBy(s => s.EntryRegNo == regNo);
                            if (student != null)
                            {
                                transcriptRequest.Person = new Person()
                                {
                                    Id = student.StudentId
                                };
                            }
                            transcriptRequest.MatricNumber = format[i].MatricNumber;
                            transcriptRequest.DateRequested = Convert.ToDateTime(format[i].DateRequested);
                            transcriptRequest.DestinationAddress = format[i].DestinationAddress;
                            transcriptRequest.State = new State()
                            {
                                Id = format[i].State
                            };
                            string countryName = format[i].Country;
                            Country country = countryLogic.GetModelsBy(c => c.Name.Contains(countryName)).LastOrDefault();
                            if (country != null)
                            {
                                transcriptRequest.Country = country;
                            }
                            else
                            {
                                transcriptRequest.Country = new Country()
                                {
                                    Name = format[i].Country
                                };
                            }
                            if (!string.IsNullOrEmpty(format[i].PaymentId))
                            {
                                transcriptRequest.PaymentId = Convert.ToInt32(format[i].PaymentId);
                            }
                            if (!string.IsNullOrEmpty(format[i].DepartmentId) && !string.IsNullOrEmpty(format[i].ProgrammeId))
                            {
                                int programmeId = Convert.ToInt32(format[i].ProgrammeId);
                                int departmentId = Convert.ToInt32(format[i].DepartmentId);
                                transcriptRequest.Programme = programmeLogic.GetModelBy(s => s.ProgrammeTypeId == programmeId && s.DepartmentId == departmentId);
                                if (transcriptRequest.Programme == null)
                                {

                                    Department department = departmentLogic.GetModelBy(d => d.DepartmentId == departmentId);
                                    if (department == null)
                                    {
                                        int facultyId = Convert.ToInt32(format[i].FacultyId);
                                        Faculty facutly = facultyLogic.GetModelBy(s => s.FacultyId == facultyId);
                                        department = new Department()
                                        {
                                            Name = format[i].DepartmentName,
                                            Department_Code = facutly.FacultyCode,
                                            faculty = facutly
                                        };
                                        department = departmentLogic.Create(department);
                                    }

                                    ProgrammeType programmeType = new ProgrammeType()
                                    {
                                        Id = programmeId
                                    };

                                    Programme programme = new Programme()
                                    {
                                        department = department,
                                        ProgrammeType = programmeType
                                    };
                                    transcriptRequest.Programme = programmeLogic.Create(programme);
                                }
                            }
                            if (!string.IsNullOrEmpty(format[i].LevelId))
                            {
                                transcriptRequest.Level = new Level()
                                {
                                    Id = Convert.ToInt32(format[i].LevelId)
                                };
                            }

                            transcriptRequest.TranscriptclearanceStatusId = Convert.ToInt32(format[i].TranscriptclearanceStatusId);
                            transcriptRequest.TranscriptStatusId = Convert.ToInt32(format[i].TranscriptStatusId);
                            transcriptRequest.Status = "Unprocessed";
                            transcriptRequest.Processed = false;
                            transcriptRequest.EmailAddress = format[i].EmailAddress;
                            transcriptRequest.DeliveryZone = format[i].DeliveryZone;
                            transcriptRequest.DeliveryService = format[i].DeliveryService;
                            transcriptRequests.Add(transcriptRequest);
                        }

                    }
                    int createdTranscriptRequest = requestLogic.Create(transcriptRequests);
                    if (createdTranscriptRequest > 0)
                    {
                        SetMessage("Record Updated Successfully", Message.Category.Warning);
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;

            }
            return false;
        }
        public ActionResult ViewDepartment()
        {
            UploadResultViewModel departmentViewModel = new UploadResultViewModel();
            try
            {
                DepartmentLogic departmentLogic = new DepartmentLogic();
                departmentViewModel.Departments = departmentLogic.GetAll();

            }
            catch (Exception ex)
            {
                SetMessage("Error Occured " + ex.Message, Message.Category.Error);
            }
            return View(departmentViewModel);
        }
        public ActionResult CreateDepartment()
        {
            DegreeAwardedViewModel viewModel = new DegreeAwardedViewModel();
            ViewBag.FacultyId = viewModel.FacultySelectListItem;

            return View();
        }
        [HttpPost]
        public ActionResult CreateDepartment(UploadResultViewModel viewModel)
        {
            try
            {
                if (viewModel.DepartmentModel.faculty.Id > 0 && !string.IsNullOrEmpty(viewModel.DepartmentModel.Name))
                {
                    DepartmentLogic departmentLogic = new DepartmentLogic();
                    Department department = departmentLogic.Create(viewModel.DepartmentModel);
                    if (department != null)
                    {
                        SetMessage("Operation Successful", Message.Category.Information);
                    }
                    else
                    {
                        SetMessage("Error Occured! Please Try Again", Message.Category.Error);
                    }
                }
                else
                {
                    SetMessage("Error Occured! One or more Parameters not set kindly check and try Again", Message.Category.Error);
                }

            }
            catch (Exception ex)
            {

                throw;
            }

            ViewBag.FacultyId = viewModel.FacultySelectListItem;


            return View();
        }
        public ActionResult EditDepartment(int? editId)
        {
            UploadResultViewModel departmentViewModel = new UploadResultViewModel();
            try
            {
                if (editId > 0)
                {
                    DepartmentLogic departmentLogic = new DepartmentLogic();
                    departmentViewModel.DepartmentModel = departmentLogic.GetModelBy(c => c.DepartmentId == editId);
                    ViewBag.FacultyId = new SelectList(departmentViewModel.FacultySelectListItem, Utility.VALUE, Utility.TEXT, departmentViewModel.DepartmentModel.faculty.Id);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return View(departmentViewModel);
        }
        public ActionResult SaveEditedDepartment(UploadResultViewModel viewModel)
        {
            try
            {

                if (viewModel.DepartmentModel.Name != null && viewModel.DepartmentModel.Department_Code != null && viewModel.DepartmentModel.faculty.Id > 0)
                {


                    DepartmentLogic departmentLogic = new DepartmentLogic();

                    var checkduplicate = departmentLogic.GetModelBy(da => da.Name == viewModel.DepartmentModel.Name && da.Department_Code == viewModel.DepartmentModel.Department_Code && da.FacultyId == viewModel.DepartmentModel.faculty.Id);
                    if (checkduplicate == null)
                    {
                        bool modifiedDegreeAwarded = departmentLogic.Modify(viewModel.DepartmentModel);
                        if (modifiedDegreeAwarded)
                        {
                            SetMessage("Operation Successful", Message.Category.Information);

                        }
                        else
                        {
                            SetMessage("Error Occured Department Could not be Modified", Message.Category.Error);
                        }
                    }
                    else
                    {
                        SetMessage("Error Occured Department Already Exist", Message.Category.Error);
                    }
                }

            }
            catch (Exception ex)
            {
                SetMessage("Record could not be Modified please contact your System Administrator for details", Message.Category.Error);
                return RedirectToAction("ViewDepartment");
            }

            return RedirectToAction("ViewDepartment");
        }
        public ActionResult DeleteDepartment(string deleteId)
        {
            try
            {

                if (deleteId != null)
                {
                    int id = Convert.ToInt32(deleteId);
                    DepartmentLogic departmentLogic = new DepartmentLogic();
                    bool deletedCourse = departmentLogic.Delete(da => da.DepartmentId == id);
                    if (deletedCourse)
                    {

                        SetMessage("Record has been Succesfully Deleted", Message.Category.Information);
                    }
                    else
                    {
                        SetMessage("Record Could not be Deleted", Message.Category.Error);
                    }
                }

            }
            catch (Exception ex)
            {
                SetMessage("Record could not be deleted please contact your System Administrator for details", Message.Category.Error);
                return RedirectToAction("ViewDepartment");
            }

            return RedirectToAction("ViewDepartment");
        }
        public ActionResult SyncRecord()
        {
            try
            {
                int noOfRowsBackedUp = BackupRecord();
                int noOfRowsRestored = RestoreRecord();
                if (noOfRowsBackedUp == -1 && noOfRowsRestored == -1)
                {
                    SetMessage("Operation Successful", Message.Category.Information);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Records Could not be synced online", Message.Category.Error);
                return RedirectToAction("ViewTranscriptRequest");

            }
            return RedirectToAction("ViewTranscriptRequest");
        }
        private int BackupRecord()
        {

            string filePath = Server.MapPath("~/Content/Junk/ABSU_RESULT_MANAGER.bak");
            string fileName = "ABSU_RESULT_MANAGER.bak";

            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            string commandString = String.Format("USE [ABSU_RESULT_MANAGER] BACKUP DATABASE ABSU_RESULT_MANAGER TO DISK = '{0}'", filePath);

            SqlConnection sqlconn = new SqlConnection("data source=.;initial catalog=ABSU_RESULT_MANAGER;integrated security=True;");
            SqlCommand sqlcmd = new SqlCommand(commandString, sqlconn);
            sqlconn.Open();
            var noOfRowsAffected = sqlcmd.ExecuteNonQuery();
            sqlconn.Close();
            return noOfRowsAffected;

        }
        private int RestoreRecord()
        {
            SqlConnection sqlconn = new SqlConnection("data source=.;initial catalog=ABSU_RESULT_MANAGER;integrated security=True;");
            string commandString = "";
            SqlCommand sqlcmd = new SqlCommand();
            int noOfRowsAffected = 0;

            string filePath = Server.MapPath("~/Content/Junk/ABSU_RESULT_MANAGER.bak");
            string fileName = "ABSU_RESULT_MANAGER.bak";

            if (!System.IO.File.Exists(filePath))
            {
                commandString = String.Format("USE [ABSU_RESULT_MANAGER] BACKUP DATABASE ABSU_RESULT_MANAGER TO DISK = '{0}'", filePath);


                sqlcmd = new SqlCommand(commandString, sqlconn);
                sqlconn.Open();
                noOfRowsAffected = sqlcmd.ExecuteNonQuery();
                sqlconn.Close();
            }

            commandString = String.Format("USE [master] ALTER DATABASE ABSU_RESULT_MANAGER SET SINGLE_USER WITH ROLLBACK IMMEDIATE RESTORE DATABASE ABSU_RESULT_MANAGER FROM DISK = '{0}' WITH REPLACE", filePath);
            sqlcmd = new SqlCommand(commandString, sqlconn);
            sqlconn.Open();
            noOfRowsAffected = sqlcmd.ExecuteNonQuery();
            sqlconn.Close();
            return noOfRowsAffected;
        }
        public ActionResult ViewCourses()
        {
            UploadResultViewModel degreeAwardedViewModel = new UploadResultViewModel();
            try
            {
                CourseLogic courseLogic = new CourseLogic();
                degreeAwardedViewModel.Courses = courseLogic.GetAll();

            }
            catch (Exception ex)
            {
                SetMessage("Error Occured " + ex.Message, Message.Category.Error);
            }
            return View(degreeAwardedViewModel);

        }
        public ActionResult EditCourse(int? editId)
        {
            UploadResultViewModel courseViewModel = new UploadResultViewModel();
            try
            {
                if (editId > 0)
                {
                    CourseLogic courseLogic = new CourseLogic();
                    courseViewModel.Course = courseLogic.GetModelBy(c => c.CourseId == editId);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return View(courseViewModel);
        }
        public ActionResult SaveEditedCourse(UploadResultViewModel viewModel)
        {
            try
            {

                if (viewModel.Course.Name != null && viewModel.Course.Course_Code != null)
                {


                    CourseLogic courseLogic = new CourseLogic();

                    var checkduplicate = courseLogic.GetModelBy(da => da.Title == viewModel.Course.Name && da.Course_Code == viewModel.Course.Course_Code);
                    if (checkduplicate == null)
                    {
                        bool modifiedDegreeAwarded = courseLogic.Modify(viewModel.Course);
                        if (modifiedDegreeAwarded)
                        {
                            SetMessage("Operation Successful", Message.Category.Information);

                        }
                        else
                        {
                            SetMessage("Error Occured Course Could not be Modified", Message.Category.Error);
                        }
                    }
                    else
                    {
                        SetMessage("Error Occured Course Already Exist", Message.Category.Error);
                    }
                }

            }
            catch (Exception ex)
            {
                SetMessage("Record could not be Modified please contact your System Administrator for details", Message.Category.Error);
                return RedirectToAction("ViewCourses");
            }

            return RedirectToAction("ViewCourses");
        }
        public ActionResult DeleteCourse(string deleteId)
        {
            try
            {

                if (deleteId != null)
                {
                    int id = Convert.ToInt32(deleteId);
                    CourseLogic courseLogic = new CourseLogic();
                    bool deletedCourse = courseLogic.Delete(da => da.CourseId == id);
                    if (deletedCourse)
                    {

                        SetMessage("Record has been Succesfully Deleted", Message.Category.Information);
                    }
                    else
                    {
                        SetMessage("Record Could not be Deleted", Message.Category.Error);
                    }
                }

            }
            catch (Exception ex)
            {
                SetMessage("Record could not be deleted please contact your System Administrator for details", Message.Category.Error);
                return RedirectToAction("ViewCourses");
            }

            return RedirectToAction("ViewCourses");
        }
        public ActionResult ViewFaculty()
        {
            UploadResultViewModel facultyViewModel = new UploadResultViewModel();
            try
            {
                FacultyLogic courseLogic = new FacultyLogic();
                facultyViewModel.Faculties = courseLogic.GetAll();

            }
            catch (Exception ex)
            {
                SetMessage("Error Occured " + ex.Message, Message.Category.Error);
            }
            return View(facultyViewModel);

        }
        public ActionResult CreateFaculty()
        {

            return View();
        }
        [HttpPost]
        public ActionResult CreateFaculty(Faculty faculty)
        {
            try
            {
                if (!string.IsNullOrEmpty(faculty.Name) && !string.IsNullOrEmpty(faculty.FacultyCode))
                {
                    FacultyLogic facultyLogic = new FacultyLogic();
                    var createdFaculty = facultyLogic.Create(faculty);
                    if (createdFaculty != null)
                    {
                        SetMessage("Operation Successful", Message.Category.Information);
                    }
                    else
                    {
                        SetMessage("Error Occured! Please Try Again", Message.Category.Error);
                    }
                }
                else
                {
                    SetMessage("Error Occured! One or more Parameters not set kindly check and try Again", Message.Category.Error);
                }

            }
            catch (Exception ex)
            {

                throw;
            }


            return RedirectToAction("ViewFaculty");
        }
        public ActionResult EditFaculty(int? editId)
        {
            Faculty faculty = new Faculty();
            try
            {
                if (editId > 0)
                {
                    FacultyLogic facultyLogic = new FacultyLogic();
                    faculty = facultyLogic.GetModelBy(c => c.FacultyId == editId);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return View(faculty);
        }
        public ActionResult SaveEditedFaculty(Faculty faculty)
        {
            try
            {

                if (!string.IsNullOrEmpty(faculty.Name) && !string.IsNullOrEmpty(faculty.FacultyCode))
                {


                    FacultyLogic facultyLogic = new FacultyLogic();

                    var checkduplicate = facultyLogic.GetModelBy(da => da.Name == faculty.Name && da.FacultyCode == faculty.FacultyCode);
                    if (checkduplicate == null)
                    {
                        bool modifiedFaculty = facultyLogic.Modify(faculty);
                        if (modifiedFaculty)
                        {
                            SetMessage("Operation Successful", Message.Category.Information);

                        }
                        else
                        {
                            SetMessage("Error Occured Faculty Could not be Modified", Message.Category.Error);
                        }
                    }
                    else
                    {
                        SetMessage("Error Occured Faculty Already Exist", Message.Category.Error);
                    }
                }

            }
            catch (Exception ex)
            {
                SetMessage("Record could not be Modified please contact your System Administrator for details", Message.Category.Error);
                return RedirectToAction("ViewFaculty");
            }

            return RedirectToAction("ViewFaculty");
        }
        public ActionResult DeleteFaculty(string deleteId)
        {
            try
            {

                if (deleteId != null)
                {
                    int id = Convert.ToInt32(deleteId);
                    FacultyLogic facultyLogic = new FacultyLogic();
                    bool deletedFaculty = facultyLogic.Delete(da => da.FacultyId == id);
                    if (deletedFaculty)
                    {

                        SetMessage("Record has been Succesfully Deleted", Message.Category.Information);
                    }
                    else
                    {
                        SetMessage("Record Could not be Deleted", Message.Category.Error);
                    }
                }

            }
            catch (Exception ex)
            {
                SetMessage("Record could not be deleted please contact your System Administrator for details", Message.Category.Error);
                return RedirectToAction("ViewFaculty");
            }

            return RedirectToAction("ViewFaculty");
        }

        public ActionResult EditStudentResult()
        {
            TranscriptViewModel viewModel = new TranscriptViewModel();
            try
            {
                ViewBag.Session = viewModel.SessionSelectListItems;
                ViewBag.Semester = new SelectList(new List<Semester>(), "Id", "Name");
                ViewBag.Programme = viewModel.ProgrammeTypeSelectListItem;
                ViewBag.Department = new SelectList(new List<Department>(), "Id", "Name");
                ViewBag.Level = viewModel.LevelSelectListItems;
                //ViewBag.ScoreGrade = viewModel.GradeSelectListItems;
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult EditStudentResult(TranscriptViewModel viewModel)
        {
            try
            {
                if (viewModel != null)
                {
                    StudentLogic studentLogic = new StudentLogic();
                    StudentCourseMarkLogic studentCourseMarkLogic = new StudentCourseMarkLogic();
                    viewModel.StudentCourseMark = new List<StudentCourseMark>();
                    ProgrammeCourseRegistrationLogic programmeCourseRegistrationLogic = new ProgrammeCourseRegistrationLogic();
                    List<Model.Model.Student> students = studentLogic.GetModelsBy(s => s.EntryRegNo == viewModel.Student.MatricNo);
                    if (students.Count == 0 || students.Count > 1)
                    {
                        SetMessage("Duplicate Matric Number OR Matric Number does not exist!", Message.Category.Error);
                        RetainDropdownState(viewModel);
                        return View(viewModel);
                    }

                    Model.Model.Student student = students.FirstOrDefault();
                    List<Model.Model.Student> studentLevel = studentLogic.GetModelsBy(sl => sl.StudentId == student.Id && sl.DepartmentId == viewModel.Department.DepartmentId && sl.PROGRAMME.ProgrammeTypeId == viewModel.ProgrammeType.ProgrammeTypeId);
                    if (studentLevel.Count <= 0)
                    {
                        SetMessage("Student is not in this Programme, Department!", Message.Category.Error);
                        RetainDropdownState(viewModel);
                        return View(viewModel);
                    }
                    viewModel.ProgrammeCourseRegistrations = programmeCourseRegistrationLogic.GetModelsBy(pcr => pcr.StudentId == student.Id && pcr.SESSION_SEMESTER.Session_Id == viewModel.Session.SessionId);
                    viewModel.Student = student;
                    if (viewModel.ProgrammeCourseRegistrations.Count > 0)
                    {

                        int index = 0;
                        for (int i = 0; i < viewModel.ProgrammeCourseRegistrations.Count; i++)
                        {
                            long programmeId = viewModel.ProgrammeCourseRegistrations[i].Id;
                            StudentCourseMark studnetCourseMark = studentCourseMarkLogic.GetModelBy(scm => scm.CourseRegId == programmeId);
                            viewModel.ProgrammeCourseRegistrations[i].Grade = studnetCourseMark.Grade.Trim();
                            viewModel.ProgrammeCourseRegistrations[i].GradePoint = studnetCourseMark.GradePoint;
                            viewModel.StudentCourseMark.Add(studnetCourseMark);
                        }
                        viewModel.ProgrammeCourseRegistrations = viewModel.ProgrammeCourseRegistrations.OrderBy(c => c.ProgrammeCourse.Semester.Id).ToList();

                        viewModel.FirstSemesterCourses = viewModel.ProgrammeCourseRegistrations.Where(s => s.ProgrammeCourse.Semester.Id == 1).OrderBy(c => c.Level.Id).ToList();
                        viewModel.SecondSemesterCourses = viewModel.ProgrammeCourseRegistrations.Where(s => s.ProgrammeCourse.Semester.Id == 2).OrderBy(c => c.Level.Id).ToList();
                        int fIndex = 0;
                        foreach (ProgrammeCourseRegistration fCourse in viewModel.FirstSemesterCourses)
                        {
                            ViewData["fCourse" + fIndex] = new SelectList(viewModel.GradeSelectListItems, Utility.VALUE, Utility.TEXT, fCourse.Grade);
                            fIndex++;
                        }
                        int sIndex = 0;
                        foreach (ProgrammeCourseRegistration sCourse in viewModel.SecondSemesterCourses)
                        {
                            ViewData["sCourse" + sIndex] = new SelectList(viewModel.GradeSelectListItems, Utility.VALUE, Utility.TEXT,
                                sCourse.Grade);
                            sIndex++;
                        }
                    }
                    else
                    {
                        SetMessage("No Record Found", Message.Category.Error);
                    }
                    RetainDropdownState(viewModel);
                    return View(viewModel);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
            }

            RetainDropdownState(viewModel);
            return View(viewModel);
        }
        public void RetainDropdownState(TranscriptViewModel viewModel)
        {
            try
            {
                SemesterLogic semesterLogic = new SemesterLogic();
                DepartmentLogic departmentLogic = new DepartmentLogic();
                LevelLogic levelLogic = new LevelLogic();
                if (viewModel != null)
                {
                    if (viewModel.Session != null)
                    {
                        ViewBag.Session = new SelectList(viewModel.SessionSelectListItems, "Value", "Text", viewModel.Session.SessionId);
                    }
                    else
                    {
                        ViewBag.Session = viewModel.SessionSelectListItems;
                    }
                    if (viewModel.Semester != null)
                    {
                        ViewBag.Semester = new SelectList(semesterLogic.GetAll(), ID, NAME, viewModel.Semester.Id);
                    }
                    else
                    {
                        ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                    }
                    if (viewModel.ProgrammeType != null)
                    {
                        ViewBag.Programme = new SelectList(viewModel.ProgrammeTypeSelectListItem, "Value", "Text", viewModel.ProgrammeType.ProgrammeTypeId);
                    }
                    else
                    {
                        ViewBag.Programme = viewModel.ProgrammeTypeSelectListItem;
                    }
                    if (viewModel.Department != null && viewModel.ProgrammeType != null)
                    {
                        ViewBag.Department = new SelectList(departmentLogic.GetBy(viewModel.ProgrammeType), "DepartmentId", NAME, viewModel.Department.DepartmentId);
                    }
                    else
                    {
                        ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
                    }
                    if (viewModel.Level != null)
                    {
                        ViewBag.Level = new SelectList(viewModel.LevelSelectListItems, "Value", "Text", viewModel.Level.Id);
                    }
                    else
                    {
                        ViewBag.Level = new SelectList(viewModel.LevelSelectListItems, "Value", "Text");
                    }
                    if (viewModel.ProgrammeCourseRegistrations != null && viewModel.Level != null && viewModel.Semester != null && viewModel.Department != null)
                    {
                        Department department = new Department() { Id = viewModel.Department.DepartmentId };
                        List<Course> courseList = Utility.GetCoursesByLevelDepartmentAndSemester(viewModel.Level, department, viewModel.Semester);
                        ViewBag.Course = new SelectList(courseList, ID, NAME, viewModel.Level.Id);
                    }
                    else
                    {
                        ViewBag.Course = new SelectList(new List<Course>(), ID, NAME);
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public void KeepDropdownState(TranscriptViewModel viewModel)
        {
            try
            {
                if (viewModel.Session != null && viewModel.ProgrammeType != null && viewModel.Department != null &&
                viewModel.Level != null)
                {
                    ViewBag.Session = new SelectList(viewModel.SessionSelectListItems, "Value", "Text", viewModel.Session.SessionId);
                    //ViewBag.Semester = new SelectList(new List<Semester>(), "Id", "Name");
                    ViewBag.Programme = new SelectList(viewModel.ProgrammeTypeSelectListItem, "Value", "Text", viewModel.ProgrammeType.ProgrammeTypeId);
                    ViewBag.Department = new SelectList(viewModel.DepartmentSelectListItem, "Value", "Text", viewModel.Department.DepartmentId);
                    ViewBag.Level = new SelectList(viewModel.LevelSelectListItems, "Value", "Text", viewModel.Level.Id);
                }
                else
                {
                    ViewBag.Session = viewModel.SessionSelectListItems;
                    //ViewBag.Semester = new SelectList(new List<Semester>(), "Id", "Name");
                    ViewBag.Programme = viewModel.ProgrammeTypeSelectListItem;
                    ViewBag.Department = new SelectList(new List<Department>(), "Id", "Name");
                    ViewBag.Level = viewModel.LevelSelectListItems;
                }
                // ViewBag.ScoreGrade = viewModel.GradeSelectListItems;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult SaveEditedResult(TranscriptViewModel viewModel)
        {
            var modifiedCount = 0;
            try
            {
                if (viewModel != null)
                {

                    StudentLogic studentLogic = new StudentLogic();
                    StudentCourseMarkLogic studentCourseMarkLogic = new StudentCourseMarkLogic();
                    SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                    List<StudentCourseMark> studentCourseMarks = new List<StudentCourseMark>();
                    List<Model.Model.Student> students = studentLogic.GetModelsBy(s => s.EntryRegNo == viewModel.Student.MatricNo);

                    if (students.Count != 1)
                    {
                        SetMessage("Duplicate Matric Number OR Matric Number does not exist!", Message.Category.Error);
                        KeepDropdownState(viewModel);

                        //return View("AddExtraCourse", viewModel);
                        return RedirectToAction("EditStudentResult", new { personId = students.LastOrDefault().Id });
                    }
                    viewModel.ProgrammeCourseRegistrations = new List<ProgrammeCourseRegistration>();
                    if (viewModel.FirstSemesterCourses != null)
                    {
                        viewModel.ProgrammeCourseRegistrations.AddRange(viewModel.FirstSemesterCourses);
                    }
                    if (viewModel.SecondSemesterCourses != null)
                    {
                        viewModel.ProgrammeCourseRegistrations.AddRange(viewModel.SecondSemesterCourses);
                    }

                    for (int i = 0; i < viewModel.ProgrammeCourseRegistrations.Count; i++)
                    {

                        long programmeId = viewModel.ProgrammeCourseRegistrations[i].Id;
                        StudentCourseMark studnetCourseMark = studentCourseMarkLogic.GetModelBy(scm => scm.CourseRegId == programmeId);
                        studnetCourseMark.Grade = viewModel.ProgrammeCourseRegistrations[i].Grade;
                        studnetCourseMark.GradePoint = viewModel.ProgrammeCourseRegistrations[i].GradePoint;
                        int semesterId = viewModel.ProgrammeCourseRegistrations[i].ProgrammeCourse.Semester.Id;
                        viewModel.ProgrammeCourseRegistrations[i].SessionSemester = sessionSemesterLogic.GetModelBy(s => s.Semester_Id == semesterId && s.Session_Id == viewModel.Session.SessionId);
                        studentCourseMarks.Add(studnetCourseMark);
                    }
                    if (studentCourseMarks.Count > 0)
                    {
                        modifiedCount += studentCourseMarks.Select(t => studentCourseMarkLogic.Modify(t)).Count(modified => modified);
                        if (modifiedCount > 0)
                        {
                            SetMessage("Operation Sucessful", Message.Category.Information);
                        }
                        else
                        {
                            SetMessage("No Record Found to Modify", Message.Category.Error);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return RedirectToAction("EditStudentResult", "Transcript", new { area = "Admin" });
        }
        public JsonResult GetSemester(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }
                Session session = new Session() { Id = Convert.ToInt32(id) };
                List<SessionSemester> sessionSemesterList = new List<SessionSemester>();
                SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                sessionSemesterList = sessionSemesterLogic.GetModelsBy(p => p.Session_Id == session.Id);

                List<Semester> semesters = new List<Semester>();
                foreach (SessionSemester sessionSemester in sessionSemesterList)
                {
                    semesters.Add(sessionSemester.Semester);
                }

                return Json(new SelectList(semesters, ID, NAME), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetProgrammeCourses(string programme, string semester, string level)
        {
            try
            {
                if (string.IsNullOrEmpty(programme) || string.IsNullOrEmpty(semester) || string.IsNullOrEmpty(level))
                {
                    return null;
                }
                var programmeId = Convert.ToInt32(programme);
                var semesterId = Convert.ToInt32(semester);
                var levelId = Convert.ToInt32(level);

                List<ProgrammeCourse> programmeCourseList = new List<ProgrammeCourse>();
                ProgrammeCourseLogic programmeCourseLogic = new ProgrammeCourseLogic();
                programmeCourseList = programmeCourseLogic.GetModelsBy(p => p.LevelId == levelId && p.SemesterId == semesterId && p.PROGRAMME.PROGRAMME_TYPE.ProgrammeTypeId == programmeId);

                List<Course> courses = new List<Course>();
                foreach (ProgrammeCourse programmeCourse in programmeCourseList)
                {
                    courses.Add(programmeCourse.Course);
                }

                return Json(new SelectList(courses, ID, NAME), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult StudentDetails()
        {
            return View();
        }
        [HttpPost]
        public ActionResult StudentDetails(TranscriptViewModel viewModel)
        {
            try
            {
                StudentCourseMarkLogic studentCourseMarkLogic = new StudentCourseMarkLogic();
                SessionLogic sessionLogic = new SessionLogic();
                ProgrammeCourseRegistrationLogic programmeCourseRegistrationLogic = new ProgrammeCourseRegistrationLogic();
                StudentLogic studentLogic = new StudentLogic();
                StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                List<Model.Model.Student> students = studentLogic.GetModelsBy(x => x.EntryRegNo == viewModel.Student.EntryRegNo);
                Model.Model.Student myStudent = new Model.Model.Student();
                if (students.Count > 1)
                {
                    SetMessage("Matric Number is duplicate", Message.Category.Error);
                    return View();
                }
                if (students.Count == 0)
                {
                    SetMessage("No record found", Message.Category.Error);
                    return View();
                }

                if (students.Count == 1)
                {
                    myStudent = students.FirstOrDefault();
                }

                List<ProgrammeCourseRegistration> courseRegistrationlist = programmeCourseRegistrationLogic.GetModelsBy(x => x.StudentId == myStudent.Id);

                for (int i = 0; i < courseRegistrationlist.Count; i++)
                {
                    long courseRegId = courseRegistrationlist[i].Id;
                    StudentCourseMark studentCourseMarks = studentCourseMarkLogic.GetModelBy(x => x.CourseRegId == courseRegId);
                    if (studentCourseMarks != null)
                    {
                        courseRegistrationlist[i].Detail = studentCourseMarks;
                    }
                    else
                    {
                        var courseRegistrationToDelete = courseRegistrationlist[i];
                        courseRegistrationlist.Remove(courseRegistrationlist[i]);
                        programmeCourseRegistrationLogic.Delete(p => p.CourseRegId == courseRegistrationToDelete.Id);
                        i -= 1;
                    }
                }
                viewModel.StudentLevelList = studentLevelLogic.GetModelsBy(s => s.STUDENT.PERSON.PersonId == myStudent.Id);
                if (viewModel.StudentLevelList.Count <= 0)
                {

                    StudentLevel studentLevel = new StudentLevel();
                    studentLevel.Student = myStudent;
                    studentLevel.Session = sessionLogic.GetModelBy(s => s.Name == myStudent.YearOfEntry);
                    studentLevel.Level = new Level() { Id = 1 };
                    studentLevel.ProgrammeType = myStudent.Programme.ProgrammeType;
                    studentLevel.Department = myStudent.Programme.department;
                    studentLevelLogic.Create(studentLevel);
                    viewModel.StudentLevelList.Add(studentLevel);

                }

                viewModel.Student = myStudent;
                viewModel.ProgrammeCourseRegistrations = courseRegistrationlist.OrderBy(s => s.ProgrammeCourse.Level.Id).ToList();

                return View(viewModel);
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return RedirectToAction("StudentDetails");
        }
        public JsonResult DeleteStudentLevel(string id)
        {
            try
            {
                long studentLevelId = Convert.ToInt64(id);
                StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                var studentLevel = studentLevelLogic.GetModelBy(a => a.Student_Level_Id == studentLevelId);
                if (studentLevel != null)
                {
                    var studentLevels =
                        studentLevelLogic.GetModelsBy(
                            a => a.Person_Id == studentLevel.Student.Id && a.Session_Id == studentLevel.Session.Id);
                    if (studentLevels != null && studentLevels.Count > 1)
                    {
                        studentLevelLogic.Delete(s => s.Student_Level_Id == studentLevelId);
                        return Json(new { result = "Success" });
                    }

                }
                return Json(new { result = "Failed! Student Level can't be deleted" });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult RemoveCourse(string id)
        {
            try
            {
                string postBackResult = "";
                long courseRegdetailId = Convert.ToInt64(id);

                UserLogic userLogic = new UserLogic();
                StudentCourseMarkAuditLogic studentCourseMarkAuditLogic = new StudentCourseMarkAuditLogic();
                StudentCourseMarkLogic studentCourseMarkLogic = new StudentCourseMarkLogic();
                ProgrammeCourseRegistrationLogic programmeCourseRegistrationLogic = new ProgrammeCourseRegistrationLogic();
                StudentCourseMark studentCourseMark = studentCourseMarkLogic.GetModelBy(cr => cr.CourseRegId == courseRegdetailId);

                if (studentCourseMark != null)
                {
                    using (TransactionScope scope = new TransactionScope())
                    {
                        StudentCourseMarkAudit studentCourseMarkAudit = new StudentCourseMarkAudit();
                        string operation = "REMOVED COURSE REGISTRATION";
                        string action = "ADMIN :CHANGES FROM ADMIN CONSOLE (StudentCourseRegistrationController)";
                        string client = Request.LogonUserIdentity.Name + " (" + HttpContext.Request.UserHostAddress + ")";
                        studentCourseMarkAudit.Action = action;
                        studentCourseMarkAudit.Operation = operation;
                        studentCourseMarkAudit.Client = client;
                        studentCourseMarkAudit.User = userLogic.GetModelsBy(u => u.User_Name == User.Identity.Name).LastOrDefault();
                        studentCourseMarkAudit.Time = DateTime.Now;
                        studentCourseMarkAudit.ProgrammeCourse = studentCourseMark.ProgrammeCourseRegistration.ProgrammeCourse;
                        studentCourseMarkAuditLogic.Create(studentCourseMarkAudit);

                        studentCourseMarkLogic.Delete(s => s.CourseRegId == courseRegdetailId);
                        programmeCourseRegistrationLogic.Delete(s => s.ProgrammeCourseId == courseRegdetailId);

                        scope.Complete();
                    }

                }
            }
            catch (DbEntityValidationException errors)
            {
                throw errors;
            }
            return Json(new { success = true, responseText = "Course was removed!" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult EditStudentLevel(int sid)
        {
            TranscriptViewModel viewModel = new TranscriptViewModel();
            try
            {
                if (sid > 0)
                {
                    StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                    StudentLevel studentLevel = studentLevelLogic.GetModelBy(sl => sl.Student_Level_Id == sid);

                    viewModel.StudentLevel = studentLevel;
                    ViewBag.Level = new SelectList(viewModel.LevelSelectListItems, ID, NAME, studentLevel.Level.Id);
                    ViewBag.Session = viewModel.SessionSelectListItems;
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult EditStudentLevel(TranscriptViewModel viewModel)
        {
            try
            {
                if (viewModel.StudentLevel != null)
                {
                    StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                    StudentLevel studentLevel = studentLevelLogic.GetModelBy(sl => sl.Student_Level_Id == viewModel.StudentLevel.Id);

                    studentLevel.Session = viewModel.StudentLevel.Session;
                    studentLevel.Level = viewModel.StudentLevel.Level;

                    studentLevelLogic.Modify(studentLevel);

                    SetMessage("Operation Successful! ", Message.Category.Information);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return RedirectToAction("StudentDetails");
        }
        public ActionResult AddExtraCourse(string studentId, string semesterId)
        {
            TranscriptViewModel viewModel = new TranscriptViewModel();
            try
            {
                Model.Model.Student studentRecord = new Model.Model.Student() { Id = Convert.ToInt64(Utility.Decrypt(studentId)) };
                Semester semester = new Semester() { Id = Convert.ToInt32(Utility.Decrypt(semesterId)) };
                if (studentRecord.Id <= 0 || semester.Id <= 0)
                {
                    return RedirectToAction("StudentDetails");
                }

                StudentLogic studentLogic = new StudentLogic();
                ProgrammeCourseLogic courseLogic = new ProgrammeCourseLogic();
                StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                SemesterLogic semesterLogic = new SemesterLogic();
                StudentCourseMarkLogic studentCourseMarkLogic = new StudentCourseMarkLogic();

                studentRecord = studentLogic.GetModelBy(s => s.StudentId == studentRecord.Id);
                viewModel.Student = studentRecord;
                viewModel.StudentLevel = studentLevelLogic.GetModelsBy(s => s.STUDENT.StudentId == viewModel.Student.Id).LastOrDefault();
                viewModel.Semester = semesterLogic.GetModelBy(s => s.SemesterId == semester.Id);
                viewModel.ProgrammeCoures = courseLogic.GetModelsBy(c => c.PROGRAMME.DepartmentId == viewModel.StudentLevel.Department.Id && c.PROGRAMME.ProgrammeTypeId == viewModel.StudentLevel.ProgrammeType.Id && c.LevelId <= viewModel.StudentLevel.Level.Id && c.SemesterId == semester.Id && c.Activated);

                var carryOverCourses = studentCourseMarkLogic.GetModelsBy(
                                        c =>
                                            c.PROGRAMME_COURSE_REGISTRATION.StudentId == viewModel.Student.Id &&
                                            c.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.PROGRAMME.DepartmentId ==
                                            viewModel.StudentLevel.Department.Id &&
                                            c.PROGRAMME_COURSE_REGISTRATION.IsCarriedOverCourses &&
                                            !(bool)c.PROGRAMME_COURSE_REGISTRATION.Approved && c.PROGRAMME_COURSE_REGISTRATION.SESSION_SEMESTER.Semester_Id == semester.Id)
                                        .Select(s => s.ProgrammeCourseRegistration.ProgrammeCourse)
                                        .ToList();

                if (viewModel.ProgrammeCoures.Count > 0)
                {
                    if (carryOverCourses.Count > 0)
                    {
                        for (int i = 0; i < carryOverCourses.Count; i++)
                        {
                            int courseId = Convert.ToInt32(carryOverCourses[i].Id);
                            var existingCourse = viewModel.ProgrammeCoures.Where(s => s.Id == courseId);

                            if (existingCourse.Any())
                            {
                                viewModel.ProgrammeCoures.RemoveAll(s => s.Course.Id == courseId);
                                carryOverCourses[i].IsCarryOverCourse = true;
                                carryOverCourses[i].IsApproved = false;
                                viewModel.ProgrammeCoures.Add(carryOverCourses[i]);
                            }
                        }
                    }
                    viewModel.ProgrammeCoures.OrderBy(c => c.Level).ThenBy(c => c.Course.CodeName);
                    SetSemesterCoursesDropDown(viewModel);
                }

                RetainDropdownState(viewModel);
                return View(viewModel);
            }
            catch (Exception ex)
            {
                RetainDropdownState(viewModel);
                SetMessage("Error Occurred!" + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
        public ActionResult SaveAddedCourse(TranscriptViewModel viewModel)
        {
            try
            {
                if (viewModel != null)
                {
                    StudentCourseMarkLogic studentCourseMarkLogic = new StudentCourseMarkLogic();
                    StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                    SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                    StudentLogic studentLogic = new StudentLogic();
                    ProgrammeCourseRegistrationLogic programmeCourseRegistrationLogic = new ProgrammeCourseRegistrationLogic();
                    ScoreGradeLogic scoreGradeLogic = new ScoreGradeLogic();
                    StudentCourseMark studentCourseMark = new StudentCourseMark();
                    List<ProgrammeCourseRegistration> courseRegistrationList = new List<ProgrammeCourseRegistration>();

                    List<Model.Model.Student> students = studentLogic.GetModelsBy(s => s.EntryRegNo == viewModel.Student.EntryRegNo);
                    if (students.Count != 1)
                    {
                        SetMessage("Duplicate Matric Number OR Matric Number does not exist!", Message.Category.Error);
                        RetainDropdownState(viewModel);
                        return View("AddExtraCourse", viewModel);
                    }
                    Model.Model.Student student = students.FirstOrDefault();
                    StudentLevel studentLevel = studentLevelLogic.GetModelsBy(s => s.STUDENT.StudentId == student.Id).LastOrDefault();
                    SessionSemester sessionSemester = sessionSemesterLogic.GetModelBy(s => s.Semester_Id == viewModel.Semester.Id && s.Session_Id == viewModel.Session.SessionId);

                    if (studentLevel == null)
                    {
                        SetMessage("Student has not been registered in this level for this session!",
                            Message.Category.Error);
                        RetainDropdownState(viewModel);
                        return View("AddExtraCourse", viewModel);
                    }

                    string operation = "INSERT";
                    string action = "REGISTRATION :STUDENT DETAILS- ADMIN";
                    string client = Request.LogonUserIdentity.Name + " (" + HttpContext.Request.UserHostAddress + ")";
                    var courseRegistrationDetailAudit = new StudentCourseMarkAudit();
                    courseRegistrationDetailAudit.Action = action;
                    courseRegistrationDetailAudit.Operation = operation;
                    courseRegistrationDetailAudit.Client = client;
                    UserLogic loggeduser = new UserLogic();
                    courseRegistrationDetailAudit.User = loggeduser.GetModelBy(u => u.User_Name == User.Identity.Name);

                    int uploadedCount = 0;

                    using (TransactionScope scope = new TransactionScope())
                    {

                        for (int i = 0; i < viewModel.ProgrammeCoures.Count; i++)
                        {
                            long courseId = viewModel.ProgrammeCoures[i].Id;
                            if (string.IsNullOrEmpty(viewModel.ProgrammeCoures[i].Grade) && !viewModel.ProgrammeCoures[i].IsRegistered)
                            {
                                continue;
                            }
                            string grade = viewModel.ProgrammeCoures[i].Grade;
                            string courseGrade = viewModel.GradeSelectListItems.Where(s => s.Value == grade).Select(s => s.Text).LastOrDefault();
                            string[] yearSplit = student.YearOfEntry.Split('/');
                            int compareYear = Convert.ToInt32(yearSplit.FirstOrDefault());

                            ScoreGrade scoreGrade = scoreGradeLogic.GetModelsBy(sg => (compareYear >= sg.Session_Start && compareYear <= sg.Session_End) && sg.Grade == courseGrade).LastOrDefault();
                            courseRegistrationList =
                                programmeCourseRegistrationLogic.GetModelsBy(
                                    p =>
                                        p.StudentId == studentLevel.Student.Id &&
                                        p.PROGRAMME_COURSES.PROGRAMME.ProgrammeTypeId == studentLevel.ProgrammeType.Id &&
                                        p.PROGRAMME_COURSES.PROGRAMME.DepartmentId == studentLevel.Department.Id &&
                                        p.SESSION_SEMESTER.Session_Id == viewModel.Session.SessionId &&
                                        p.SESSION_SEMESTER.Semester_Id == viewModel.Semester.Id
                                        && p.PROGRAMME_COURSES.ProgrammeCourseId == courseId);

                            StudentCourseMark courseRegistrationDetailCheck =
                                studentCourseMarkLogic.GetModelsBy(
                                    crd =>
                                        crd.PROGRAMME_COURSE_REGISTRATION.PROGRAMME_COURSES.CourseId == courseId &&
                                        crd.PROGRAMME_COURSE_REGISTRATION.StudentId == studentLevel.Student.Id)
                                    .LastOrDefault();

                            if (courseRegistrationDetailCheck == null && courseRegistrationList.Count <= 0)
                            {
                                ProgrammeCourseRegistration programmeCourseRegistration =
                                 new ProgrammeCourseRegistration
                                 {
                                     ProgrammeCourse = viewModel.ProgrammeCoures[i],
                                     IsCarriedOverCourses = viewModel.ProgrammeCoures[i].IsCarryOverCourse,
                                     Student = studentLevel.Student,
                                     SessionSemester = sessionSemester,
                                     Level = viewModel.StudentLevel.Level,
                                     CourseUnit = viewModel.ProgrammeCoures[i].CourseUnit
                                 };

                                var createdCourseReg = programmeCourseRegistrationLogic.Create(programmeCourseRegistration);

                                studentCourseMark.ProgrammeCourseRegistration = createdCourseReg;
                                viewModel.ProgrammeCoures[i].Semester = viewModel.Semester;
                                studentCourseMark.ProgrammeCourseRegistration.ProgrammeCourse = viewModel.ProgrammeCoures[i];

                                if (scoreGrade != null)
                                {
                                    studentCourseMark.Grade = scoreGrade.Grade;
                                    studentCourseMark.GradePoint = (double)scoreGrade.GradePoint;
                                    AssignScores(studentCourseMark, scoreGrade.Grade);
                                    studentCourseMark = studentCourseMarkLogic.Create(studentCourseMark, courseRegistrationDetailAudit);
                                    uploadedCount += 1;
                                }
                            }
                        }
                        scope.Complete();
                    }
                    if (uploadedCount > 0)
                    {
                        SetMessage("Operation Successful!", Message.Category.Information);
                    }
                    else
                    {
                        SetMessage("Error Occured While Processing your Request", Message.Category.Error);
                    }

                    RetainDropdownState(viewModel);
                    return RedirectToAction("StudentDetails");
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
            }
            RetainDropdownState(viewModel);
            return View("StudentDetails", viewModel);
        }
        public void SetSemesterCoursesDropDown(TranscriptViewModel viewModel)
        {
            try
            {
                if (viewModel.ProgrammeCoures != null && viewModel.ProgrammeCoures.Count > 0)
                {

                    int fIndex = 0;
                    foreach (ProgrammeCourse fCourse in viewModel.ProgrammeCoures)
                    {
                        if (fCourse.Grade != null)
                        {
                            var selectListItem = viewModel.GradeSelectListItems.LastOrDefault(s => s.Text == fCourse.Grade.Trim());
                            if (selectListItem != null)
                            {
                                var gradeValue = selectListItem.Value;
                                var value = Convert.ToInt32(gradeValue);
                                ViewData["fCourse" + fIndex] = new SelectList(viewModel.GradeSelectListItems, Utility.VALUE, Utility.TEXT,
                                    value);
                            }
                        }
                        else
                        {
                            ViewData["fCourse" + fIndex] = new SelectList(viewModel.GradeSelectListItems, Utility.VALUE, Utility.TEXT,
                           fCourse.Grade);
                        }


                        fIndex++;
                    }

                }

            }
            catch (Exception)
            {
                throw;
            }
        }
        private void AssignScores(StudentCourseMark studentCourseMark, string grade)
        {
            try
            {
                switch (grade)
                {
                    case "A":
                        studentCourseMark.TestScore = 30M;
                        studentCourseMark.ExamScore = 50M;
                        break;
                    case "AB":
                        studentCourseMark.TestScore = 30M;
                        studentCourseMark.ExamScore = 40M;
                        break;
                    case "B":
                        studentCourseMark.TestScore = 20M;
                        studentCourseMark.ExamScore = 40M;
                        break;
                    case "BC":
                        studentCourseMark.TestScore = 20M;
                        studentCourseMark.ExamScore = 30M;
                        break;
                    case "C":
                        studentCourseMark.TestScore = 20M;
                        studentCourseMark.ExamScore = 20M;
                        break;
                    case "CD":
                        studentCourseMark.TestScore = 10M;
                        studentCourseMark.ExamScore = 20M;
                        break;
                    case "D":
                        studentCourseMark.TestScore = 10M;
                        studentCourseMark.ExamScore = 35M;
                        break;
                    case "E":
                        studentCourseMark.TestScore = 10M;
                        studentCourseMark.ExamScore = 30M;
                        break;
                    case "F":
                        studentCourseMark.TestScore = 0M;
                        studentCourseMark.ExamScore = 0M;
                        break;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public ActionResult TranscripRequests()
        {
            TranscriptViewModel viewModel = new TranscriptViewModel();

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult TranscripRequests(TranscriptViewModel viewModel)
        {
            try
            {
                DateTime DateFrom = new DateTime(Convert.ToInt32(viewModel.DateFrom.Split('-')[0]), Convert.ToInt32(viewModel.DateFrom.Split('-')[1]), Convert.ToInt32(viewModel.DateFrom.Split('-')[2]));
                DateTime DateTo = new DateTime(Convert.ToInt32(viewModel.DateTo.Split('-')[0]), Convert.ToInt32(viewModel.DateTo.Split('-')[1]), Convert.ToInt32(viewModel.DateTo.Split('-')[2]));

                TranscriptRequestLogic requestLogic = new TranscriptRequestLogic();
                List<TranscriptRequest> transcriptRequests = new List<TranscriptRequest>();
                viewModel.TranscriptRequests = new List<TranscriptRequest>();

                transcriptRequests = requestLogic.GetModelsBy(t => t.Date_Requested <= DateTo && t.Date_Requested >= DateFrom && t.Processed == false);
                viewModel.TranscriptRequests = transcriptRequests.OrderBy(t => t.DateRequested).ToList();
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
    }
}