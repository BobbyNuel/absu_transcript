﻿using Abundance_Rm.Business;
using Abundance_Rm.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Abundance_Rm.Areas.Admin.Controllers
{
    public class CertificateController : Controller
    {
        CertificateLogic certificateLogic;
        public CertificateController()
        {
            certificateLogic = new CertificateLogic();
        }
        // GET: Admin/Certificate
        public ActionResult Index()
        {
            List<Certificate> certificateList = new List<Certificate>();
            Certificate certificate = new Certificate();
            certificateList = certificateLogic.GetAll().Where(p => p.IsRemoveRequested == false).ToList();
            certificate.CertificateList = certificateList;
            return View(certificate);
        }
        [HttpPost]
        public ActionResult AddCertificate(Certificate certificate)
        {
            try
            {
                certificateLogic.Create(certificate);
                TempData["msg"] = "Operation Successful";
            }
            catch (Exception)
            {
                TempData["msg"] = "Operation Failed";
            }
            return RedirectToAction("Index");
        }
    }
}