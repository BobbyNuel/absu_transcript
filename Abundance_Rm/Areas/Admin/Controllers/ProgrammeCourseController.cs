﻿using Abundance_Rm.Areas.Admin.ViewModel;
using Abundance_Rm.Business;
using Abundance_Rm.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Abundance_Rm.Controllers;
using Abundance_Rm.Models;

namespace Abundance_Rm.Areas.Admin.Controllers
{
    public class ProgrammeCourseController : BaseController
    {
         private ProgrammeLogic programmeLogic;
        private CourseTypeLogic courseTypeLogic;
        private CourseLogic courseLogic;
        private SemesterLogic semesterLogic;
        private LevelLogic levelLogic;
        private ProgrammeCourseLogic programmeCourseLogic;
        public ProgrammeCourseController()
        {
            programmeLogic = new ProgrammeLogic();
            programmeCourseLogic = new ProgrammeCourseLogic();
            courseTypeLogic = new CourseTypeLogic();
            courseLogic = new CourseLogic();
            semesterLogic = new SemesterLogic();
            levelLogic = new LevelLogic();
        }
        // GET: Admin/ProgrammeCourse
        public ActionResult Index()
        {
            ProgrammeCourseViewModel viewModel = new ProgrammeCourseViewModel();
            PopulateDropdown(viewModel);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddProgrammeCourse (ProgrammeCourseViewModel viewModel)
        {
            try
            {
                ProgrammeLogic programmeLogic = new ProgrammeLogic();
                if (viewModel != null)
                {
                    var programmeCourse = programmeCourseLogic.GetModelsBy(
                        s =>
                            s.PROGRAMME.ProgrammeTypeId == viewModel.Programme.ProgrammeType.Id &&
                            s.PROGRAMME.DepartmentId == viewModel.Programme.department.Id && s.CourseId == viewModel.Course.Id)
                        .LastOrDefault();

                    if (programmeCourse == null)
                    {
                        Programme programme =
                            programmeLogic.GetModelBy(
                                p =>
                                    p.PROGRAMME_TYPE.ProgrammeTypeId == viewModel.Programme.ProgrammeType.Id &&
                                    p.DepartmentId == viewModel.Programme.department.Id);

                        programmeCourse = new ProgrammeCourse();
                        programmeCourse.Course = viewModel.Course;
                        programmeCourse.CourseType = viewModel.CourseType;
                        programmeCourse.Semester = viewModel.Semester;
                        programmeCourse.Level = viewModel.Level;
                        programmeCourse.Activated = viewModel.Activated;
                        programmeCourse.Pass_mark = viewModel.Passmark;
                        programmeCourse.CourseUnit = viewModel.CourseUnit;
                        programmeCourse.Programme = programme;
                        programmeCourse.IsRemoveRequested = viewModel.IsRemoveRequested;
                        var created = programmeCourseLogic.Create(programmeCourse);

                        if (created != null)
                        {
                            SetMessage("Operation Successful", Message.Category.Error);
                        }
                    }
                    else
                    {
                        SetMessage("Operation Failed",Message.Category.Error);
                    }

                    
                }
               
            }
            catch (Exception ex)
            {
                    
                throw ex;
            }

            return RedirectToAction("Index");
        }
        public ActionResult EditProgrammeCourse(int? editId)
        {
            ProgrammeCourseViewModel viewModel = new ProgrammeCourseViewModel();
            try
            {
                if (editId > 0)
                {
                    ProgrammeCourseLogic programmeCourseLogic = new ProgrammeCourseLogic();
                    viewModel.ProgrammeCourse = programmeCourseLogic.GetModelBy(s => s.ProgrammeId == editId);
                    viewModel.Course = viewModel.ProgrammeCourse.Course;
                    viewModel.Programme = viewModel.ProgrammeCourse.Programme;
                    viewModel.CourseType = viewModel.ProgrammeCourse.CourseType;
                    viewModel.Level = viewModel.ProgrammeCourse.Level;
                    viewModel.Semester = viewModel.Semester;
                    
                }
                RetainDropDown(viewModel);
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured Programme Course Could not be created", Message.Category.Error);
            }
            return View(viewModel);
        }
        public ActionResult SaveCourse(Course course)
        {
            try
            {

                if (course != null)
                {
                    CourseLogic courseLogic = new CourseLogic();
                    var checkduplicate = courseLogic.GetModelBy(da => da.Course_Code == course.Course_Code && da.Course_Code == course.Course_Code);
                    if (checkduplicate == null)
                    {
                        bool modifiedSession = courseLogic.Modify(course);
                        if (modifiedSession)
                        {
                            SetMessage("Operation Successful", Message.Category.Information);

                        }
                        else
                        {
                            SetMessage("Error Occured Course Could not be Modified", Message.Category.Error);
                        }
                    }
                    else
                    {
                        SetMessage("Error Occured Course Already Exist", Message.Category.Error);
                    }
                }

            }
            catch (Exception ex)
            {
                SetMessage("Record could not be Modified please contact your System Administrator for details", Message.Category.Error);
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }
        public ActionResult DeleteCourse(string deleteId)
        {
            try
            {

                if (deleteId != null)
                {
                    int id = Convert.ToInt32(deleteId);
                    CourseLogic courseLogic = new CourseLogic();
                    bool deletedCourse = courseLogic.Delete(da => da.CourseId == id);
                    if (deletedCourse)
                    {

                        SetMessage("Record has been Succesfully Deleted", Message.Category.Information);
                    }
                    else
                    {
                        SetMessage("Record Could not be Deleted", Message.Category.Error);
                    }
                }

            }
            catch (Exception ex)
            {
                SetMessage("Record could not be deleted please contact your System Administrator for details", Message.Category.Error);
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        private void PopulateDropdown(ProgrammeCourseViewModel viewModel)
        {
            try
            {
                ViewBag.programme = viewModel.ProgrammeTypeSelectList;
                ViewBag.course = viewModel.CourseSelectList;
                ViewBag.courseType = viewModel.CourseTypeSelectList;
                ViewBag.semester = viewModel.SemesterSelectList;
                ViewBag.level = viewModel.LevelSelectList;
                ViewBag.department = viewModel.DepartmentSelectListItem;
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }

        private void RetainDropDown(ProgrammeCourseViewModel viewModel)
        {
            try
            {
                if (viewModel.Programme != null && viewModel.Programme.ProgrammeType.Id > 0)
                {
                    ViewBag.programme = new SelectList(viewModel.ProgrammeTypeSelectList, Utility.VALUE, Utility.TEXT,
                        viewModel.Programme.ProgrammeType.Id);
                }
                else
                {
                    ViewBag.programme = viewModel.ProgrammeTypeSelectList;
                }
                if (viewModel.Programme != null && viewModel.Programme.department.Id > 0)
                {
                    ViewBag.programme = new SelectList(viewModel.DepartmentSelectListItem, Utility.VALUE, Utility.TEXT,
                        viewModel.Programme.department.Id);
                }
                else
                {
                    ViewBag.department = viewModel.DepartmentSelectListItem;
                }
                if (viewModel.Course != null && viewModel.Course.Id > 0)
                {
                    ViewBag.course = new SelectList(viewModel.CourseSelectList, Utility.VALUE, Utility.TEXT,
                        viewModel.Course.Id);
                }
                else
                {
                    ViewBag.course = viewModel.CourseSelectList;
                }

                if (viewModel.CourseType != null && viewModel.CourseType.Id > 0)
                {
                    ViewBag.programme = new SelectList(viewModel.CourseTypeSelectList, Utility.VALUE, Utility.TEXT,
                       viewModel.CourseType.Id);
                }
                else
                {
                    ViewBag.courseType = viewModel.CourseTypeSelectList;
                }
                if (viewModel.CourseType != null && viewModel.CourseType.Id > 0)
                {
                    ViewBag.semester = new SelectList(viewModel.SemesterSelectList, Utility.VALUE, Utility.TEXT,
                       viewModel.Semester.Id);
                }
                else
                {
                    ViewBag.semester = viewModel.SemesterSelectList;
                }
                if (viewModel.Level != null && viewModel.Level.Id > 0)
                {
                    ViewBag.level = new SelectList(viewModel.LevelSelectList, Utility.VALUE, Utility.TEXT,
                       viewModel.Level.Id);
                }
                else
                {
                    ViewBag.level = viewModel.LevelSelectList;
                }
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }
        public ActionResult Edit()
        {
            try
            {
                ProgrammeCourseViewModel viewmodel = new ProgrammeCourseViewModel();
                ViewBag.Departments = viewmodel.DepartmentSelectListItem;
                ViewBag.Programmes = viewmodel.ProgrammeTypeSelectList;
                ViewBag.levels = viewmodel.LevelSelectList;
                ViewBag.Session = viewmodel.SessionSelectList;
                ViewBag.Semesters = new SelectList(new List<Semester>(), "Id", "Name");
            }
            catch (Exception ex)
            {
                
                throw;
            }
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(ProgrammeCourseViewModel vModel)
        {
            try
            {
                // for medical students
                if (vModel.ProgrammeCourse.Programme.department.Id == 50)
                {
                    SemesterLogic semesterLogic = new SemesterLogic();
                    Semester firstSemester = new Semester() { Id = vModel.Semester.Id};
                    vModel.FirstSemesterCourses = programmeCourseLogic.GetBy(vModel.ProgrammeCourse.Programme.department, firstSemester,vModel.Session);

                    ProgrammeCourse firstSemesterBlanks = new ProgrammeCourse();
                    firstSemesterBlanks.Unit = 0;
                    firstSemesterBlanks.Id = -1;
                    firstSemesterBlanks.Semester = semesterLogic.GetModelBy(s => s.SemesterId == firstSemester.Id);
                    firstSemesterBlanks.Programme = vModel.ProgrammeCourse.Programme;
                    firstSemesterBlanks.Level = GetLevelBySemester(firstSemesterBlanks.Semester);
                    firstSemesterBlanks.Session = vModel.Session;

                    int blankCount = 5;
                    if (vModel.FirstSemesterCourses.Count < 1)
                    {
                        blankCount = 15;
                    }
                    for (int i = 0; i < blankCount; i++)
                    {
                        firstSemesterBlanks.Course = new Course() {Course_Code = "Med"};
                        firstSemesterBlanks.CourseUnit = 0;
                        vModel.FirstSemesterCourses.Add(firstSemesterBlanks);
                    }
                }
                else
                {
                    Semester firstSemester = new Semester() { Id = 1 };
                    Semester secondSemester = new Semester() { Id = 2 };
                    ProgrammeCourseLogic programmeCourseLogic = new ProgrammeCourseLogic();


                    vModel.FirstSemesterCourses = programmeCourseLogic.GetBy(vModel.ProgrammeCourse.Programme.department, vModel.Level, firstSemester,vModel.Session);
                    vModel.SecondSemesterCourses = programmeCourseLogic.GetBy(vModel.ProgrammeCourse.Programme.department, vModel.Level, secondSemester,vModel.Session);

                    ProgrammeCourse firstSemesterBlanks = new ProgrammeCourse();
                    firstSemesterBlanks.Unit = 0;
                    firstSemesterBlanks.Id = -1;
                    firstSemesterBlanks.Semester = firstSemester;
                    firstSemesterBlanks.Programme = vModel.ProgrammeCourse.Programme;
                    firstSemesterBlanks.Level = vModel.Level;
                    firstSemesterBlanks.Session = vModel.Session;

                    ProgrammeCourse secondSemesterBlanks = new ProgrammeCourse();
                    secondSemesterBlanks.Unit = 0;
                    secondSemesterBlanks.Id = -1;
                    secondSemesterBlanks.Semester = secondSemester;
                    secondSemesterBlanks.Programme = vModel.ProgrammeCourse.Programme;
                    secondSemesterBlanks.Level = vModel.Level;
                    secondSemesterBlanks.Session = vModel.Session;

                    int blankCount = 5;
                    if (vModel.FirstSemesterCourses.Count < 1 && vModel.SecondSemesterCourses.Count < 1)
                    {
                        blankCount = 15;
                    }
                    for (int i = 0; i < blankCount; i++)
                    {
                        vModel.FirstSemesterCourses.Add(firstSemesterBlanks);
                        vModel.SecondSemesterCourses.Add(secondSemesterBlanks);
                    }
                }
               

                RetainDropdown(vModel);
            }
            catch (Exception ex)
            {
                SetMessage(ex.Message, Message.Category.Information);
            }
            return View(vModel);
        }
        public void RetainDropdown(ProgrammeCourseViewModel vModel)
        {
            try
            {

                if (vModel.ProgrammeCourse.Programme.department != null && vModel.ProgrammeCourse.Programme.department.Id > 0)
                {
                    ViewBag.Departments = new SelectList(vModel.DepartmentSelectListItem, Utility.VALUE, Utility.TEXT, vModel.ProgrammeCourse.Programme.department.Id);
                   
                }
                else
                {
                    ViewBag.Departments = vModel.DepartmentSelectListItem;

                }
                if (vModel.Level != null && vModel.Level.Id > 0)
                {
                    ViewBag.levels = new SelectList(vModel.LevelSelectList, Utility.VALUE, Utility.TEXT, vModel.Level.Id);

                }
                else
                {
                    ViewBag.levels = vModel.LevelSelectList;
                }
                if (vModel.Session != null && vModel.Session.Id > 0)
                {
                    ViewBag.Session = new SelectList(vModel.SessionSelectList, Utility.VALUE, Utility.TEXT, vModel.Session.Id);

                }
                else
                {
                    ViewBag.Session = vModel.SessionSelectList;
                }
                if (vModel.ProgrammeCourse.Programme.ProgrammeType != null && vModel.ProgrammeCourse.Programme.ProgrammeType.Id > 0)
                {
                    ViewBag.Programmes = new SelectList(vModel.ProgrammeTypeSelectList, Utility.VALUE, Utility.TEXT, vModel.ProgrammeCourse.Programme.ProgrammeType.Id);

                }
                else
                {
                    ViewBag.Programmes = vModel.ProgrammeTypeSelectList;
                }  
                if (vModel.Semester != null && vModel.Semester.Id > 0 && vModel.ProgrammeCourse.Programme.department != null && vModel.ProgrammeCourse.Programme.department.Id == 50)
                {
                    var medicalSemesters = PopulateMedicalSemesters();
                    ViewBag.Semesters = new SelectList(medicalSemesters, Utility.ID, Utility.NAME, vModel.Semester.Id);

                }
                else
                {
                    var medicalSemesters = PopulateMedicalSemesters();
                    ViewBag.Semesters = new SelectList(medicalSemesters, Utility.ID, Utility.NAME);
                }
          
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult SaveCourseChanges(ProgrammeCourseViewModel vModel)
        {
           
            try
            {
                ProgrammeCourseLogic programmeCourseLogic = new ProgrammeCourseLogic();
                ProgrammeLogic programmeLogic = new ProgrammeLogic();
                var modifiedSecondSemester = false;
                var modifiedFirstSemester = false;

                Programme progamme =
                           programmeLogic.GetModelBy(
                               s =>
                                   s.DepartmentId == vModel.ProgrammeCourse.Programme.department.Id &&
                                   s.PROGRAMME_TYPE.ProgrammeTypeId ==
                                   vModel.ProgrammeCourse.Programme.ProgrammeType.Id);

                if (vModel.FirstSemesterCourses.Count > 0)
                {
                    var firstOrDefault = vModel.FirstSemesterCourses.FirstOrDefault();
                    if (firstOrDefault != null && firstOrDefault.Programme.Id <= 0 && progamme != null)
                    {
                        vModel.FirstSemesterCourses[0].Programme = progamme;

                    }
                    modifiedFirstSemester = programmeCourseLogic.Modify(vModel.FirstSemesterCourses);
                }
                if (vModel.SecondSemesterCourses != null && vModel.SecondSemesterCourses.Count > 0)
                {
                    var firstOrDefault = vModel.SecondSemesterCourses.FirstOrDefault();
                    if (firstOrDefault != null && firstOrDefault.Programme.Id <= 0 && progamme != null)
                    {
                        vModel.SecondSemesterCourses[0].Programme = progamme;

                    }
                   modifiedSecondSemester = programmeCourseLogic.Modify(vModel.SecondSemesterCourses);
                }

                if (modifiedFirstSemester || modifiedSecondSemester)
                {

                    SetMessage("Courses were updated successfully", Message.Category.Information);
                }
                else
                {
                    SetMessage("Error Occured while processing your request! Kindly cross-Check and try again", Message.Category.Information);
                }
            }
            catch (Exception ex)
            {
                SetMessage(ex.Message, Message.Category.Error);
            }
          
            return RedirectToAction("Edit");
        }
        public JsonResult GetSemester()
        {
            try
            {
                List<Semester> semesterList = new List<Semester>();
                SemesterLogic semesterLogic = new SemesterLogic();
                semesterList = semesterLogic.GetModelsBy(p => p.SemesterId > 2);

                List<Semester> semesters = new List<Semester>();
                foreach (Semester semester in semesterList)
                {
                    semesters.Add(semester);
                }

                return Json(new SelectList(semesters, "Id", "Name"), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private List<Semester> PopulateMedicalSemesters()
        {
              List<Semester> semesters = new List<Semester>();
            try
            {

                List<Semester> semesterList = new List<Semester>();
                SemesterLogic semesterLogic = new SemesterLogic();
                semesterList = semesterLogic.GetModelsBy(p => p.SemesterId > 2);
                foreach (Semester semester in semesterList)
                {
                    semesters.Add(semester);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return semesters;
        }

        private Level GetLevelBySemester(Semester semester)
        {
            Level level;
            try
            {
                switch (semester.Id)
                {
                    case  3 : level = new Level(){Id = 1};
                             break;
                    case 4: level = new Level() { Id = 2 };
                             break;
                    case 5: level = new Level() { Id = 3 };
                             break;
                    case 6: level = new Level() { Id = 4 };
                             break;
                    case 7: level = new Level() { Id = 5 };
                             break;
                    default: level = new Level() { Id = 1 };
                             break;
                }
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            return level;
            
        }
    }
}