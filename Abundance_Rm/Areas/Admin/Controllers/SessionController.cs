﻿using Abundance_Rm.Areas.Admin.ViewModel;
using Abundance_Rm.Business;
using Abundance_Rm.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Abundance_Rm.Areas.Admin.Controllers
{
    public class SessionController : Controller
    {
        private SessionLogic sessionLogic;
        private SessionViewModel sessionViewModel;
        private Session session;
        public SessionController()
        {
            sessionLogic = new SessionLogic();
            sessionViewModel = new SessionViewModel();
            session = new Session();
        }
        // GET: Admin/Session
        public ActionResult Index()
        {
            List<Session> sessionList = new List<Session>();
            sessionList = sessionLogic.GetAll().Where(p => p.IsRemoveRequested == false).ToList();
            sessionViewModel.SessionList = sessionList;
            return View(sessionViewModel);
        }

        public ActionResult AddSession(SessionViewModel viewModel)
        {
            try
            {
                var sessionselectlist = viewModel.SessionSectList.Where(p => p.Value == viewModel.SessionId.ToString()).SingleOrDefault().Text;   
                session.Name = sessionselectlist;
                session.IsRemoveRequested = false;
                session.Description = viewModel.Description;
                session.IsCurrenSession = viewModel.IsCurrenSession;
                sessionLogic.Create(session);
                TempData["msg"] = "Operation Successful";
            }
            catch (Exception)
            {
                TempData["msg"] = "Operation Failed";
            }
            return RedirectToAction("Index");
        }
    }
}