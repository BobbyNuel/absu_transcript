﻿using Abundance_Rm.Business;
using Abundance_Rm.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Abundance_Rm.Controllers;

namespace Abundance_Rm.Areas.Admin.Controllers
{
    public class CourseController : BaseController
    {
        private CourseLogic courseLogic;
        public CourseController()
        {
            courseLogic = new CourseLogic();
        }
        // GET: Admin/Course
        public ActionResult Index()
        {
            
            Course course = new Course();
            try
            {
                List<Course> courseList = new List<Course>();
                courseList = courseLogic.GetAll().Where(p => p.IsRemoveRequested == false).ToList();
                course.CourseList = courseList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View(course);
        }
        [HttpPost]
        public ActionResult AddCourse(Course course)
        {
            try
            {
                courseLogic.Create(course);
                SetMessage("Operation Successful",Message.Category.Information);
            }
            catch (Exception)
            {
                TempData["msg"] = "Operation Failed";
            }
            return RedirectToAction("Index");
        }
        public ActionResult EditCourse(int? editId)
        {
            Course course = new Course();
            try
            {
                if (editId > 0)
                {
                    CourseLogic courseLogic = new CourseLogic();
                    course = courseLogic.GetModelBy(s => s.CourseId == editId);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return View(course);
        }
        public ActionResult SaveCourse(Course course)
        {
            try
            {

                if (course != null)
                {


                    CourseLogic courseLogic = new CourseLogic();

                    var checkduplicate = courseLogic.GetModelBy(da => da.Course_Code == course.Course_Code && da.Course_Code == course.Course_Code);
                    if (checkduplicate == null)
                    {
                        bool modifiedSession = courseLogic.Modify(course);
                        if (modifiedSession)
                        {
                            SetMessage("Operation Successful", Message.Category.Information);

                        }
                        else
                        {
                            SetMessage("Error Occured Course Could not be Modified", Message.Category.Error);
                        }
                    }
                    else
                    {
                        SetMessage("Error Occured Course Already Exist", Message.Category.Error);
                    }
                }

            }
            catch (Exception ex)
            {
                SetMessage("Record could not be Modified please contact your System Administrator for details", Message.Category.Error);
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }
        public ActionResult DeleteCourse(string deleteId)
        {
            try
            {

                if (deleteId != null)
                {
                    int id = Convert.ToInt32(deleteId);
                    CourseLogic courseLogic = new CourseLogic();
                    bool deletedCourse = courseLogic.Delete(da => da.CourseId == id);
                    if (deletedCourse)
                    {

                        SetMessage("Record has been Succesfully Deleted", Message.Category.Information);
                    }
                    else
                    {
                        SetMessage("Record Could not be Deleted", Message.Category.Error);
                    }
                }

            }
            catch (Exception ex)
            {
                SetMessage("Record could not be deleted please contact your System Administrator for details", Message.Category.Error);
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }
    }
}