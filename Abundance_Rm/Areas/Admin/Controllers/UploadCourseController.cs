﻿using Abundance_Rm.Areas.Admin.ViewModel;
using Abundance_Rm.Business;
using Abundance_Rm.Controllers;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Abundance_Rm.Areas.Admin.Controllers
{
    public class UploadCourseController : BaseController
    {
        // GET: Admin/UploadCourse
        public const string ID = "Id";
        public const string NAME = "Name";
        public ActionResult UploadCourse()
        {
            try
            {               
                   PopulateDropdowns();
                             
            }
            catch (Exception ex)
            {
                PopulateDropdowns();
                SetMessage("Error occured! " + " "+ex.Message, Message.Category.Error);
               
            }
            return View();
        }
        [HttpPost]
        public ActionResult UploadCourse(UploadCourseViewModel uploadCourseViewModel)
        {
            try
            {

                    List<CourseData> courseDataList = new List<CourseData>();
                    foreach (string file in Request.Files)
                    {
                        HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                        string pathForSaving = Server.MapPath("~/Content/ExcelUploads");
                        string savedFileName = Path.Combine(pathForSaving, hpf.FileName);
                        hpf.SaveAs(savedFileName);
                        DataSet courseList = ReadExcel(savedFileName);

                        if (courseList != null && courseList.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < courseList.Tables[0].Rows.Count; i++)
                            {
                                CourseData courseData = new CourseData();
                                courseData.Level = courseList.Tables[0].Rows[i][0].ToString().Trim();
                                courseData.Semester = courseList.Tables[0].Rows[i][1].ToString().Trim();
                                courseData.CourseCode = courseList.Tables[0].Rows[i][2].ToString().Trim(); 
                                courseData.CourseTitle = courseList.Tables[0].Rows[i][3].ToString().Trim();
                                courseData.CourseUnit = courseList.Tables[0].Rows[i][4].ToString().Trim();
                                if (courseData.Level != "" && courseData.Semester != "" && courseData.CourseCode != "" && courseData.CourseTitle != "" && courseData.CourseUnit != "")
                                {
                                    courseDataList.Add(courseData);
                                }
                            }
                        }
                    }
                    keepDropDownState(uploadCourseViewModel);
                    uploadCourseViewModel.CourseDataList = courseDataList;
                    TempData["uploadCourseViewModel"] = uploadCourseViewModel;
                

            }
            catch (Exception ex)
            {
                keepDropDownState(uploadCourseViewModel);
                SetMessage("Error occured! " + " " + ex.Message, Message.Category.Error);

            }
            return View(uploadCourseViewModel);
        }
        public ActionResult SaveUpload()
        {
            UploadCourseViewModel uploadCourseViewModel = (UploadCourseViewModel)TempData["uploadCourseViewModel"];
            List<ProgrammeCourse> programmeCourseList = new List<ProgrammeCourse>();
            try
            {              
                if (uploadCourseViewModel != null)
                {
                    foreach (CourseData courseData in uploadCourseViewModel.CourseDataList)
                    {

                        Programme programme = GetProgramme(uploadCourseViewModel);
                        Level level = getLevel(courseData.Level);
                        CourseType courseType = new CourseType() { Id = 1 };
                        Course course = new Course();
                        CourseLogic courseLogic = new CourseLogic();                   
                        string courseCode = trim(courseData.CourseCode);
                        course = courseLogic.GetModelBy(p => p.Course_Code == courseCode);
                        if (course == null)
                        {
                            Course newCourse = new Course();
                            newCourse.Course_Code = courseCode;
                            newCourse.Name = courseData.CourseTitle;
                            newCourse.IsRemoveRequested = true;
                            newCourse = courseLogic.Create(newCourse);
                            ProgrammeCourse programmeCourse = CreateProgrammeCourse(newCourse, programme, level, courseType, courseData);
                            programmeCourseList.Add(programmeCourse);
                        }
                        else
                        {
                            bool isProgrammeCourseExisting = checkProgrammeCourse(programme, course, courseData, level);
                            if (!isProgrammeCourseExisting)
                            {
                                ProgrammeCourse programmeCourse = CreateProgrammeCourse(course, programme, level, courseType, courseData);
                                programmeCourseList.Add(programmeCourse);
                            }
                           
                        }
           
                    }

                }
                keepDropDownState(uploadCourseViewModel);
                SetMessage("Upload successful " + programmeCourseList.Count + " " +"course(s) added", Message.Category.Information);
                return View("UploadCourse");
            }
            catch (Exception ex)
            {
                keepDropDownState(uploadCourseViewModel);
                SetMessage("Error occured! " + " "+ ex.Message, Message.Category.Error);
                return View("UploadCourse");
            }
            
        }

        private bool checkProgrammeCourse(Programme programme, Course course, CourseData courseData, Level level)
        {
            try
            {
                 Semester semester = getSemester(courseData.Semester);
                ProgrammeCourse programmeCourse = new ProgrammeCourse();
                ProgrammeCourseLogic programmeCourseLogic = new ProgrammeCourseLogic();
                programmeCourse = programmeCourseLogic.GetModelBy(p => p.CourseId == course.Id && p.ProgrammeId == programme.Id && p.SemesterId == semester.Id && p.LevelId == level.Id);
                if (programmeCourse != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        private ProgrammeCourse CreateProgrammeCourse(Course course, Programme programme, Level level, CourseType courseType, CourseData courseData)
        {
            try
            {
                Semester semester = getSemester(courseData.Semester);
                ProgrammeCourse programmeCourse = new ProgrammeCourse();
                ProgrammeCourseLogic programmeCourseLogic = new ProgrammeCourseLogic();
                programmeCourse.Course = course;
                programmeCourse.Programme = programme;
                programmeCourse.Level = level;
                programmeCourse.CourseType = courseType;
                programmeCourse.CourseUnit = Convert.ToInt32(courseData.CourseUnit);       
                programmeCourse.IsRemoveRequested = true;
                programmeCourse.Activated = true;
                programmeCourse.Semester = semester;
                programmeCourse = programmeCourseLogic.Create(programmeCourse);
                return programmeCourse;

            }
            catch (Exception)
            {
                
                throw;
            }
        }

        private Programme GetProgramme(UploadCourseViewModel uploadCourseViewModel)
        {
            try
            {
                ProgrammeLogic programmeLogic = new ProgrammeLogic();
                Programme programme = new Programme();
                programme = programmeLogic.GetModelBy(p => p.DepartmentId == uploadCourseViewModel.Department.DepartmentId && p.ProgrammeTypeId == uploadCourseViewModel.ProgrammeType.ProgrammeTypeId);
                if (programme != null)
                {
                    return programme; 
                }
                else
                {
                    return new Programme();
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        private void PopulateDropdowns()
        {
            UploadCourseViewModel uploadCourseViewModel = new UploadCourseViewModel();
            ViewBag.ProgrammeTypeId = uploadCourseViewModel.ProgrammeTypeSelectListItem;
            ViewBag.DepartmentId = new SelectList(new List<Department>(), ID, NAME);
        }
        public JsonResult GetDepartments(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }

                PROGRAMME_TYPE programmeType = new PROGRAMME_TYPE() { ProgrammeTypeId = Convert.ToInt32(id) };
                DepartmentLogic departmentLogic = new DepartmentLogic();
                List<DEPARTMENT> departments = departmentLogic.GetBy(programmeType);

                return Json(new SelectList(departments, ID, NAME), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private DataSet ReadExcel(string filepath)
        {
            DataSet Result = null;
            try
            {
                string xConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + filepath + ";" + "Extended Properties=Excel 8.0;";
                OleDbConnection connection = new OleDbConnection(xConnStr);

                connection.Open();
                DataTable sheet = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                foreach (DataRow dataRow in sheet.Rows)
                {
                    string sheetName = dataRow[2].ToString().Replace("'", "");
                    OleDbCommand command = new OleDbCommand("Select * FROM [" + sheetName + "]", connection);
                    // Create DbDataReader to Data Worksheet

                    OleDbDataAdapter MyData = new OleDbDataAdapter();
                    MyData.SelectCommand = command;
                    DataSet ds = new DataSet();
                    ds.Clear();
                    MyData.Fill(ds);
                    connection.Close();

                    Result = ds;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;

        }
        private void keepDropDownState(UploadCourseViewModel viewModel)
        {
            try
            {
                ProgrammeTypeLogic programmeTypeLogic = new ProgrammeTypeLogic();
                DepartmentLogic departmentLogic = new DepartmentLogic();
                List<DEPARTMENT> departmentList = departmentLogic.GetBy(viewModel.ProgrammeType);
                ViewBag.ProgrammeTypeId = new SelectList(programmeTypeLogic.GetAll(), ID, NAME, viewModel.ProgrammeType.ProgrammeTypeId);
                ViewBag.DepartmentId = new SelectList(departmentList, ID, NAME, viewModel.Department.DepartmentId);
            }
            catch (Exception)
            {

                throw;
            }
        }
        private Level getLevel(string level)
        {
            try
            {
                const string ND1 = "ND1";
                const string ND2 = "ND2";
                const string HND1 = "HND1";
                const string HND2 = "HND2";
                Level myLevel;

                if (trim(level) == ND1)
                {
                    return myLevel = new Level() { Id = 1 };
                }
                else if (trim(level) == ND2)
                {
                    return myLevel = new Level() { Id = 2 };
                }
                else if (trim(level) == HND1)
                {
                    return myLevel = new Level() { Id = 3 };
                }
                else if (trim(level) == HND2)
             
                {
                    return myLevel = new Level() { Id = 4 };
                }
                else
                {
                    return myLevel = new Level() { Id = 1 };
                }
            }
            catch (Exception)
            {

                throw;
            }



        }
        private string trim(string trimValue)
        {
            string[] arrayString = trimValue.Split(' ');
            string trimmedValue = "";
            for (int i = 0; i < arrayString.Length; i++)
            {
                trimmedValue += arrayString[i];
                trimmedValue.Trim();

            }
            return trimmedValue;
        }
        private Semester getSemester(string semester)
        {
            try
            {
                int semesterNumber = Convert.ToInt32(semester.Trim());
                Semester currentSemester;
                if (semesterNumber == 1)
                {
                    return currentSemester = new Semester() { Id = 1 };
                }
                else if (semesterNumber == 2)
                {
                    return currentSemester = new Semester() { Id = 2 };
                }
                else if (semesterNumber == 3)
                {
                    return currentSemester = new Semester() { Id = 3 };
                }
                else
                {
                    return currentSemester = new Semester() { Id = 4 };
                }
            }
            catch (Exception)
            {

                throw;
            }

        }
        public ActionResult BulkUpload()
        {
            return View();
        }


        [HttpPost]
        public ActionResult BulkUpload(UploadCourseViewModel uploadCourseViewModel)
        {
            try
            {

                List<CourseData> courseDataList = new List<CourseData>();
                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    string pathForSaving = Server.MapPath("~/Content/ExcelUploads");
                    string savedFileName = Path.Combine(pathForSaving, hpf.FileName);
                    hpf.SaveAs(savedFileName);
                    IExcelManager excelManager = new ExcelManager();
                    DataSet courseList = excelManager.ReadExcel(savedFileName);

                    if (courseList != null && courseList.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < courseList.Tables[0].Rows.Count; i++)
                        {
                            CourseData courseData = new CourseData();
                            courseData.CourseCode = courseList.Tables[0].Rows[i][0].ToString().Trim();
                            courseData.CourseTitle = courseList.Tables[0].Rows[i][1].ToString().Trim();
                            courseData.CourseUnit = courseList.Tables[0].Rows[i][2].ToString().Trim();
                            courseData.Session = courseList.Tables[0].Rows[i][3].ToString().Trim();
                            courseData.Semester = courseList.Tables[0].Rows[i][4].ToString().Trim();
                            courseData.Level = courseList.Tables[0].Rows[i][5].ToString().Trim();
                            courseData.Programme = courseList.Tables[0].Rows[i][6].ToString().Trim();


                            if (courseData.Level != "" && courseData.Semester != "" && courseData.CourseCode != "" && courseData.CourseTitle != "" && courseData.CourseUnit != "")
                            {
                                courseDataList.Add(courseData);
                            }
                        }
                    }
                }
                uploadCourseViewModel.CourseDataList = courseDataList;
                TempData["uploadCourseViewModel"] = uploadCourseViewModel;


            }
            catch (Exception ex)
            {
                SetMessage("Error occured! " + " " + ex.Message, Message.Category.Error);

            }
            return View(uploadCourseViewModel);
        }

        public ActionResult SaveBulkUpload()
        {
            UploadCourseViewModel uploadCourseViewModel = (UploadCourseViewModel)TempData["uploadCourseViewModel"];
            List<ProgrammeCourse> programmeCourseList = new List<ProgrammeCourse>();
            try
            {
                if (uploadCourseViewModel != null)
                {
                    foreach (CourseData courseData in uploadCourseViewModel.CourseDataList)
                    {

                        Programme programme = new Programme() { Id = Convert.ToInt32(courseData.Programme) };
                        Level level = new Level { Id = Convert.ToInt32(courseData.Level) };
                        Session session = new Session() { Id = Convert.ToInt32(courseData.Session) };
                        CourseType courseType = new CourseType() { Id = 1 };
                        Course course = new Course();
                        CourseLogic courseLogic = new CourseLogic();
                        string courseCode = courseData.CourseCode.Trim();
                        string courseTitle = courseData.CourseTitle.Trim();
                        course = courseLogic.GetModelBy(p => p.Course_Code == courseCode && p.Title == courseTitle);
                        if (course == null)
                        {
                            Course newCourse = new Course();
                            newCourse.Course_Code = courseCode;
                            newCourse.Name = courseData.CourseTitle;
                            newCourse.IsRemoveRequested = true;
                            newCourse = courseLogic.Create(newCourse);
                            ProgrammeCourse programmeCourse = CreateProgrammeCourse(newCourse, programme, level, courseType, courseData, session);
                            programmeCourseList.Add(programmeCourse);
                        }
                        else
                        {
                            bool isProgrammeCourseExisting = checkProgrammeCourse(programme, course, courseData, level, session);
                            if (!isProgrammeCourseExisting)
                            {
                                ProgrammeCourse programmeCourse = CreateProgrammeCourse(course, programme, level, courseType, courseData, session);
                                programmeCourseList.Add(programmeCourse);
                            }

                        }

                    }

                }
                //keepDropDownState(uploadCourseViewModel);
                SetMessage("Upload successful " + programmeCourseList.Count + " " + "course(s) added", Message.Category.Information);
                return View("BulkUpload");
            }
            catch (Exception ex)
            {
                //keepDropDownState(uploadCourseViewModel);
                SetMessage("Error occured! " + " " + ex.Message, Message.Category.Error);
                return View("BulkUpload");
            }

        }

        private ProgrammeCourse CreateProgrammeCourse(Course course, Programme programme, Level level, CourseType courseType, CourseData courseData, Session session)
        {
            try
            {
                Semester semester = getSemester(courseData.Semester);
                ProgrammeCourse programmeCourse = new ProgrammeCourse();
                ProgrammeCourseLogic programmeCourseLogic = new ProgrammeCourseLogic();
                programmeCourse.Course = course;
                programmeCourse.Programme = programme;
                programmeCourse.Level = level;
                programmeCourse.CourseType = courseType;
                programmeCourse.CourseUnit = Convert.ToInt32(courseData.CourseUnit);
                programmeCourse.IsRemoveRequested = true;
                programmeCourse.Activated = true;
                programmeCourse.Semester = semester;
                programmeCourse.Session = session;

                programmeCourse = programmeCourseLogic.Create(programmeCourse);
                return programmeCourse;

            }
            catch (Exception)
            {

                throw;
            }
        }
        private bool checkProgrammeCourse(Programme programme, Course course, CourseData courseData, Level level, Session session)
        {
            try
            {
                Semester semester = getSemester(courseData.Semester);
                ProgrammeCourse programmeCourse = new ProgrammeCourse();
                ProgrammeCourseLogic programmeCourseLogic = new ProgrammeCourseLogic();
                programmeCourse = programmeCourseLogic.GetModelBy(p => p.CourseId == course.Id && p.ProgrammeId == programme.Id && p.SemesterId == semester.Id && p.LevelId == level.Id && p.Session_Id == session.Id);
                if (programmeCourse != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}