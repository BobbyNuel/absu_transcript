﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Models;
using Menu = Abundance_Rm.Model.Model.Menu;

namespace Abundance_Rm.Areas.Admin.ViewModel
{
    public class MenuViewModel
    {
        public MenuViewModel()
        {
            RoleSelectList = Utility.PopulateRoleSelectListItem();
            MenuGroupSelectList = Utility.PopulateMenuGroupSelectListItem();
            MenuSelectList = Utility.PopulateMenuSelectListItem();
        }

        public Role Role { get; set; }
        public MenuGroup MenuGroup { get; set; }
        public Menu Menu { get; set; }
        public MenuInRole MenuInRole { get; set; }
        public List<Menu> MenuList { get; set; }
        public List<MenuInRole> MenuInRoleList { get; set; }
        public List<SelectListItem> RoleSelectList { get; set; }
        public List<SelectListItem> MenuGroupSelectList { get; set; }
        public List<SelectListItem> MenuSelectList { get; set; }
    }

}