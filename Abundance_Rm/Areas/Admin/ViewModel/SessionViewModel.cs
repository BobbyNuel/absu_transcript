﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Models;
using System.Web.Mvc;

namespace Abundance_Rm.Areas.Admin.ViewModel
{
    public class SessionViewModel
    {
        public SessionViewModel()
        {
            SessionSectList = Utility.PopulateSessionList();
        }
        public int SessionId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsRemoveRequested { get; set; }
        public bool IsCurrenSession { get; set; }
        public List<SelectListItem> SessionSectList { get; set; }
        public List<Session> SessionList { get; set; }
    }
}