﻿using Abundance_Rm.Model.Model;
using Abundance_Rm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Abundance_Rm.Areas.Admin.ViewModel
{
    public class ProgrammeViewModel
    {
        public  ProgrammeViewModel()
        {
            CertificateSelectList = Utility.PopulateCerficateListItem();
            DepartmentSelectList = Utility.PopulateDepartmentListItem();
            ProgrammeTypeSelectList = Utility.PopulateProgrammeTypeListItem();
            DurationSelectList = Utility.DurationListItem();
        }
        public int ProgrammeId { get; set; }
        public ProgrammeType programmeType { get; set; }
        public Department department { get; set; }
        public Certificate certificate { get; set; }
        public string ProgrammeName { get; set; }
        public string StartLevel { get; set; }
        public string EndLevel { get; set; }
        public Nullable<int> Duration { get; set; }
        public Nullable<int> UnitsRequired { get; set; }
        public bool Activated { get; set; }
        public Nullable<short> CategoryId { get; set; }
        public bool IsRemoveRequested { get; set; }
        public List<SelectListItem> DurationSelectList { get; set; }
        public List<SelectListItem> CertificateSelectList { get; set; }
        public List<SelectListItem> DepartmentSelectList { get; set; }
        public List<SelectListItem> ProgrammeTypeSelectList { get; set; }

    }
}