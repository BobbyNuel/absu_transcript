﻿using Abundance_Rm.Model.Model;
using Abundance_Rm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Abundance_Rm.Areas.Admin.ViewModel
{
    public class StaffCourseAllocationViewModel
    {
        public StaffCourseAllocationViewModel()
        {
            StaffSelectList = Utility.PopulateStaffSelectListItem();
            ProgrammeTypeSelectList = Utility.PopulateProgrammeTypeListItem();
            DepartmentSelectList = Utility.PopulateDepartmentListItem();
            SessionSelectList = Utility.PopulateSessionListItem();
            LevelSelectList = Utility.PopulateLevelListItem();
        }
        public List<SelectListItem> LevelSelectList { get; set; }
        public List<SelectListItem> ProgrammeTypeSelectList { get; set; }
        public List<SelectListItem> DepartmentSelectList { get; set; }
        public List<SelectListItem> SessionSelectList { get; set; }
        public List<SelectListItem> StaffSelectList { get; set; }
        public Level Level { get; set; }
        public ProgrammeType ProgrammeType { get; set; }
        public Department Department { get; set; }
        public Session Session { get; set; }
        public Semester Semester { get; set; }
        public Course Course { get; set; }
        public User User { get; set; }
        public long cid { get; set; }
        public CourseAllocation CourseAllocation { get; set; }
        public List<CourseAllocation> CourseAllocationList { get; set; }
    }
}