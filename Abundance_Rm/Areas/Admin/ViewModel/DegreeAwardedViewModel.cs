﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Models;
using Microsoft.Owin.Security;

namespace Abundance_Rm.Areas.Admin.ViewModel
{
    public class DegreeAwardedViewModel
    {
        public DegreeAwardedViewModel()
        {
            ProgrammeTypeSelectListItem = Utility.PopulateProgrammeTypeListItem();
            DepartmentSelectListItem = Utility.PopulateDepartmentListItem();
            FacultySelectListItem = Utility.PopulateFacultSelectListItem();

        }
        public DegreeAwardsByProgrammeDepartment DegreeAwardsByProgrammeDepartment { get; set; }
        public List<DegreeAwardsByProgrammeDepartment> DegreeAwarsProgrammeDepartments { get; set; }
        public List<SelectListItem> ProgrammeTypeSelectListItem { get; set; }
        public List<SelectListItem>DepartmentSelectListItem { get; set; }
        public List<SelectListItem> FacultySelectListItem { get; set; } 
    }
}