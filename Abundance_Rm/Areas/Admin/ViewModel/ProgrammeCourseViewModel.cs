﻿using Abundance_Rm.Model.Model;
using Abundance_Rm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Abundance_Rm.Areas.Admin.ViewModel
{
    public class ProgrammeCourseViewModel
    {
        public  ProgrammeCourseViewModel()
        {
            CourseTypeSelectList = Utility.PopulateCourseTypeListItem();
            CourseSelectList = Utility.PopulateCourseListItem();
            ProgrammeSelectList = Utility.PopulateProgrammeListItem();
            LevelSelectList = Utility.PopulateLevelListItem();
            SemesterSelectList = Utility.PopulateSemesterListItem();
            CourseUnitSelectList = Utility.PopulateCourseUnitListItem();
            ProgrammeTypeSelectList = Utility.PopulateProgrammeTypeListItem();
            DepartmentSelectListItem = Utility.PopulateDepartmentListItem();
            SessionSelectList = Utility.PopulateSessionListItem();
        }
        public long ProgrammeCourseId { get; set; }
        public Programme Programme { get; set; }
        public Course Course { get; set; }
        public CourseType CourseType { get; set; }
        public int CourseUnit { get; set; }
        public Semester Semester { get; set; }
        public Level Level { get; set; }
        public bool Activated { get; set; }
        public decimal? Passmark { get; set; }
        public bool IsRemoveRequested { get; set; }
        public List<SelectListItem> CourseSelectList { get; set; }
        public List<SelectListItem> CourseTypeSelectList { get; set; }
        public List<SelectListItem> SemesterSelectList { get; set; }
        public List<SelectListItem> ProgrammeSelectList { get; set; }
        public List<SelectListItem> LevelSelectList { get; set; }
        public List<SelectListItem> CourseUnitSelectList { get; set; }
        public List<SelectListItem> ProgrammeTypeSelectList { get; set; }
        public List<SelectListItem> DepartmentSelectListItem { get; set; }

        public ProgrammeCourse ProgrammeCourse { get; set; }

        public List<ProgrammeCourse> FirstSemesterCourses { get; set; }

        public List<ProgrammeCourse> SecondSemesterCourses { get; set; }
        public Session Session { get; set; }
        public List<SelectListItem> SessionSelectList { get; set; }
    }
}