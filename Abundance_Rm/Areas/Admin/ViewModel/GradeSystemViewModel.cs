﻿using Abundance_Rm.Model.Model;
using Abundance_Rm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Abundance_Rm.Areas.Admin.ViewModel
{
    public class GradeSystemViewModel
    {
        public GradeSystemViewModel()
        {
            LevelSelectList = Utility.PopulateLevelListItem();
            GradeSelectList = Utility.PopulateGradeListItem();
            GradePointSelectList = Utility.PopulateGradePointListItem();
            StartMarkSelectList = Utility.PopulateStartMarkListItem();
            EndMarkSelectList = Utility.PopulateEndMarkListItem();
        }
        public int Grade_SystemId { get; set; }
        public int StartMark { get; set; }
        public int EndMark { get; set; }

        public string Grade { get; set; }
        public decimal GradePoint { get; set; }
        public Level Level { get; set; }
        public bool RemoveRequested { get; set; }
        public List<SelectListItem> LevelSelectList { get; set; }
        public List<SelectListItem> GradeSelectList { get; set; }
        public List<SelectListItem> GradePointSelectList { get; set; }
        public List<SelectListItem> StartMarkSelectList { get; set; }
        public List<SelectListItem> EndMarkSelectList { get; set; }

    }
}