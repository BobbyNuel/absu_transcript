﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Abundance_Rm.Model.Model;
using System.Web.Mvc;
using Abundance_Rm.Models;
using Abundance_Rm.Model.Entity;
using Microsoft.Owin.Security;

namespace Abundance_Rm.Areas.Admin.ViewModel
{
    public class TranscriptViewModel
    {
        public TranscriptViewModel()
        {
            ProgrammeTypeSelectListItem = Utility.PopulateProgrammeTypeListItem();
            DepartmentSelectListItem = Utility.PopulateDepartmentListItem();
            YearOfEntrySelectListItem = Utility.PopulateYearOfEntryListItem();
            SessionSelectListItems = Utility.PopulateSessionListItem();
            LevelSelectListItems = Utility.PopulateLevelListItem();
            GradeSelectListItems = Utility.PopulateScoreGradeSelectListItem();
            TranscriptDataTable = new DataTable();


        }
        public PROGRAMME_TYPE ProgrammeType { get; set; }
        public DEPARTMENT Department { get; set; }
        public List<SelectListItem> ProgrammeTypeSelectListItem { get; set; }
        public List<SelectListItem> DepartmentSelectListItem { get; set; }
        public List<SelectListItem> YearOfEntrySelectListItem { get; set; }
        public List<SelectListItem> SessionSelectListItems { get; set; }
        public List<SelectListItem> LevelSelectListItems { get; set; }
        public string YearOfEntry { get; set; }
        public SESSION Session { get; set; }
        public List<STUDENT> StudentList { get; set; }
        public string MatricNumber { get; set; }
        public List<TranscriptRequest> TranscriptRequests { get; set; }
        public TranscriptRequest TranscriptRequest { get; set; }
        public DataTable TranscriptDataTable { get; set; }
        public Semester Semester { get; set; }
        public Level Level { get; set; }
        public Model.Model.Student Student { get; set; }
        public List<ProgrammeCourseRegistration> ProgrammeCourseRegistrations { get; set; }
        public List<StudentCourseMark> StudentCourseMark { get; set; }
        public List<SelectListItem> GradeSelectListItems { get; set; }

        public List<ProgrammeCourseRegistration> FirstSemesterCourses { get; set; }

        public List<ProgrammeCourseRegistration> SecondSemesterCourses { get; set; }
        public ProgrammeCourseRegistration ProgrammeCourseRegistration { get; set; }
        public List<StudentLevel> StudentLevelList { get; set; }

        public StudentLevel StudentLevel { get; set; }

        public List<ProgrammeCourse> ProgrammeCoures { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
    }
}