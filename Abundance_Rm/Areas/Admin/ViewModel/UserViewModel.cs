﻿using Abundance_Rm.Model.Model;
using Abundance_Rm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Abundance_Rm.Areas.Admin.ViewModel
{
    public class UserViewModel
    {
        public UserViewModel()
        {
            SexSelectList = Utility.PopulateSexSelectListItem();
            RoleSelectList = Utility.PopulateRoleSelectListItem();
            SecurityQuestionSelectList = Utility.PopulateSecurityQuestionSelectListItem();
        }
        public User User { get; set; }
        public List<User> Users { get; set; }
        public List<SelectListItem> SexSelectList { get; set; }
        public List<SelectListItem> RoleSelectList { get; set; }
        public List<SelectListItem> SecurityQuestionSelectList { get; set; }
        public ValueHolder value { get; set; }
        public Department Department { get; set; }
        public Staff Staff { get; set; }
    }
}