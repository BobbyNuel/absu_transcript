﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Web;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Models;
using System.Web.Mvc;
using Abundance_Rm.Model.Entity;
namespace Abundance_Rm.Areas.Admin.ViewModel
{
    public class UploadResultViewModel
    {
        const int START_YEAR = 1920;
        public UploadResultViewModel()
        {
            ProgrammeTypeSelectListItem = Utility.PopulateProgrammeTypeListItem();
            DepartmentSelectListItem = Utility.PopulateDepartmentListItem();
            SessionSelectList = Utility.PopulateSessionListItem();
            LevelSelectList = Utility.PopulateLevelListItem();
            CountrySelectListItem = Utility.PopulateCountryListItem();
            SexSelectListItem = Utility.PopulateSexListItem();
            YearOfBirthSelectList = Utility.PopulateYearSelectListItem(START_YEAR ,false);
            MonthOfBirthSelectList = Utility.PopulateMonthSelectListItem();
            SessionOfEntrySelectListItem = Utility.PopulateYearOfEntryListItem();
            SemesterSelectListItem = Utility.PopulateSemesterListItem();
            FacultySelectListItem = Utility.PopulateFacultSelectListItem();
            GradeSelectList = Utility.PopulateGradeListItem();
            MbbselectListItem = Utility.PopulateMbbsListItem();

        }
        public PROGRAMME_TYPE ProgrammeType { get; set; }
        public DEPARTMENT Department { get; set; }
        public List<SelectListItem> ProgrammeTypeSelectListItem { get; set; }
        public List<SelectListItem> DepartmentSelectListItem { get; set; }
        public HttpPostedFileBase File { get; set; }
        public List<StudentData> StudentDataList { get; set; }
        public List<StudentData> AllStudentDataList { get; set; }
        public List<string> NewRegNos { get; set; }
        public List<SelectListItem> SessionSelectList { get; set; }
        public List<SelectListItem> LevelSelectList { get; set; }
        public Level Level { get; set; }
        public Session Session { get; set; }
        public Semester Semester { get; set; }
        public Course Course { get; set; }
        public ProgrammeType ProgrammeTypeModel { get; set; }
        public Department DepartmentModel { get; set; }
        public CourseAllocation CourseAllocation { get; set; }
        public List<CourseAllocation> CourseAllocations { get; set; }
        public long CourseAllocationId { get; set; }
        public List<ResultFormat> resultFormatList { get; set; }
        public Country Country { get; set; }
        public Person Person { get; set; }
        public List<SelectListItem> CountrySelectListItem { get; set; }
        public List<SelectListItem> SexSelectListItem { get; set; }
        public StudentData StudentData { get; set; }
        public List<SelectListItem> MonthOfBirthSelectList { get; set; }
        public List<SelectListItem> YearOfBirthSelectList { get; set; }
        public List<SelectListItem> SessionOfEntrySelectListItem { get; set; }
        public List<SelectListItem> SessionOfGraduationSelectListItem { get; set; }
        public List<SelectListItem> SemesterSelectListItem { get; set; }
        public List<SelectListItem> MbbselectListItem { get; set; }
        public string FileUrl { get; set; }
        public DegreeAwardsByProgrammeDepartment DegreeAward { get; set; }
        public List<DegreeAwardsByProgrammeDepartment> DegreeAwards { get; set; }
        public TranscriptRequest TranscriptRequest { get; set; }
        public List<Course> Courses { get; set; }
        public List<Department> Departments { get; set; }
        public List<SelectListItem> FacultySelectListItem { get; set; }
        public List<SelectListItem> DayOfBirthSelectList { get; set; }
        public List<Faculty> Faculties { get; set; }
        public Model.Model.Student Student { get; set; }
        public bool ModeOfEntry { get; set; }
        public List<SelectListItem> GradeSelectList { get; set; }
        public List<StudentCourseMark> StudentCourseMarks { get; set; }

        public List<ProgrammeCourse> ProgrammeCourses { get; set; }

        public List<ProgrammeCourse> FirstSemesterCourses { get; set; }
        public List<ProgrammeCourse> SecondSemesterCourses { get; set; }

        public SessionSemester SessionSemester { get; set; }

        public ProgrammeCourse ProgrammeCourse { get; set; }
    }

    public class PersonModel
    {
        public string surName { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string regNo { get; set; }
        public string sex { get; set; }
        public string dob { get; set; }
        public string mob { get; set; }
        public string yob { get; set; }
        public string programme { get; set; }
        public string department { get; set; }
        public string country { get; set; }
        public string yearOfEntry { get; set; }
        public string transcriptAddress { get; set; } 
        public string yearOfGraduation { get; set; }
        public bool IsError { get; set; }
        public string Message { get; set; }
        public string Id { get; set; }
        public string level { get; set; }
        public bool modeOfEntry { get; set; }
       public string wesVerificationNumber { get; set; }
    }
}