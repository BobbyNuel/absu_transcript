﻿using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Abundance_Rm.Areas.Admin.ViewModel
{
    public class UploadCourseViewModel
    {
        public UploadCourseViewModel()
        {
            ProgrammeTypeSelectListItem = Utility.PopulateProgrammeTypeListItem();
            DepartmentSelectListItem = Utility.PopulateDepartmentListItem();

        }
        public PROGRAMME_TYPE ProgrammeType { get; set; }
        public DEPARTMENT Department { get; set; }
        public List<SelectListItem> ProgrammeTypeSelectListItem { get; set; }
        public List<SelectListItem> DepartmentSelectListItem { get; set; }
        public HttpPostedFileBase File { get; set; }
        public List<CourseData> CourseDataList { get; set; }
        public List<string> NewRegNos { get; set; }
    }
}