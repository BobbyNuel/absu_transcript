﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Abundance_Rm.Business;
using Abundance_Rm.Model.Model;
using Microsoft.Reporting.WebForms;

namespace Abundance_Rm.Report.Presenter
{
    public partial class ProcessedTranscript : System.Web.UI.Page
    { 
        
        private void DisplayReportBy()
        {
            try
            {
               UploadedResultLogic uploadedResultLogic = new UploadedResultLogic();
               List<ProcessedStudent> processedStudents = new List<ProcessedStudent>();
                processedStudents = uploadedResultLogic.GetProcessedStudentBy(txtBoxDateTo.Text, txtBoxDateFrom.Text);

                for (int i = 0; i < processedStudents.Count; i++)
                {
                    processedStudents[i].DateFrom = txtBoxDateFrom.Text;
                    processedStudents[i].DateTo = txtBoxDateTo.Text;
                }
                if (processedStudents.Count <= 0)
                {
                    lblMessage.Text = "No Record Found for the seleted Date";
                    throw new Exception("No Record Found for the seleted Date");
                    
                }
                string reportPath = @"Report\ProcessedStudentList.rdlc";
               

                rv.Reset();
                rv.LocalReport.DisplayName = "Processed Student Transcript";
                rv.LocalReport.ReportPath = reportPath;

                if (processedStudents != null && processedStudents.Count > 0)
                {
                    rv.ProcessingMode = ProcessingMode.Local;
                    rv.LocalReport.DataSources.Add(new ReportDataSource("dsProcessedStudentList", processedStudents));
                    rv.LocalReport.Refresh();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        private bool InvalidUserInput()
        {
            try
            {

                if (string.IsNullOrEmpty(txtBoxDateTo.Text) || string.IsNullOrEmpty(txtBoxDateTo.Text))
                {
                    lblMessage.Text = "Please select a Valid Date";
                    return true;
                }
               
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void btnDisplayReport_Click(object sender, EventArgs e)
        {
            try
            {
                if (InvalidUserInput())
                {
                    return;
                }

                DisplayReportBy();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

      
    
    }
}