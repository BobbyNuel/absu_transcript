﻿using Abundance_Rm.Business;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Models;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Globalization;
using System.IO;
using System.Linq;
using ZXing;
using ZXing.Common;
using Image = System.Web.UI.WebControls.Image;

namespace Abundance_Rm.Report.Presenter
{

    public partial class ResultTranscript : System.Web.UI.Page
    {
        string studentId;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    lblMessage.Text = "";
                    if (Request.QueryString["Id"] != null)
                    {
                        studentId = Request.QueryString["Id"];
                        BuildStudentTranscript(studentId);
                    }
                }


            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message + ex.InnerException.Message;
            }
        }

        private void BuildStudentTranscript(string sId)
        {
            try
            {
                StudentLogic resultLogic = new StudentLogic();
                Student student;
                string reportPath;
                string addreessUrl = "";

                if (sId.Contains('_'))
                {
                    var regNo = sId.Replace('_', '/');
                    student = resultLogic.GetModelBy(s => s.EntryRegNo == regNo);
                    reportPath = @"Report\VerifyTranscript.rdlc";
                }
                else
                {
                    long convertedSId = Convert.ToInt64(sId);
                    student = resultLogic.GetModelBy(s => s.StudentId == convertedSId);
                    if (student.Department.Id == 50)
                    {
                        reportPath = @"Report\ResultTranscript3.rdlc";
                    }
                    else
                    {
                        reportPath = string.IsNullOrEmpty(student.Person.ModeOfEntry) ? @"Report\ResultTranscript2.rdlc" : @"Report\ResultTranscriptDirectEntry.rdlc";
                    }
                }
                List<Model.Model.Result> results = resultLogic.GetTranscriptBy(student);

                string bind_ds = "dsMasterSheet";

                var firstOrDefault = results.FirstOrDefault();

                if (results != null && results.Count > 0 && firstOrDefault != null && firstOrDefault.DepartmentId != 50)
                {
                    GenerateTranscriptForOtherDepartments(results, student, sId);
                }
                else if (results != null && results.Count > 0 && firstOrDefault != null && firstOrDefault.DepartmentId == 50)
                {
                    GenerateTranscriptForMedicalStudents(results, student, sId);
                }

                ReportViewer1.Reset();
                ReportViewer1.LocalReport.ReportPath = reportPath;

                if (results != null && results.Count > 0)
                {
                    if (!sId.Contains('_'))
                    {
                        string replacRegNumber = results[0].MatricNumber.Replace('/', '_');
                        ReportViewer1.LocalReport.DisplayName = results[1].Name + " " + "Transcript Result";
                        ReportViewer1.LocalReport.EnableExternalImages = true;
                        ReportViewer1.ProcessingMode = ProcessingMode.Local;
                        ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource(bind_ds.Trim(), results));
                        string imageFilePath = "~/Content/ExcelUploads/" + replacRegNumber + ".Jpeg";
                        string imagePath = new Uri(Server.MapPath(imageFilePath)).AbsoluteUri;
                        string addressPath = new Uri(Server.MapPath(addreessUrl)).AbsoluteUri;

                        ReportParameter parameter = new ReportParameter("ImagePath", imagePath);
                        ReportParameter parameter1 = new ReportParameter("AddressPath", addressPath);
                        ReportViewer1.LocalReport.SetParameters(new[] { parameter, parameter1 });
                        ReportViewer1.LocalReport.Refresh();
                    }
                    else
                    {
                        ReportViewer1.LocalReport.DisplayName = results[1].Name + " " + "Transcript Result";
                        ReportViewer1.LocalReport.EnableExternalImages = true;
                        ReportViewer1.ProcessingMode = ProcessingMode.Local;
                        ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource(bind_ds.Trim(), results));
                        ReportViewer1.LocalReport.Refresh();
                    }
                }
                else
                {
                    lblMessage.Text = "No result to display";
                    ReportViewer1.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        public string GenerateQrCode(string regNo, string alt = "QR code", int height = 100, int width = 100, int margin = 0)
        {

            string replacRegNumber = regNo.Replace('/', '_');
            string folderPath = "~/Content/ExcelUploads/";
            string imagePath = "~/Content/ExcelUploads/" + replacRegNumber + ".Jpeg";
            string verifyUrl = "localhost:6390/Student/Result/VerifyTranscript/" + replacRegNumber;


            string wildCard = replacRegNumber + "*.*";
            IEnumerable<string> files = Directory.EnumerateFiles(Server.MapPath("~/Content/ExcelUploads/"), wildCard, SearchOption.TopDirectoryOnly);

            if (files != null && files.Count() > 0)
            {
                return imagePath;
            }
            // If the directory doesn't exist then create it.
            if (!Directory.Exists(Server.MapPath(folderPath)))
            {
                Directory.CreateDirectory(folderPath);
            }

            var barcodeWriter = new BarcodeWriter();
            barcodeWriter.Format = BarcodeFormat.QR_CODE;
            var result = barcodeWriter.Write(verifyUrl);

            string barcodePath = Server.MapPath(imagePath);
            var barcodeBitmap = new Bitmap(result);
            using (MemoryStream memory = new MemoryStream())
            {
                using (FileStream fs = new FileStream(barcodePath, FileMode.Create))
                {
                    barcodeBitmap.Save(memory, ImageFormat.Jpeg);
                    byte[] bytes = memory.ToArray();
                    fs.Write(bytes, 0, bytes.Length);
                }


            }

            return imagePath;
        }

        public string GenerateWaterMark(string address)
        {
            string returnImagePath = "~/Content/Junk/defaultImage1.bmp";
            string imagePath = Server.MapPath("~/Content/Junk/defaultImage1.bmp");
            string defultImagePath = Server.MapPath("~/Content/Junk/defaultImage.bmp");

            Bitmap bmp = new Bitmap(defultImagePath);
            RectangleF rectf = new RectangleF(0, 0, bmp.Width, bmp.Height);
            Graphics g = Graphics.FromImage(bmp);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            g.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;
            StringFormat format = new StringFormat()
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };
            g.DrawString(address, new Font("Tahoma", 16), Brushes.Gainsboro, rectf, format);
            g.Flush();
            using (MemoryStream memory = new MemoryStream())
            {
                using (FileStream fs = new FileStream(imagePath, FileMode.Create))
                {
                    bmp.Save(memory, ImageFormat.Bmp);
                    byte[] bytes = memory.ToArray();
                    fs.Write(bytes, 0, bytes.Length);
                }


            }
            return returnImagePath;
        }

        private int CheckIfStudiesWhereComplete(int startAndEndSessionDifference, int numberofSessions)
        {
            int numberNotCompleted;
            try
            {
                numberNotCompleted = startAndEndSessionDifference - numberofSessions;

            }
            catch (Exception ex)
            {

                throw;
            }
            return numberNotCompleted;
        }

        private void GenerateTranscriptForMedicalStudents(List<Model.Model.Result> results,Student student,string sId )
        {

            try
            {
                DegreeAwardsByProgrammeDepartmentLogic degreeAwardsLogic = new DegreeAwardsByProgrammeDepartmentLogic();
                StudentLogic resultLogic = new StudentLogic();
                string imageUrl = "";
                string addreessUrl = GenerateWaterMark(results[0].Address);
                var firstOrDefault = results.FirstOrDefault();
                string remark = "";
                string graduationDate = "";
                string admissionDate = "";

                if (!sId.Contains('_'))
                {
                    imageUrl = GenerateQrCode(results[0].MatricNumber);
                }
                
                if (firstOrDefault != null)
                {
                    int currentDepartmentId = firstOrDefault.DepartmentId;
                    int currentProgrammeId = firstOrDefault.ProgrammeId;

                    if (student.Person.ModeOfEntry != null)
                    {
                        var semesterCode = 1 + " " + "MBBS";
                       
                        SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                        SESSION_SEMESTER sessionSemester = sessionSemesterLogic.GetEntityBy(s => s.SEMESTER.SemesterCode == semesterCode);
                        Model.Model.Result result = new Model.Model.Result();
                        result.Name = results[0].Name;
                        result.DepartmentName = results[0].DepartmentName;
                        result.FacultyName = results[0].FacultyName;
                        result.Sex = results[0].Sex;
                        result.DateOfBirth = results[0].DateOfBirth;
                        result.Country = results[0].Country;
                        result.GradePoint = 0;
                        result.WesVerificationNumber = student.WesVerificationNumber;
                        result.SequenceNumber = sessionSemester.Sequence_Number;
                        result.SemesterName = sessionSemester.SEMESTER.SemesterName;
                        result.MatricNumber = student.EntryRegNo;
                        result.CourseName = "Direct Entry";
                        results.Insert(0, result);
                    }

                    for (int i = 0; i < results.Count; i++)
                    {
                       DegreeAwardsByProgrammeDepartment degreeAward = degreeAwardsLogic.GetModelBy(ad => ad.Department_Id == currentDepartmentId && ad.Programme_Id == currentProgrammeId);
                        if (degreeAward != null && degreeAward.Id > 0)
                        {
                            remark = student.CurrentLevel.Id < 5 ? "NOT YET GRADUATED" : degreeAward.DegreeName;

                        }
                        else
                        {
                            throw new Exception("No Degree Award Was Set For The Department kindly Set Degree And Try Again.");
                        }
                        results[i].GraduationDate = graduationDate;
                        results[i].AdmissionDate = admissionDate;
                        results[i].QrCode = imageUrl;
                        results[i].WaterMark = addreessUrl;
                        results[i].cGPA = "MBBS";
                        results[i].Remark = remark;
                        results[i].DegreeClassification = "UNCLASSIFIED";
                        results[i].Date = resultLogic.GetTodaysDateFormat();
                        results[i].DateOfBirth = student.Person.DateofBirth;
                        results[i].WesVerificationNumber = !string.IsNullOrEmpty(student.WesVerificationNumber) ? student.WesVerificationNumber : " ";
                        results[i].Country = (results[i].Country + "n");

                    }
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        private void GenerateTranscriptForOtherDepartments(List<Model.Model.Result> results, Student student, string sId)
        {
            try
            {
                List<int> totalUnits = new List<int>();
                List<decimal> totalGradePoint = new List<decimal>();
                StudentLogic resultLogic = new StudentLogic();
                string imageUrl = "";
                string remark = "";
                string graduationDate = "";
                string admissionDate = "";
                int distinctSession = 0;

                if (!sId.Contains('_'))
                {
                    imageUrl = GenerateQrCode(results[0].MatricNumber);
                }
               
                string yearofEntry = student.YearOfEntry;
                string yearofGraduation = student.GraduationDate;
                string[] yearofEntryText = yearofEntry.Split('/');
                string[] yearofGraduationText = yearofGraduation.Split('/');
                graduationDate = yearofGraduationText.LastOrDefault();
                admissionDate = yearofEntryText.FirstOrDefault();

                var numberOfCompletedSessions = Convert.ToInt32(graduationDate) - Convert.ToInt32(admissionDate);
                string addreessUrl = GenerateWaterMark(results[0].Address);

                var firstOrDefault = results.FirstOrDefault();
                DegreeAwardsByProgrammeDepartment degreeAward  = new DegreeAwardsByProgrammeDepartment();
                foreach (Model.Model.Result result in results)
                {
                    if (firstOrDefault != null)
                    {
                        var id = firstOrDefault.DepartmentId;
                        var programmeId = firstOrDefault.ProgrammeId;

                       distinctSession = results.Select(s => s.SessionId).ToList().Distinct().Count();

                      DegreeAwardsByProgrammeDepartmentLogic degreeAwardsLogic = new DegreeAwardsByProgrammeDepartmentLogic();
                      degreeAward = degreeAwardsLogic.GetModelBy(ad => ad.Department_Id == id && ad.Programme_Id == programmeId);
                    }

                    if (degreeAward != null && degreeAward.Id > 0)
                    {
                        remark = degreeAward.DegreeName;
                    }
                    else
                    {
                        throw new Exception("No Degree Award Was Set For The Department kindly Set Degree And Try Again.");
                    }

                    if (degreeAward.Programme.ProgrammeType.Id == 11)
                    {
                        result.SemesterName = "LONG VACATION";
                    }

                    if (string.IsNullOrWhiteSpace(result.Grade))
                    {
                        result.GradePoints = " ";
                    }
                    else
                    {
                        string converttodecimal = Math.Round((Decimal)result.GPCU, 2).ToString("0.0");
                        result.GradePoints = converttodecimal;
                    }

                    result.GraduationDate = graduationDate;
                    result.AdmissionDate = admissionDate;
                    result.QrCode = imageUrl;
                    result.AddressHeader = student.YearOfEntry;
                    result.WaterMark = addreessUrl;
                    totalUnits.Add(result.CourseUnit);
                    totalGradePoint.Add((decimal)result.GPCU);

                }
                for (int i = 0; i < totalUnits.Count; i++)
                {
                    if (string.IsNullOrEmpty(results[i].cGPA))
                    {
                        results[i].TotalSemesterCourseUnit = totalUnits.Sum();
                        results[i].TotalGradePoint = totalGradePoint.Sum();
                        string cGPA = Math.Round((decimal)(results[i].TotalGradePoint / results[i].TotalSemesterCourseUnit), 2).ToString("0.00");
                        results[i].CGPA = Math.Round(Convert.ToDecimal(results[i].cGPA), 2);
                        results[i].cGPA = cGPA;
                        results[i].Remark = remark;
                        results[i].DegreeClassification = resultLogic.GetGraduationStatus(results[i].CGPA, results[i].GraduationDate);
                        results[i].Date = resultLogic.GetTodaysDateFormat();
                        results[i].DateOfBirth = student.Person.DateofBirth;
                        results[i].WesVerificationNumber = student.WesVerificationNumber;
                        results[i].NumberOfNonCompletedSession = CheckIfStudiesWhereComplete(numberOfCompletedSessions, distinctSession);
                    }
                    else
                    {
                        results[i].Remark = remark;
                        results[i].CGPA = Convert.ToDecimal(results[i].cGPA);
                        results[i].DegreeClassification = resultLogic.GetGraduationStatus(results[i].CGPA, results[i].GraduationDate);
                        results[i].Date = resultLogic.GetTodaysDateFormat();
                        results[i].DateOfBirth = student.Person.DateofBirth;
                        results[i].TotalSemesterCourseUnit = totalUnits.Sum();
                        results[i].TotalGradePoint = totalGradePoint.Sum();
                        results[i].WesVerificationNumber = !string.IsNullOrEmpty(student.WesVerificationNumber)?student.WesVerificationNumber : " ";
                    }

                }
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }
    }
}