﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Abundance_Rm.Model.Model;
using Abundance_Rm.Business;
using System.Configuration;
using Abundance_Rm.Models;
using Microsoft.Reporting.WebForms;
using Abundance_Rm.Models.Intefaces;
using Abundance_Rm.Model.Entity;


namespace Abundance_Rm.Report.Presenter.Result
{
    public partial class MasterSheet : System.Web.UI.Page, IReport
    {
        public string Message
        {
            get { return lblMessage.Text; }
            set { lblMessage.Text = value; }
        }

        public ReportViewer Viewer
        {
            get { return rv; }
            set { rv = value; }
        }

        public int ReportType
        {
            get { throw new NotImplementedException(); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Message = "";

                if (!IsPostBack)
                {
                    ddlDepartment.Visible = false;
                    PopulateAllDropDown();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        public SessionSemester SelectedSession
        {
            get { return new SessionSemester() { Id = Convert.ToInt32(ddlSession.SelectedValue), Name = ddlSession.SelectedItem.Text }; }
            set { ddlSession.SelectedValue = value.Id.ToString(); }
        }

        public Level Level
        {
            get { return new Level() { Id = Convert.ToInt32(ddlLevel.SelectedValue), Name = ddlLevel.SelectedItem.Text }; }
            set { ddlLevel.SelectedValue = value.Id.ToString(); }
        }

        public ProgrammeType Programme
        {
            get { return new ProgrammeType() { Id = Convert.ToInt32(ddlProgramme.SelectedValue), Name = ddlProgramme.SelectedItem.Text }; }
            set { ddlProgramme.SelectedValue = value.Id.ToString(); }
        }

        public Department Department
        {
            get { return new Department() { Id = Convert.ToInt32(ddlDepartment.SelectedValue), Name = ddlDepartment.SelectedItem.Text }; }
            set { ddlDepartment.SelectedValue = value.Id.ToString(); }
        }


        private void DisplayReportBy(SessionSemester session, Level level, ProgrammeType programme, Department department, int semesterId)
        {
            try
            {
                StudentCourseMarkLogic studentCourseMarkLogic = new StudentCourseMarkLogic();
                List<Model.Model.Result> results = studentCourseMarkLogic.GetMaterSheetBy(session, level, programme, department);
                
                string reportPath = "";
                string bind_ds = "dsMasterSheet";
                if (semesterId == 1)
                {
                    reportPath = @"Report\Result\MasterGradeSheetFirstSemester.rdlc";
                }
                else if (level.Id == 1 && semesterId == 2)
                {
                    reportPath = @"Report\Result\MasterGradeSheetSecondSemesterND.rdlc";                    
                }
                else if (level.Id == 3 && semesterId == 2)
                {
                    reportPath = @"Report\Result\MasterGradeSheetSecondSemesterHND.rdlc";
                }
                else if (level.Id == 2 && semesterId == 2)
                {
                    reportPath = @"Report\Result\MasterGradeSheetND.rdlc";
                }
                else if (level.Id == 4 && semesterId == 2)
                {
                    reportPath = @"Report\Result\MasterGradeSheetHND.rdlc";
                }

                rv.Reset();
                rv.LocalReport.DisplayName = "Student Master Sheet";
                rv.LocalReport.ReportPath = reportPath;
                rv.LocalReport.EnableExternalImages = true;

                string programmeName = programme.Id > 3 ? "Higher National Diploma" : "National Diploma";
                ReportParameter programmeParam = new ReportParameter("Programme", programmeName);
                ReportParameter departmentParam = new ReportParameter("Department", department.Name);
                ReportParameter sessionSemesterParam = new ReportParameter("SessionSemester", session.Name);
                ReportParameter[] reportParams = new ReportParameter[] { departmentParam, programmeParam, sessionSemesterParam };
                rv.LocalReport.SetParameters(reportParams);

                if (results != null)
                {
                    rv.ProcessingMode = ProcessingMode.Local;
                    rv.LocalReport.DataSources.Add(new ReportDataSource(bind_ds.Trim(), results));
                    rv.LocalReport.Refresh();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        private void PopulateAllDropDown()
        {
            try
            {
                Utility.BindDropdownItem(ddlSession, Utility.GetAllSessionSemesters(), Utility.ID, Utility.NAME);
                Utility.BindDropdownItem(ddlLevel, Utility.GetAllLevels(), Utility.ID, Utility.NAME);
                Utility.BindDropdownItem(ddlProgramme, Utility.GetAllProgrammeTypes(), Utility.ID, Utility.NAME);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        private bool InvalidUserInput()
        {
            try
            {
                if (SelectedSession == null || SelectedSession.Id <= 0)
                {
                    lblMessage.Text = "Please select Session";
                    return true;
                }
                else if (Programme == null || Programme.Id <= 0)
                {
                    lblMessage.Text = "Please select Programme";
                    return true;
                }
                else if (Department == null || Department.Id <= 0)
                {
                    lblMessage.Text = "Please select Department";
                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void btnDisplayReport_Click(object sender, EventArgs e)
        {
            try
            {
                if (InvalidUserInput())
                {
                    return;
                }
                SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                SessionSemester sessionSemester = sessionSemesterLogic.GetModelBy(ss => ss.Session_Semester_Id == SelectedSession.Id);
                Semester semester = new Semester(){Id = sessionSemester.Semester.Id};
                DisplayReportBy(SelectedSession, Level, Programme, Department, semester.Id);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        protected void ddlProgramme_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Programme != null && Programme.Id > 0)
                {
                    PopulateDepartmentDropdownByProgramme(Programme);
                }
                else
                {
                    ddlDepartment.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        private void PopulateDepartmentDropdownByProgramme(ProgrammeType programme)
        {
            try
            {
                ProgrammeTypeLogic programmeTypeLogic = new ProgrammeTypeLogic();
                PROGRAMME_TYPE entityProgrammeType = programmeTypeLogic.GetEntityBy(pt => pt.ProgrammeTypeId == programme.Id);
                DepartmentLogic departmentLogic = new DepartmentLogic();
                List<DEPARTMENT> departments = departmentLogic.GetBy(entityProgrammeType);

                List<Department> departmentList = new List<Department>();
                foreach (DEPARTMENT department in departments)
                {
                    departmentList.Add(departmentLogic.GetModelBy(d => d.DepartmentId == department.DepartmentId));
                }
                if (departments != null && departments.Count > 0)
                {
                    Utility.BindDropdownItem(ddlDepartment, departmentList, Utility.ID, Utility.NAME);
                    ddlDepartment.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }



        
    }
}