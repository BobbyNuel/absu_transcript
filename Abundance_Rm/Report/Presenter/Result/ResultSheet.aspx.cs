﻿using Abundance_Rm.Business;
using Abundance_Rm.Model.Model;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Abundance_Rm.Report.Presenter.Result
{
    public partial class ResultSheet : System.Web.UI.Page
    {
        string deptId;
        string progId;
        string sessionId;
        string levelId;
        string semesterId;
        string courseId;
        protected void Page_Load(object sender, EventArgs e)
        {
            
                try
                {                                        
                    
                        lblMessage.Text = "";
                        if (Request.QueryString["levelId"] != null && Request.QueryString["semesterId"] != null && Request.QueryString["progId"] != null && Request.QueryString["deptId"] != null && Request.QueryString["sessionId"] != null && Request.QueryString["courseId"] != null)
                        {
                            levelId = Request.QueryString["levelId"];
                            semesterId = Request.QueryString["semesterId"];
                            progId = Request.QueryString["progId"];
                            deptId = Request.QueryString["deptId"];
                            sessionId = Request.QueryString["sessionId"];
                            courseId = Request.QueryString["courseId"];
                        }

                        if (!Page.IsPostBack)
                        {
                            Load();
                        }
                    }
                    
               
                catch (Exception ex)
                {
                    lblMessage.Text = ex.Message + ex.InnerException.Message;
                }
            
           
        }
        protected void Load()
        {
            int deptId1 = Convert.ToInt32(deptId);
            int progId1 = Convert.ToInt32(progId);
            int sessionId1 = Convert.ToInt32(sessionId);
            int levelId1 = Convert.ToInt32(levelId);
            int semesterId1 = Convert.ToInt32(semesterId);
            int courseId1 = Convert.ToInt32(courseId);

            BuildResultSheet(levelId1, semesterId1, progId1, deptId1, sessionId1, courseId1);
        }
        private void BuildResultSheet(int levelId1, int semesterId1, int progId, int deptId, int sessionId1, int courseId1)
        {
            try
            {
                Department department = new Department() { Id = deptId };
                ProgrammeType programmeType = new ProgrammeType() { Id = progId };
                Session session = new Session() { Id = sessionId1 };
                Level level = new Level() { Id = levelId1 };
                Semester semester = new Semester { Id = semesterId1 };
                Course course = new Course { Id = courseId1 };

                StudentCourseMarkLogic studentCourseMarkLogic = new StudentCourseMarkLogic();

                List<Abundance_Rm.Model.Model.Result> ResultSheet = studentCourseMarkLogic.GetResultSheet(department, programmeType, level, session, semester, course);
                string bind_dsResultSheet = "dsResultSheet";

                string reportPath = @"Report\Result\ResultSheet.rdlc";

                report.Reset();
                report.LocalReport.DisplayName = "Result Sheet";
                report.LocalReport.ReportPath = reportPath;

                if (ResultSheet != null)
                {
                    report.ProcessingMode = ProcessingMode.Local;
                    report.LocalReport.DataSources.Add(new ReportDataSource(bind_dsResultSheet.Trim(), ResultSheet));

                    report.LocalReport.Refresh();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message + ex.InnerException.Message;

            }
        }
    }
}