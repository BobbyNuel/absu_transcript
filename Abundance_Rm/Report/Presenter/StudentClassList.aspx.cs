﻿using Abundance_Rm.Business;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Abundance_Rm.Report.Presenter
{
    
    public partial class StudentClassList : System.Web.UI.Page
    {
        
        string departmentId;
        string programmeId;
        string yearOfEntry;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                if (Request.QueryString["deptId"] != null && Request.QueryString["progId"] != null && Request.QueryString["yearOfGraduation"] != null)
                {
                    departmentId = Request.QueryString["deptId"];
                    programmeId = Request.QueryString["progId"];
                    yearOfEntry = Request.QueryString["yearOfGraduation"];


                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message + ex.InnerException.Message;
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            int deptId = Convert.ToInt32(departmentId);
            int progTypeId = Convert.ToInt32(programmeId);
            BuildStudentClassList(deptId, progTypeId, yearOfEntry);
        }
        private void BuildStudentClassList(int deptId, int progTypeId, string yearOfGraduation )
        {
            try
            {

                DEPARTMENT department = new DEPARTMENT() { DepartmentId = deptId };
                PROGRAMME_TYPE programmeType = new PROGRAMME_TYPE() { ProgrammeTypeId = progTypeId };
                StudentLogic studentLogic = new StudentLogic();
                STUDENT student = new STUDENT();
                StudentCourseMarkLogic courseMarkLogic = new StudentCourseMarkLogic();

                List<STUDENT> studentList = studentLogic.GetBy(yearOfGraduation, department, programmeType);
                List<StudentClassListReport> studentClassList = courseMarkLogic.GetStudentClassListWithCGPA(studentList);

                string bind_dsStudentClassList = "dsStudentClassList";

                string reportPath = @"Report\StudentClassList.rdlc";

                ReportViewer1.Reset();
                ReportViewer1.LocalReport.DisplayName = "Student Class List ";
                ReportViewer1.LocalReport.ReportPath = reportPath;

                if (studentClassList != null )
                {
                    ReportViewer1.ProcessingMode = ProcessingMode.Local;
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource(bind_dsStudentClassList.Trim(), studentClassList));
                   
                    ReportViewer1.LocalReport.Refresh();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message + ex.InnerException.Message;
                
            }
        }




    }
}