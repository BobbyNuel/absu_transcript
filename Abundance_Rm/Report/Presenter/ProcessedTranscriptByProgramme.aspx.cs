﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Abundance_Rm.Business;
using Abundance_Rm.Model.Entity;
using Abundance_Rm.Model.Model;
using Abundance_Rm.Models;
using Microsoft.Reporting.WebForms;

namespace Abundance_Rm.Report.Presenter
{
    public partial class ProcessedTranscriptByProgramme : System.Web.UI.Page
    {
        private List<Department> departments;
        public ProgrammeType SelectedProgramme
        {
            get
            {
                return new ProgrammeType()
                {
                    Id = Convert.ToInt32(ddlProgramme.SelectedValue),
                    Name = ddlProgramme.SelectedItem.Text
                };
            }
            set { ddlProgramme.SelectedValue = value.Id.ToString(); }
        }

        public Department SelectedDepartment
        {
            get
            {
                return new Department
                {
                    Id = Convert.ToInt32(ddlDepartment.SelectedValue),
                    Name = ddlDepartment.SelectedItem.Text
                };
            }
            set { ddlDepartment.SelectedValue = value.Id.ToString(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";

                if (!IsPostBack)
                {
                  
                    Utility.BindDropdownItem(ddlProgramme, Utility.GetAllProgrammeTypes(), Utility.ID, Utility.NAME);
                    ddlDepartment.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message + ex.InnerException.Message;
            }
        }
        private void DisplayReportBy(Department department,ProgrammeType programmeType)
        {
            try
            {
                UploadedResultLogic uploadedResultLogic = new UploadedResultLogic();
                List<ProcessedStudent> processedStudents = new List<ProcessedStudent>();
                processedStudents = uploadedResultLogic.GetProcessedStudentBy(department, programmeType,txtBoxDateTo.Text, txtBoxDateFrom.Text);

                for (int i = 0; i < processedStudents.Count; i++)
                {
                    processedStudents[i].DateFrom = txtBoxDateFrom.Text;
                    processedStudents[i].DateTo = txtBoxDateTo.Text;
                }
                if (processedStudents.Count <= 0)
                {
                    lblMessage.Text = "No Record Found for the seleted Date";
                    throw new Exception("No Record Found for the seleted Date");

                }
                string reportPath = @"Report\ProcessedStudentTranscriptByProgramme.rdlc";


                rv.Reset();
                rv.LocalReport.DisplayName = "Processed Student Transcript";
                rv.LocalReport.ReportPath = reportPath;

                if (processedStudents != null && processedStudents.Count > 0)
                {
                    rv.ProcessingMode = ProcessingMode.Local;
                    rv.LocalReport.DataSources.Add(new ReportDataSource("dsProcessedStudentByProgramme", processedStudents));
                    rv.LocalReport.Refresh();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        private bool InvalidUserInput()
        {
            try
            {

              
                if (SelectedDepartment == null || SelectedDepartment.Id <= 0 || SelectedProgramme == null || SelectedProgramme.Id <= 0 || string.IsNullOrEmpty(txtBoxDateTo.Text) || string.IsNullOrEmpty(txtBoxDateTo.Text))
                {
                    lblMessage.Text = "Please select a Valid Date";
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void btnDisplayReport_Click(object sender, EventArgs e)
        {
            try
            {
                if (InvalidUserInput())
                {
                    return;
                }

                DisplayReportBy(SelectedDepartment, SelectedProgramme);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }
        protected void ddlProgramme_SelectedIndexChanged(object sender, EventArgs e)
        {
            var programme = new ProgrammeType { Id = Convert.ToInt32(ddlProgramme.SelectedValue) };
            var departmentLogic = new DepartmentLogic();
            departments = departmentLogic.GetBy(programme);
            Utility.BindDropdownItem(ddlDepartment, departments, Utility.ID, Utility.NAME);
            ddlDepartment.Visible = true;
        }

    }
}