﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Abundance_Rm.Business;
using Abundance_Rm.Model.Model;
using System.Web.UI.WebControls;

namespace Abundance_Rm.Models
{
    public class Utility
    {
        public const string Select = "-- Select --";
        public const string SelectLevel = "--Select Level--";
        public const string SelectSession = "-- Select Session --";
        public const string SelectYearOfEntry = "-- Select Year Of Entry --";
        public const string SelectProgramme = "-- Select Programme --";
        public const string SelectDepartment = "-- Select Department --";
        public const string PROGRAMMENAME = "ProgrammeName";
        public const string ID = "Id";
        public const string NAME = "Name";
        public const string VALUE = "Value";
        public const string TEXT = "Text";
        public const string DEFAULT_AVATAR = "/Content/Images/ts_avatar.jpg";
        public static void BindDropdownItem<T>(DropDownList dropDownList, T items, string dataValueField, string dataTextField)
        {
            dropDownList.Items.Clear();

            dropDownList.DataValueField = dataValueField;
            dropDownList.DataTextField = dataTextField;


            dropDownList.DataSource = items;
            dropDownList.DataBind();
        }
        public static List<SelectListItem> PopulateCerficateListItem()
        {
            try
            {
                CertificateLogic certificateLogic = new CertificateLogic();
                List<Certificate> certificat = certificateLogic.GetAll();
                if(certificat == null || certificat.Count<=0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> certificateList = new List<SelectListItem>();
                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = Select;
                certificateList.Add(list);

                foreach(Certificate certificates in certificat )
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = certificates.Id.ToString();
                    selectList.Text = certificates.Name;
                    certificateList.Add(selectList);

                }
                return certificateList;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public static List<SelectListItem> PopulateDepartmentListItem()
        {
            try
            {
                DepartmentLogic departmentLogic = new DepartmentLogic();
                List<Department> department = departmentLogic.GetAll();
                if (department == null || department.Count <= 0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> departmentList = new List<SelectListItem>();
                
                foreach (Department departments in department)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = departments.Id.ToString();
                    selectList.Text = departments.Name;
                    departmentList.Add(selectList);

                }
               var sortedList =  departmentList.OrderBy(s => s.Text).ToList();
                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = SelectDepartment;
                sortedList.Insert(0, list);

                return sortedList;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static List<SelectListItem> PopulateProgrammeTypeListItem()
        {
            try
            {
                ProgrammeTypeLogic programmeTypeLogic = new ProgrammeTypeLogic();
                List<ProgrammeType> programmeType = programmeTypeLogic.GetModelsBy(p => p.Activated);
                if (programmeType == null || programmeType.Count <= 0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> programmeTypeList = new List<SelectListItem>();
                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = SelectProgramme;
                programmeTypeList.Add(list);

                foreach (ProgrammeType programmeTypes in programmeType)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = programmeTypes.Id.ToString();
                    selectList.Text = programmeTypes.Name;
                    programmeTypeList.Add(selectList);

                }
                return programmeTypeList;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static List<SelectListItem> PopulateProgrammeListItem()
        {
            try
            {
                ProgrammeLogic programmeLogic = new ProgrammeLogic();
                List<Programme> programme = programmeLogic.GetAll();
                if (programme == null || programme.Count <= 0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> programmeList = new List<SelectListItem>();
                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = Select;
                programmeList.Add(list);

                foreach (Programme programmes in programme)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = programmes.Id.ToString();
                    selectList.Text = programmes.ProgrammeName;
                    programmeList.Add(selectList);

                }
                return programmeList;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static List<SelectListItem> PopulateCourseTypeListItem()
        {
            try
            {
                CourseTypeLogic courseTypeLogic = new CourseTypeLogic();
                List<CourseType> courseType = courseTypeLogic.GetAll();
                if (courseType == null || courseType.Count <= 0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> courseTypeList = new List<SelectListItem>();
                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = Select;
                courseTypeList.Add(list);

                foreach (CourseType courseTypes in courseType)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = courseTypes.Id.ToString();
                    selectList.Text = courseTypes.Name;
                    courseTypeList.Add(selectList);

                }
                return courseTypeList;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static List<SelectListItem> PopulateCourseListItem()
        {
            try
            {
                CourseLogic courseLogic = new CourseLogic();
                List<Course> course = courseLogic.GetAll();
                if (course == null || course.Count <= 0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> courseList = new List<SelectListItem>();
                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = Select;
                courseList.Add(list);

                foreach (Course courses in course)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = courses.Id.ToString();
                    selectList.Text = courses.Name;
                    courseList.Add(selectList);

                }
                return courseList;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static List<SelectListItem> PopulateLevelListItem()
        {
            try
            {
                LevelLogic levelLogic = new LevelLogic();
                List<Level> level = levelLogic.GetAll();
                if (level == null || level.Count <= 0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> levelList = new List<SelectListItem>();
                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = SelectLevel;
                levelList.Add(list);

                foreach (Level levels in level)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = levels.Id.ToString();
                    selectList.Text = levels.Name;
                    levelList.Add(selectList);

                }
                return levelList;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static List<SelectListItem> PopulateSemesterListItem()
        {
            try
            {
                SemesterLogic semesterLogic = new SemesterLogic();
                List<Semester> semester = semesterLogic.GetAll();
                if (semester == null || semester.Count <= 0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> semesterList = new List<SelectListItem>();
                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = Select;
                semesterList.Add(list);

                foreach (Semester semesters in semester)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = semesters.Id.ToString();
                    selectList.Text = semesters.Name;
                    semesterList.Add(selectList);

                }
                return semesterList;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public static List<SelectListItem> PopulateMbbsListItem()
        {
            try
            {
                SemesterLogic semesterLogic = new SemesterLogic();
                List<Semester> semester = semesterLogic.GetModelsBy(s => s.SemesterId > 2);
                if (semester == null || semester.Count <= 0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> semesterList = new List<SelectListItem>();
                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = Select;
                semesterList.Add(list);

                foreach (Semester semesters in semester)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = semesters.Id.ToString();
                    selectList.Text = semesters.Name;
                    semesterList.Add(selectList);

                }
                return semesterList;
            }
            catch (Exception)
            {

                throw;
            }
        }


        public static List<SelectListItem> PopulateCourseUnitListItem()
        {
            try
            {
                List<SelectListItem> CourseUnitList = new List<SelectListItem>();
                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = Select;
                CourseUnitList.Add(list);

                for (int i = 1; i <=20; i++)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = i.ToString();
                    selectList.Text = i.ToString();
                    CourseUnitList.Add(selectList);

                }
                return CourseUnitList;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static List<SelectListItem> DurationListItem()
        {
            try
            {
                string[] duration = { "--Select--","1 Year", "2 Years", "3 Years", "4 Years", "5 Years", "6 Years", "7 Years", "8 Years"};
                if (duration == null || duration.Length <= 0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> durationList = new List<SelectListItem>();
                
                for (int i = 0;i<duration.Length;i++)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = i.ToString();
                    selectList.Text = duration[i];
                    durationList.Add(selectList);

                }
                return durationList;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static List<SelectListItem> PopulateGradeListItem()
        {
            try
            {
                ScoreGradeLogic scoreGradeLogic = new ScoreGradeLogic();
                List<ScoreGrade> scoreGrades = scoreGradeLogic.GetAll();

                if (scoreGrades == null || scoreGrades.Count <= 0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> selectItemList = new List<SelectListItem>();

                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = Select;
                selectItemList.Add(list);

                var distintGrades = scoreGrades.Select(s => s.Grade).Distinct().ToList();
                for (int i = 0; i < distintGrades.Count; i++)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = i.ToString();
                    selectList.Text = distintGrades[i].Trim();

                    selectItemList.Add(selectList);
                    
                }
               

                return selectItemList;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static List<SelectListItem> PopulateGradePointListItem()
        {
            try
            {
                double[] gradePoint = {0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5};
                if (gradePoint == null || gradePoint.Length <= 0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> GradePointList = new List<SelectListItem>();
                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = Select;
                GradePointList.Add(list);

                for (int i = 0; i < gradePoint.Length; i++)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = i.ToString();
                    selectList.Text = gradePoint[i].ToString();
                    GradePointList.Add(selectList);

                }
                return GradePointList;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static List<SelectListItem> PopulateStartMarkListItem()
        {
            try
            {

                List<SelectListItem> StartMarkList = new List<SelectListItem>();
                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = Select;
                StartMarkList.Add(list);

                for (int i = 0; i <=100; i++)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = i.ToString();
                    selectList.Text = i.ToString();
                    StartMarkList.Add(selectList);

                }
                return StartMarkList;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static List<SelectListItem> PopulateEndMarkListItem()
        {
            try
            {

                List<SelectListItem> StartMarkList = new List<SelectListItem>();
                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = Select;
                StartMarkList.Add(list);

                for (int i = 0; i <= 100; i++)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = i.ToString();
                    selectList.Text = i.ToString();
                    StartMarkList.Add(selectList);

                }
                return StartMarkList;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static List<Session> GetAllSessions()
        {
            try
            {
                SessionLogic sessionLogic = new SessionLogic();
                List<Session> sessions = sessionLogic.GetAll();

                if (sessions != null && sessions.Count > 0)
                {
                    sessions.Insert(0, new Session() { Id = 0, Name = SelectSession });
                }

                return sessions;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<Programme> GetAllProgrammes()
        {
            try
            {
                ProgrammeLogic programmeLogic = new ProgrammeLogic();
                List<Programme> programmes = programmeLogic.GetAll();

                if (programmes != null && programmes.Count > 0)
                {
                    //programmes.Add(new Programme() { Id = -100, Name = "All" });
                    programmes.Insert(0, new Programme() { Id = 0, ProgrammeName = SelectProgramme });
                }

                return programmes;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static List<ProgrammeType> GetAllProgrammeTypes()
        {
            try
            {
                ProgrammeTypeLogic programmeTypeLogic = new ProgrammeTypeLogic();
                List<ProgrammeType> programmeTypes = programmeTypeLogic.GetAll();

                if (programmeTypes != null && programmeTypes.Count > 0)
                {
                    //programmes.Add(new Programme() { Id = -100, Name = "All" });
                    programmeTypes.Insert(0, new ProgrammeType() { Id = 0, Name = SelectProgramme });
                }

                return programmeTypes;
            }
            catch (Exception)
            {
                throw;
            }
        }
        

        public static List<SessionSemester> GetAllSessionSemesters()
        {
            try
            {
                SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                List<SessionSemester> sessionSemesters = sessionSemesterLogic.GetAll();

                if (sessionSemesters != null && sessionSemesters.Count > 0)
                {
                    foreach (SessionSemester sessionSemester in sessionSemesters)
                    {
                        sessionSemester.Name = sessionSemester.Semester.Name + " " + sessionSemester.Session.Name; 
                    }
                    sessionSemesters.Insert(0, new SessionSemester() { Id = 0, Name = "--Select Semester--" });
                }

                return sessionSemesters;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<Level> GetAllLevels()
        {
            try
            {
                LevelLogic levelLogic = new LevelLogic();
                List<Level> levels = levelLogic.GetAll();

                if (levels != null && levels.Count > 0)
                {
                    levels.Insert(0, new Level() { Id = 0, Name = SelectLevel });
                }

                return levels;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static List<SelectListItem> PopulateSessionListItem()
        {
            try
            {
                SessionLogic sessionLogic = new SessionLogic();
                List<Session> sessions = sessionLogic.GetModelsBy(s => s.IsCurrenSession == true);
                if (sessions == null || sessions.Count <= 0)
                {
                   return new List<SelectListItem>();
                }
                List<SelectListItem> selectList = new List<SelectListItem>();
                SelectListItem selectListItem = new SelectListItem();
                selectListItem.Value = "";
                selectListItem.Text = SelectSession;
                selectList.Add(selectListItem);
                foreach (Session session in sessions)
                {
                    SelectListItem Item = new SelectListItem();
                    Item.Value = session.Id.ToString();
                    Item.Text = session.Name.ToString();
                    selectList.Add(Item);
                }
                return selectList;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public static List<SelectListItem> PopulateYearOfEntryListItem()
        {
            try
            {
                List<SelectListItem> selectList = new List<SelectListItem>();
                SessionLogic sessionLogic = new SessionLogic();
                List<Session> sessions = new List<Session>();
                sessions = sessionLogic.GetAll();
                SelectListItem selectListItem = new SelectListItem();
                selectListItem.Value = "";
                selectListItem.Text = Select;
                selectList.Add(selectListItem);
                foreach (Session session in sessions)
                {
                    SelectListItem Item = new SelectListItem();
                    Item.Value = session.Id.ToString();
                    Item.Text = session.Name.ToString();
                    selectList.Add(Item);
                }
                return selectList;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public static List<SelectListItem> PopulateSexSelectListItem()
        {
            try
            {
                string[] genders = {"--Select--", "Male", "Female"};

                List<SelectListItem> sexList = new List<SelectListItem>();

                for (int i = 0; i < genders.Length; i++)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = genders[i].ToString();
                    selectList.Text = genders[i].ToString();
    
                    sexList.Add(selectList);
                }

                return sexList;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static List<ValueHolder> PopulateSexValue()
        {
            try
            {
                string[] genders = { "--Select--", "Male", "Female" };


                List<ValueHolder> valueHolderList = new List<ValueHolder>();
                for (int i = 0; i < genders.Length; i++)
                {
                   
                    ValueHolder valueHolder = new ValueHolder();
                    valueHolder.Id = genders[i].ToString();
                    valueHolder.Name = genders[i].ToString();
                    valueHolderList.Add(valueHolder);            
                }

                return valueHolderList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<SelectListItem> PopulateRoleSelectListItem()
        {
            try
            {
                RoleLogic roleLogic = new RoleLogic();
                List<Role> roles = roleLogic.GetAll();
                if (roles == null || roles.Count <= 0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> roleList = new List<SelectListItem>();

                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = Select;
                roleList.Add(list);

                foreach (Role role in roles)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = role.Id.ToString();
                    selectList.Text = role.Name;

                    roleList.Add(selectList);
                }

                return roleList;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static List<SelectListItem> PopulateSecurityQuestionSelectListItem()
        {
            try
            {
                SecurityQuestionLogic securityQuestionLogic = new SecurityQuestionLogic();
                List<SecurityQuestion> questions = securityQuestionLogic.GetAll();
                if (questions == null || questions.Count <= 0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> roleList = new List<SelectListItem>();

                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = Select;
                roleList.Add(list);

                foreach (SecurityQuestion question in questions)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = question.Id.ToString();
                    selectList.Text = question.Name;

                    roleList.Add(selectList);
                }

                return roleList;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static List<SelectListItem> PopulateStaffSelectListItem()
        {
            try
            {
                StaffLogic staffLogic = new StaffLogic();
                List<Staff> staffs = staffLogic.GetAll();
                if (staffs == null || staffs.Count <= 0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> userList = new List<SelectListItem>();

                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = Select;
                userList.Add(list);

                foreach (Staff staff in staffs)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = staff.Id.ToString();
                    selectList.Text = staff.User.Username;

                    userList.Add(selectList);
                }

                return userList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<Course> GetCoursesByLevelDepartmentAndSemester(Level level, Department department, Semester semester)
        {
            try
            {
                ProgrammeCourseLogic programmeCourseLogic = new ProgrammeCourseLogic();
                List<ProgrammeCourse> programmeCourses = programmeCourseLogic.GetModelsBy(pc => pc.LevelId == level.Id && pc.SemesterId == semester.Id);
                List<Course> courses = new List<Course>();
                foreach (ProgrammeCourse programmeCourse in programmeCourses)
                {
                    courses.Add(programmeCourse.Course);
                }

                return courses;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<SelectListItem> PopulateCountryListItem()
        {
            try
            {
                CountryLogic countryLogic = new CountryLogic();
                List<Country> country = countryLogic.GetAll();
                if (country == null || country.Count <= 0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> programmeList = new List<SelectListItem>();
                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = Select;
                programmeList.Add(list);

                foreach (Country countries in country)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = countries.Id.ToString();
                    selectList.Text = countries.Name;
                    programmeList.Add(selectList);

                }
                return programmeList;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public static List<SelectListItem> PopulateSexListItem()
        {
            try
            {
                SexLogic sexLogic = new SexLogic();
                List<Sex> Sex = sexLogic.GetAll();
                if (Sex == null || Sex.Count <= 0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> SexList = new List<SelectListItem>();
                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = Select;
                SexList.Add(list);

                foreach (Sex sex in Sex)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = sex.Id.ToString();
                    selectList.Text = sex.Name;
                    SexList.Add(selectList);

                }
                return SexList;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static List<Value> GetNumberOfDaysInMonth(Value month, Value year)
        {
            try
            {
                int noOfDays = DateTime.DaysInMonth(year.Id, month.Id);
                List<Value> days = CreateNumberListFrom(1, noOfDays);
                return days;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static List<Value> CreateNumberListFrom(int startValue, int endValue)
        {
            var values = new List<Value>();

            try
            {
                int j = 1;
                for (int i = startValue; i <= endValue; i++)
                {
                    var value = new Value();
                    value.Id = j++;
                    value.Name = i.ToString();
                    values.Add(value);
                }

                //values.Insert(0, new Value() { Id = 0, Name = Select });
                return values;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static List<SelectListItem> PopulateMonthSelectListItem()
        {
            try
            {
                var months = new List<Value>();
                var january = new Value { Id = 1, Name = "January" };
                var february = new Value { Id = 2, Name = "February" };
                var march = new Value { Id = 3, Name = "March" };
                var april = new Value { Id = 4, Name = "April" };
                var may = new Value { Id = 5, Name = "May" };
                var june = new Value { Id = 6, Name = "June" };
                var july = new Value { Id = 7, Name = "July" };
                var august = new Value { Id = 8, Name = "August" };
                var september = new Value { Id = 9, Name = "September" };
                var october = new Value { Id = 10, Name = "October" };
                var november = new Value { Id = 11, Name = "November" };
                var december = new Value { Id = 12, Name = "December" };

                months.Add(january);
                months.Add(february);
                months.Add(march);
                months.Add(april);
                months.Add(may);
                months.Add(june);
                months.Add(july);
                months.Add(august);
                months.Add(september);
                months.Add(october);
                months.Add(november);
                months.Add(december);

                var monthList = new List<SelectListItem>();

                var list = new SelectListItem();
                list.Value = "";
                list.Text = "--MM--";
                monthList.Add(list);

                foreach (Value month in months)
                {
                    var selectList = new SelectListItem();
                    selectList.Value = month.Id.ToString();
                    selectList.Text = month.Name;

                    monthList.Add(selectList);
                }

                return monthList;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static List<SelectListItem> PopulateYearSelectListItem(int startYear, bool withSelect)
        {
            try
            {
                int END_YEAR = DateTime.Now.Year + 6;
                List<Value> years = CreateYearListFrom(startYear, END_YEAR);
                if (years == null || years.Count <= 0)
                {
                    return new List<SelectListItem>();
                }

                var yearList = new List<SelectListItem>();

                var list = new SelectListItem();
                list.Value = "";
                if (withSelect)
                {
                    list.Text = Select;
                }
                else
                {
                    list.Text = "--YY--";
                }

                yearList.Add(list);

                foreach (Value year in years)
                {
                    var selectList = new SelectListItem();
                    selectList.Value = year.Id.ToString();
                    selectList.Text = year.Name;

                    yearList.Add(selectList);
                }

                return yearList;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static List<Value> CreateYearListFrom(int startYear, int endYear)
        {
            var years = new List<Value>();

            try
            {
                int j = 1;
                for (int i = startYear; i <= endYear; i++)
                {
                    var value = new Value();
                    value.Id = j++;
                    value.Name = i.ToString();
                    years.Add(value);
                }

                //years.Insert(0, new Value() { Id = 0, Name = Select });
                return years;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static List<SelectListItem> PopulateDaySelectListItem(Value month, Value year)
        {
           
                int noOfDays = DateTime.DaysInMonth(year.Id, month.Id);
                var values = new List<SelectListItem>();

                try
                {
                    int j = 1;
                    for (int i = 1; i <= noOfDays; i++)
                    {
                        var jToString = j++;
                        var value = new SelectListItem();
                        value.Text = jToString.ToString();
                        value.Value = i.ToString();
                        values.Add(value);
                    }



                    return values;



                }
                catch (Exception)
                {

                    throw;
                }
            
        }
        public static List<SelectListItem> PopulateFacultSelectListItem()
        {
            try
            {
                FacultyLogic facultyLogic  = new FacultyLogic();
                List<Faculty> faculty = facultyLogic.GetAll();
                if (faculty == null || faculty.Count <= 0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> facultyList = new List<SelectListItem>();
                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = Select;
                facultyList.Add(list);

                foreach (Faculty sex in faculty)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = sex.Id.ToString();
                    selectList.Text = sex.Name;
                    facultyList.Add(selectList);

                }
                return facultyList;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public static List<SelectListItem> PopulateMenuGroupSelectListItem()
        {
            try
            {
                var menuGroupLogic = new MenuGroupLogic();
                List<MenuGroup> menuGroups = menuGroupLogic.GetAll().OrderBy(a => a.Name).ToList();
                if (menuGroups == null || menuGroups.Count <= 0)
                {
                    return new List<SelectListItem>();
                }

                var MenuGroupList = new List<SelectListItem>();

                var list = new SelectListItem();
                list.Value = "";
                list.Text = Select;
                MenuGroupList.Add(list);

                foreach (MenuGroup role in menuGroups)
                {
                    var selectList = new SelectListItem();
                    selectList.Value = role.Id.ToString();
                    selectList.Text = role.Name;

                    MenuGroupList.Add(selectList);
                }

                return MenuGroupList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<SelectListItem> PopulateMenuSelectListItem()
        {
            try
            {
                var menuLogic = new MenuLogic();
                List<Model.Model.Menu> menuList = menuLogic.GetAll().OrderBy(a => a.DisplayName).ToList();
                if (menuList == null || menuList.Count <= 0)
                {
                    return new List<SelectListItem>();
                }

                var MenuList = new List<SelectListItem>();

                var list = new SelectListItem();
                list.Value = "";
                list.Text = Select;
                MenuList.Add(list);

                foreach (Model.Model.Menu menu in menuList)
                {
                    var selectList = new SelectListItem();
                    selectList.Value = menu.Id.ToString();
                    selectList.Text = menu.DisplayName + ", In " + menu.MenuGroup.Name;

                    MenuList.Add(selectList);
                }

                return new List<SelectListItem>(MenuList.OrderBy(x => x.Text)); ;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static string Encrypt(string encrypData)
        {
            string data = "";
            try
            {
                string CharData = "";
                string ConChar = "";
                for (int i = 0; i < encrypData.Length; i++)
                {
                    CharData = Convert.ToString(encrypData.Substring(i, 1));
                    ConChar = Char.ConvertFromUtf32(Char.ConvertToUtf32(CharData, 0) + 115);
                    data = data + ConChar;
                }
            }
            catch (Exception ex)
            {
                data = "1";
                return data;
            }
            return data;
        }

        public static string Decrypt(string encrypData)
        {
            string data = "";
            try
            {
                string CharData = "";
                string ConChar = "";
                for (int i = 0; i < encrypData.Length; i++)
                {
                    CharData = Convert.ToString(encrypData.Substring(i, 1));
                    ConChar = Char.ConvertFromUtf32(Char.ConvertToUtf32(CharData, 0) - 115);
                    data = data + ConChar;
                }
            }
            catch (Exception ex)
            {
                data = "1";
                return data;
            }
            return data;
        }
        public static List<SelectListItem> PopulateScoreGradeSelectListItem()
        {
            try
            {
                ScoreGradeLogic scoreGradeLogic = new ScoreGradeLogic();
                var scoreGradeList = scoreGradeLogic.GetAll();
                List<string> scoreGrades = scoreGradeList.Select(s => s.Grade).Distinct().ToList();
                if (scoreGrades.Count <= 0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> selectItemList = new List<SelectListItem>();

                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = Select;
                selectItemList.Add(list);
                foreach (string grade in scoreGrades)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = grade.Trim();
                    selectList.Text = grade.Trim();

                    selectItemList.Add(selectList);
                }

                return selectItemList;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
  
     
    public class ValueHolder
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

 
}