﻿using System.Web;
using System.Web.Optimization;

namespace Abundance_Rm
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js", 
                        "~/Scripts/dataTables.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.min.js",
                       "~/Scripts//bootstrap-select.min.js",
                        "~/Scripts/jquery.dataTables.min.js",
                         "~/Scripts/dataTables.bootstrap.min.js",
                          "~/Scripts/Chart.min.js",
                           "~/Scripts/fileinput.js",
                            "~/Scripts/chartData.js",
                                "~/Scripts/main.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/font-awesome.min.css",
                       "~/Content/bootstrap.css",
                        "~/Content/dataTables.bootstrap.min.css",
                         "~/Content/bootstrap-social.css",
                          "~/Content/bootstrap-select.css",
                          "~/Content/fileinput.min.css",
                          "~/Content/awesome-bootstrap-checkbox.css",
                          "~/Content/style.css",
                          "~/Content/dataTables.css"));
        }
    }
}
